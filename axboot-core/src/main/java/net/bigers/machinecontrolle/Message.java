package net.bigers.machinecontrolle;

import java.io.Serializable;

import javax.swing.ImageIcon;

public class Message implements Serializable{

	private String message;
	private ImageIcon screenShot;
	private int width = 0;
	private int hieght = 0;

	public Message() {}
	public Message(String message, ImageIcon screenShot) {
		this.message = message;
		this.screenShot = screenShot;
	}

	public Message(String message, int width, int height, ImageIcon screenShot) {
		this.message = message;
		this.screenShot = screenShot;
		this.width = width;
		this.hieght = height;				
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHieght() {
		return hieght;
	}
	public void setHieght(int hieght) {
		this.hieght = hieght;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ImageIcon getScreenShot() {
		return screenShot;
	}

	public void setScreenShot(ImageIcon screenShot) {
		this.screenShot = screenShot;
	}
	public static Message of(String message, ImageIcon screenShot){
		return new Message(message, screenShot);
	}
	public static Message of(String message){
		return new Message(message, null);
	}
	
	public static Message of(String message ,int width , int height){
		return new Message(message, width, height,null);
	}

}
