package com.axisj.axboot.core.domain.company;

import com.axisj.axboot.core.domain.BaseService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class CompanyService extends BaseService<Company, String> {

	private CompanyRepository companyRepository;

	@Inject
	public CompanyService(CompanyRepository companyRepository) {
		super(companyRepository);
		this.companyRepository = companyRepository;
	}

}