package com.axisj.axboot.core.domain.notice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoticeRepository extends JpaRepository<Notice, Integer> {
	Page<Notice> findAllByCompCdInOrderByIdDesc(List<String> compCd, Pageable pageable);
}
