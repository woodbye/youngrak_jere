package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;

import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "RoomVTO", description = "제례실 정보 세팅")
public class RoomVTO {

	@ApiModelProperty(value = "제례실코드")
	private String roomCd;
	
	@ApiModelProperty(value = "제례실명")
	private String roomNm;
	
	@ApiModelProperty(value = "제례관")
	private String areaCd;
	
	@ApiModelProperty(value = "설치장비코드")
	private String machineCd;
	
	@ApiModelProperty(value = "비고")
	private String remark;
	
	@ApiModelProperty(value = "등록 시간")
    private String regTime;
    
	@ApiModelProperty(value = "등록 유저")
    private String regId; 
    
	@ApiModelProperty(value = "수정 시간")
    private String udtTime; 
    
	@ApiModelProperty(value = "수정 유저")
    private String udtId;  
	
	public static RoomVTO of(Room room) {
		RoomVTO roomVTO = DozerBeanMapperUtils.map(room, RoomVTO.class);
		return roomVTO;
	}
	
	public static List<RoomVTO> of(List<Room> list){
		return list.stream().map(room -> of(room)).collect(toList());
	}
}
