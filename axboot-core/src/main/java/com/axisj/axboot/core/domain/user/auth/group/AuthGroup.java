package com.axisj.axboot.core.domain.user.auth.group;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.user.auth.group.menu.AuthGroupMenu;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "AUTH_GRP_M")
public class AuthGroup extends BaseJpaModel<String> {

	@Id
	@Column(name = "GRP_AUTH_CD", length = 10)
	private String grpAuthCd;

	@OneToMany(mappedBy = "authGroup", cascade = CascadeType.ALL)
	private List<AuthGroupMenu> authGroupMenus;

	@Column(name = "GRP_AUTH_NM", length = 50)
	private String grpAuthNm;

	@Column(name = "REMARK", length = 200)
	private String remark;

	@Override
	public String getId() {
		return grpAuthCd;
	}
}
