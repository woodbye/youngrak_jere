package com.axisj.axboot.core.domain.notice;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.company.Company;
import com.axisj.axboot.core.domain.notice.file.NoticeFile;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "NOTICE_M")
@Alias("notice")
public class Notice extends BaseJpaModel<Integer> {

	@Id
	@Column(name = "ID", precision = 11)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "COMP_CD", length = 10)
	private String compCd;

	@Column(name = "TITLE", length = 1000)
	private String title;

	@Column(name = "CONTENT", columnDefinition = "TEXT")
	private String content;

	@Column(name = "ST_DT", length = 8)
	private String stDt;

	@Column(name = "END_DT", length = 8)
	private String endDt;

	@Column(name = "POPUP_YN", length = 1)
	private String popupYn;

	@Column(name = "DISP_YN", length = 1)
	private String dispYn;

	@OneToMany(mappedBy = "notice")
	private List<NoticeFile> noticeFileList;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMP_CD", referencedColumnName = "COMP_CD", insertable = false, updatable = false)
	private Company company;

	@Override
	public Integer getId() {
		return id;
	}
}
