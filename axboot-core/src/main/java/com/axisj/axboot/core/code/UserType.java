package com.axisj.axboot.core.code;

public class UserType {

	public static final String SUPER_ADMIN = "S";

	public static final String ADMIN = "A";

	public static final String USER = "U";
}
