package com.axisj.axboot.core.domain.asp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AspMasterRepository extends JpaRepository<AspMaster, String> {
}
