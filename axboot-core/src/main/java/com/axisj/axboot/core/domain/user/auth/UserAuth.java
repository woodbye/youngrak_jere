package com.axisj.axboot.core.domain.user.auth;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.user.User;
import com.axisj.axboot.core.domain.user.auth.group.AuthGroup;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "USER_AUTH_M")
@IdClass(UserAuthId.class)
public class UserAuth extends BaseJpaModel<UserAuthId> {

	@Id
	@Column(name = "USER_CD")
	private String userCd;

	@Id
	@Column(name = "GRP_AUTH_CD")
	private String grpAuthCd;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_CD", referencedColumnName = "USER_CD", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GRP_AUTH_CD", referencedColumnName = "GRP_AUTH_CD", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
	private AuthGroup authGroup;

	@Column(name = "REMARK", length = 200)
	private String remark;

	@Column(name = "USE_YN", length = 1)
	private String useYn;

	@Override
	public UserAuthId getId() {
		return UserAuthId.of(userCd, grpAuthCd);
	}
}
