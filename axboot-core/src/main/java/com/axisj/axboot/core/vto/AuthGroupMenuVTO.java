package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.program.Program;
import com.axisj.axboot.core.domain.user.auth.group.menu.AuthGroupMenu;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "AuthGroupMenuVTO", description = "권한 그룹 메뉴 정보")
public class AuthGroupMenuVTO {

	@ApiModelProperty(value = "권한그룹코드")
	private String grpAuthCd;

	@ApiModelProperty(value = "메뉴코드")
	private String mnuCd;

	@ApiModelProperty(value = "메뉴명")
	private String mnuNm;

	@ApiModelProperty(value = "프로그램코드")
	private String progCd;

	@ApiModelProperty(value = "프로그램명")
	private String progNm;

	@ApiModelProperty(value = "조회권한")
	private String schAh;

	@ApiModelProperty(value = "저장권한")
	private String savAh;

	@ApiModelProperty(value = "엑셀권한")
	private String exlAh;

	@ApiModelProperty(value = "기능키1권한")
	private String fn1Ah;

	@ApiModelProperty(value = "기능키2권한")
	private String fn2Ah;

	@ApiModelProperty(value = "기능키3권한")
	private String fn3Ah;

	@ApiModelProperty(value = "기능키4권한")
	private String fn4Ah;

	@ApiModelProperty(value = "기능키5권한")
	private String fn5Ah;

	@ApiModelProperty(value = "조회권한")
	private String progSchAh;

	@ApiModelProperty(value = "저장권한")
	private String progSavAh;

	@ApiModelProperty(value = "엑셀권한")
	private String progExlAh;

	@ApiModelProperty(value = "기능키1권한")
	private String progFn1Ah;

	@ApiModelProperty(value = "기능키2권한")
	private String progFn2Ah;

	@ApiModelProperty(value = "기능키3권한")
	private String progFn3Ah;

	@ApiModelProperty(value = "기능키4권한")
	private String progFn4Ah;

	@ApiModelProperty(value = "기능키5권한")
	private String progFn5Ah;

	public static AuthGroupMenuVTO of(AuthGroupMenu authGroupMenu) {
		AuthGroupMenuVTO authGroupMenuVTO = DozerBeanMapperUtils.map(authGroupMenu, AuthGroupMenuVTO.class);


		if (CommonUtils.canConvertMappingVTO(authGroupMenu.getMenu())) {
			authGroupMenuVTO.setMnuNm(authGroupMenu.getMenu().getMnuNm());

			if (CommonUtils.canConvertMappingVTO(authGroupMenu.getMenu().getProgram())) {
				Program program = authGroupMenu.getMenu().getProgram();
				authGroupMenuVTO.setProgNm(program.getProgNm());
				authGroupMenuVTO.setProgCd(program.getProgCd());
				authGroupMenuVTO.setProgSchAh(program.getSchAh());
				authGroupMenuVTO.setProgSavAh(program.getSavAh());
				authGroupMenuVTO.setProgExlAh(program.getExlAh());
				authGroupMenuVTO.setProgFn1Ah(program.getFn1Ah());
				authGroupMenuVTO.setProgFn2Ah(program.getFn2Ah());
				authGroupMenuVTO.setProgFn3Ah(program.getFn3Ah());
				authGroupMenuVTO.setProgFn4Ah(program.getFn4Ah());
				authGroupMenuVTO.setProgFn5Ah(program.getFn5Ah());
			}
		}

		return authGroupMenuVTO;
	}

	public static List<AuthGroupMenuVTO> of(List<AuthGroupMenu> authGroupMenuList) {
		return authGroupMenuList.stream().map(authGroupMenu -> of(authGroupMenu)).collect(toList());
	}
}
