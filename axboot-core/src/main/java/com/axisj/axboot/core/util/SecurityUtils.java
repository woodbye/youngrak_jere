package com.axisj.axboot.core.util;

import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

	/**
	 * 
	 * @Method Name  : getUserDetails
	 * @작성일   : 2015. 12. 26. 오후 2:14:32
	 * @작성자   : wofmaker
	 * @변경이력  :
	 * @Method 설명 : 스프링 시큐리티에서 User Details 반환
	 * @return
	 */
	public static Object getUserDetails() {
		return SecurityContextHolder.getContext().getAuthentication().getDetails();
	}

	/**
	 * 
	 * 메소드 명칭 : hexToByteArray 
	 * 메소드 설명 : hex => byte 배열
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 5. wofmaker 최초작성
	 *
	 * @param hex
	 * @return
	 */

	public static byte[] hexToByteArray(String hex) {
		if (hex == null || hex.length() == 0) {
			return null;
		}
		byte[] byteArr = new byte[hex.length() / 2];
		for (int i = 0; i < byteArr.length; i++) {
			byteArr[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return byteArr;
	}

	/**
	 * 
	 * 메소드 명칭 : byteArrayToHex 
	 * 메소드 설명 : byte 배열 => hex
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 5. wofmaker 최초작성
	 *
	 * @param byteArr
	 * @return
	 */

	public static String byteArrayToHex(byte[] byteArr) {
		if (byteArr == null || byteArr.length == 0) {
			return null;
		}
		StringBuffer sb = new StringBuffer(byteArr.length * 2);
		String hexNumber = null;
		for (int i = 0; i < byteArr.length; i++) {
			hexNumber = "0" + Integer.toHexString(0xff & byteArr[i]);
			sb.append(hexNumber.substring(hexNumber.length() - 2));
		}
		return sb.toString();
	}

	/**
	 * 
	 * 메소드 명칭 : encrypt 
	 * 메소드 설명 : AES 암호화
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 5. wofmaker 최초작성
	 *
	 * @param key
	 * @param content
	 * @return
	 * @throws Exception
	 */

	public static String encrypt(Key key, String content) throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encrypted = cipher.doFinal(content.getBytes());
		return byteArrayToHex(encrypted);
	}

	/**
	 * 
	 * 메소드 명칭 : decrypt 
	 * 메소드 설명 : AES 복호화
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 5. wofmaker 최초작성
	 *
	 * @param key
	 * @param encrypted
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(Key key, String encrypted) throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] original = cipher.doFinal(hexToByteArray(encrypted));
		String orignalString = new String(original);
		return orignalString;
	}

	/**
	 * 
	 * 메소드 명칭 : generateAESKey 
	 * 메소드 설명 : KEY 생성
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 5. wofmaker 최초작성
	 *
	 * @param password
	 * @param bit
	 * @return
	 * @throws Exception
	 */

	public static Key generateAESKey(String password, int bit) throws Exception {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		SecureRandom random = SecureRandom.getInstance(password);
		generator.init(bit, random);
		Key secureKey = generator.generateKey();
		return secureKey;
	}

	/**
	 * 
	 * 메소드 명칭 : encodeAES128 
	 * 메소드 설명 : AES128 암호화 password 는 128비트여야 한다. (영문 16자)
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 5. wofmaker 최초작성
	 *
	 * @param password
	 * @param content
	 * @return
	 * @throws Exception
	 */

	public static String encodeAES128(String password, String content) throws Exception {
		SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		byte[] encrypted = cipher.doFinal(content.getBytes());
		return byteArrayToHex(encrypted);
	}

	/**
	 * 
	 * 메소드 명칭 : decodeAES128 
	 * 메소드 설명 : AES 복호화 password 는 128비트여야 한다. (영문 16자)
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 5. wofmaker 최초작성
	 *
	 * @param password
	 * @param content
	 * @return
	 * @throws Exception
	 */

	public static String decodeAES128(String password, String content) throws Exception {
		SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, keySpec);
		byte[] original = cipher.doFinal(hexToByteArray(content));
		String orignalString = new String(original);
		return orignalString;
	}

	/**
	 * 
	 * 프로그램명 : SimplexType 
	 * 설 명 : 단방향 암호화 알고리즘 type
	 * ------------------------------------------
	 *
	 * 이력사항 2014. 11. 10. wofmaker 최초작성 <BR/>
	 */

	public enum SimplexType {
		SHA256, MD5
	}

	/**
	 * 
	 * 메소드 명칭 : simplex
	 * 메소드 설명 : 단방향 암호화
	 * ---------------------------------------- 
	 * 이력사항 2014. 11. 10. wofmaker 최초작성
	 *
	 * @param str
	 * @return
	 */

	public static String simplex(SimplexType simplexType, String str) {
		String res = "";
		try {
			MessageDigest md = null;

			switch (simplexType) {
			case SHA256:
				md = MessageDigest.getInstance("SHA-256");
				break;
			case MD5:
				md = MessageDigest.getInstance("MD5");
				break;
			default:
				break;
			}

			md.update(str.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			res = sb.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			res = null;
		}
		return res;
	}

}