package com.axisj.axboot.core.domain.inspectroom;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectId;
import com.axisj.axboot.core.domain.inspect.InspectRepository;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomRepository;

@Service
public class InspectroomService extends BaseService<Inspectroom, InspectroomId> {

	private InspectroomRepository inspectroomRepository;
	
	@Inject
	private RoomRepository roomRepository;
	
	@Inject
	private InspectRepository inspectRepository;
	
	@Inject
	public InspectroomService(InspectroomRepository repository) {
		super(repository);
		this.inspectroomRepository = repository;
	}

	public List<Inspectroom> selectInspectRoomList(String workDate, int seqNo) throws Exception {
			
		//return inspectroomRepository.findByWorkDateAndSeqNo(inspectroom.getWorkDate(),inspectroom.getSeqNo());
		return inspectroomRepository.findByWorkDateAndSeqNo(workDate,seqNo);
	}

	public List<Room> selectInspectNotRoomList(String workDate, int seqNo) throws Exception {		
	 
		return roomRepository.findByRoomList(workDate, seqNo);
	}

	@Transactional
	public void saveInspectRoomList(List<Inspectroom> list) throws Exception {
	
		
		if(list.size() > 0){
			List<Inspectroom> deleteList = inspectroomRepository.findByWorkDateAndSeqNo(list.get(0).getWorkDate(),list.get(0).getSeqNo());
			deleteList.forEach(this::delete);
			for(Inspectroom inspectroom : list){
				
				if(inspectroom.getRoom().getRoomCd() != null){
					this.save(inspectroom);
				}			
			}
		}	
				
	}

	public boolean canBooking(String roomCd) throws Exception{
		
		boolean res = false;
		
		//List<Inspect> inspectList = inspectRepository.getByStringDate(new SimpleDateFormat("yyyyMMddHHmm").format(new Date()));
		Inspect inspectList = inspectRepository.findFirstByStdateLessThanEqualOrderByWorkDateDescSeqNoDesc(new SimpleDateFormat("yyyyMMddHHmm").format(new Date()));
		List<Inspectroom> inspectroomList = inspectroomRepository.findByWorkDateAndSeqNo(inspectList.getId().getWorkDate(), inspectList.getId().getSeqNo());
		
/*		List<Inspectroom> inspectroomList = inspectList.stream().map(inspect-> {
			Inspectroom inspectroom = null;
			inspectroom = inspectroomRepository.findOne(InspectroomId.of(inspect.getId().getWorkDate(), inspect.getId().getSeqNo(), roomCd));
			return inspectroom;
		}).collect(Collectors.toList());*/
		
		
		while (inspectroomList.contains(null)) {
			inspectroomList.remove(null);			
		}
		
		if(inspectroomList == null || inspectroomList.size() == 0){
			return true;
		}
		
		if(inspectroomList.stream().allMatch(inspectroom -> !roomCd.equals(inspectroom.getRoomCd()))){
			return true;
		}
		
		return false;
	}
	
	public Inspectroom selectInspectRoom(String workDate, Integer seqNo, String roomCd) throws Exception {
		// TODO Auto-generated method stub		
		return inspectroomRepository.findByWorkDateAndSeqNoAndRoomCd(workDate,seqNo,roomCd);
	}
	
	/*public List<Room> selectInspectNotRoomList(String workDate, int seqNo) throws Exception {
		
	//List<Inspectroom> list	= inspectroomRepository.findByRoomList(workDate, seqNo);
	List<Inspectroom> list	= inspectroomRepository.findByWorkDateAndSeqNo(workDate,seqNo);
	List<Room> roomAll = SpringUtils.getBean(RoomRepository.class).findAll();
	List<Room> rooms = ListUtils.pluck(list, "room");

		List<Room> roomAll = roomR.findAll();
		List<String> insR = inspectroomRepository.findbyBSE(start, end);
		
		List res = ListUtils.select(roomAll, new Function<Room, Boolean>() {

			@Override
			public Boolean apply(Room t) {
				// TODO Auto-generated method stub
				
				
				return insR.contains(t.getRoomCd());
			}
			
		});
	//inspectroomRepository.findByRoomList(workDate, seqNo);
		return rooms;
	}
*/
}
