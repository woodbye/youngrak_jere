package com.axisj.axboot.core.domain.program.menu;

import com.axisj.axboot.core.domain.program.menu.display.MainMenu;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor(staticName = "of")
public class UserMenuInfo {

	@NonNull
	private Map<String, AuthorizedMenu> authorizedMenuMap;

	@NonNull
	private List<MainMenu> mainMenuList;

	@NonNull
	private String mainMenuJson;

	@NonNull
	private String mainMenuJsonHash;

}
