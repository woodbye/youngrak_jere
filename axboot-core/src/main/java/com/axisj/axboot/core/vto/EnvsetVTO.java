package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.util.List;

import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.user.auth.group.AuthGroup;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "EnvsetVTO", description = "환경 세팅 정보")
public class EnvsetVTO {

	@ApiModelProperty(value = "작업 일자")
	private String workDate;
    
	@ApiModelProperty(value = "작업 등록 순서")
    private Integer seqNo;

	@ApiModelProperty(value = "시작일")
    private String stDate;
      
	@ApiModelProperty(value = "일과 시작 시간")
    private String openTime;    
    
	@ApiModelProperty(value = "일과 종료 시간")
    private String closeTime;
    
	@ApiModelProperty(value = "제례시간")
    private Integer runingTime;
    
	@ApiModelProperty(value = "연장 시간")
    private Integer extTime;
  
	@ApiModelProperty(value = "연장횟수")
    private Integer extCnt;
	
	@ApiModelProperty(value = "정리시간")
    private Integer cleanTime;
    
	@ApiModelProperty(value = "TTS 방송 여부")
    private String ttsYn;
    
	@ApiModelProperty(value = "SMS 수신 여부")
    private String mmsYn;
    
	@ApiModelProperty(value = "비고")
    private String remark;
    
	@ApiModelProperty(value = "등록 시간")
    private String regTime;
    
	@ApiModelProperty(value = "등록 유저")
    private String regId; 
    
	@ApiModelProperty(value = "수정 시간")
    private String udtTime; 
    
	@ApiModelProperty(value = "수정 유저")
    private String udtId;  
	
	
	public static EnvsetVTO of(Envset envset) {
		EnvsetVTO envsetVTO = DozerBeanMapperUtils.map(envset, EnvsetVTO.class);
		return envsetVTO;
	}
	
	public static List<EnvsetVTO> of(List<Envset> list){
		return list.stream().map(envset -> of(envset)).collect(toList());
	}
}
