package com.axisj.axboot.core.domain.inspect;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.user.auth.group.AuthGroupId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName="of")
@ToString(includeFieldNames=true)
public class InspectId implements Serializable {

	@NonNull
	private String  workDate;	
	@NonNull
	private int seqNo;
	
}
