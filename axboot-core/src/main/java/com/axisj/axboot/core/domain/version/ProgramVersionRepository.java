package com.axisj.axboot.core.domain.version;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgramVersionRepository extends JpaRepository<ProgramVersion,Double> {

	
}
