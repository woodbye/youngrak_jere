package com.axisj.axboot.core.dto;

import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class PageableTO {

	@NonNull
	@ApiModelProperty(value = "전체 페이지 수", required = true)
	private Integer totalPages;

	@NonNull
	@ApiModelProperty(value = "전체 리스트 수", required = true)
	private Long totalElements;

	@NonNull
	@ApiModelProperty(value = "현재 페이지 넘버(zero-base)", required = true)
	private Integer currentPage;

	@NonNull
	@ApiModelProperty(value = "페이지 크기", required = true)
	private Integer pageSize;

	public static PageableTO of(Page pages) {
		return of(pages.getTotalPages(), pages.getTotalElements(), pages.getNumber(), pages.getSize());
	}
}
