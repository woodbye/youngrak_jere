package com.axisj.axboot.core.code;

public class ApplicationProfiles {

	public static final String LOCAL = "local";
	public static final String ALPHA = "alpha";
	public static final String BETA = "beta";
	public static final String PRODUCTION = "production";
}
