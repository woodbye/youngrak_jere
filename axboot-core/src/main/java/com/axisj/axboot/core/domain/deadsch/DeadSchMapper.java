package com.axisj.axboot.core.domain.deadsch;

import java.util.List;
import java.util.Map;

import com.axisj.axboot.core.mybatis.SecondMyBatisMapper;

public interface DeadSchMapper extends SecondMyBatisMapper{

	List<DeadSch> selectDeadSchList(Map<String, Object> params) throws Exception;

	Long selectDeadSchListCount(Map<String, Object> params) throws Exception;
}
