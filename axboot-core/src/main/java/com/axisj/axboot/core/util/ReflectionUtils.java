package com.axisj.axboot.core.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.axisj.axboot.core.domain.envset.Envset;

public class ReflectionUtils {

	private ReflectionUtils() {}
	
	/**
	 * 
	 * @Method Name  : getMethodByName
	 * @작성일   : 2015. 12. 26. 오후 3:34:13
	 * @작성자   : Lee
	 * @변경이력  : Method 이름으로 클래스의 메소드를 반환
	 * @Method 설명 :
	 * @param obj
	 * @param methodName
	 * @return
	 */
	public static Method getMethodByName(Object obj, String methodName){
		return getMethodByName(obj.getClass(), methodName);
	}
	
	/**
	 * 
	 * @Method Name  : getMethodByName
	 * @작성일   : 2015. 12. 26. 오후 3:34:36
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : Method 이름으로 클래스의 메소드를 반환
	 * @param clz
	 * @param methodName
	 * @return
	 */
	public static Method getMethodByName(Class clz, String methodName){
		for(Method m : clz.getDeclaredMethods()){
			if(m.getName().equals(methodName)){
				return m;
			}
		}
		return null;
	}
	
	public static Method[] getGetterMethods(Object obj){
		return getGetterMethods(obj.getClass());
	}
	
	public static Method[] getGetterMethods(Class clz){
		
		List<Method> methods = new ArrayList<>();
		
		for(Method m : clz.getDeclaredMethods()){
			if(m.getName().startsWith("get")){
				methods.add(m);
			}
		}
		
		return methods.toArray(new Method[methods.size()]);
	}
	
	public static Method getGetterMethodByName(Object obj, String methodName){
		return getGetterMethodByName(obj.getClass(), methodName);
	}
	
	public static Method getGetterMethodByName(Class clz, String methodName){
		
		String realName = "get";
		realName = realName + (methodName.charAt(0) + "").toUpperCase();
		realName = realName + methodName.substring(1);
		
		Method[] methods = getGetterMethods(clz);
		
		for(Method m : methods){
			if(m.getName().equals(realName)){
				return m;
			}
		}
		
		return null;
	}
	
	public static <V> V getValue(Object obj, String key) throws Exception{
		
		Method getter = getGetterMethodByName(obj.getClass(), key);
		
		return (V) getter.invoke(obj, null);
	}

}
