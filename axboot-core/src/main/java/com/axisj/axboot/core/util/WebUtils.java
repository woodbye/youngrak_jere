package com.axisj.axboot.core.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.axisj.axboot.core.code.Constants;

public class WebUtils {

	public static void setCookie(HttpServletResponse response, String key, String value, int maxAge) {
		Cookie cookie = new Cookie(key, value);
		cookie.setMaxAge(maxAge);
		cookie.setPath("/");

		setCookie(response, cookie);
	}

	public static void setCookie(HttpServletResponse response, String key, String value) {
		setCookie(response, key, value, -1);
	}

	public static void setCookie(HttpServletResponse response, Cookie cookie) {
		if (response != null) {
			response.addCookie(cookie);
		}
	}

	public static void deleteCookie(HttpServletRequest request, HttpServletResponse response, String key) {
		Cookie cookie = getCookie(request, key);
		if (cookie != null) {
			cookie.setPath(cookie.getPath());
			cookie.setMaxAge(0);
			setCookie(response, cookie);
		}
	}

	public static Cookie getCookie(HttpServletRequest request, String key) {
		if (request != null) {
			if (request.getCookies() != null && request.getCookies().length > 0) {
				for (Cookie cookie : request.getCookies()) {
					if (cookie.getName().equals(key)) {
						return cookie;
					}
				}
			}
		}
		return null;
	}

	public static String getStringCookieValue(HttpServletRequest request, String key) {
		Cookie cookie = getCookie(request, key);

		if (cookie != null) {
			return cookie.getValue();
		}

		return null;
	}

	public static HttpServletRequest getCurrentRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}

	public static HttpServletResponse getCurrentResponse() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
	}

	public static String getJsonContentType(HttpServletRequest request) {
		return isExplorerBrowser(request) ? "text/plain; charset=UTF-8" : "application/json; charset=UTF-8";
	}

	public static boolean isExplorerBrowser(HttpServletRequest request) {
		if (getAgent(request).contains("MSIE")) {
			return true;
		}
		return false;
	}

	public static String getAgent(HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		return userAgent;
	}

	public static boolean isLoggedIn(HttpServletRequest request) {
		return !StringUtils.isEmpty(getStringCookieValue(request, Constants.AUTH_TOKEN_KEY));
	}

	public static void setLastNavigatedPage(HttpServletRequest request, HttpServletResponse response) {
		String requestURI = request.getRequestURI();

		if (requestURI.equals("/") ||
				requestURI.startsWith("/api") ||
				requestURI.contains("index.jsp") ||
				requestURI.contains("modal") ||
				requestURI.contains("error") ||
				requestURI.contains("login.jsp") ||
				!requestURI.contains(".jsp")) {
			// SKIP
		} else {
			setCookie(response, Constants.LAST_NAVIGATED_PAGE, requestURI, 60 * 10 * 10);
		}
	}

	public static String getLastNavigatedPage(HttpServletRequest request) {
		String lastNavigatedPage = WebUtils.getStringCookieValue(request, Constants.LAST_NAVIGATED_PAGE);

		if (StringUtils.isEmpty(lastNavigatedPage) ||
				lastNavigatedPage.contains("login.jsp") ||
				lastNavigatedPage.contains("error") ||
				lastNavigatedPage.contains("modal") ||
				!lastNavigatedPage.contains(".jsp")) {

			lastNavigatedPage = "/jsp/main.jsp";
		}

		return lastNavigatedPage;
	}

	public static String pageForAfterLoggedIn(HttpServletRequest request) {
		String lastNavigatedPage = null;

		if (isLoggedIn(request)) {
			lastNavigatedPage = getLastNavigatedPage(request);
		}

		return lastNavigatedPage;
	}

	public static String getTheme(HttpServletRequest request) {
		String theme  = getStringCookieValue(request, Constants.THEME);
		return theme == null ? "bigers" : theme;
	}
	
	/**
	 * 
	 * @Method Name : getSession
	 * @작성일 : 2015. 11. 8. 오전 10:58:38
	 * @작성자 : wofmaker
	 * @변경이력 :
	 * @Method 설명 : HttpSession 객체 반환
	 * @param request
	 * @return
	 */
	public static HttpSession getSession(HttpServletRequest request) {
		return request.getSession();
	}

	/**
	 * 
	 * @Method Name : getClientIp
	 * @작성일 : 2015. 11. 8. 오전 11:03:20
	 * @작성자 : wofmaker
	 * @변경이력 :
	 * @Method 설명 : HttpServletRequest로 부터 클라이언트 ip 반환
	 * @param request
	 * @return
	 */
	public static String getClientIp(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 
	 * @Method Name  : responseText
	 * @작성일   : 2015. 11. 8. 오전 11:05:34
	 * @작성자   : wofmaker
	 * @변경이력  :
	 * @Method 설명 : Url 요청 후 응답을 text로 받아온다.
	 * @param urlStr
	 * @param encoding
	 * @return
	 * @throws Exception
	 */
	public static String responseText(String urlStr, String encoding) throws Exception {
		return responseText(new URL(urlStr), encoding);
	}
	
	/**
	 * 
	 * @Method Name  : responseText
	 * @작성일   : 2015. 11. 8. 오전 11:08:17
	 * @작성자   : wofmaker
	 * @변경이력  :
	 * @Method 설명 : Url 요청 후 응답을 text로 받아온다.
	 * @param url
	 * @param encoding
	 * @return
	 * @throws Exception
	 */
	public static String responseText(URL url, String encoding) throws Exception {

		StringBuffer sb = new StringBuffer();

		URLConnection con = url.openConnection();

		BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), encoding));

		try {
			String line = null;
			while (true) {

				line = br.readLine();

				if (line == null) {
					break;
				}

				sb.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			br.close();
		}

		return sb.toString();
	}
	
	/**
	 * 
	 * @Method Name  : getServletContext
	 * @작성일   : 2015. 11. 8. 오전 11:16:43
	 * @작성자   : wofmaker
	 * @변경이력  :
	 * @Method 설명 : ServletContext 반환
	 * @return
	 */
	public static ServletContext getServletContext(HttpServletRequest request){
		return getSession(request).getServletContext();
	}
	
	/**
	 * 
	 * @Method Name  : getRealPath
	 * @작성일   : 2015. 11. 8. 오전 11:17:42
	 * @작성자   : wofmaker
	 * @변경이력  :
	 * @Method 설명 : webapps 경로
	 * @return
	 */
	public static String getRealPath(HttpServletRequest request){
		return getServletContext(request).getRealPath("/");
	}
	
	/**
	 * 
	 * @Method Name  : getApplicationPath
	 * @작성일   : 2015. 11. 8. 오전 11:19:08
	 * @작성자   : wofmaker
	 * @변경이력  :
	 * @Method 설명 : 어플리케이션 경로.  was에서는 bin 폴더
	 * @return
	 */
	public String getApplicationPath(){
		return new File(".").getAbsolutePath();
	}
}
