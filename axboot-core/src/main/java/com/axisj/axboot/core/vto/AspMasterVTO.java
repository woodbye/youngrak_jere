package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.asp.AspMaster;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "AspMasterVTO", description = "ASP 정보")
public class AspMasterVTO {

	private String id;

	private String theme;

	public static AspMasterVTO of(AspMaster aspMaster) {
		AspMasterVTO aspMasterVTO = DozerBeanMapperUtils.map(aspMaster, AspMasterVTO.class);
		return aspMasterVTO;
	}

	public static List<AspMasterVTO> of(List<AspMaster> aspMasterList) {
		return aspMasterList.stream().map(aspMaster -> of(aspMaster)).collect(toList());
	}
}
