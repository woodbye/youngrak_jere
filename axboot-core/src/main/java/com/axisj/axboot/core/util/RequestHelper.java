package com.axisj.axboot.core.util;

import com.axisj.axboot.core.domain.user.AdminLoginUser;
import com.axisj.axboot.core.dto.PageContentTO;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class RequestHelper {

	private HttpServletRequest request;

	private RequestHelper(HttpServletRequest request) {
		this.request = request;
	}

	public static RequestHelper of(HttpServletRequest request) {
		RequestHelper requestHelper = new RequestHelper(request);
		return requestHelper;
	}

	public String getString(String key) {
		return request.getParameter(key);
	}

	public String[] getStringArray(String key) {
		String[] targets = getString(key).split(",");
		return targets;
	}

	public String getString(String key, String defaultValue) {
		String value = getString(key);

		if (StringUtils.isEmpty(value)) {
			return defaultValue;
		}

		return value;
	}

	public int getInt(String key) {
		try {
			return Integer.parseInt(getString(key));
		} catch (Exception e) {
			return 0;
		}
	}

	public int getInt(String key, int defaultValue) {
		String value = getString(key);

		if (StringUtils.isEmpty(value)) {
			return defaultValue;
		}

		return getInt(key);
	}

	public long getLong(String key) {
		try {
			return Long.parseLong(getString(key));
		} catch (Exception e) {
			return 0L;
		}
	}

	public long getLong(String key, long defaultValue) {
		String value = getString(key);

		if (StringUtils.isEmpty(value)) {
			return defaultValue;
		}

		return getLong(key);
	}

	public void setAttribute(String key, String value) {
		request.setAttribute(key, value);
	}

	public String getStringAttribute(String key) {
		return getStringAttribute(key, "");
	}

	public String getStringAttribute(String key, String defaultValue) {
		Object value = request.getAttribute(key);

		if (value == null) {
			return defaultValue;
		}

		return value.toString();
	}

	public void setSessionAttribute(String key, Object value) {
		request.getSession().setAttribute(key, value);
	}

	public Object getSessionAttributeObject(String key) {
		return request.getSession().getAttribute(key);
	}

	public <T> T getSessionAttributeObject(String key, Class<T> clazz) {
		Object object = getSessionAttributeObject(key);

		if (object != null) {
			return clazz.cast(object);
		}
		return null;
	}


	public String getSessionAttributeString(String key) {
		return getSessionAttributeString(key, "");
	}

	public String getSessionAttributeString(String key, String defaultValue) {
		Object value = getSessionAttributeObject(key);

		if (value == null) {
			return defaultValue;
		}

		return value.toString();
	}

	public boolean hasSessionAttribute(String key) {
		return getSessionAttributeObject(key) == null ? false : true;
	}

	public String setAuth(String value) {
		if (value != null && value.equals("Y")) {
			return "Y";
		}
		return null;
	}

	public boolean hasParameter(String key) {
		if (request.getParameterMap() != null) {
			return request.getParameterMap().containsKey(key);
		}

		return false;
	}

	public PageContentTO getPageContent() {
		return getSessionAttributeObject(PageContentTO.PAGE_CONTENT, PageContentTO.class);
	}

	public void setSessionAttributes(PageContentTO pageContent, AdminLoginUser user) {
		setSessionAttribute(PageContentTO.PAGE_CONTENT, pageContent);

		setSessionAttribute(PageContentTO.PAGE_ID, pageContent.getPageId());
		setSessionAttribute(PageContentTO.PAGE_NAME, pageContent.getMenuName());
		setSessionAttribute(PageContentTO.PAGE_REMARK, pageContent.getMenuRemark());
		setSessionAttribute(PageContentTO.CLASS_NAME, pageContent.getClassName());

		setSessionAttribute(PageContentTO.SEARCH_AUTH, setAuth(pageContent.getSearchAuth()));
		setSessionAttribute(PageContentTO.SAVE_AUTH, setAuth(pageContent.getSaveAuth()));
		setSessionAttribute(PageContentTO.EXCEL_AUTH, setAuth(pageContent.getExcelAuth()));
		setSessionAttribute(PageContentTO.FUNCTION_1_AUTH, setAuth(pageContent.getFunction1Auth()));
		setSessionAttribute(PageContentTO.FUNCTION_2_AUTH, setAuth(pageContent.getFunction2Auth()));
		setSessionAttribute(PageContentTO.FUNCTION_3_AUTH, setAuth(pageContent.getFunction3Auth()));
		setSessionAttribute(PageContentTO.FUNCTION_4_AUTH, setAuth(pageContent.getFunction4Auth()));
		setSessionAttribute(PageContentTO.FUNCTION_5_AUTH, setAuth(pageContent.getFunction5Auth()));

		setSessionAttribute(PageContentTO.LOGIN_USER_NAME, user.getUserNm());
		setSessionAttribute(PageContentTO.LOGIN_USER_ID, user.getUsername());
		setSessionAttribute(PageContentTO.COMPANY_CODE, user.getCompanyCode());

		setSessionAttribute(PageContentTO.ENV, CommonUtils.activeProfile());

	}

	public String getMenuJson() {
		return getSessionAttributeString(PageContentTO.MENU_JSON);
	}
}

