package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.code.BasicCode;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.google.common.collect.Maps;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Id;

import org.springframework.web.multipart.MultipartFile;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "MdcontentsVTO", description = "고인및사진 정보")
public class MdcontentsVTO {

	@ApiModelProperty(value = "봉안실 번호")
	private String contId;
	
	@ApiModelProperty(value = "사진타입")
	private String contType;
	
	@ApiModelProperty(value = "고인명")
	private String contTitle;
	
	@ApiModelProperty(value = "원본 파일명")
	private String contFile;
	
	@ApiModelProperty(value = "파일URL")
	private String contURL;
		
	@ApiModelProperty(value = "등록일")
	private String contDate;
	
	@ApiModelProperty(value = "신청자")
	private String contRegUser;
	
	@ApiModelProperty(value = "실제 파일명")
	private String contRealFileName;
	
	private MultipartFile contMultipartFile;
	
	//@Column(name="ContOriFileName" length=100)
	//private String contOriFileName;


	public static MdcontentsVTO of(Mdcontents mdcontents) {
		return DozerBeanMapperUtils.map(mdcontents, MdcontentsVTO.class);
	}

	public static List<MdcontentsVTO> of(List<Mdcontents> mdcontentsList) {
		return mdcontentsList.stream().map(mdcontents -> of(mdcontents)).collect(toList());
	}	

}
