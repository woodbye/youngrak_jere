package com.axisj.axboot.core.domain.inspect;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.persistence.QueryHint;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.room.Room;

@Repository
public interface InspectRepository extends JpaRepository<Inspect, InspectId> {

	List<Inspect> findByWorkDateBetween(String strDate, String endDate) throws SQLException;

	@Query(nativeQuery=true,value="SELECT IFNULL(MAX(SEQ_NO), 0)+1 FROM INSPECT WHERE WORK_DATE = ?1")
	Integer findNextSeqNo(String workDate) throws SQLException;

	@Query("SELECT I FROM Inspect I WHERE ?1 BETWEEN STDATE AND EDDATE")
	List<Inspect> getByStringDate(String date) throws SQLException;

	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	//@Cacheable
	Inspect findFirstByStdateLessThanEqualOrderByWorkDateDescSeqNoDesc(String stDate) throws SQLException;

}
