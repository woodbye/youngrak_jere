package com.axisj.axboot.core.util;
 
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
 
enum OS {
    LINUX, WINDOW
}
enum Encoding {
    EUCKR, UTF8;
}
public class CommandUtils {
 
    public OS os = OS.WINDOW;
    public Encoding encoding = Encoding.EUCKR;
    
    public Process p = null;
    public BufferedWriter bw = null;
    public BufferedReader errorBr = null;
    public BufferedReader br = null;
    
    /**
     * 
     * @Method Name  : setOs
     * @작성일   : 2015. 12. 20. 오후 4:47:43
     * @작성자   : Lee
     * @변경이력  :
     * @Method 설명 : os 정보 세팅
     */
    public void setOs(){
        String osName = System.getProperty("os.name");
        if(osName.toLowerCase().indexOf("win") != -1){
            os = OS.WINDOW;
        }else{
            os = OS.LINUX;
        }
    }
    
    /**
     * 
     * @Method Name  : runCmd
     * @작성일   : 2015. 12. 20. 오후 4:47:19
     * @작성자   : Lee
     * @변경이력  :
     * @Method 설명 : cmd를 실행하여 인풋 아웃풋을 생성
     * @throws Exception
     */
    public void runCmd() throws Exception{
        
        setOs();
        
        String command = null;
        switch (os) {
        case WINDOW:
            command = "cmd";
            break;
        }
        
        ProcessBuilder pb = new ProcessBuilder(command);
        pb.redirectOutput(Redirect.PIPE);
        pb.redirectError(Redirect.PIPE);
 
        p = pb.start();
        bw = new BufferedWriter(new OutputStreamWriter(p.getOutputStream(), encoding.name()));
        errorBr = new BufferedReader(new InputStreamReader(p.getErrorStream(), encoding.name()));
        br = new BufferedReader(new InputStreamReader(p.getInputStream(), encoding.name()));
    }
    
    /**
     * 
     * @Method Name  : destroy
     * @작성일   : 2015. 12. 20. 오후 4:53:09
     * @작성자   : Lee
     * @변경이력  :
     * @Method 설명 : 프로세스 죽이기
     */
    public void destroy(){
        if(bw != null) try {    bw.close();    bw = null;} catch (IOException e) {}
        if(br != null) try {    br.close();    br = null;} catch (IOException e) {}
        if(p != null) {p.destroy(); p = null;};
    }
    
    /**
     * 
     * @Method Name  : runOutputThread
     * @작성일   : 2015. 12. 20. 오후 4:46:52
     * @작성자   : Lee
     * @변경이력  :
     * @Method 설명 : 콘솔에 출력을 담당하는 쓰레드 실행
     */
    public void runOutputThread(){
        new Thread(new Runnable() {
            
            @Override
            public void run() {
                while(true){
                    try {
                        int data = br.read();
                        System.out.print((char)data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    
                }
            }
        }).start();
        new Thread(new Runnable() {
            
            @Override
            public void run() {
                while(true){
                    try {
                        String data = errorBr.readLine();
                        System.err.print(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    
                }
            }
        }).start();
    }
    
    /**
     * 
     * @Method Name  : runCmd
     * @작성일   : 2015. 12. 20. 오후 4:46:34
     * @작성자   : Lee
     * @변경이력  :
     * @Method 설명 : input 스트림을 받아 cmd 실행
     * @param is
     * @throws Exception
     */
    public void runCmd(InputStream is) throws Exception{
        
        Scanner sc = new Scanner(is);
        
        String line = null;
        if(bw == null){
            runCmd();
            runOutputThread();
        }
        while(true){
            line = sc.nextLine();
            
            if(line.equalsIgnoreCase("q")){
                System.exit(0);
            }
            
            bw.write(line+String.format("%n"));
            bw.flush();
        }
    }
    
    /**
     * 
     * @Method Name  : runCmdSystemIn
     * @작성일   : 2015. 12. 20. 오후 4:46:10
     * @작성자   : Lee
     * @변경이력  :
     * @Method 설명 : 시스템 인풋으로 cmd 실행
     * @throws Exception
     */
    public void runCmdSystemIn() throws Exception{
        runCmd(System.in);
    }
    
    /**
     * 
     * @Method Name  : excuteCommand
     * @작성일   : 2015. 12. 20. 오후 4:45:44
     * @작성자   : Lee
     * @변경이력  :
     * @Method 설명 : 하나의 명령어를 실행하고 완료될때까지 기다리는 메소드
     * @param command
     * @return
     * @throws Exception
     */
    public int excuteCommand(String command) throws Exception{
        
        setOs();
        
        List<String> commands = new ArrayList<String>();
        
        if(os ==OS.WINDOW){
            commands.add("cmd");
            commands.add("/C");
        }
        commands.add(command);
        
        ProcessBuilder pb = new ProcessBuilder(commands);
        Process p = pb.start();
        
        return p.waitFor();
    }
}