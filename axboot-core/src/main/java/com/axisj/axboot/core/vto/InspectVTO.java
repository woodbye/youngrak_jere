package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;

import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectId;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "InspectVTO", description = "점검 제례실")
public class InspectVTO {
	
	@ApiModelProperty(value = "등록일자 PK")
    private String workDate;
	@ApiModelProperty(value = "일련번호 PK")
    private Integer seqNo;
	@ApiModelProperty(value = "적용시작일시(년월일시분초)")    
    private String stdate;
	@ApiModelProperty(value = "적용종료일시(년월일시분초)")
    private String eddate;
	@ApiModelProperty(value = "점검사유")
    private String remark;
	@ApiModelProperty(value = "등록 시간")
    private String regTime;    
	@ApiModelProperty(value = "등록 유저")
    private String regId;     
	@ApiModelProperty(value = "수정 시간")
    private String udtTime;     
	@ApiModelProperty(value = "수정 유저")
    private String udtId;  
	
	public static InspectVTO of(Inspect inspect) {
		InspectVTO inspectVTO = DozerBeanMapperUtils.map(inspect, InspectVTO.class);
		return inspectVTO;
	}
	
	public static List<InspectVTO> of(List<Inspect> list){
		return list.stream().map(inspect -> of(inspect)).collect(toList());
	}

}
