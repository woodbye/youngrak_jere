package com.axisj.axboot.core.domain.inspectroom;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.axisj.axboot.core.domain.inspect.InspectId;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Embeddable
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName="of")
@ToString(includeFieldNames=true)
public class InspectroomId implements Serializable {

	@NonNull
	private String  workDate;
	@NonNull
	private Integer seqNo;
	@NonNull
	private String roomCd;
}
