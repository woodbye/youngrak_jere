package com.axisj.axboot.core.domain.asp;

import com.axisj.axboot.core.domain.BasicJpaModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "ASP_M")
public class AspMaster extends BasicJpaModel<String> {

	@Id
	@Column(name = "ID", length = 50)
	private String id;

	@Column(name = "THEME", length = 100)
	private String theme;

	@Override
	public String getId() {
		return id;
	}
}
