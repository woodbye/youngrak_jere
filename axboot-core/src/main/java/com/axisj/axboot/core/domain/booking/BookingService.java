package com.axisj.axboot.core.domain.booking;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetRepository;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectRepository;
import com.axisj.axboot.core.domain.inspect.InspectService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomRepository;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsRepository;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomRepository;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.axisj.axboot.core.vto.BookingVTO;

@Service
public class BookingService  extends BaseService<Booking, BookingId>{

	private BookingRepository bookingRepository;
	
	@Inject
	public BookingService(BookingRepository repository) {
		super(repository);
		this.bookingRepository = repository;
	}
	
	@Inject
	private InspectRepository inspectRepository;

	@Inject
	private InspectroomService inspectroomService;
	
	@Inject
	private InspectroomRepository inspectroomRepository;
	
	@Inject
	private EnvsetRepository envsetRepository;
	
	@Inject
	private RoomRepository roomRepository;
			
	@Inject
	private MdcontentsRepository mdcontentsRepository;

	public Page<Booking> findByBookDate(String bookDate, Pageable pageable) throws SQLException {
		return bookingRepository.findByBookDate(bookDate, pageable);
	}
	
	public List<Booking> findAfterBookings(String bookDate, String status) throws SQLException {
		// 시간단위로
		Date now = new Date();
		now.setHours(now.getHours()-1);
		now.setMinutes(0);
		return bookingRepository.findByBookDateAndStatusAndStTimeGreaterThanEqual(bookDate, status, DateUtils.formatToDateString("HHmm", now));
	}
	
	
	public void saveBookList(List<BookingVTO> bookingVTOList) {
		
		List<Booking> bookingList = DozerBeanMapperUtils.mapList(bookingVTOList, Booking.class);
		
	}
	
	public void initBooking(Booking booking) throws Exception{
		
		
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");				
		
		// 현시간 적용되는 환경 설정 정보
		Envset envset = envsetRepository.findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
		
		// 금일 제례실의 마지막 예약
		Booking lastBooking = bookingRepository.findFirstByBookDateAndRoomCdAndStatusOrderByBookSeqDesc(booking.getBookDate(), booking.getRoomCd(), "R");
		
		// 다음 연번
		Booking temp = bookingRepository.findFirstByBookDateOrderByBookSeqDesc(DateUtils.formatToDateString("yyyyMMdd"));
		Integer nextBookSeq = (temp==null ? 0: temp.getBookSeq())+1;
		booking.setBookSeq(nextBookSeq);
		String nextStTime = "";
		
		// 다음 제례 시작 시간
		Date lastEdTime = null;
		if(lastBooking == null){
			lastEdTime = new Date();
		}else{
			lastEdTime = new SimpleDateFormat("yyyyMMddHHmm").parse(DateUtils.formatToDateString("yyyyMMdd")+lastBooking.getEdTime());
			lastEdTime.setMinutes(lastEdTime.getMinutes() + envset.getCleanTime());
			if(lastEdTime.getTime() <= System.currentTimeMillis()){
				
				lastEdTime = new Date();
				
			}
			//String stTime = new SimpleDateFormat("HHmm").format(lastEdTime);		
			//SimpleDateFormat fm = new SimpleDateFormat("HHmm");
			
			//long now = fm.parse(stTime).getTime()+60000;
			//lastEdTime = new Date(now);
		}
		
		
		nextStTime = new SimpleDateFormat("HHmm").format(lastEdTime);
		booking.setStTime(nextStTime);
		
		//다음 제례 종료 시간
		lastEdTime.setMinutes(lastEdTime.getMinutes() + envset.getRuningTime());
		String nextEdTime = new SimpleDateFormat("HHmm").format(lastEdTime);
		booking.setEdTime(nextEdTime);
		
		//고인 ID 가 없을 경우 사진 사용 안함 처리
		if(StringUtils.isEmpty(booking.getContid())){
			booking.setPhotoYn("N");
		}
		
		// 신규는 예약으로 처리
		booking.setStatus("R");
	}

	public void saveBooking(Booking booking, boolean isManager) throws Exception {
		
		booking.setBookDate(booking.getBookDate().replaceAll("-", ""));
		// 금일
		String workDate = DateUtils.formatToDateString("yyyyMMdd");
		
		if(!workDate.equals(booking.getBookDate())){
			throw new Exception("예약은 당일만 가능합니다.");
		}
		
		//제례실 점검여부 체크
		boolean canBooking = inspectroomService.canBooking(booking.getRoomCd());
		
		if(!canBooking && !isManager){
			throw new Exception("제례 예약을 할 수 없습니다.");
		}
		
		if(booking.getBookSeq() == null){  // 신규
			//예약정보 세팅
			initBooking(booking);
		}
		
		this.save(booking);
		
	}

	public List<Booking> selectBookingList(String stDate) throws Exception {
		
		String stTime = stDate.substring(8,12);
		String bookDate = stDate.substring(0,8);	
		Envset envset = envsetRepository.findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate); //운영정보
		List<Booking> list = bookingRepository.selectRoomByLastEdTime(bookDate, stTime, envset.getCleanTime()); // 모든제례실 현재 예약정보
		
		//종료시간=종료시간+클린타입 세팅
		list = list.stream().map(booking ->{
			Date edTime = null;
			try {
				edTime = new SimpleDateFormat("yyyyMMddHHmm").parse(booking.getBookDate()+booking.getEdTime());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			edTime.setMinutes(edTime.getMinutes()+envset.getCleanTime());
			String extTime = new SimpleDateFormat("HHmm").format(edTime);
			booking.setEdTime(extTime);
			return booking;
		}).collect(Collectors.toList());
		
		return list;
	}

	public Page<Booking> selectAllBookingList(String stDate ,Pageable pageable) throws Exception {

		String edTime = stDate.substring(8,12);
		String bookDate = stDate.substring(0,8);	
		String status = "R";
		return bookingRepository.findByBookDateAndEdTimeGreaterThanEqualAndStatus(bookDate, edTime,status,pageable);
	}	
	
	
	public synchronized Booking romrSaveBooking(Booking booking)  throws Exception {
		
		
		String bookDate = DateUtils.formatToDateString("yyyyMMdd");
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");	
		String stTime = DateUtils.formatToDateString("HHmm");	
		
		if(!bookDate.equals(booking.getBookDate())){
			throw new Exception("예약은 당일만 가능합니다.");
		}		
		// 방선택안하고 다음버튼 눌렀을경우 즉시사용가능한방으로 예약처리
		abc : if(booking.getRoomCd() == null || booking.getRoomCd().equals("")){
		// 현시간 적용되는 환경 설정 정보
			Envset envset = envsetRepository.findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
							
			//전체 제례실
			List<Room> roomList = roomRepository.findAll();	
			
			//현시간 적용되는 제례실점검일정
			Inspect inspectList = inspectRepository.findFirstByStdateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
			
			//현시간 적용되는 점검대상 제례실 목록
			List<Inspectroom> inspectroomList = inspectroomRepository.findByWorkDateAndSeqNo(inspectList.getWorkDate(), inspectList.getSeqNo());
			
			//점검 제례실제거
			List<Room> removeRoomList =	inspectroomList.stream().map(inpectroom ->{
					Room room = inpectroom.getRoom();
					return room;
			}).collect(Collectors.toList());			
			roomList.removeAll(removeRoomList);
			
						
			TreeMap<String, Booking> map = new TreeMap<String, Booking>();		
						
			//점검제례실이 아닌 제례실로 예약자료 조회
			for(Room room : roomList){								
				
				Booking lastBooking = bookingRepository.findFirstByBookDateAndRoomCdAndEdTimeGreaterThanEqualAndStatusOrderByEdTimeDesc(booking.getBookDate(), room.getRoomCd(), stTime, "R");					
				map.put(room.getRoomCd(), lastBooking);					
			}					
			
			//즉시 사용가능한 방이 하나라도 있을경우 방번호세팅
			for (Entry<String, Booking> entry : map.entrySet())	{							
				if(entry.getValue() == null){
					booking.setRoomCd(entry.getKey());
					break abc;
				}
			}					
			
			//예약중일경우 종료시간이 가장빠른 방으로 방번호세팅
			TreeMap<Integer,String> edTimes = new TreeMap<Integer, String>();
			for (Entry<String, Booking> entry : map.entrySet()){
				if(entry.getValue() == null){
					booking.setRoomCd(entry.getKey());
					break;
				}				
				if(entry.getValue() != null){
					edTimes.put(Integer.parseInt(entry.getValue().getEdTime()), entry.getValue().getRoomCd());
				}				
			}
			booking.setRoomCd(edTimes.get(edTimes.firstKey()));
			
		}
		
		boolean canBooking = inspectroomService.canBooking(booking.getRoomCd());
		
		if(!canBooking){
			throw new Exception("제례 예약을 할 수 없습니다.");
		}
				
		if(booking.getBookSeq() == null){ 
			initBooking(booking);
		}		
		
		this.save(booking);
		
		return booking;
	}

	public Booking selectBookingByContTitle(String bookDate, String stTime, String contTitle) throws Exception {
		//검색한 고인이 현재 예약중인지 확인
		return bookingRepository.findFirstByBookDateAndContidAndEdTimeGreaterThanAndStatus(bookDate,contTitle,stTime,"R");
	}
	
	@Autowired
	InspectService inspectService;

	public Booking getFastestEndingBook(List<Booking> books) throws Exception {
		
		HashMap<String, List<Booking>> groupByRoomCd = new HashMap<>();
		
		List<Room> rooms = inspectService.selectAvailableRooms();
		
		rooms.forEach(room->groupByRoomCd.put(room.getRoomCd(), new ArrayList<Booking>()));
		
		// 제례실 그룹별로 나누고
		books.forEach(book->{
			List<Booking> item = groupByRoomCd.get(book.getRoomCd());
			if(item == null){
				groupByRoomCd.put(book.getRoomCd(), new ArrayList<Booking>());
			}
			groupByRoomCd.get(book.getRoomCd()).add(book);
		});
		
		TreeMap<Integer, Booking> sortedByEdTime = new TreeMap<>();
		// 그룹으로 나눈 것들의 max값을 가져와 종료 시간으로 소트 후 
		for(Entry<String, List<Booking>> entry : groupByRoomCd.entrySet()){
			if(entry.getValue().size() == 0){
				return null;
			}
			Booking bk = entry.getValue().stream().max((b1,b2)->Integer.parseInt(b1.getEdTime()) < Integer.parseInt(b2.getEdTime()) ? 1 : -1).get();
			sortedByEdTime.put(Integer.parseInt(bk.getEdTime()), bk);
		}
		
		if(sortedByEdTime.size() == 0){
			return null;
		}
		
		// 첫번째 값을 가지고 오면 가장빠른 예약시간을 구할 수 있음.
		return sortedByEdTime.firstEntry().getValue();
	}

	public String getFastestTime(List<Booking> books, int cleanTime) throws Exception {
		
		Date now = new Date();
		
		// 각 방별로 마지막 예약중 가장 빨리 끝나는 예약
		Booking fastestEndingBook = this.getFastestEndingBook(books);
		
		if(fastestEndingBook == null){
			return DateUtils.formatToDateString("HH:mm", now);
		}
		// 예약 가능 시간
		Date fastestTime = DateUtils.parseDate("yyyyMMddHHmm", DateUtils.formatToDateString("yyyyMMdd")+fastestEndingBook.getEdTime());
		fastestTime.setMinutes(fastestTime.getMinutes()+cleanTime);
		fastestTime = fastestTime.getTime() < now.getTime() ? now : fastestTime;
		
		return DateUtils.formatToDateString("HH:mm", fastestTime);
	}

	public String selectRemainTime(String edTime) throws Exception{
		//대기시간 구하기
		return bookingRepository.selectRemainTime(edTime);
	}

	public void updateExtTime(Booking booking) throws Exception {
		
		
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");				
		
		// 현시간 적용되는 환경 설정 정보
		Envset envset = envsetRepository.findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
		
		booking = bookingRepository.findByBookDateAndBookSeq(booking.getId().getBookDate(),booking.getId().getBookSeq());
		
		//연장횟수 체크
		if((booking.getExCnt() == null ? 0 : booking.getExCnt()) >= envset.getExtCnt()){
			 return;
		}
	
		//연장정보 세팅
		booking.setExCnt(booking.getExCnt() == null ? 1 : booking.getExCnt()+1);
		booking.setExMinute(envset.getExtTime());
		
		Date edTime = null;
		edTime = new SimpleDateFormat("yyyyMMddHHmm").parse(booking.getBookDate()+booking.getEdTime());
		edTime.setMinutes(edTime.getMinutes()+envset.getExtTime());
		String extTime = new SimpleDateFormat("HHmm").format(edTime);
		booking.setEdTime(extTime);
		
		bookingRepository.save(booking);
		
	}

	public Booking nextBooking(Booking booking) throws Exception {
		
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");	
		// 현시간 적용되는 환경 설정 정보
		Envset envset = envsetRepository.findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
	
		//종료시간+=클린타임 세팅
		Date edTime = null;
		edTime = new SimpleDateFormat("yyyyMMddHHmm").parse(booking.getBookDate()+booking.getEdTime());
		edTime.setMinutes(edTime.getMinutes()+envset.getCleanTime());
		String extTime = new SimpleDateFormat("HHmm").format(edTime);
		//booking.setEdTime(extTime);
		
		
		//현재시간에서 가장빠른 예약정보 가져온다.
		Booking nextBooking = null;
		nextBooking = bookingRepository.findFirstByBookDateAndStatusAndEdTimeGreaterThanAndRoomCdOrderByEdTimeAsc(booking.getBookDate(), booking.getStatus(), extTime,booking.getRoomCd());
		return nextBooking;
	}

	public void updateEnd(Booking booking) throws Exception {
		
		Booking endBooking = findOne(booking.getId());
		
		
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");	
		// 현시간 적용되는 환경 설정 정보
		Envset envset = envsetRepository.findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
	
		//종료시간+=연장시간 세팅
		Date edTime = null;
		edTime = new SimpleDateFormat("yyyyMMddHHmm").parse(booking.getBookDate()+DateUtils.formatToDateString("HHmm"));
		edTime.setMinutes(edTime.getMinutes());
		String extTime = new SimpleDateFormat("HHmm").format(edTime);
		endBooking.setEdTime(extTime);
		
		repository.save(endBooking);
	}

	public Booking selectBooking( Integer cleanTime ,String roomCd) throws Exception {
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");	
		String bookDate = DateUtils.formatToDateString("yyyyMMdd");	
		
		return bookingRepository.selectBooking(stDate.substring(0,8), stDate, cleanTime, roomCd);
		
	}

}
