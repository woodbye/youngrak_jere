package com.axisj.axboot.core.domain.user;


import com.axisj.axboot.core.domain.program.menu.AuthorizedMenu;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Map;

@Data
public class AdminLoginUser implements UserDetails {

	@JsonProperty("username")
	private String username;

	@JsonProperty("password")
	private String password;

	@JsonProperty("userNm")
	private String userNm;

	@JsonProperty("expires")
	private long expires;

	@JsonProperty("companyCode")
	private String companyCode;

	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("userType")
	private String userType;

	@JsonProperty("menuJsonHash")
	private String menuJsonHash;

	@JsonIgnore
	private Map<String, AuthorizedMenu> authorizedMenuMap;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}
}
