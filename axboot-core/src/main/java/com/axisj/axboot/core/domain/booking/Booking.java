package com.axisj.axboot.core.domain.booking;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name="BOOKING")
@IdClass(BookingId.class)
@ToString(includeFieldNames=true)
//@Cacheable
//@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Booking extends BasicJpaModel<BookingId>{

    @Id
    @Column(name = "BOOK_DATE", length = 8)
	private String bookDate;

    @Id
    @Column(name = "BOOK_SEQ", precision = 3)
	private Integer bookSeq;

	@Column(name = "ROOM_CD", length = 3)
	private String roomCd;

	//@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROOM_CD", referencedColumnName = "ROOM_CD", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Room room;

	//@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
	@NotFound(action=NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CONTID", referencedColumnName = "CONTID", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Mdcontents mdcontents;

	@Column(name = "ST_TIME", length = 4)
	private String stTime;

	@Column(name = "EX_CNT", precision = 1)
	private Integer exCnt;

	@Column(name = "EX_MINUTE", precision = 2)
	private Integer exMinute;


	@Column(name = "ED_TIME", length = 4)
	private String edTime;

	@Column(name = "PHOTO_YN", length = 1)
	private String photoYn;

	@Column(name = "CONTID", length = 20)
	private String contid;

	@Column(name = "CONTTITLE", length = 20)
	private String conttitle;

	@Column(name = "PHONE_NO", length = 15)
	private String phoneNo;

	@Column(name = "SMS_YN", length = 1)
	private String smsYn;

	@Column(name = "STATUS", length = 1)
	private String status;

	@Column(name = "BOOK_MAN", length = 1)
	private String bookMan;

	@Column(name = "BOOK_BIGO", length = 100)
	private String bookBigo;

	@Column(name = "CANCEL_MAN", length = 1)
	private String cancelMan;

	@Column(name = "CANCEL_BIGO", length = 100)
	private String cancelBigo;

	@Column(name = "REGTIME", length = 14)
	private String regtime;

	@Column(name = "REGID", length = 20)
	private String regid;

	@Column(name = "UDTTIME", length = 14)
	private String udttime;

	@Column(name = "UDTID", length = 20)
	private String udtid;

	@Override
	public BookingId getId() {
		return BookingId.of(bookDate, bookSeq);
	}

	@PrePersist
	private void onPersist() {
		this.regid = this.udtid = CommonUtils.getCurrentLoginUserCd();
		this.regtime = this.udttime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}

	@PreUpdate
	private void onUpdate() {
		this.udtid = CommonUtils.getCurrentLoginUserCd();
		this.udttime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	@PostLoad
	private void onPostLoad() {
	}
}
