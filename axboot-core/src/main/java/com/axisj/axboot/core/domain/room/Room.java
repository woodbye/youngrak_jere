package com.axisj.axboot.core.domain.room;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name="ROOM")
@ToString(includeFieldNames=true)
@EqualsAndHashCode
//@Cacheable
public class Room extends BasicJpaModel<String> {

	
	@Id
	@Column(name = "ROOM_CD", length = 3)
	private String roomCd;
	
	@Column(name = "ROOM_NM", length = 40)
	private String roomNm;
	
	@Column(name = "AREA_CD", length = 1)
	private String areaCd;
	
	@Column(name = "MACHINE_CD", length = 3)
	private String machineCd;
	
	@Column(name = "REMARK", length = 200)
	private String remark;
	
	@Column(name = "REGTIME", length = 14)
	private String regTime;
	
	@Column(name = "REGID", length = 20)
	private String regId; 
	
	@Column(name = "UDTTIME", length = 14)
	private String udtTime; 
	
	@Column(name = "UDTID", length = 20)
	private String udtId;  

	@Override
	public String getId() {
		
		return roomCd;
	}

	@PrePersist
	private void onPersist() {		
		this.regId = this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.regTime = this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	
	@PreUpdate
	private void onUpdate() {
		this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
}
