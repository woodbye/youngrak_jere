package com.axisj.axboot.core.util;


import com.axisj.axboot.core.code.ApplicationProfiles;
import com.axisj.axboot.core.context.AppContextManager;
import com.axisj.axboot.core.domain.user.User;
import com.axisj.axboot.core.domain.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.regex.Pattern;

@Component
public class CommonUtils {

	public static String activeProfile() {
		Environment environment = getEnvironment();
		String[] activeProfiles = environment.getActiveProfiles();

		if (activeProfiles != null && activeProfiles.length > 0) {
			return Arrays.toString(activeProfiles).replaceAll("\\[", "").replaceAll("\\]", "");
		}

		return ApplicationProfiles.LOCAL;
	}

	public static UserDetails getCurrentUserDetail() {
		try {
			UserDetails userDetail = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
			return userDetail;
		} catch (Exception e) {
			// ignored
		}
		return null;
	}

	public static User getCurrentLoginUser() {
		UserDetails userDetails = getCurrentUserDetail();

		if (userDetails != null) {
			UserService userService = AppContextManager.getAppContext().getBean(UserService.class);
			User user = userService.findOne(userDetails.getUsername());
			return user;
		}
		return null;
	}

	public static String getCurrentLoginUserCd() {
		UserDetails userDetails = getCurrentUserDetail();
		return userDetails == null ? "" : userDetails.getUsername();
	}

	public static boolean isProduction() {
		return activeProfile().equals(ApplicationProfiles.PRODUCTION);
	}

	public static Environment getEnvironment() {
		return AppContextManager.getAppContext().getBean(Environment.class);
	}

	public static boolean canConvertMappingVTO(Object object) {
		try {
			if (object != null && !StringUtils.isEmpty(object.toString())) {
				return true;
			}
		} catch (Exception e) {
			// ignore
		}
		return false;
	}
	
	public static boolean isIpaddress(String ip){
		String validIp = "^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
	    if(ip.isEmpty()){
	    	return false;
	    }
		if (!Pattern.matches(validIp, ip )) {
			return false;
		}
		  
		return true;
	}
	

}
