package com.axisj.axboot.core.domain.code;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BasicCodeRepository extends JpaRepository<BasicCode, BasicCodeId> {
	List<BasicCode> findByBasicCd(String basicCd);

	List<BasicCode> findByPosUseYn(String posUseYn);

	Page<BasicCode> findByBasicCdContainingOrBasicNmContaining(String basicCd, String basicNm, Pageable pageable);

	List<BasicCode> findByBasicCdContainingOrBasicNmContaining(String basicCd, String basicNm);

	List<BasicCode> findAllByOrderByBasicCdAscCodeAsc();

	List<BasicCode> findByBasicCdAndUseYn(String basicCd, String useYn);

}
