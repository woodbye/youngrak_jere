package com.axisj.axboot.core;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.machine.Machine;
import com.axisj.axboot.core.domain.machine.MachineService;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import net.bigers.machinecontrolle.Message;

//@EnableScheduling
public class Demon {

	@Inject
	private MachineService machineService;

	@Inject
	private EnvsetService envsetService;


	//@Scheduled(fixedRate = 60000)
	public void scheduler () throws Exception{

		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");
		Envset envset = envsetService.selectEnvSet(stDate);
		//운영환경 오픈시간이랑 같으면 브라우저 시작
		if(envset.getOpenTime().equals(stDate.substring(8, 12))){
			monitorOn();
		}else if(envset.getCloseTime().equals(stDate.substring(8,12))){ // 브라우저 종료
			monitorOff();
		}

	}

	//@Scheduled(cron="*/10 * * * * *")
	public void monitorOn() throws Exception{

		List<Machine> machines = machineService.selectMachineList("");
		machines.forEach(machine -> {

			try {
				if(CommonUtils.isIpaddress(machine.getIpaddress())){
					processCommand("startProcess",machine.getIpaddress());
					System.out.println("startProcess : " +machine.getMachineNm()  +" //// " + machine.getIpaddress());
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	//@Scheduled(cron="*/10 * * * * *")
	public void monitorOff() throws Exception{

		List<Machine> machines = machineService.selectMachineList("");
		machines.forEach(machine -> {

			try {

				if(CommonUtils.isIpaddress(machine.getIpaddress())){
					processCommand("destroyProcess",machine.getIpaddress());
					System.out.println("destroyProcess : " + machine.getMachineNm()  +" //// " + machine.getIpaddress());
				}

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	public void processCommand(String command, String ip ) throws Exception{

		Socket s = new Socket(ip, 9900);
		String str = "";
		ObjectOutputStream oos = null;

		DataInputStream inputStream = null;
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			oos.writeObject(Message.of(command));
			inputStream = new DataInputStream(s.getInputStream());
			str = inputStream.readUTF();
			if(str.equals("OK")){
				s.close();
			};

		}catch(EOFException e){

		}catch (Exception e) {
			//e.printStackTrace();
		}finally {
			oos.close();
			s.close();
			inputStream.close();
		}

	}
}
