package com.axisj.axboot.core.util;

import com.axisj.axboot.core.context.AppContextManager;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;

import java.util.ArrayList;
import java.util.List;

public class DozerBeanMapperUtils {

	public static <T> T map(Object source, Class<T> destinationClass) throws MappingException {
		return getDozerBeanMapper().map(source, destinationClass);
	}

	public static <T> List<T> mapList(List<?> listSources, Class<T> destinationClass) {
		List<T> resultList = new ArrayList<>();

		if (!ArrayUtils.isEmpty(listSources)) {
			for (Object source : listSources) {
				resultList.add(map(source, destinationClass));
			}
		}

		return resultList;
	}

	public static DozerBeanMapper getDozerBeanMapper() {
		return AppContextManager.getBean("dozerBeanMapper", DozerBeanMapper.class);
	}
}
