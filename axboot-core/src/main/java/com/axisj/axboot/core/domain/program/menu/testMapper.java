package com.axisj.axboot.core.domain.program.menu;

import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.mybatis.MyBatisMapper;

import java.util.List;

public interface testMapper extends MyBatisMapper{

   List<Inspectroom> roomTest() throws Exception;

}
