package com.axisj.axboot.core.domain.code;

import com.axisj.axboot.core.domain.BaseJpaModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "BASIC_M")
@IdClass(BasicCodeId.class)
public class BasicCode extends BaseJpaModel<BasicCodeId> {

	@Id
	@Column(name = "BASIC_CD", length = 50)
	private String basicCd;

	@Id
	@Column(name = "CODE", length = 10)
	private String code;

	@Column(name = "BASIC_NM", length = 100)
	private String basicNm;

	@Column(name = "NAME", length = 50)
	private String name;

	@Column(name = "SORT", precision = 3)
	private Integer sort;

	@Column(name = "DATA1", length = 100)
	private String data1;

	@Column(name = "DATA2", length = 100)
	private String data2;

	@Column(name = "DATA3", length = 100)
	private String data3;

	@Column(name = "DATA4", precision = 10)
	private Integer data4;

	@Column(name = "DATA5", precision = 10)
	private Integer data5;

	@Column(name = "POS_USE_YN", length = 1)
	private String posUseYn;

	@Column(name = "REMARK", length = 200)
	private String remark;

	@Column(name = "USE_YN", length = 1)
	private String useYn;

	@Override
	public BasicCodeId getId() {
		return BasicCodeId.of(basicCd, code);
	}
}
