package com.axisj.axboot.core.dto;

import com.axisj.axboot.core.domain.program.menu.AuthorizedMenu;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
public class PageContentTO {

	public static final String MENU_MAP = "MENU_MAP";
	public static final String MENU_JSON = "MENU_JSON";
	public static final String MENU_JSON_HASH = "MENU_JSON_HASH";

	public static final String PAGE_CONTENT = "PAGE_CONTENT";
	public static final String ENV = "ENV";
	public static final String PAGE_ID = "PAGE_ID";
	public static final String PAGE_NAME = "PAGE_NAME";
	public static final String PAGE_REMARK = "PAGE_REMARK";
	public static final String CLASS_NAME = "CLASS_NAME";
	public static final String SEARCH_AUTH = "SEARCH_AUTH";
	public static final String SAVE_AUTH = "SAVE_AUTH";
	public static final String EXCEL_AUTH = "EXCEL_AUTH";
	public static final String FUNCTION_1_AUTH = "FUNCTION_1_AUTH";
	public static final String FUNCTION_2_AUTH = "FUNCTION_2_AUTH";
	public static final String FUNCTION_3_AUTH = "FUNCTION_3_AUTH";
	public static final String FUNCTION_4_AUTH = "FUNCTION_4_AUTH";
	public static final String FUNCTION_5_AUTH = "FUNCTION_5_AUTH";

	public static final String LOGIN_USER_NAME = "LOGIN_USER_NAME";
	public static final String LOGIN_USER_ID = "LOGIN_USER_ID";
	public static final String COMPANY_CODE = "COMPANY_CODE";

	private List<String> authSkippedMenu = Arrays.asList("main", "login");

	private String pageId;

	private boolean authorized = true;

	private AuthorizedMenu authorizedMenu;

	private String menuName = "";

	private String menuRemark = "";

	private String programName = "";

	private String searchAuth = null;

	private String saveAuth = null;

	private String excelAuth = null;

	private String className = null;

	private String function1Auth = null;

	private String function2Auth = null;

	private String function3Auth = null;

	private String function4Auth = null;

	private String function5Auth = null;

	public static PageContentTO of(String pageId, AuthorizedMenu authorizedMenu) {
		PageContentTO pageContent = new PageContentTO();
		pageContent.setPageId(pageId);

		if (authorizedMenu == null) {
			if (!pageContent.isAuthSkip(pageId)) {
				pageContent.setAuthorized(false);
			}
		} else {
			pageContent.setMenuName(authorizedMenu.getMenuName());
			pageContent.setMenuRemark(authorizedMenu.getRemark());
			pageContent.setProgramName(authorizedMenu.getProgramName());
			pageContent.setSearchAuth(authorizedMenu.getSearchAuth());
			pageContent.setSaveAuth(authorizedMenu.getSaveAuth());
			pageContent.setExcelAuth(authorizedMenu.getExcelAuth());
			pageContent.setFunction1Auth(authorizedMenu.getFunction1Auth());
			pageContent.setFunction2Auth(authorizedMenu.getFunction2Auth());
			pageContent.setFunction3Auth(authorizedMenu.getFunction3Auth());
			pageContent.setFunction4Auth(authorizedMenu.getFunction4Auth());
			pageContent.setFunction5Auth(authorizedMenu.getFunction5Auth());
			pageContent.setClassName(authorizedMenu.getRemark());
		}

		return pageContent;
	}

	public boolean isAuthSkip(String pageId) {
		return authSkippedMenu.contains(pageId.trim());
	}
}
