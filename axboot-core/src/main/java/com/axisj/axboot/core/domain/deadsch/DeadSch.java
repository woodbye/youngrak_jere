package com.axisj.axboot.core.domain.deadsch;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.ibatis.type.Alias;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.room.Room;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Alias("deadSch")
@ToString(includeFieldNames=true)

public class DeadSch implements Serializable {
	

    
    private String deadName;
	
    
    private String deadSex;
	
    
    private String deadDate;
    
   
    private String tombDate;
	
    private String applName;
    
   
    private String loc;    
    
 
    private String kind;
    
    
    private String kindName;



}
