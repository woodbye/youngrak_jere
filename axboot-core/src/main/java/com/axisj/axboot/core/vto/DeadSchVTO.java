package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;


import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "LayDeathVTO", description = "봉안 사망자 정보")
public class DeadSchVTO {
	
	@ApiModelProperty(value = "사망자명")
    private String deadName;	
	@ApiModelProperty(value = "사망자 성별")
    private String deadSex;	
	@ApiModelProperty(value = "사망일")
    private String deadDate;    
	@ApiModelProperty(value = "안치일")
    private String tombDate;	
	@ApiModelProperty(value = "신청자명")
    private String applName;    
	@ApiModelProperty(value = "안치위치")
    private String loc;
	@ApiModelProperty(value = "구분코드")
    private String kind;    
	@ApiModelProperty(value = "구분명")
    private String kindName;
	
	public static DeadSchVTO of(DeadSch deadSch) {
		
		DeadSchVTO deadSchVTO = DozerBeanMapperUtils.map(deadSch, DeadSchVTO.class);		
		return deadSchVTO;		
	}
	
	public static List<DeadSchVTO> of(List<DeadSch> list){
		
		return list.stream().map(deadSch -> of(deadSch)).collect(toList());
	}

}
