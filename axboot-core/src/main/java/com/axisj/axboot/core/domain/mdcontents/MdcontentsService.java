package com.axisj.axboot.core.domain.mdcontents;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.axisj.axboot.core.code.Constants;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.util.FileUtils;
import com.axisj.axboot.core.vto.MdcontentsVTO;


@Service
public class MdcontentsService extends BaseService<Mdcontents,String> {
	
	private MdcontentsRepository mdcontentsRepository;
	
	@Inject
	private MdcontentsMapper mdcontentsMapper;
	
	@Inject
	public MdcontentsService(MdcontentsRepository repository) {
		super(repository);
		this.mdcontentsRepository = repository;
	}

	public Page<Mdcontents> selectMdcontentsList(Map<String, Object> params) throws Exception {
		List<Mdcontents> contents = null;
		Integer pageNo = null;
		Integer pageSize = null;
		Long totalElements = null;
		Integer totalPages = null;
		Integer startElement = null;
		Integer endElement = null;
		
		
		Pageable pageable = (Pageable) params.get("pageable");
		pageNo = pageable.getPageNumber();
		pageSize = pageable.getPageSize();
		
		
		startElement = (pageNo) * pageSize;
		
		params.put("startElement", startElement);
		params.put("elementLimit", pageSize);
		
		totalElements = mdcontentsMapper.selectMdcontentsListCnt(params);
		
		Long totalPagesLongValue = (totalElements % pageSize == 0 ? totalElements / pageSize : totalElements/pageSize + 1);
		
		totalPages = totalPagesLongValue.intValue();
		
		contents = mdcontentsMapper.selectMdcontentsList(params);
		Page<Mdcontents> pages = new PageImpl(contents, new PageRequest(pageNo, pageSize), totalElements);

		return pages;
		
		
		
		
	}
	@Autowired
	BaseConverter baseConverter;
	public void saveMdcontent(List<MdcontentsVTO> mdcontentsVTOs)  throws Exception{		
		
		List<Mdcontents> list = new ArrayList<>();
		
		List<File> deleteFiles = new ArrayList<>();
		
		Mdcontents beforeMd = null;
		Mdcontents md = null;
		
		for(MdcontentsVTO mv : mdcontentsVTOs){
			
			md = baseConverter.convert(mv, Mdcontents.class);
			
			boolean existsUploadedFile = mv.getContMultipartFile() != null;
			
			beforeMd =mdcontentsRepository.findOne(md.getId()); 
			if(beforeMd != null){
				md.setContURL(beforeMd.getContURL());
				if(beforeMd.getContURL() != null && existsUploadedFile){
					
					//String path = SpringUtils.getRealPath()+beforeMd.getContURL();
					//String path = SpringUtils.getRealPath()+beforeMd.getContURL();
					String path = Constants.REAL_PATH_PHOTO+(beforeMd.getContURL().replaceAll(Constants.UPLOAD_PATH_PHOTO, ""));
					deleteFiles.add( new File(path));
				}
			}
	
			
			
			if(existsUploadedFile){
				//md.setContRealFileName(mv.getContMultipartFile().getOriginalFilename());
				md.setContURL(
						Constants.UPLOAD_PATH_PHOTO
						+ md.getContId()
						+ "_"
						+ System.currentTimeMillis()
						+ "."
						+ StringUtils.getFilenameExtension(mv.getContMultipartFile().getOriginalFilename()));
				md.setContRealFileName(md.getContURL().replaceAll(Constants.UPLOAD_PATH_PHOTO, ""));
				String realPath = Constants.REAL_PATH_PHOTO+(md.getContURL().replaceAll(Constants.UPLOAD_PATH_PHOTO, ""));
				Files.copy(mv.getContMultipartFile().getInputStream(), Paths.get(realPath));
			}
			
			list.add(md);
		}
		
		list.forEach(this::save);
		
		// 예외가 발생하면 db가 rollback 되므로 파일은 마지막에 지워서 롤백되었을 경우 대비
		for(File f : deleteFiles){
			if(f.exists()){
				f.delete();
			}
		}
	}
	
	public Page<Mdcontents> findByFirstnameContaining(String contTitle, Pageable pageable) throws SQLException {
		return mdcontentsRepository.findByContTitleContaining(contTitle, pageable);
	}
	
	
	@Transactional
	public void deleteMdcontentList(List<String> contId) throws Exception {
		List<Mdcontents> list =  mdcontentsRepository.findAll(contId);

		
		contId.forEach(this::delete);		
		//contId.forEach(action);
		for (int i = 0; i < contId.size(); i++) {
			FileUtils.fileDelete(list.get(i).getContURL());
		}
	}	
	
}
