package com.axisj.axboot.core.domain.user.auth.group.menu;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.program.menu.Menu;
import com.axisj.axboot.core.domain.user.auth.group.AuthGroup;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "GRP_AUTH_M")
@IdClass(AuthGroupMenuId.class)
public class AuthGroupMenu extends BaseJpaModel<AuthGroupMenuId> {

	@Id
	@Column(name = "GRP_AUTH_CD", length = 10)
	private String grpAuthCd;

	@Id
	@Column(name = "MNU_CD", length = 50)
	private String mnuCd;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GRP_AUTH_CD", referencedColumnName = "GRP_AUTH_CD", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
	private AuthGroup authGroup;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MNU_CD", referencedColumnName = "MNU_CD", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
	private Menu menu;

	@Column(name = "SCH_AH", length = 1)
	private String schAh;

	@Column(name = "SAV_AH", length = 1)
	private String savAh;

	@Column(name = "EXL_AH", length = 1)
	private String exlAh;

	@Column(name = "FN1_AH", length = 1)
	private String fn1Ah;

	@Column(name = "FN2_AH", length = 1)
	private String fn2Ah;

	@Column(name = "FN3_AH", length = 1)
	private String fn3Ah;

	@Column(name = "FN4_AH", length = 1)
	private String fn4Ah;

	@Column(name = "FN5_AH", length = 1)
	private String fn5Ah;

	@Override
	public AuthGroupMenuId getId() {
		return AuthGroupMenuId.of(grpAuthCd, mnuCd);
	}

	@Override
	public boolean equals(Object object) {
		if (object != null && object instanceof AuthGroupMenu) {
			AuthGroupMenu authGroupMenu = (AuthGroupMenu) object;
			if (authGroupMenu.getId().equals(this.getId())) {
				return true;
			}
		}
		return false;
	}
}
