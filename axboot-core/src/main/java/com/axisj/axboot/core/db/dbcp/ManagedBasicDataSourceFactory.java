package com.axisj.axboot.core.db.dbcp;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

public class ManagedBasicDataSourceFactory {
	private static Logger logger = LoggerFactory.getLogger(ManagedBasicDataSourceFactory.class);

	public enum ManagedDataSourceProperties {
		initialSize,
		minIdle,
		dataSourceId,
		slowQueryTime,
		accessToUnderlyingConnectionAllowed,
		connectionInitSqls;
	}

	private static final int DEFAULT_SLOW_QUERY_TIME = 7;

	public static ManagedBasicDataSource create(String dataSourceId, Map<String, String> dsPropertyMap) throws Exception {
		try {
			if (!dsPropertyMap.containsKey(ManagedDataSourceProperties.initialSize.name())) {
				dsPropertyMap.put(ManagedDataSourceProperties.initialSize.name(), dsPropertyMap.get(ManagedDataSourceProperties.minIdle.name()));
			}

			if (!dsPropertyMap.containsKey(ManagedDataSourceProperties.slowQueryTime.name())) {
				dsPropertyMap.put(ManagedDataSourceProperties.slowQueryTime.name(), String.valueOf(DEFAULT_SLOW_QUERY_TIME));
			}

			if (!dsPropertyMap.containsKey(ManagedDataSourceProperties.accessToUnderlyingConnectionAllowed.name())) {
				dsPropertyMap.put(ManagedDataSourceProperties.accessToUnderlyingConnectionAllowed.name(), "true");
			}

			dsPropertyMap.put(ManagedDataSourceProperties.dataSourceId.name(), dataSourceId);

			Collection<String> initSqls = null;

			if (dsPropertyMap.containsKey(ManagedDataSourceProperties.connectionInitSqls.name())) {
				String connectionInitSqls = dsPropertyMap.get(ManagedDataSourceProperties.connectionInitSqls.name());
				String[] initSqlTokens = StringUtils.split(connectionInitSqls, ",");
				if (ArrayUtils.isNotEmpty(initSqlTokens)) {
					initSqls = Lists.newArrayListWithExpectedSize(initSqlTokens.length);
					for (String initSqlToken : initSqlTokens) {
						initSqls.add(initSqlToken);
					}
				}
				dsPropertyMap.remove(ManagedDataSourceProperties.connectionInitSqls.name());
			}

			ManagedBasicDataSource managedBasicDataSource = new ManagedBasicDataSource();
			if (!CollectionUtils.isEmpty(initSqls)) {
				managedBasicDataSource.setConnectionInitSqls(initSqls);
			}
			BeanUtils.populate(managedBasicDataSource, dsPropertyMap);

			Connection conn = managedBasicDataSource.getConnection();
			conn.close();
			logger.info("success to create DataSource('{}')", dataSourceId);
			return managedBasicDataSource;
		} catch (Exception exception) {
			logger.error("fail to create DataSource('{}')", dataSourceId, exception);
			throw exception;
		}
	}
}
