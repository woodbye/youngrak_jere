package com.axisj.axboot.core.domain.envset;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

@Repository
public interface EnvsetRepository extends JpaRepository<Envset, EnvsetId> {

	public List<Envset> findByWorkDateBetween(String start, String end) throws SQLException;

	@Query(nativeQuery=true,value="SELECT IFNULL(MAX(SEQ_NO), 0)+1 FROM ENVSET WHERE WORK_DATE = ?1")
	Integer findNextSeqNo(String workDate) throws Exception;	
	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	@Cacheable(value = "envset")
	public Envset findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(String stDate)  throws SQLException;
	//@CacheEvict(value = "envset")
	Envset save(Envset envset);
}
