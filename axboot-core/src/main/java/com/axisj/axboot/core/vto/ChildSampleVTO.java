package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.sample.child.ChildSample;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ApiModel(value = "Child Sample VTO", description = "")
public class ChildSampleVTO {

	private String key;

	private String parentKey;

	private String value;

	private String etc1;

	private String etc2;

	private String etc3;

	public static ChildSampleVTO of(ChildSample childSample) {
		ChildSampleVTO vto = DozerBeanMapperUtils.map(childSample, ChildSampleVTO.class);
		return vto;
	}

	public static List<ChildSampleVTO> of(List<ChildSample> childSampleList) {
		List<ChildSampleVTO> vtoList = new ArrayList<>();

		for (ChildSample object : childSampleList) {
			vtoList.add(of(object));
		}

		return vtoList;
	}
}
