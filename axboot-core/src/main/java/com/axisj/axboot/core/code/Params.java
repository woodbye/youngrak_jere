package com.axisj.axboot.core.code;

public class Params {

	public static final String COMP_CD = "compCd";
	public static final String USER_CD = "userCd";
	public static final String USER_TYPE = "userType";
	public static final String ALL = "ALL";
	public static final String Y = "Y";
	public static final String N = "N";
}
