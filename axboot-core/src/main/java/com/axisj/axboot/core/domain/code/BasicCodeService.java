package com.axisj.axboot.core.domain.code;

import com.axisj.axboot.core.domain.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.List;

@Service
public class BasicCodeService extends BaseService<BasicCode, BasicCodeId> {

	private BasicCodeRepository basicCodeRepository;

	@Inject
	public BasicCodeService(BasicCodeRepository basicCodeRepository) {
		super(basicCodeRepository);
		this.basicCodeRepository = basicCodeRepository;
	}

	public List<BasicCode> findByBasicCd(String basicCd) {
		return basicCodeRepository.findByBasicCd(basicCd);
	}

	public Page<BasicCode> searchBasicCode(String searchParam, PageRequest pageable) {
		Page<BasicCode> basicCodes;
		if (!StringUtils.isEmpty(searchParam)) {
			basicCodes = basicCodeRepository.findByBasicCdContainingOrBasicNmContaining(searchParam, searchParam, pageable);
		} else {
			basicCodes = basicCodeRepository.findAll(pageable);
		}
		return basicCodes;
	}


	public List<BasicCode> searchBasicCode(String searchParam) {
		List<BasicCode> basicCodes;
		if (!StringUtils.isEmpty(searchParam)) {
			basicCodes = basicCodeRepository.findByBasicCdContainingOrBasicNmContaining(searchParam, searchParam);
		} else {
			basicCodes = basicCodeRepository.findAllByOrderByBasicCdAscCodeAsc();
		}
		return basicCodes;
	}


	public List<BasicCode> findByPosUserYn(String posUseYn) {
		return basicCodeRepository.findByPosUseYn("Y");
	}

	public List<BasicCode> findByBasicCdAndUseYn(String basicCd) {
		// TODO Auto-generated method stub
		return basicCodeRepository.findByBasicCdAndUseYn(basicCd,"Y");
	}
}
