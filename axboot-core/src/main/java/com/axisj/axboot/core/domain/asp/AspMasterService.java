package com.axisj.axboot.core.domain.asp;

import com.axisj.axboot.core.domain.BaseService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class AspMasterService extends BaseService<AspMaster, String> {

	private AspMasterRepository aspMasterRepository;

	@Inject
	public AspMasterService(AspMasterRepository aspMasterRepository) {
		super(aspMasterRepository);
		this.aspMasterRepository = aspMasterRepository;
	}
}
