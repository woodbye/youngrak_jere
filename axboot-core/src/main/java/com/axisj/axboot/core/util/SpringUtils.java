package com.axisj.axboot.core.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SpringUtils {

	private SpringUtils() {}
	
	/**
	 * 
	 * @Method Name  : getServletContext
	 * @작성일   : 2015. 12. 26. 오후 3:37:26
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : ServletContext 반환
	 * @return
	 */
	public static ServletContext getServletContext(){
		return WebUtils.getServletContext(getRequest());
	}
	
	/**
	 * 
	 * @Method Name  : getWebApplicationContext
	 * @작성일   : 2015. 12. 26. 오후 3:37:09
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : WebApplicationContext 반환
	 * @return
	 */
	public static WebApplicationContext getWebApplicationContext(){
		return WebApplicationContextUtils.getWebApplicationContext(getServletContext());
	}
	
	/**
	 * 
	 * @Method Name  : getBean
	 * @작성일   : 2015. 12. 26. 오후 3:36:26
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : 웹 어플리케이션에 등록된 Bean 반환
	 * @param clz
	 * @return
	 */
	public static <B> B getBean(Class<B> clz){
		return getWebApplicationContext().getBean(clz);
	}
	/**
	 * 
	 * @Method Name  : getBean
	 * @작성일   : 2015. 12. 26. 오후 3:36:50
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : 웹 어플리케이션에 등록된 Bean 반환
	 * @param beanName
	 * @return
	 */
	public static Object getBean(String beanName){
		return getWebApplicationContext().getBean(beanName);
	}
	/**
	 * 
	 * @Method Name  : getBean
	 * @작성일   : 2015. 12. 26. 오후 3:36:54
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : 웹 어플리케이션에 등록된 Bean 반환
	 * @param beanName
	 * @param clz
	 * @return
	 */
	public static <B> B getBean(String beanName, Class<B> clz){
		return getWebApplicationContext().getBean(beanName, clz);
	}
	
	/**
	 * 
	 * @Method Name  : getCurrentDevice
	 * @작성일   : 2015. 12. 26. 오후 3:35:40
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : 웹서버로 요청한 현재 Device 정보를 가진 객체를 반환
	 * @return
	 */
	public static Device getCurrentDevice(){
		return DeviceUtils.getCurrentDevice(getRequest());
//		return DeviceUtils.getCurrentDevice(RequestContextHolder.currentRequestAttributes());
	}
	
	/**
	 * @Method Name : getRequest
	 * @작성일 : 2015. 11. 8. 오전 10:54:44
	 * @작성자 : wofmaker
	 * @변경이력 :
	 * @Method 설명 : Spring web mvc 사용시 request객체를 반환
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	}
	
	/**
	 * 
	 * @Method Name : getClientIp
	 * @작성일 : 2015. 11. 8. 오전 11:02:52
	 * @작성자 : wofmaker
	 * @변경이력 :
	 * @Method 설명 : Spring web mvc 에서 클라이언트의 ip 반환
	 * @return
	 */
	public static String getClientIp() {
		return WebUtils.getClientIp(getRequest());
	}
	
	/**
	 * 
	 * @Method Name : getSession
	 * @작성일 : 2015. 11. 8. 오전 10:57:01
	 * @작성자 : wofmaker
	 * @변경이력 :
	 * @Method 설명 : Spring web mvc 사용시 HttpSession 객체 반환
	 * @return
	 */
	public static HttpSession getSession() {
		return WebUtils.getSession(getRequest());
	}

	/**
	 * 
	 * @Method Name  : getRealPath
	 * @작성일   : 2015. 12. 26. 오후 3:35:05
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : webapp의 실제 경로 반환
	 * @return
	 */
	public static String getRealPath() {
		return WebUtils.getRealPath(getRequest());
	}
	
	/**
	   * 문자열이 널이면 대체할 문자열을 리턴
	   * 
	   * @author 자바지기
	   * @param  String , String 
	   * @return String
	   */
	  public static String nvl(String str, String NVLString) {
	    if( (str == null) || (str.trim().equals(""))
	      || (str.trim().equals("null")) ) {
	      return NVLString;
	    } else {
	      return str;
	    }
	  }
	  
	  /**
	   * 문자열이 널이면 "" 문자열을 리턴
	   * 
	   * @author soon
	   * @param  String 
	   * @return String
	   */
	  public static String nvl(String str) {
	    if( (str == null) || (str.trim().equals(""))
	      || (str.trim().equals("null")) ) {
	      return "";
	    } else {
	      return str;
	    }
	  }

}
