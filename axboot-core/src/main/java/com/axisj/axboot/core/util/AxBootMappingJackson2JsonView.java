package com.axisj.axboot.core.util;

import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AxBootMappingJackson2JsonView extends MappingJackson2JsonView {

	private static final String TEXT_PLAIN = "text/plain";

	public AxBootMappingJackson2JsonView() {
		super();
	}

	@Override
	protected void setResponseContentType(HttpServletRequest request, HttpServletResponse response) {
		if (WebUtils.isExplorerBrowser(request)) {
			response.setContentType(TEXT_PLAIN);
		} else {
			super.setResponseContentType(request, response);
		}
	}
}
