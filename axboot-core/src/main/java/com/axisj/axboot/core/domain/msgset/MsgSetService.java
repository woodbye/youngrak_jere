package com.axisj.axboot.core.domain.msgset;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.axisj.axboot.core.domain.BaseService;

@Service
public class MsgSetService extends BaseService<MsgSet, String> {

	private MsgSetRepository msgSetRepository;
	
	@Inject
	public MsgSetService(MsgSetRepository repository) {
		super(repository);
		this.msgSetRepository = repository;
	}

	public List<MsgSet> selectMsgUseList(String msgUse) throws Exception {
	
		return msgSetRepository.findByMsgUseStartingWith(msgUse);
	}
	
	public void deleteMsgset(List<MsgSet> msgSets) throws Exception{
		
		msgSets.forEach(this::delete);
				
	}

	public MsgSet findByMsgId(String msgId) throws Exception {
		return msgSetRepository.findByMsgId(msgId);
		
	}

}
