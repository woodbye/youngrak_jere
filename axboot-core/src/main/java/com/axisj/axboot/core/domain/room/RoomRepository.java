package com.axisj.axboot.core.domain.room;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, String> {

	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	List<Room> findByRoomNm(String string) throws Exception;

	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	List<Room> findByRoomNmStartingWith(String roomNm) throws Exception;

	@Query(value="SELECT * "			
			+ "FROM ROOM R "
			+ "WHERE NOT EXISTS (SELECT ROOM_CD FROM INSPECTROOM WHERE WORK_DATE = ?1 AND SEQ_NO = ?2  AND ROOM_CD = R.ROOM_CD)",nativeQuery=true)
	List<Room> findByRoomList(String workDate, int seqNo) throws Exception;

/*	@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	Room findOne(String id);

	@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	List<Room> findAll();
	
	@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	Room findByRoomCd(String roomCd);*/
}
