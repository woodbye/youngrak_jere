package com.axisj.axboot.core.domain.mdcontents;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import com.axisj.axboot.core.mybatis.MyBatisMapper;

public interface MdcontentsMapper extends MyBatisMapper{

	List<Mdcontents> selectMdcontentsList(Map<String, Object> params) throws Exception;

	Long selectMdcontentsListCnt(Map<String, Object> params) throws Exception;
}
