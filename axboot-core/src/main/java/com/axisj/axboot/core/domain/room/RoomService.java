package com.axisj.axboot.core.domain.room;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.core.domain.BaseService;


@Service
public class RoomService extends BaseService<Room, String> {
	
	private RoomRepository roomRepository;
	
	@Inject
	public RoomService(RoomRepository repository) {
		super(repository);
		this.roomRepository = repository;
	}

	public List<Room> selectRoomList(String roomNm) throws Exception {
		
		return roomRepository.findByRoomNmStartingWith(roomNm);
	}
	

	@Transactional
	public void saveRoomList(List<Room> rooms) throws Exception {
		
		rooms.forEach(this::save);		
	}
	
	@Transactional
	public void deleteRoom(List<String> roomCd) throws Exception{
		roomCd.forEach(this::delete);		
	}
}
