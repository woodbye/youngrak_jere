package com.axisj.axboot.core.domain.program.menu;

import com.axisj.axboot.core.mybatis.MyBatisMapper;

import java.util.List;

public interface MenuMapper extends MyBatisMapper{

    List<AuthorizedMenu> getAuthorizedMenuByUserCode(String userCode);

    List<Menu> activeMenus();

}
