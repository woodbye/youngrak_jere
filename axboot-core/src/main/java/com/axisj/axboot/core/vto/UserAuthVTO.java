package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.user.auth.UserAuth;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "UserAuthVTO", description = "사용자 권한 정보")
public class UserAuthVTO {
	@ApiModelProperty(value = "사용자 코드")
	private String userCd;

	@ApiModelProperty(value = "사용자명")
	private String userNm;

	@ApiModelProperty(value = "권한 그룹 코드")
	private String grpAuthCd;

	@ApiModelProperty(value = "권한 그룹명")
	private String grpAuthNm;

	@ApiModelProperty(value = "비고")
	private String remark;

	@ApiModelProperty(value = "사용여부")
	private String useYn;

	public static UserAuthVTO of(UserAuth userAuth) {
		UserAuthVTO userAuthVTO = DozerBeanMapperUtils.map(userAuth, UserAuthVTO.class);


		if (CommonUtils.canConvertMappingVTO(userAuth.getAuthGroup())) {
			userAuthVTO.setGrpAuthNm(userAuth.getAuthGroup().getGrpAuthNm());
		}

		if (CommonUtils.canConvertMappingVTO(userAuth.getUser())) {
			userAuthVTO.setUserNm(userAuth.getUser().getUserNm());
		}

		return userAuthVTO;
	}

	public static List<UserAuthVTO> of(List<UserAuth> userAuthList) {
		return userAuthList.stream().map(userAuth -> of(userAuth)).collect(toList());
	}
}
