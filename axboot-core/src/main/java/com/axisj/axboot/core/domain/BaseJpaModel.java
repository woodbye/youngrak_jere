package com.axisj.axboot.core.domain;

import com.axisj.axboot.core.domain.user.User;
import com.axisj.axboot.core.util.CommonUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@DynamicInsert
@DynamicUpdate
public abstract class BaseJpaModel<PK extends Serializable> implements Persistable<PK>, Serializable {

	@Override
	@JsonIgnore
	public boolean isNew() {
		return null == getId();
	}

	@Column(name = "INS_DT", updatable = false)
	protected Date insDt;

	@Column(name = "UPT_DT")
	protected Date uptDt;

	@Column(name = "INS_USER", updatable = false)
	protected String insUser;

	@Column(name = "UPT_USER")
	protected String uptUser;

	@Transient
	protected User createdUser;

	@Transient
	protected User modifiedUser;

	@PrePersist
	protected void onPersist() {
		this.insUser = uptUser = CommonUtils.getCurrentLoginUserCd();
		this.insDt = this.uptDt = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		this.uptUser = CommonUtils.getCurrentLoginUserCd();
		this.uptDt = new Date();
	}

	@PostLoad
	protected void onPostLoad() {
	}


}
