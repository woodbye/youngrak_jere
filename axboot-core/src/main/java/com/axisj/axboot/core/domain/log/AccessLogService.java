package com.axisj.axboot.core.domain.log;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.util.SpringUtils;
import com.axisj.axboot.core.util.WebUtils;

@Service
public class AccessLogService extends BaseService<AccessLog, String> {


	private AccessLogRepository accessLogRepository;
	
	@Inject
	public AccessLogService(AccessLogRepository repository) {
		super(repository);
		this.accessLogRepository = repository;
	}
	
	public void saveAccessLog(HttpServletRequest request) {
		
	 String url = SpringUtils.nvl(request.getRequestURI(),"")+SpringUtils.nvl(request.getQueryString(),"");	 
	 String ip = WebUtils.getClientIp(request);
	 
	 AccessLog accessLog = new AccessLog();
	 accessLog.setIpaddress(ip);
	 accessLog.setUrl(url);
	 accessLogRepository.save(accessLog);
	}
	
}
