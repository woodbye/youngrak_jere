package com.axisj.axboot.core.domain.booking;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Embeddable
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@ToString(callSuper = true, includeFieldNames = true)
public class BookingId implements Serializable {

	@NonNull
	private String bookDate;

	@NonNull
	private Integer bookSeq;
	
	
}
