package com.axisj.axboot.core.domain.program.menu;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Alias("authorizedMenu")
@Data
public class AuthorizedMenu implements Serializable {

    private String searchAuth;

    private String saveAuth;

    private String excelAuth;

    private String function1Auth;

    private String function2Auth;

    private String function3Auth;

    private String function4Auth;

    private String function5Auth;

    private String menuCode;

    private String menuName;

    private String className;

    private Integer menuLevel;

    private Integer menuIndex;

    private String parentMenuCode;

    private String icon;

    private String programPath;

    private String programName;

    private String target;

    private String remark;

}
