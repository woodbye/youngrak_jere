package com.axisj.axboot.core.domain.mdcontents;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MdcontentsRepository extends JpaRepository<Mdcontents, String> {


	Page<Mdcontents> findByContTitleContaining(String contTitle, Pageable pageable) throws SQLException;
}
