package com.axisj.axboot.core.domain.deadsch;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DeadSchService {

	@Inject
	private DeadSchMapper deadSchMapper;


	
	public Page<DeadSch> selectDeadSchList(Map<String, Object> params) throws Exception {
		
		List<DeadSch> contents = null;
		Integer pageNo = null;
		Integer pageSize = null;
		Long totalElements = null;
		Integer totalPages = null;
		Integer startElement = null;
		Integer endElement = null;
		
		
		Pageable pageable = (Pageable) params.get("pageable");
		pageNo = pageable.getPageNumber();
		pageSize = pageable.getPageSize();
		
		
		startElement = (pageNo) * pageSize;
		
		params.put("startElement", startElement);
		params.put("elementLimit", pageSize);
		
		totalElements = deadSchMapper.selectDeadSchListCount(params);
		
		Long totalPagesLongValue = (totalElements % pageSize == 0 ? totalElements / pageSize : totalElements/pageSize + 1);
		
		totalPages = totalPagesLongValue.intValue();
		
		contents = deadSchMapper.selectDeadSchList(params);
		Page<DeadSch> pages = new PageImpl(contents, new PageRequest(pageNo, pageSize), totalElements);
//		pages.setContents(contents);
//		pages.setCurrentPage(pageNo);
//		pages.setPageSize(pageSize);
//		pages.setTotalPages(totalPages);
//		pages.setTotalElements(totalElements);
		return pages;
		
		
		
		//return deadSchMapper.selectDeadSchList(parms);
	}

}
