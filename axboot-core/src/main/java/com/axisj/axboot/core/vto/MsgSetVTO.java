package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.persistence.Column;

import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.msgset.MsgSet;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "MsgSetVTO", description = "알림문구 관리")
public class MsgSetVTO {
	
	@ApiModelProperty(value = "메세지 아이디(PK) ")
	private String msgId;

	@ApiModelProperty(value = "메세지 용도 ")
	private String msgUse;

	@ApiModelProperty(value = "내용 ")
	private String msgContent;

	@ApiModelProperty(value = "타입 D:Display, T:TTS, B:둘다.")
	private String msgType;

	@ApiModelProperty(value = "방송간격")
	private int msgTerm;

	@ApiModelProperty(value = "방송반복 횟수")
	private int repeatCnt;

	@ApiModelProperty(value = "초기화면 복귀시간")
	private int closeTime;
	
	@ApiModelProperty(value = "비고")
    private String remark;
    
	@ApiModelProperty(value = "등록 시간")
    private String regTime;
    
	@ApiModelProperty(value = "등록 유저")
    private String regId; 
    
	@ApiModelProperty(value = "수정 시간")
    private String udtTime; 
    
	@ApiModelProperty(value = "수정 유저")
    private String udtId;  
	
	public static MsgSetVTO of(MsgSet msgSet) {
		MsgSetVTO msgSetVTO = DozerBeanMapperUtils.map(msgSet, MsgSetVTO.class);
		return msgSetVTO;
	}
	
	public static List<MsgSetVTO> of(List<MsgSet> list){
		return list.stream().map(msgSet -> of(msgSet)).collect(toList());
	}
}
