package com.axisj.axboot.core.domain.inspect;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomRepository;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomService;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.ListUtils;

@Service
public class InspectService extends BaseService<Inspect, InspectId> {

	private InspectRepository inspectRepository;
	@Inject
	private InspectroomRepository inspectroomRepository;
	
	@Inject
	InspectroomService inspectroomService;
	
	@Inject
	public InspectService(InspectRepository repository) {
		super(repository);
		this.inspectRepository = repository;
	}

	public List<Inspect> selectInspectList(String strDate, String endDate) throws Exception {
		
		return inspectRepository.findByWorkDateBetween(strDate,endDate);
	}

	
	public void saveInspectList(List<Inspect> inspects) throws Exception {
		
		inspects.forEach(this::save);
	}
	
	
	@Transactional
	public void deleteInspectList(List<Inspect> inspects) throws Exception {
		inspects.forEach(this::delete);
		for(int i=0; i<inspects.size(); i++){
			List<Inspectroom> list = inspectroomRepository.findByWorkDateAndSeqNo(inspects.get(i).getWorkDate(), inspects.get(i).getSeqNo());
			list.forEach(inspectroomService::delete);
		}
		
		
	}
	
	public Inspect selectInspect(String stDate) throws Exception {
		
		return inspectRepository.findFirstByStdateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
	}

	@Autowired
	RoomService roomService;
	
	public List<Room> selectAvailableRooms() throws Exception {
		
		List<Inspect> inspects = inspectRepository.getByStringDate(DateUtils.formatToDateString("yyyyMMddHHmm"));
		
		List<List<Inspectroom>> inspectrooms = ListUtils.pluck(inspects, "inspectroom");
		
		List<Inspectroom> inspectroomAll = new ArrayList<>();
		
		for(List list : inspectrooms){
			inspectroomAll.addAll(list);
		}
		
		List<Room> notAbailableRooms = ListUtils.pluck(inspectroomAll, "room");
		
		List<Room> rooms = roomService.findAll();
		
		rooms.removeAll(notAbailableRooms);
		
		return rooms;
	}

	

}
