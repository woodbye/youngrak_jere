package com.axisj.axboot.core.domain.code;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;


@Embeddable
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName="of")
@ToString(includeFieldNames=true)
public class CodeBaseDetailId implements Serializable{

	@NonNull
	private String  mastCd;
	@NonNull
	private String detailCd;
	
}
