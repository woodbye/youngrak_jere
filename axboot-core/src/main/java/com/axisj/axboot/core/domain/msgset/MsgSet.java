package com.axisj.axboot.core.domain.msgset;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "MSGSET")
@ToString(includeFieldNames = true)
public class MsgSet extends BasicJpaModel<String> {

	@Override
	public String getId() {
		return msgId;
	}

	@Id
	@Column(name = "MSG_ID", length = 10)
	private String msgId;

	@Column(name = "MSG_USE", length = 200)
	private String msgUse;

	@Column(name = "MSG_CONTENT", length = 200)
	private String msgContent;

	@Column(name = "MSG_TYPE", length = 1)
	private String msgType;

	@Column(name = "MSG_TERM")
	private int msgTerm;

	@Column(name = "REPEAT_CNT")
	private int repeatCnt;

	@Column(name = "CLOSE_TIME")
	private int closeTime;

	@Column(name = "REMARK", length = 200)
	private String remark;

	@Column(name = "REGTIME", length = 14)
	private String regTime;

	@Column(name = "REGID", length = 20)
	private String regId;

	@Column(name = "UDTTIME", length = 14)
	private String udtTime;

	@Column(name = "UDTID", length = 20)
	private String udtId;
	
	@PrePersist
	private void onPersist() {		
		this.regId = this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.regTime = this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	
	@PreUpdate
	private void onUpdate() {
		this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}

}
