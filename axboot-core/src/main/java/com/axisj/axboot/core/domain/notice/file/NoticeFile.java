package com.axisj.axboot.core.domain.notice.file;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.notice.Notice;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.bytecode.internal.javassist.FieldHandled;
import org.hibernate.bytecode.internal.javassist.FieldHandler;

import javax.persistence.*;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "NOTICE_FILE_L")
public class NoticeFile extends BaseJpaModel<Integer> implements FieldHandled {

	private FieldHandler fieldHandler;

	@Id
	@Column(name = "ID", precision = 11)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "NOTICE_ID", precision = 10)
	private Integer noticeId;

	@Column(name = "FILE_NAME", length = 300)
	private String fileName;

	@Lob
	@Column(name = "FILE_BLOB", length = 4000)
	@Basic(fetch = FetchType.LAZY)
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private byte[] fileBlob;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NOTICE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	public Notice notice;

	@Override
	public Integer getId() {
		return id;
	}

	public byte[] getFileBlob() {
		if (fileBlob != null) {
			return fileBlob;
		}
		return (byte[]) fieldHandler.readObject(this, "fileBlob", fileBlob);
	}

	public void setFileBlob(byte[] fileBlob) {
		this.fileBlob = fileBlob;
	}

	@Override
	public void setFieldHandler(FieldHandler handler) {
		this.fieldHandler = handler;
	}

	@Override
	public FieldHandler getFieldHandler() {
		return fieldHandler;
	}
}
