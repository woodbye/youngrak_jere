package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.sample.parent.ParentSample;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ApiModel(value = "Parent Sample VTO", description = "")
public class ParentSampleVTO {

	private String key;

	private String value;

	private String etc1;

	private String etc2;

	private String etc3;

	private String etc4;

	public static ParentSampleVTO of(ParentSample parentSample) {
		ParentSampleVTO parentVTO = DozerBeanMapperUtils.map(parentSample, ParentSampleVTO.class);
		return parentVTO;
	}

	public static List<ParentSampleVTO> of(List<ParentSample> parentList) {
		List<ParentSampleVTO> vtoList = new ArrayList<>();

		for (ParentSample object : parentList) {
			vtoList.add(of(object));
		}

		return vtoList;
	}
}
