package com.axisj.axboot.core;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@PropertySource("classpath:core-${spring.profiles.active:production}.properties")
//@PropertySource("classpath:core-${spring.profiles.active:local}.properties")
public class CoreConfiguration {
}
