package com.axisj.axboot.core.db.dbcp;

import com.axisj.axboot.core.db.DatabaseType;
import com.axisj.axboot.core.db.aop.CreateStatementInterceptor;
import com.axisj.axboot.core.db.aop.StatementExecutionInfo;
import com.axisj.axboot.core.db.monitor.sql.SqlTaskPool;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.aop.framework.ProxyFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Statement;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

@Setter
@Getter
public class ManagedBasicDataSource extends BasicDataSource {
	private SqlTaskPool sqlTaskPool = new SqlTaskPool();
	private ConcurrentHashMap<Statement, StatementExecutionInfo> statementInfoMap = new ConcurrentHashMap<>();

	/**
	 * 데이터베이스 벤더.
	 */
	private DatabaseType databaseType = DatabaseType.mysql;

	private String dataSourceId;

	private Integer queryTimeout;

	private Long slowQueryTime;

	private boolean useSqlLogFormat = false;

	private boolean useStatementCache = false;

	private int statementCacheSize = 250;

	private boolean sqlLogging = false;

	public void setSqlLogging(boolean sqlLogging) {
		this.sqlLogging = sqlLogging;
	}

	public boolean isSqlLogging() {
		return sqlLogging;
	}

	@Override
	public Connection getConnection() throws SQLException {
		Connection connection = super.getConnection();
		return createProxy(connection);
	}

	@Override
	public Connection getConnection(String user, String pass) throws SQLException {
		Connection connection = super.getConnection(user, pass);
		return createProxy(connection);
	}

	@Override
	public synchronized void close() throws SQLException {
		super.close();
	}

	private Connection createProxy(Connection originalConnection) {
		ProxyFactory proxyFactory = new ProxyFactory();
		proxyFactory.setTarget(originalConnection);
		proxyFactory.addAdvice(new CreateStatementInterceptor(getDataSourceId(), getDatabaseType(), getQueryTimeout(), getSlowQueryTime(), isUseSqlLogFormat(),
				statementInfoMap, sqlTaskPool, sqlLogging));
		proxyFactory.setInterfaces(new Class[]{Connection.class});
		return (Connection) proxyFactory.getProxy();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return null;
	}
}
