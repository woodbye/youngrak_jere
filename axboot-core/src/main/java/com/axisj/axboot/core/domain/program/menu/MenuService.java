package com.axisj.axboot.core.domain.program.menu;

import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.domain.program.menu.display.MainMenu;
import com.axisj.axboot.core.util.HashUtils;
import com.axisj.axboot.core.util.JsonUtils;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class MenuService extends BaseService<Menu, String> {

	private MenuRepository menuRepository;

	@Inject
	private MenuMapper menuMapper;

	@Inject
	public MenuService(MenuRepository menuRepository) {
		super(menuRepository);
		this.menuRepository = menuRepository;
	}

	public List<Menu> findAllByOrderByMnuLvAscMnuIxAsc() {
		return menuRepository.findAllByOrderByMnuLvAscMnuIxAsc();
	}

	public List<Menu> findByMnuUpCd(String mnuUpCd) {
		return menuRepository.findByMnuUpCd(mnuUpCd);
	}

	public List<AuthorizedMenu> getAuthorizedMenuList(String userCode) {
		List<AuthorizedMenu> authorizedMenus = menuMapper.getAuthorizedMenuByUserCode(userCode);
		return authorizedMenus;
	}

	public Map<String, AuthorizedMenu> getAuthorizedMenuMap(String userName) {
		List<AuthorizedMenu> authorizedMenus = getAuthorizedMenuList(userName);
		return toAuthorizedMenuMap(authorizedMenus);
	}

	public Map<String, AuthorizedMenu> toAuthorizedMenuMap(List<AuthorizedMenu> authorizedMenus) {
		Map map = Maps.newHashMap();

		for (AuthorizedMenu authorizedMenu : authorizedMenus) {
			map.put(authorizedMenu.getMenuCode(), authorizedMenu);
		}

		return map;
	}

	public List<MainMenu> toMainMenuList(List<AuthorizedMenu> authorizedMenus) {
		Map<String, MainMenu> mainMenuMap = Maps.newLinkedHashMap();

		Iterator<AuthorizedMenu> authorizedMenuIterator = authorizedMenus.iterator();

		while (authorizedMenuIterator.hasNext()) {
			AuthorizedMenu authorizedMenu = authorizedMenuIterator.next();

			switch (authorizedMenu.getMenuLevel()) {
				case 1:
					mainMenuMap.put(authorizedMenu.getMenuCode(), new MainMenu(authorizedMenu));
					break;

				case 2:
					MainMenu rootMenu = mainMenuMap.get(authorizedMenu.getParentMenuCode());
					if (rootMenu != null) {
						rootMenu.addChild(authorizedMenu);
					}
					break;

				case 3:
					MainMenu secondMenu = null;

					for (MainMenu _rootMenu : mainMenuMap.values()) {
						for (MainMenu _secondMenu : _rootMenu.getChild()) {
							if (_secondMenu.getId().equals(authorizedMenu.getParentMenuCode())) {
								secondMenu = _secondMenu;
								break;
							}
						}
					}

					if (secondMenu != null) {
						secondMenu.addChild(authorizedMenu);
					}
					break;
			}
		}

		return new ArrayList<>(mainMenuMap.values());
	}

	public UserMenuInfo getUserMenuInfo(String userName) {
		List<AuthorizedMenu> authorizedMenus = getAuthorizedMenuList(userName);

		Map<String, AuthorizedMenu> authorizedMenuMap = toAuthorizedMenuMap(authorizedMenus);

		List<MainMenu> mainMenuList = toMainMenuList(authorizedMenus);

		String mainMenuJson = JsonUtils.toJson(mainMenuList);

		return UserMenuInfo.of(authorizedMenuMap, mainMenuList, mainMenuJson, HashUtils.MD5(mainMenuJson));
	}

	public List<Menu> findActiveMenus() {
		return menuMapper.activeMenus();
	}

	public Menu getParentMenu(String mnuCd) {
		Menu menu = findOne(mnuCd);

		if (menu != null && !StringUtils.isEmpty(menu.getMnuUpCd())) {
			Menu parentMenu = findOne(menu.getMnuUpCd());
			return parentMenu;
		}
		return null;
	}

	@Transactional
	public void saveMenus(List<Menu> menus) {
		menus.stream().filter(menu -> StringUtils.isEmpty(menu.getProgCd())).forEach(menu -> {
			menu.setProgCd(null);
		});

		save(menus);
	}
}
