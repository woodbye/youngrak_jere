package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.company.Company;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "CompanyVTO", description = "회사 정보")
public class CompanyVTO {

	@ApiModelProperty(value = "회사코드(PK)")
	private String compCd;

	@ApiModelProperty(value = "공항공사 회사코드")
	private String iiacCompCd;

	@ApiModelProperty(value = "회사명")
	private String compNm;

	@ApiModelProperty(value = "대표자명")
	private String compCeo;

	@ApiModelProperty(value = "사업자등록번호")
	private String compRegno;

	@ApiModelProperty(value = "법인등록번호")
	private String compCono;

	@ApiModelProperty(value = "전화번호")
	private String telNo;

	@ApiModelProperty(value = "우편번호")
	private String zipNo;

	@ApiModelProperty(value = "주소")
	private String addr1;

	@ApiModelProperty(value = "기타주소")
	private String addr2;

	@ApiModelProperty(value = "사용여부")
	private String useYn;

	public static CompanyVTO of(Company company) {
		return DozerBeanMapperUtils.map(company, CompanyVTO.class);
	}

	public static List<CompanyVTO> of(List<Company> companyList) {
		return companyList.stream().map(company -> of(company)).collect(toList());
	}
}
