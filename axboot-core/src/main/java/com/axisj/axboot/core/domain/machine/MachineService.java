package com.axisj.axboot.core.domain.machine;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.domain.envset.Envset;

@Service
public class MachineService extends BaseService<Machine, String> {


	private MachineRepository machineRepository;
	
	@Inject
	public MachineService(MachineRepository repository) {
		super(repository);
		this.machineRepository = repository;
	}
	/**
	 * 
	 * 메소드명칭 : selectMachineList
	 * 메소드설명 :	설치장비 목록 출력	
	 * ----------------------------------------
	 * 이력사항 2016. 1. 5. 김동호 최초작성
	 *
	 * @param machineName
	 * @return
	 * @throws Exception
	 */
	public List<Machine>  selectMachineList(String machineName) throws Exception{
		
		return machineRepository.findByMachineNmStartingWith(machineName);
	}
	/**
	 * 
	 * 메소드명칭 : saveMachineList
	 * 메소드설명 :	설치장비 등록,수정	
	 * ----------------------------------------
	 * 이력사항 2016. 1. 5. 김동호 최초작성
	 *
	 * @param machines
	 * @throws Exception
	 */
	@Transactional
	public void saveMachineList(List<Machine> machines) throws Exception {
		
		machines.forEach(this::save);		
	}
	/**
	 * 
	 * 메소드명칭 : deleteBykey
	 * 메소드설명 :	설치장비 삭제	
	 * ----------------------------------------
	 * 이력사항 2016. 1. 5. 김동호 최초작성
	 *
	 * @param machineCd
	 * @throws Exception
	 */
	@Transactional
	public void deleteRoom(List<String> machineCd) throws Exception{
		machineCd.forEach(this::delete);	
		
	}
	
	/**
	 * 
	 * 메소드명칭 : findUrlByIpaddress
	 * 메소드설명 : 설치 장비의 기본 url
	 * ----------------------------------------
	 * 이력사항 2016. 1. 22. 이승호 최초작성
	 *
	 * @param clientIp : 클라이언트 아이피
	 * @throws Exception
	 */
	public String findUrlByIpaddress(String clientIp) throws Exception {
		
		List<Machine> list = machineRepository.findByIpaddress(clientIp);
		
		if(list == null || list.size() == 0){
			throw new Exception("등록되지 않은 장비 입니다.");
		}
		
		return list.get(0).getUrl();
	}
	
	
}
