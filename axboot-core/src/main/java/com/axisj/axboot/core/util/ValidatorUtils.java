package com.axisj.axboot.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * 프로그램명 : ValidatorUtil 
 * 설 명 : 유효성 검사를 위한 클래스
 * ------------------------------------------
 *
 * 이력사항 2014. 10. 31. wofmaker 최초작성 <BR/>
 */
public class ValidatorUtils {

	private ValidatorUtils() {
	}

	public enum PhoneType {
		ALL, MOBILE, TEL
	}

	public enum StringType {
		URL, EMAIL
	}

	private final static String[] MOBILE_NUM_PRIFIX = { "010", "011", "016", "017", "019" };
	private final static String[] TEL_NUM_PRIFIX = { "02", "031", "032", "033", "041", "043", "042", "044", "051", "052", "053", "054", "055", "061", "062",
			"063", "064", "070" };

	/**
	 *
	 * 메소드 명칭 : checkPhoneNumber 
	 * 메소드 설명 : 전화번호 체크
	 * ---------------------------------------- 
	 * 이력사항 2014. 10. 31. wofmaker 최초작성
	 *
	 * @param number
	 * @return
	 */
	public static boolean checkPhoneNumber(PhoneType phoneType, String number) {

		if (number == null || number.length() == 0) {
			return false;
		}

		String replaceNumber = number.replaceAll("-", "");

		if (!(replaceNumber.length() == 10 || replaceNumber.length() == 11)) {
			return false;
		}

		if (!isNumber(replaceNumber)) {
			return false;
		}

		boolean res = false;

		List<String> checkList = new ArrayList<String>();

		switch (phoneType) {
		case ALL:
			for (String s : MOBILE_NUM_PRIFIX) {
				checkList.add(s);
			}
			for (String s : TEL_NUM_PRIFIX) {
				checkList.add(s);
			}
			break;
		case MOBILE:
			for (String s : MOBILE_NUM_PRIFIX) {
				checkList.add(s);
			}
			break;
		case TEL:
			for (String s : TEL_NUM_PRIFIX) {
				checkList.add(s);
			}
			break;
		}

		for (String s : checkList) {
			if (number.startsWith(s)) {
				res = true;
			}
		}

		return res;
	}

	/**
	 *
	 * 메소드 명칭 : isNumber 
	 * 메소드 설명 : 숫자인지 체크
	 * ---------------------------------------- 
	 * 이력사항 2014. 10. 31. wofmaker 최초작성
	 *
	 * @param number
	 * @return
	 */
	public static boolean isNumber(String number) {
		boolean res = true;

		try {
			Integer.parseInt(number);
		} catch (Exception e) {
			res = false;
		}

		return res;
	}

	/**
	 *
	 * 메소드 명칭 : isValidEmail 
	 * 메소드 설명 : 이메일 검증
	 * ---------------------------------------- 
	 * 이력사항 2014. 12. 29. wofmaker 최초작성
	 *
	 * @param email
	 * @return
	 */
	public static boolean isValidEmail(String email) {
		boolean err = false;
		String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
		if (checkRegex(regex, email)) {
			err = true;
		}
		return err;
	}

	/**
	 *
	 * 메소드 명칭 : isValidUrl 
	 * 메소드 설명 : url 검증
	 * ---------------------------------------- 
	 * 이력사항 2014. 12. 29. wofmaker 최초작성
	 *
	 * @param url
	 * @return
	 */
	public static boolean isValidUrl(String url) {
		boolean err = false;
		String regex = "^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/?([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$";
		if (checkRegex(regex, url)) {
			err = true;
		}
		return err;
	}

	/**
	 *
	 * 메소드 명칭 : checkRegex 
	 * 메소드 설명 : 정규식 검증
	 * ---------------------------------------- 
	 * 이력사항 2014. 12. 29. wofmaker 최초작성
	 *
	 * @param regex
	 * @param content
	 * @return
	 */
	public static boolean checkRegex(String regex, String content) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(content);
		return m.matches();
	}

	/**
	 *
	 * 메소드 명칭 : isStringType 
	 * 메소드 설명 : type별 유효성 검사
	 * ---------------------------------------- 
	 * 이력사항 2014. 12. 29. wofmaker 최초작성
	 *
	 * @param content
	 * @param type
	 * @return
	 */
	public static boolean isStringType(String content, StringType type) {

		switch (type) {
		case URL:
			return isValidUrl(content);
		case EMAIL:
			return isValidEmail(content);
		}

		return false;
	}
}