package com.axisj.axboot.core.domain.inspectroom;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.axisj.axboot.core.domain.room.Room;

@Repository
public interface InspectroomRepository extends JpaRepository<Inspectroom, InspectroomId> {

	
	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	//@Cacheable
	List<Inspectroom> findByWorkDateAndSeqNo(String workDate, int seqNo) throws SQLException;

	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	//@Cacheable
	Inspectroom findByWorkDateAndSeqNoAndRoomCd(String workDate, Integer seqNo, String roomCd) throws SQLException;	

}
