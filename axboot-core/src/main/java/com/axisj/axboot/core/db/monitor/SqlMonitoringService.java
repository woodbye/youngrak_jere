package com.axisj.axboot.core.db.monitor;

import com.axisj.axboot.core.db.SqlMonitoringLogUtil;
import com.axisj.axboot.core.db.dbcp.ManagedBasicDataSource;
import com.axisj.axboot.core.db.monitor.sql.SqlExecutionInfo;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

public class SqlMonitoringService implements InitializingBean, DisposableBean {

	private ManagedBasicDataSource dataSource;

	private SqlMonitoringLogUtil sqlMonitoringLogUtil;

	public SqlMonitoringService(DataSource dataSource) {
		if (dataSource instanceof ManagedBasicDataSource) {
			this.dataSource = (ManagedBasicDataSource) dataSource;
		}
	}

	public List<SqlExecutionInfo> getSqlExecutionInfos() {
		if (this.dataSource != null) {
			return this.dataSource.getSqlTaskPool().getSqlExecutionInfoList();
		} else {
			return Collections.emptyList();
		}
	}

	public void saveAll() {
		sqlMonitoringLogUtil.saveSqlMonitoringInfo();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (this.dataSource != null) {
			sqlMonitoringLogUtil = new SqlMonitoringLogUtil(this.dataSource.getSqlTaskPool().getSqlExecutionInfoList());
		}
	}

	@Override
	public void destroy() throws Exception {
		if (this.dataSource != null) {
			saveAll();
		}
	}
}
