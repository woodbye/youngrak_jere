package com.axisj.axboot.core.util;

import java.io.BufferedWriter;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public class ExcelUtil {

	/**
	 * MS EXCEL XML FORMAT 앞부분
	 */
	private static final String EXCEL_HEAD = "<?xml version=\"1.0\"?>"
					+ "<?mso-application progid=\"Excel.Sheet\"?>"
					+ "<Workbook"
					+ "   xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\""
					+ "   xmlns:o=\"urn:schemas-microsoft-com:office:office\""
					+ "   xmlns:x=\"urn:schemas-microsoft-com:office:excel\""
					+ "   xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\""
					+ "   xmlns:html=\"http://www.w3.org/TR/REC-html40\">"
					+ "  <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">"
					+ "    <Author>Someone</Author>"
					+ "    <LastAuthor>Self</LastAuthor>"
					+ "    <Created>2012-03-15T23:04:04Z</Created>"
					+ "    <Company>Eaton Corporation</Company>"
					+ "    <Version>11.8036</Version>"
					+ "  </DocumentProperties>"
					+ "  <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">"
					+ "    <WindowHeight>6795</WindowHeight>"
					+ "    <WindowWidth>8460</WindowWidth>"
					+ "    <WindowTopX>120</WindowTopX>"
					+ "    <WindowTopY>15</WindowTopY>"
					+ "    <ProtectStructure>False</ProtectStructure>"
					+ "    <ProtectWindows>False</ProtectWindows>"
					+ "  </ExcelWorkbook>"
					+ "  <Styles>"
					+ "    <Style ss:ID=\"Default\" ss:Name=\"Normal\">"
					+ "      <Alignment ss:Vertical=\"Bottom\" />"
					+ "      <Borders />"
					+ "      <Font />"
					+ "      <Interior />"
					+ "      <NumberFormat />"
					+ "      <Protection />"
					+ "    </Style>"
					+ "    <Style ss:ID=\"s21\">"
					+ "      <Font x:Family=\"Swiss\" ss:Bold=\"1\" />"
					+ "    </Style>"
					+ "  </Styles>"
					+ "  <Worksheet ss:Name=\"Sheet1\">"
					+ "    <Table ss:ExpandedColumnCount=\"{column.count}\" ss:ExpandedRowCount=\"{row.count}\""
					+ "	   x:FullColumns=\"1\" x:FullRows=\"1\">";

	/**
	 * MS EXCEL XML FORMAT 뒷부분
	 */
	private static final String EXCEL_FOOT = "    </Table>"
					+ "    <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">"
					+ "      <Print>"
					+ "        <ValidPrinterInfo />"
					+ "        <HorizontalResolution>600</HorizontalResolution>"
					+ "        <VerticalResolution>600</VerticalResolution>"
					+ "      </Print>"
					+ "      <Selected />"
					+ "      <Panes>"
					+ "        <Pane>"
					+ "          <Number>3</Number>"
					+ "          <ActiveRow>5</ActiveRow>"
					+ "          <ActiveCol>1</ActiveCol>"
					+ "        </Pane>"
					+ "      </Panes>"
					+ "      <Table>"
					+ "      </Table>"
					+ "      <ProtectObjects>False</ProtectObjects>"
					+ "      <ProtectScenarios>False</ProtectScenarios>"
					+ "    </WorksheetOptions>"
					+ "  </Worksheet>"
					+ "</Workbook>";
	
	private ExcelUtil() {}

	/**
	 * 
	 * @Method Name  : exportLargeData
	 * @작성일   : 2015. 12. 26. 오후 3:33:21
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : Spring Data Jpa를 사용한 대용량 엑셀 다운로드
	 * @param fileName
	 * @param repository
	 * @param methodName
	 * @param response
	 * @param parser
	 * @param params
	 * @throws Exception
	 */
	public static <E,I extends Serializable> void exportLargeData(
				String fileName
				, JpaRepository<E, I> repository
				, String methodName
				, HttpServletResponse response
				, Function<E, String> parser
				, Object...params
			) throws Exception{
		
		/*
		 * 헤더 세팅
		 */
		response.setContentType("application/octet-stream");
		response.setHeader( "Content-Disposition", "filename=" + fileName );
		
		/*
		 * 대용량 엑셀을 처리할 경우 Pageable 인터페이스를 구현한 객체를 포함시킨다.
		 */
		int i=0;
		boolean chk = false;
		for(; i<params.length; i++){
			if(params[i] instanceof Pageable){
				chk = true;
				break;
			}
		}
		
		Page<E> entity = null;
		List<E> list = null;
		
		Method method =  ReflectionUtils.getMethodByName(repository, methodName);
		
		String row = null;
		
		/*
		 * 컬럼 및 로우 갯수 확인
		 */
		long rowCount = 0;
		long columnCount = 0;
		if(chk){
			entity = (Page<E>) method.invoke(repository, params);
			rowCount = entity.getTotalElements();
		}else{
			list = (List<E>) method.invoke(repository, params);
			rowCount = list.size();
		}

		
		if(rowCount != 0){
			E e = chk ? entity.getContent().get(0) : list.get(0);
			if(e instanceof Map){
				columnCount = ((Map)e).size();
			}else{
				columnCount = e.getClass().getDeclaredFields().length;
			}
		}
		
		BufferedWriter bw = new BufferedWriter(response.getWriter());
		
		String excelHead = EXCEL_HEAD;
		excelHead=excelHead.replaceAll("\\{column.count\\}", columnCount+"");
		excelHead=excelHead.replaceAll("\\{row.count\\}", rowCount+"");
		bw.write(excelHead);
		
		if(chk){
			
			do{
				
				entity = (Page<E>) method.invoke(repository, params);
				
				list = entity.getContent();
				
				for(E e : list){
					row = parser.apply(e);
					bw.write(row);
				}
			
				params[i] = ((Pageable)params[i]).next();
				
			}while(entity.hasNext());
			
		}else{
			
			for(E e : list){
				row = parser.apply(e);
				bw.write(row);
			}
			
		}
		
		bw.write(EXCEL_FOOT);
		
		bw.close();
	}
	
	public static String getStringCell(Object obj){
		StringBuilder sb = new StringBuilder();
		sb
	        .append("<Cell>")
	        .append("<Data ss:Type=\"String\">")
	        .append(obj)
	        .append("</Data>")
	        .append("</Cell>")
	        ;
		return sb.toString();
	}
}
