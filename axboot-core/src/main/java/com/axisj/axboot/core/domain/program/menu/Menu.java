package com.axisj.axboot.core.domain.program.menu;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.program.Program;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "MNU_M")
@Alias("menu")
public class Menu extends BaseJpaModel<String> {

    @Id
    @Column(name = "MNU_CD", length = 50)
    private String mnuCd;

    @Column(name = "MNU_NM", length = 50)
    private String mnuNm;

    @Column(name = "ICON", length = 100)
    private String icon;

    @Column(name = "MNU_LV", precision = 1)
    private Integer mnuLv;

    @Column(name = "MNU_IX", precision = 2)
    private Integer mnuIx;

    @Column(name = "MNUUP_CD", length = 50)
    private String mnuUpCd;

    @Column(name = "PROG_CD", length = 50)
    private String progCd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROG_CD", referencedColumnName = "PROG_CD", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Program program;

    @Column(name = "REMARK", length = 200)
    private String remark;

    @Column(name = "USE_YN", length = 1)
    private String useYn;

    @Override
    public String getId() {
        return mnuCd;
    }
}
