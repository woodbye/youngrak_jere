package com.axisj.axboot.core.domain.inspect;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.SpringUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name="INSPECT")
@IdClass(InspectId.class)
@ToString(includeFieldNames=true)
//@Cacheable
public class Inspect extends BasicJpaModel<InspectId> {

	@Id
    @Column(name = "WORK_DATE", length = 8)
    private String workDate;
	@Id
    @Column(name = "SEQ_NO", precision = 2)
    private Integer seqNo;
	
	@NotFound(action=NotFoundAction.IGNORE)
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="WORK_DATE", referencedColumnName="WORK_DATE", insertable=false, updatable=false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
		, @JoinColumn(name="SEQ_NO", referencedColumnName="SEQ_NO", insertable=false, updatable=false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
		})
	private List<Inspectroom> inspectroom;
	
    @Column(name = "STDATE", length = 14)
    private String stdate;
    @Column(name = "EDDATE", length = 14)
    private String eddate;
    @Column(name = "REMARK", length = 200)
    private String remark;
    @Column(name = "REGTIME", length = 14)
    private String regTime;
    @Column(name = "REGID", length = 20)
    private String regId;
    @Column(name = "UDTTIME", length = 14)
    private String udtTime;
    @Column(name = "UDTID", length = 20)
    private String udtId;
	
	@Override
	public InspectId getId() {
		if(seqNo == null){
			try {
				seqNo = SpringUtils.getBean(InspectRepository.class).findNextSeqNo(workDate);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return InspectId.of(workDate, seqNo);
	}
	
	@PrePersist
	private void onPersist() {		
		this.regId = this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.regTime = this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
		//this.seqNo = SpringUtils.getBean(InspectRepository.class).findNextSeqNo();
	}
	
	@PreUpdate
	private void onUpdate() {
		this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	
}
