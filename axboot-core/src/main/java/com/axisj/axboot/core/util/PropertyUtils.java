package com.axisj.axboot.core.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;

public class PropertyUtils {

	public static boolean getProperty(String key, boolean defaultValue) {
		Environment environment = CommonUtils.getEnvironment();

		if (environment != null) {
			String value = environment.getProperty(key);

			if (StringUtils.isEmpty(value)) {
				return defaultValue;
			}

			return Boolean.valueOf(value);
		}
		return false;
	}
}
