package com.axisj.axboot.core.domain.program.menu.display;

import com.axisj.axboot.core.domain.program.menu.AuthorizedMenu;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class MainMenu implements Serializable {

    @JsonProperty("_id")
    private String id;

    @JsonProperty("label")
    private String label;

    @JsonProperty("icon")
    private String icon;

    @JsonProperty("url")
    private String url = "#";

    @JsonProperty("target")
    private String target = "_self";

    @JsonProperty("cn")
    private List<MainMenu> child = new ArrayList<>();

    private final static String ICON_CSS_STYLE = "<i class='axi %s'></i> ";

    public MainMenu(AuthorizedMenu authorizedMenu) {
        this.id = authorizedMenu.getMenuCode();
        this.label = authorizedMenu.getMenuName();
        this.icon = authorizedMenu.getIcon();

        if (!StringUtils.isEmpty(icon)) {
            this.label = String.format(ICON_CSS_STYLE, icon) + this.label;
        }

        if (!StringUtils.isEmpty(authorizedMenu.getProgramPath())) {
            this.url = authorizedMenu.getProgramPath();
            this.target = authorizedMenu.getTarget();
            this.url = authorizedMenu.getProgramPath();
        }
    }

    public void addChild(AuthorizedMenu authorizedMenu) {
        child.add(new MainMenu(authorizedMenu));
    }
}
