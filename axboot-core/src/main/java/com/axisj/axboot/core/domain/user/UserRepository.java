package com.axisj.axboot.core.domain.user;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
	Page<User> findByCompCd(String compCd, Pageable pageable);

	List<User> findByCompCd(String compCd);

	List<User> findByUserType(String userType);

	List<User> findByCompCdAndUserType(String compCd, String userType);
}
