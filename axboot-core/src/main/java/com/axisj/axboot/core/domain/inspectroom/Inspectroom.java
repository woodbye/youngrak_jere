package com.axisj.axboot.core.domain.inspectroom;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectId;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name="INSPECTROOM")
@IdClass(InspectroomId.class)
@ToString(includeFieldNames=true)
//@Cacheable
public class Inspectroom extends BasicJpaModel<InspectroomId> {
	
	@Id
    @Column(name = "WORK_DATE", length=8)
    private String workDate;
	@Id
    @Column(name = "SEQ_NO", precision=2)
    private Integer seqNo;
	@Id
	@Column(name = "ROOM_CD", length = 3)
    private String roomCd;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROOM_CD", referencedColumnName = "ROOM_CD", insertable = false, updatable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Room room;
    @Column(name = "USE_YN", length=1)
    private String useYn;
    @Column(name = "REGTIME", length=14)
    private String regTime;
    @Column(name = "REGID", length=20)
    private String regId;
    @Column(name = "UDTTIME", length=14)
    private String udtTime;
    @Column(name = "UDTID", length=20)
    private String udtId;
	  
	@Override
	public InspectroomId getId() {
		return InspectroomId.of(workDate, seqNo, room.getRoomCd());
	}
	
	@PrePersist
	private void onPersist() {		
		this.regId = this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.regTime = this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
		//this.seqNo = SpringUtils.getBean(InspectRepository.class).findNextSeqNo();
	}
	
	@PreUpdate
	private void onUpdate() {
		this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}

}
