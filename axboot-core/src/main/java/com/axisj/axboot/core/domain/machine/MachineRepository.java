package com.axisj.axboot.core.domain.machine;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface MachineRepository extends JpaRepository<Machine, String> {

	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	@org.springframework.cache.annotation.Cacheable
	List<Machine> findByMachineNmStartingWith(String machineName) throws Exception;
	List<Machine> findByIpaddress(String clientIp) throws Exception;

}
