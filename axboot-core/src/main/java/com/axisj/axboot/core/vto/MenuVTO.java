package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.program.menu.Menu;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "MenuVTO", description = "메뉴정보")
public class MenuVTO {

	@ApiModelProperty(value = "메뉴코드")
	private String mnuCd;

	@ApiModelProperty(value = "메뉴명")
	private String mnuNm;

	@ApiModelProperty(value = "메뉴 ICON Class")
	private String icon;

	@ApiModelProperty(value = "LEVEL")
	private Integer mnuLv;

	@ApiModelProperty(value = "정렬순서")
	private Integer mnuIx;

	@ApiModelProperty(value = "상위메뉴코드")
	private String mnuUpCd;

	@ApiModelProperty(value = "프로그램코드")
	private String progCd;

	@ApiModelProperty(value = "클래스 명")
	private String classNm;

	@ApiModelProperty(value = "프로그램명")
	private String progNm;

	@ApiModelProperty(value = "비고")
	private String remark;

	@ApiModelProperty(value = "사용여부")
	private String useYn;

	@JsonProperty("program")
	private ProgramVTO programVTO;

	public static MenuVTO of(Menu menu) {
		MenuVTO menuVTO = DozerBeanMapperUtils.map(menu, MenuVTO.class);

		if (CommonUtils.canConvertMappingVTO(menu.getProgram())) {
			ProgramVTO programVTO = DozerBeanMapperUtils.map(menu.getProgram(), ProgramVTO.class);
			menuVTO.setProgramVTO(programVTO);
			menuVTO.setProgCd(programVTO.getProgCd());
			menuVTO.setProgNm(programVTO.getProgNm());
		}

		return menuVTO;
	}

	public static List<MenuVTO> of(List<Menu> menuList) {
		return menuList.stream().map(menu -> of(menu)).collect(toList());
	}
}
