package com.axisj.axboot.core.domain.notice;

import com.axisj.axboot.core.code.Params;
import com.axisj.axboot.core.domain.BaseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;


@Service
public class NoticeService extends BaseService<Notice, Integer> {

	private NoticeRepository noticeRepository;

	@Inject
	public NoticeService(NoticeRepository noticeRepository) {
		super(noticeRepository);
		this.noticeRepository = noticeRepository;
	}

	public void saveNotice(Notice notice) {
		if (StringUtils.isEmpty(notice.getCompCd())) {
			notice.setCompCd(Params.ALL);
		}
		save(notice);
	}

	public Page<Notice> findAllByOrderByInsDtDesc(Pageable pageable) {
		return noticeRepository.findAll(pageable);
	}
}
