package com.axisj.axboot.core.domain.machine;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name="MACHINE")
@ToString(includeFieldNames=true)
//@Cacheable
public class Machine extends BasicJpaModel<String> {

	@Id
    @Column(name = "MACHINE_CD", length=3)
    private String machineCd;
    @Column(name = "MACHINE_NM", length=40)
    private String machineNm;
    @Column(name = "MSIZE", length=40)
    private String msize;
    @Column(name = "IPADDRESS", length=20)
    private String ipaddress;
    @Column(name = "URL", length=200)
    private String url;
    @Column(name = "REMARK", length=200)
    private String remark;
    @Column(name = "REGTIME", length=14)
    private String regTime;
    @Column(name = "REGID", length=20)
    private String regId;
    @Column(name = "UDTTIME", length=14)
    private String udtTime;
    @Column(name = "UDTID", length=20)
    private String udtId;
    
	@Override
	public String getId() {
		return machineCd;
	}
	
	@PrePersist
	private void onPersist() {		
		this.regId = this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.regTime = this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	
	@PreUpdate
	private void onUpdate() {
		this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
}
