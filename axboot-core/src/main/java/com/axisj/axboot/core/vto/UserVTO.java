package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.user.User;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "UserVTO", description = "사용자 정보")
public class UserVTO {

	@ApiModelProperty(value = "사용자코드")
	private String userCd;

	@ApiModelProperty(value = "사용자명")
	private String userNm;

	@ApiModelProperty(value = "비밀번호")
	private String userPs;

	@ApiModelProperty(value = "회사코드")
	private String compCd;

	@ApiModelProperty(value = "회사명")
	private String compNm;

	@ApiModelProperty(value = "이메일")
	private String email;

	@ApiModelProperty(value = "휴대폰")
	private String hpNo;

	@ApiModelProperty(value = "사용자 타입")
	private String userType;

	@ApiModelProperty(value = "비고")
	private String remark;

	@ApiModelProperty(value = "사용여부")
	private String useYn;

	public static UserVTO of(User user) {
		UserVTO userVTO = DozerBeanMapperUtils.map(user, UserVTO.class);
		return userVTO;
	}

	public static List<UserVTO> of(List<User> userList) {
		return userList.stream().map(user -> of(user)).collect(toList());
	}
}
