package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.code.BasicCode;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.google.common.collect.Maps;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "BasicCodeVTO", description = "기초코드 정보")
public class BasicCodeVTO {

	@ApiModelProperty(value = "기초코드")
	private String basicCd;

	@ApiModelProperty(value = "기초코드명")
	private String basicNm;

	@ApiModelProperty(value = "코드")
	private String code;

	@ApiModelProperty(value = "코드명")
	private String name;

	@ApiModelProperty(value = "정렬순서")
	private Integer sort = 0;

	@ApiModelProperty(value = "문자값1")
	private String data1;

	@ApiModelProperty(value = "문자값2")
	private String data2;

	@ApiModelProperty(value = "문자값3")
	private String data3;

	@ApiModelProperty(value = "숫자값1")
	private Integer data4 = 0;

	@ApiModelProperty(value = "숫자값2")
	private Integer data5 = 0;

	@ApiModelProperty(value = "POS 사용 여부")
	private String posUseYn;

	@ApiModelProperty(value = "비고")
	private String remark;

	@ApiModelProperty(value = "사용여부")
	private String useYn;

	public static BasicCodeVTO of(BasicCode basicCode) {
		return DozerBeanMapperUtils.map(basicCode, BasicCodeVTO.class);
	}

	public static List<BasicCodeVTO> of(List<BasicCode> basicCodeList) {
		return basicCodeList.stream().map(basicCode -> of(basicCode)).collect(toList());
	}

	public static Map<String, Object> ofMap(Map<String, List<BasicCode>> basicCodeMap) {
		Map<String, Object> convertedMap = Maps.newHashMap();

		for (String key : basicCodeMap.keySet()) {
			List<BasicCode> basicCodes = basicCodeMap.get(key);
			List<BasicCodeVTO> basicCodeVTOs = BasicCodeVTO.of(basicCodes);

			convertedMap.put(key, basicCodeVTOs);
		}

		return convertedMap;
	}
}
