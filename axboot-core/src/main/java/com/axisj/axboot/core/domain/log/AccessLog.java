package com.axisj.axboot.core.domain.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name="ACCESSLOG")
@ToString(includeFieldNames=true)
public class AccessLog extends BasicJpaModel<String> {

	@Id   
    @Column(name = "IPADDRESS", length=20)
    private String ipaddress;
    @Column(name = "URL", length=500)
    private String url;   
    @Column(name = "ACCTIME", length=14)
    private String accTime;

    
	@Override
	public String getId() {
		return ipaddress;
	}
	
	@PrePersist
	private void onPersist() {		
		this.accTime = this.accTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	
	@PreUpdate
	private void onUpdate() {
		this.accTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
}
