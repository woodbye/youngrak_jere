package com.axisj.axboot.core;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletContext;

import org.aspectj.util.FileUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.axisj.axboot.core.util.SpringUtils;

@Service
public class FileService{

	@Inject
	private ServletContext context;
	
	public Map<String, Object> fileUpload(MultipartFile file){
		
		Map<String,Object> res = new HashMap<>();
		
		String realPath = context.getRealPath("/static/fileUpload");
		String fileName = "file_" + System.currentTimeMillis() + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());
		
		File PathDir = new File(realPath);
		if(!PathDir.exists()){
			PathDir.mkdirs();
		}
		
		realPath = realPath + "/" + fileName;
		
		Path path = Paths.get(realPath);		
		
		try (InputStream is = file.getInputStream()) {
			
			
			Files.copy(is, path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		name:"name",
//		type:"type",
//		saveName:"saveName",
//		fileSize:"fileSize",
//		uploadedPath:"uploadedPath",
//		thumbPath:"thumbUrl"
		
		res.put("name", file.getOriginalFilename());
		res.put("type", StringUtils.getFilenameExtension(file.getOriginalFilename()));
		res.put("saveName", fileName);
		res.put("fileSize", file.getSize());
		res.put("uploadedPath", "/static/fileUpload/");
		res.put("thumbPath", "/static/fileUpload/"+fileName);
		
		
		
		return res;
	}
}
