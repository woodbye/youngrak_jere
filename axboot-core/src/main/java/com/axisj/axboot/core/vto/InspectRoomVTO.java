package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectId;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.program.menu.Menu;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value = "InspectRoomVTO", description = "점검 대상 제례실")
public class InspectRoomVTO {

	
	@ApiModelProperty(value = "등록일자 PK")
    private String workDate;
	@ApiModelProperty(value = "일련번호 PK")
    private Integer seqNo;
	@ApiModelProperty(value = "제례실코드") 
    private String roomCd;
	@ApiModelProperty(value = "제례관") 
    private String areaCd;
	@ApiModelProperty(value = "제례실명")
	private String roomNm;
	@ApiModelProperty(value = "사용유무")
    private String useYn;
	@ApiModelProperty(value = "등록 시간")
    private String regTime;    
	@ApiModelProperty(value = "등록 유저")
    private String regId;     
	@ApiModelProperty(value = "수정 시간")
    private String udtTime;     
	@ApiModelProperty(value = "수정 유저")
    private String udtId;  
	
	@JsonProperty("room")
	private RoomVTO room;	
	
	
	public static InspectRoomVTO of(Inspectroom inspectroom) {
		
		if(!CommonUtils.canConvertMappingVTO(inspectroom)){
			return null;
		}
		InspectRoomVTO inspectRoomVTO = DozerBeanMapperUtils.map(inspectroom, InspectRoomVTO.class);
		
		if (CommonUtils.canConvertMappingVTO(inspectroom.getRoom())) {
			RoomVTO roomVTO = DozerBeanMapperUtils.map(inspectroom.getRoom(), RoomVTO.class);
			inspectRoomVTO.setRoom(roomVTO);
			inspectRoomVTO.setRoomCd(roomVTO.getRoomCd());
			inspectRoomVTO.setRoomNm(roomVTO.getRoomNm());
			inspectRoomVTO.setAreaCd(roomVTO.getAreaCd());
		}
		return inspectRoomVTO;
	}
	
	public static List<InspectRoomVTO> of(List<Inspectroom> list){
		return list.stream().map(inspectroom -> of(inspectroom)).collect(toList());
	}
	
	/*public static MenuVTO of(Menu menu) {
		MenuVTO menuVTO = DozerBeanMapperUtils.map(menu, MenuVTO.class);

		if (CommonUtils.canConvertMappingVTO(menu.getProgram())) {
			ProgramVTO programVTO = DozerBeanMapperUtils.map(menu.getProgram(), ProgramVTO.class);
			menuVTO.setProgramVTO(programVTO);
			menuVTO.setProgCd(programVTO.getProgCd());
			menuVTO.setProgNm(programVTO.getProgNm());
		}

		return menuVTO;
	}

	public static List<MenuVTO> of(List<Menu> menuList) {
		return menuList.stream().map(menu -> of(menu)).collect(toList());
	}*/

}
