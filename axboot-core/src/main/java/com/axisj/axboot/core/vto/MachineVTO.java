package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;

import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.machine.Machine;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(value="MachineVTO", description="설치장비 관리")
public class MachineVTO {

	
	@ApiModelProperty(value= "장비코드")
    private String machineCd;
	@ApiModelProperty(value= "장비명")
    private String machineNm;
	@ApiModelProperty(value= "규격")
    private String msize;
	@ApiModelProperty(value= "IP ADDRESS")
    private String ipaddress;
	@ApiModelProperty(value= "url")
    private String url;
	@ApiModelProperty(value= "비고")
    private String remark;
	@ApiModelProperty(value= "등록시간")  
    private String regtime;
	@ApiModelProperty(value= "등록자")  
    private String regid;
	@ApiModelProperty(value= "수정시간") 
    private String udttime;
	@ApiModelProperty(value= "수정자")   
    private String udtid;
	
	public static MachineVTO of(Machine machine) {
		MachineVTO machineVTO = DozerBeanMapperUtils.map(machine, MachineVTO.class);
		return machineVTO;
	}
	
	public static List<MachineVTO> of(List<Machine> list){
		return list.stream().map(machine -> of(machine)).collect(toList());
	}
}
