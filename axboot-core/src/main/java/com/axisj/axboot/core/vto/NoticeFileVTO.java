package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.notice.file.NoticeFile;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "NoticeFileVTO", description = "")
public class NoticeFileVTO {

	@ApiModelProperty(value = "")
	private Integer id;

	@ApiModelProperty(value = "")
	private Integer noticeId;

	@ApiModelProperty(value = "")
	private String fileName;

	public static NoticeFileVTO of(NoticeFile noticeFile) {
		NoticeFileVTO noticeFileVTO = DozerBeanMapperUtils.map(noticeFile, NoticeFileVTO.class);
		return noticeFileVTO;
	}

	public static List<NoticeFileVTO> of(List<NoticeFile> noticeFileList) {
		return noticeFileList.stream().map(noticeFile -> of(noticeFile)).collect(toList());
	}
}
