package com.axisj.axboot.core.code;

public class Constants {

	public static final String CORE_DOMAIN_PACKAGE = "com.axisj.axboot.core.domain";
	public static final String BIGERS_DOMAIN_PACKAGE = "net.bigers.domain";
	public static final String LAST_NAVIGATED_PAGE = "LAST_NAVIGATED_PAGE";
	public static final String AUTH_TOKEN_KEY = "AX-BOOT-AUTH-TOKEN";
	public static final String API_CONTEXT_PATH = "/api";
	public static final String AX_BOOT_ASP_ID = "AX-BOOT";

	public static final String THEME = "THEME";
	public static final String DEFAULT_THEME = "ax-boot";
	
	public static final String REAL_PATH_PHOTO = "C:\\apache-tomcat-8.0.30\\webapps\\photo\\";
	//public static final String REAL_PATH_PHOTO = "D:\\apache-tomcat-8.0.30\\webapps\\photo\\";

	//public static final String UPLOAD_PATH_PHOTO = "/images/photo/";
	public static final String UPLOAD_PATH_PHOTO = "/images/photo/";
}
