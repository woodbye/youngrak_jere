package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.code.Params;
import com.axisj.axboot.core.domain.notice.Notice;
import com.axisj.axboot.core.util.ArrayUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "NoticeVTO", description = "")
public class NoticeVTO {

	@ApiModelProperty(value = "")
	private Integer id;

	@ApiModelProperty(value = "")
	private String compCd;

	@ApiModelProperty(value = "")
	private String compNm;

	@ApiModelProperty(value = "")
	private String title;

	@ApiModelProperty(value = "")
	private String content;

	@ApiModelProperty(value = "")
	private String stDt;

	@ApiModelProperty(value = "")
	private String endDt;

	@ApiModelProperty(value = "")
	private String popupYn;

	@ApiModelProperty(value = "")
	private String dispYn;

	private List<NoticeFileVTO> files = new ArrayList<>();

	private Date insDate;

	private String insUser;

	private String insUserName;

	private Date uptDate;

	private String uptUser;

	private String uptUserName;

	public static NoticeVTO of(Notice notice) {
		NoticeVTO noticeVTO = DozerBeanMapperUtils.map(notice, NoticeVTO.class);

		if (!notice.getCompCd().equals(Params.ALL)) {
			noticeVTO.setCompNm(notice.getCompany().getCompNm());
		}

		if (!ArrayUtils.isEmpty(notice.getNoticeFileList())) {
			noticeVTO.getFiles().addAll(NoticeFileVTO.of(notice.getNoticeFileList()));
		}

		noticeVTO.setInsUser(notice.getInsUser());
		noticeVTO.setInsDate(notice.getInsDt());

		return noticeVTO;
	}

	public static List<NoticeVTO> of(List<Notice> noticeList) {
		return noticeList.stream().map(notice -> of(notice)).collect(toList());
	}
}
