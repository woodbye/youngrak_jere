package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.program.Program;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "ProgramVTO", description = "프로그램 정보")
public class ProgramVTO {

	@ApiModelProperty(value = "프로그램 코드")
	private String progCd;

	@ApiModelProperty(value = "프로그램명")
	private String progNm;

	@ApiModelProperty(value = "파일명")
	private String fileNm;

	@ApiModelProperty(value = "PATH")
	private String progPh;

	@ApiModelProperty(value = "TARGET")
	private String target;

	@ApiModelProperty(value = "조회권한")
	private String schAh;

	@ApiModelProperty(value = "저장권한")
	private String savAh;

	@ApiModelProperty(value = "엑셀권한")
	private String exlAh;

	@ApiModelProperty(value = "기능키1권한")
	private String fn1Ah;

	@ApiModelProperty(value = "기능키2권한")
	private String fn2Ah;

	@ApiModelProperty(value = "기능키3권한")
	private String fn3Ah;

	@ApiModelProperty(value = "기능키4권한")
	private String fn4Ah;

	@ApiModelProperty(value = "기능키5권한")
	private String fn5Ah;

	@ApiModelProperty(value = "비고")
	private String remark;

	@ApiModelProperty(value = "사용여부")
	private String useYn;


	public static ProgramVTO of(Program program) {
		return DozerBeanMapperUtils.map(program, ProgramVTO.class);
	}

	public static List<ProgramVTO> of(List<Program> programList) {
		return programList.stream().map(program -> of(program)).collect(toList());
	}
}
