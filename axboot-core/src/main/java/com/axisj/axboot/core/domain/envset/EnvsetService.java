package com.axisj.axboot.core.domain.envset;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Function;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.util.ExcelUtil;
import com.axisj.axboot.core.util.ReflectionUtils;

@Service
public class EnvsetService extends BaseService<Envset, EnvsetId> {

	
	private EnvsetRepository envsetRepository;
	
	@Inject
	public EnvsetService(EnvsetRepository repository) {
		super(repository);
		this.envsetRepository = repository;
	}
	
	public List<Envset> findByWorkDateBetween(String start, String end) throws SQLException{
		return envsetRepository.findByWorkDateBetween(start, end);
	}

	public boolean exportExcel(String start, String end, HttpServletResponse response) throws Exception {
		
		String[] header = {	"작업일자", "연번", "적용시작일시", "운영모드", "2관 개방 여부", 
										"개방시간", "폐장 시간", "제례시간", "연장시간", "정리시간", 
										"TTS", "MMS", "비고"}; 
		String[] body = {	"workDate", "seqNo", "stdate", "opMode", "area2Useyn",
									"openTime"	, "closeTime", "runingTime", "extTime"	, "cleanTime", 
									"ttsYn", "mmsYn", "remark"}; 
		String fileName = "운영환경.xml";
		
		ExcelUtil.exportLargeData(
				fileName
				, envsetRepository
				, "findByWorkDateBetween"
				, response
				, new Function<Envset, String>() {
			
						int rownum = 0;
						
						@Override
						public String apply(Envset envset) {
							
							StringBuilder sb = new StringBuilder();
							
							if(rownum == 0){
								sb.append("<Row>");
								for(String h : header){
									sb.append(ExcelUtil.getStringCell(h));
								}
								sb.append("</Row>");
							}
							
			                sb.append("<Row>");
			                for(String key : body){
			                	try {
									sb.append(ExcelUtil.getStringCell(ReflectionUtils.getValue(envset, key)));
								} catch (Exception e) {
									e.printStackTrace();
								}
			                }
			                sb.append("</Row>");
			              
			                rownum++;
			                
			                return sb.toString();
						}
					}, start, end);
		
		return true;
	}

	public void saveEnvset(List<Envset> list) {
		
		
		
		list.forEach(this::save);
		
	/*	List<Envset> list = envsets.stream().map(
				envset->{
					boolean chk = false;
					
					return envset.getId().equals("1") ? envset : null;
				}).collect(Collectors.toList());
		list.forEach(this::save);
		list = ListUtils.map(list, (envset) -> {return envset;});
		list = ListUtils.map(list, new Function(){
			
			@Override
			public Object apply(Object t) {
				// TODO Auto-generated method stub
				return null;
			}
			
		});*/
	
	}

	public void deleteEnvList(List<Envset> list) {
		list.forEach(this::delete);		
	}

	public Envset selectEnvSet(String stDate) throws SQLException {
		
		return envsetRepository.findFirstByStDateLessThanEqualOrderByWorkDateDescSeqNoDesc(stDate);
	}

}
