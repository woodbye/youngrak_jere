package com.axisj.axboot.core.domain.msgset;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsgSetRepository extends JpaRepository<MsgSet, String> {

	List<MsgSet> findByMsgUseStartingWith(String msgUse) throws Exception;

	MsgSet findByMsgId(String msgId) throws SQLException;

}
