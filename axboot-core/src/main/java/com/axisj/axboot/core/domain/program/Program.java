package com.axisj.axboot.core.domain.program;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.program.menu.Menu;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "PROG_M")
public class Program extends BaseJpaModel<String> {

    @Id
    @Column(name = "PROG_CD", length = 50)
    private String progCd;

    @Column(name = "PROG_NM", length = 50)
    private String progNm;

    @Column(name = "FILE_NM", length = 50)
    private String fileNm;

    @Column(name = "PROG_PH", length = 100)
    private String progPh;

    @Column(name = "TARGET", length = 10)
    private String target;

    @Column(name = "SCH_AH", length = 1)
    private String schAh;

    @Column(name = "SAV_AH", length = 1)
    private String savAh;

    @Column(name = "EXL_AH", length = 1)
    private String exlAh;

    @Column(name = "FN1_AH", length = 1)
    private String fn1Ah;

    @Column(name = "FN2_AH", length = 1)
    private String fn2Ah;

    @Column(name = "FN3_AH", length = 1)
    private String fn3Ah;

    @Column(name = "FN4_AH", length = 1)
    private String fn4Ah;

    @Column(name = "FN5_AH", length = 1)
    private String fn5Ah;

    @Column(name = "REMARK", length = 200)
    private String remark;

    @Column(name = "USE_YN", length = 1)
    private String useYn;

    @OneToMany(mappedBy = "program")
    private List<Menu> menuList;

    @Override
    public String getId() {
        return progCd;
    }
}
