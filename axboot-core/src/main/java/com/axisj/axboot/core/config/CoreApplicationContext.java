package com.axisj.axboot.core.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.dozer.DozerBeanMapper;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.mybatis.spring.transaction.SpringManagedTransactionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.lookup.DataSourceLookupFailureException;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.axisj.axboot.core.code.Constants;
import com.axisj.axboot.core.db.dbcp.ManagedBasicDataSource;
import com.axisj.axboot.core.db.dbcp.ManagedBasicDataSourceFactory;
import com.axisj.axboot.core.db.monitor.SqlMonitoringService;
import com.axisj.axboot.core.mybatis.AuditInterceptor;
import com.axisj.axboot.core.mybatis.MyBatisMapper;
import com.axisj.axboot.core.mybatis.SecondMyBatisMapper;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true, mode = AdviceMode.PROXY)
@EnableJpaRepositories(basePackages = {Constants.CORE_DOMAIN_PACKAGE, Constants.BIGERS_DOMAIN_PACKAGE}, repositoryImplementationPostfix = "Impl")
@EnableAspectJAutoProxy(proxyTargetClass = true)
//@EnableCaching
public class CoreApplicationContext implements EnvironmentAware {

	private org.springframework.core.env.Environment environment;

	@Override
	public void setEnvironment(final org.springframework.core.env.Environment environment) {
		this.environment = environment;
	}

	@Primary
	@Bean(name="dataSource") @Qualifier("dataSource")
	public DataSource dataSource() {
		try {
			ManagedBasicDataSourceFactory managedBasicDataSourceFactory = new ManagedBasicDataSourceFactory();
			Map<String, String> properties = new HashMap<>();
			properties.put("initialSize", environment.getProperty("spring.datasource.initialSize"));
			properties.put("minIdle", environment.getProperty("spring.datasource.minIdle"));
			properties.put("dataSourceId", "mainDataSource");
			properties.put("slowQueryTime", "1000");
			properties.put("accessToUnderlyingConnectionAllowed", "true");
			properties.put("connectionInitSqls", "SELECT 1");
			properties.put("url", environment.getProperty("spring.datasource.url"));
			properties.put("username", environment.getProperty("spring.datasource.username"));
			properties.put("password", environment.getProperty("spring.datasource.password"));
			properties.put("driverClassName", environment.getProperty("spring.datasource.driverClassName"));
			properties.put("maxIdle", "-1");
			properties.put("maxWait", "3000");
			properties.put("testOnBorrow", "true");
			properties.put("testOnReturn", "false");
			properties.put("testWhileIdle", "true");
			properties.put("timeBetweenEvictionRunsMillis", "600000");
			properties.put("minEvictableIdleTimeMillis", "-1");
			properties.put("softMinEvictableIdleTimeMillis", "300000");
			properties.put("maxActive", environment.getProperty("spring.datasource.maxActive"));
			properties.put("queryTimeout", "300000");
			properties.put("useSqlLogFormat", "true");
			properties.put("validationQuery", "SELECT 1");
			properties.put("sqlLogging", environment.getProperty("spring.datasource.sql.logging"));
			ManagedBasicDataSource ds = managedBasicDataSourceFactory.create("mainDataSource", properties);
			return ds;
		} catch (Throwable e) {
			throw new DataSourceLookupFailureException("Creating mainDataSource Failed!");
		}
	}

	@Bean(name="secondDataSource") @Qualifier("secondDataSource")
	public DataSource secondDataSource() {
		try {
			ManagedBasicDataSourceFactory managedBasicDataSourceFactory = new ManagedBasicDataSourceFactory();
			Map<String, String> properties = new HashMap<>();
			properties.put("initialSize", environment.getProperty("spring.secondDatasource.initialSize"));
			properties.put("minIdle", environment.getProperty("spring.secondDatasource.minIdle"));
			properties.put("dataSourceId", "secondDataSource");
			properties.put("slowQueryTime", "1000");
			properties.put("accessToUnderlyingConnectionAllowed", "true");
			properties.put("connectionInitSqls", "SELECT 1");
			properties.put("url", environment.getProperty("spring.secondDatasource.url"));
			properties.put("username", environment.getProperty("spring.secondDatasource.username"));
			properties.put("password", environment.getProperty("spring.secondDatasource.password"));
			properties.put("driverClassName", environment.getProperty("spring.secondDatasource.driverClassName"));
			properties.put("maxIdle", "-1");
			properties.put("maxWait", "3000");
			properties.put("testOnBorrow", "true");
			properties.put("testOnReturn", "false");
			properties.put("testWhileIdle", "true");
			properties.put("timeBetweenEvictionRunsMillis", "600000");
			properties.put("minEvictableIdleTimeMillis", "-1");
			properties.put("softMinEvictableIdleTimeMillis", "300000");
			properties.put("maxActive", environment.getProperty("spring.secondDatasource.maxActive"));
			properties.put("queryTimeout", "300000");
			properties.put("useSqlLogFormat", "true");
			properties.put("validationQuery", "SELECT 1");
			properties.put("sqlLogging", environment.getProperty("spring.secondDatasource.sql.logging"));
			ManagedBasicDataSource ds = managedBasicDataSourceFactory.create("secondDatasource", properties);
			return ds;
		} catch (Throwable e) {
			throw new DataSourceLookupFailureException("Creating secondDatasource Failed!");
		}
	}


	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public DozerBeanMapper dozerBeanMapper() {
		DozerBeanMapper mapper = new DozerBeanMapper();
		return mapper;
	}

	@Bean
	//@DependsOn(value = "dataSource")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();

		entityManagerFactory.setDataSource(dataSource());
		entityManagerFactory.setPackagesToScan(Constants.CORE_DOMAIN_PACKAGE);

		// 캐시 설정
		//entityManagerFactory.setSharedCacheMode(SharedCacheMode.ENABLE_SELECTIVE);

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.MYSQL);
		vendorAdapter.setDatabasePlatform(MySQL5InnoDBDialect.class.getName());

		Properties additionalProperties = new Properties();

		additionalProperties.put(Environment.USE_SECOND_LEVEL_CACHE, false);
		additionalProperties.put(Environment.USE_QUERY_CACHE, false);
		//additionalProperties.put(Environment.CACHE_REGION_FACTORY, SingletonRedisRegionFactory.class.getName());
		//additionalProperties.put(Environment.CACHE_REGION_FACTORY, EhCacheRegionFactory.class.getName());
		//additionalProperties.put(Environment.CACHE_REGION_PREFIX, "hibernate");
		//additionalProperties.put(Environment.GENERATE_STATISTICS, true);
		//additionalProperties.put(Environment.USE_STRUCTURED_CACHE, "true");
		additionalProperties.put(Environment.HBM2DDL_AUTO, "none");
		//additionalProperties.put(Environment.DEFAULT_CACHE_CONCURRENCY_STRATEGY, CacheConcurrencyStrategy.READ_WRITE);
		//additionalProperties.put(Environment.CACHE_PROVIDER_CONFIG, getRedisCacheProperty());

		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
		entityManagerFactory.setJpaProperties(additionalProperties);
		entityManagerFactory.afterPropertiesSet();

		return entityManagerFactory;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public SpringManagedTransactionFactory managedTransactionFactory() {
		SpringManagedTransactionFactory managedTransactionFactory = new SpringManagedTransactionFactory();
		return managedTransactionFactory;
	}

	@Bean
	//@DependsOn(value = "dataSource")
	public SqlSessionFactory sqlSessionFactory(SpringManagedTransactionFactory springManagedTransactionFactory) throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage(Constants.CORE_DOMAIN_PACKAGE);
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/com/axisj/axboot/core/domain/**/*.xml"));
		sqlSessionFactoryBean.setPlugins(new Interceptor[]{new AuditInterceptor()});
		sqlSessionFactoryBean.setTransactionFactory(springManagedTransactionFactory);
		return sqlSessionFactoryBean.getObject();
	}

	@Bean
	//@DependsOn(value = "dataSource")
	public SqlSessionFactory secondSqlSessionFactory(SpringManagedTransactionFactory springManagedTransactionFactory) throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(secondDataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage(Constants.CORE_DOMAIN_PACKAGE);
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/com/axisj/axboot/core/domain/**/*.xml"));
		sqlSessionFactoryBean.setPlugins(new Interceptor[]{new AuditInterceptor()});
		sqlSessionFactoryBean.setTransactionFactory(springManagedTransactionFactory);
		return sqlSessionFactoryBean.getObject();
	}


	@Bean
	//@DependsOn(value = "sqlSessionFactory")
	public MapperScannerConfigurer mapperScannerConfigurer() throws Exception {
		MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
		mapperScannerConfigurer.setBasePackage(Constants.CORE_DOMAIN_PACKAGE);
		mapperScannerConfigurer.setMarkerInterface(MyBatisMapper.class);
		mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
		return mapperScannerConfigurer;
	}

	@Bean
	//@DependsOn(value = "sqlSessionFactory")
	public MapperScannerConfigurer secondMapperScannerConfigurer() throws Exception {
		MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
		mapperScannerConfigurer.setBasePackage(Constants.CORE_DOMAIN_PACKAGE);
		mapperScannerConfigurer.setMarkerInterface(SecondMyBatisMapper.class);
		mapperScannerConfigurer.setSqlSessionFactoryBeanName("secondSqlSessionFactory");
		return mapperScannerConfigurer;
	}

	@Bean(name = "transactionManager") @Qualifier("transactionManager")
	@Primary
	@DependsOn(value = {"entityManagerFactory"})
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) throws ClassNotFoundException {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
		jpaTransactionManager.afterPropertiesSet();
		//jpaTransactionManager.setDefaultTimeout(5);

		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager(secondDataSource());
		//dataSourceTransactionManager.setDefaultTimeout(5);
		ChainedTransactionManager chainedTransactionManager = new ChainedTransactionManager(jpaTransactionManager, dataSourceTransactionManager);

		return chainedTransactionManager;
	}

/*	@Bean(name = "second_transactionManager") @Qualifier("second_transactionManager")
	@DependsOn(value = {"entityManagerFactory"})
	public PlatformTransactionManager second_transactionManager(EntityManagerFactory entityManagerFactory) throws ClassNotFoundException {

		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager(secondDataSource());


		return dataSourceTransactionManager;
	}*/

	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setCorePoolSize(10);
		threadPoolTaskExecutor.setMaxPoolSize(15);
		threadPoolTaskExecutor.setQueueCapacity(25);
		//threadPoolTaskExecutor.setKeepAliveSeconds(5);
		return threadPoolTaskExecutor;
	}

	@Bean
	//@DependsOn(value = "dataSource")
	public SqlMonitoringService sqlMonitoringService() throws Exception {
		SqlMonitoringService sqlMonitoringService = new SqlMonitoringService(dataSource());
		return sqlMonitoringService;
	}
}
