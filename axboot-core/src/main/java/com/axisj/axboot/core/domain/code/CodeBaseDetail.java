package com.axisj.axboot.core.domain.code;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.inspectroom.InspectroomId;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Table(name="CODE_BASE_DETAIL", schema="gwangju")
@ToString(includeFieldNames=true)
public class CodeBaseDetail extends BaseJpaModel<CodeBaseDetailId>{
	
	
	@Id
    @Column(name = "MAST_CD", length=5)
    private String mastCd;
	
	@Id
    @Column(name = "DETAIL_CD", length=2)
    private String detailCd;
	
	@Id
    @Column(name = "DETAIL_NM1", length=50)
    private String detailNm1;	
	
	@Id
    @Column(name = "DETAIL_NM2", length=50)
    private String detailNm2;	
	
	@Id
    @Column(name = "DETAIL_NM3", length=50)
    private String detailNm3;
	
	@Override
	public CodeBaseDetailId getId() {
		// TODO Auto-generated method stub
		return CodeBaseDetailId.of(mastCd, detailCd);
	}


}
