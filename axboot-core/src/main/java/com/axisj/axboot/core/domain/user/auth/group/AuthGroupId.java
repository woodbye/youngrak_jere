package com.axisj.axboot.core.domain.user.auth.group;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class AuthGroupId implements Serializable {

	@NonNull
	private String compCd;

	@NonNull
	private String grpAuthCd;
}
