package com.axisj.axboot.core.domain.version;


import com.axisj.axboot.core.domain.BasicJpaModel;


import lombok.Data;


import javax.persistence.*;


@Entity
@Data
@Table(name = "PROGRAM_VERSION")
public class ProgramVersion extends BasicJpaModel<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3847009411651048232L;
	@Id
	@Column(name = "VERSION")
	private Double version;

	@Override
	public Double getId() {
		// TODO Auto-generated method stub
		return version;
	}


}
