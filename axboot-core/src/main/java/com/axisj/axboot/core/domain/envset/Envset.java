package com.axisj.axboot.core.domain.envset;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.SpringUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name="ENVSET")
@IdClass(EnvsetId.class)
@ToString(includeFieldNames=true)
//@Cacheable
public class Envset extends BasicJpaModel<EnvsetId>{

    @Id
    @Column(name = "WORK_DATE", length = 8)
    private String workDate;
    
    @Id
    @Column(name = "SEQ_NO", precision = 2)
    private Integer seqNo;

    @Column(name = "STDATE", length = 12)
    private String stDate;
    
    //@Column(name = "AREA1_USEYN", length = 1)
    //private String area1Useyn;
    
    //@Column(name = "AREA2_USEYN", length = 1)
    //private String area2Useyn;
  
    //@Column(name = "OP_MODE", length = 1)
    //private String opMode;
    
    @Column(name = "OPEN_TIME", length = 4)
    private String openTime;    
    
    @Column(name = "CLOSE_TIME", length = 4)
    private String closeTime;
    
    @Column(name = "RUNING_TIME", precision = 1)
    private Integer runingTime;
    
    @Column(name = "EXT_TIME", precision = 2)
    private Integer extTime;
    
    @Column(name = "EXT_CNT", precision = 1)
    private Integer extCnt;
  
    @Column(name = "CLEAN_TIME", precision = 2)
    private Integer cleanTime;
    
    @Column(name = "TTS_YN", length = 1)
    private String ttsYn;
    
    @Column(name = "MMS_YN", length = 1)
    private String mmsYn;
    
    @Column(name = "REMARK", length = 200)
    private String remark;
    
    @Column(name = "REGTIME", length = 14)
    private String regTime;
    
    @Column(name = "REGID", length = 20)
    private String regId; 
    
    @Column(name = "UDTTIME", length = 14)
    private String udtTime; 
    
    @Column(name = "UDTID", length = 20)
    private String udtId;  
    
//	@PrePersist, @PreUpdate, @PreRemove, @PreLoad, @PostPersist, @PostUpdate, @PostRemove
    
	@PrePersist
	private void onPersist() {
		/*Sms sms = new Sms();
		sms.setA(1);
		SpringUtils.getBean(SmsService.class).save(sms)*/
		this.regId = this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.regTime = this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	
	@PreUpdate
	private void onUpdate() {
		this.udtId = CommonUtils.getCurrentLoginUserCd();
		this.udtTime = DateUtils.formatToDateString("yyyyMMddHHmmss");
	}
	@PostLoad
	private void onPostLoad() {
	}

	@Override
	public EnvsetId getId() {
		if(seqNo == null){
			try {
				seqNo = SpringUtils.getBean(EnvsetRepository.class).findNextSeqNo(workDate);
			} catch (Exception e) {
			
				e.printStackTrace();
			}
		}
			
		return EnvsetId.of(workDate, seqNo);
		
	}

	
	
}
