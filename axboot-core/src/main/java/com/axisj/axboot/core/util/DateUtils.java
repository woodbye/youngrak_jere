package com.axisj.axboot.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

	private final static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final static SimpleDateFormat julianDateFormat = new SimpleDateFormat("yyyyDDD");
	private final static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
	private final static SimpleDateFormat yyyy = new SimpleDateFormat("yyyy");
	private final static SimpleDateFormat yyyyMmddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");


	public static Date now() {
		return new Date();
	}

	public static Date parseDate(String applyDt) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		return simpleDateFormat.parse(applyDt + "000000");
	}
	
	public static Date parseDate(String format, String dateString) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.parse(dateString);
	}

	public static String formatToDateString(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static String formatToDateString(String format, Date date) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}

	public static String getDateYyyyMmdd() {
		return simpleDateFormat.format(new Date());
	}

	public static String getDateYyyyMmddWithoutDash() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		return simpleDateFormat.format(new Date());
	}

	public static String getDateYyyyMmWithoutDash() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
		return simpleDateFormat.format(new Date());
	}

	public static String getTimeHHmmssWithoutDash() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmmss");
		return simpleDateFormat.format(new Date());
	}

	public static String yesterDay() {
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DATE, -1);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		return simpleDateFormat.format(calendar.getTime());
	}

	public static String getIIACTranDateTime() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
		return simpleDateFormat.format(new Date());
	}

	public static String dateToYyyyMmddString(Date date) {
		return simpleDateFormat.format(date);
	}

	public static Date toYyyyMMddToDate(String date) throws ParseException {
		return yyyyMMdd.parse(date);
	}

	public static Date toyyyyMMddHHmmssToDate(String date) throws ParseException {
		return yyyyMmddHHmmss.parse(date);
	}

	public static String getTodayLastTime() {
		return "235959";
	}
	
}
