package com.axisj.axboot.core.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.springframework.security.crypto.codec.Base64;

public class FileUtils {

	/**
	 * 테스트
	 * 
	 * @param args
	 * @throws Exception
	 */
//	public static void main(String[] args) throws Exception {
//
//		// 디렉토리 경로
//		String path = "D:/STSspace/WorkSpace/STSWorkSpace/WebGLInSpring/src";
//
//		// 소스폴더의 .java 파일들을 리스트에 담는다
//		List<File> files = addFiles(new File(path), new Function<File, Boolean>() {
//			@Override
//			public Boolean apply(File a) {
//				return getExt(a.getName()).equals("java");
//			}
//		});
//
//		// 담은 파일들의 이름
//		for (File f : files) {
//			System.out.println(f.getName());
//		}
//
//	}

	/**
	 * 디렉토리를 제외한 모든 파일을 리스트에 추가
	 * 
	 * @param directory
	 * @return
	 * @throws Exception
	 */
	public static List<File> addFiles(File directory) throws Exception {
		return addFiles(directory, new Function<File, Boolean>() {
			@Override
			public Boolean apply(File a) {
				return true;
			}
		});
	}

	/**
	 * 필터를 적용한 리스트
	 * 
	 * @param directory
	 * @param Function
	 * @return
	 * @throws Exception
	 */
	public static List<File> addFiles(File directory, Function<File, Boolean> Function) throws Exception {
		return addFiles(new ArrayList<File>(), directory, Function);
	}

	/**
	 * 필터를 적용한 리스트
	 * 
	 * @param directory
	 * @param Function
	 * @return
	 * @throws Exception
	 */
	public static List<File> addFiles(List<File> files, File directory, Function<File, Boolean> Function) throws Exception {

		if (files == null) {
			files = new ArrayList<File>();
		}

		if (directory.isDirectory()) {
			for (File f : directory.listFiles()) {
				addFiles(files, f, Function);
			}
		} else {
			if (Function.apply(directory)) {
				files.add(directory);
			}
		}

		return files;
	}

	/**
	 * 확장자
	 * 
	 * @param fileName
	 * @return
	 */

	public static String getExt(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1);
	}

	/**
	 * 
	 * @Method Name  : imageToDataURL
	 * @작성일   : 2015. 12. 26. 오후 3:37:57
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : 이미지를 Data Url 로 변환
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static String imageToDataURL(File file) throws Exception {

		StringBuilder dataURL = new StringBuilder("data: ");

		String ext = getExt(file.getName()).toLowerCase();

		if (ext.equals("jpg") || ext.equals("gif") || ext.equals("bmp") || ext.equals("jpge") || ext.equals("png")) {
			dataURL.append("image/").append(ext).append("; base64, ");
		} else {
			throw new Exception("이미지 파일이 아닙니다.");
		}

		byte[] binaryData = toBinaryData(file);

		dataURL.append(Base64.encode(binaryData));

		return dataURL.toString();
	}

	/**
	 * 
	 * @Method Name  : toBinaryData
	 * @작성일   : 2015. 12. 26. 오후 3:38:17
	 * @작성자   : Lee
	 * @변경이력  :
	 * @Method 설명 : 파일을 binary 데이타로 변환
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static byte[] toBinaryData(File file) throws Exception {

		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));

		int length = (int) file.length();

		byte[] binaryData = new byte[length];

		try {

			bis.read(binaryData);
		} finally {
			bis.close();
		}

		return binaryData;
	}
	
	// 파일삭제
	public static void fileDelete(String deleteFileName) throws Exception {
		String realPath = SpringUtils.getRealPath()+deleteFileName;
		File file = new File(realPath);
		if(file.exists()){
			file.delete();
		}
	}	
}