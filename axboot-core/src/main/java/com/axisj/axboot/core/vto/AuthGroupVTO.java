package com.axisj.axboot.core.vto;

import com.axisj.axboot.core.domain.user.auth.group.AuthGroup;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@NoArgsConstructor
@ApiModel(value = "AuthGroupVTO", description = "권한 그룹 정보")
public class AuthGroupVTO {

	@ApiModelProperty(value = "권한그룹코드")
	private String grpAuthCd;

	@ApiModelProperty(value = "권한그룹명")
	private String grpAuthNm;

	@ApiModelProperty(value = "비고")
	private String remark;

	@ApiModelProperty(value = "권한구분('1':본사용,'2':직영점용,'3':가맹점용)")
	private String grpFg;

	public static AuthGroupVTO of(AuthGroup authGroup) {
		AuthGroupVTO authGroupVTO = DozerBeanMapperUtils.map(authGroup, AuthGroupVTO.class);
		return authGroupVTO;
	}

	public static List<AuthGroupVTO> of(List<AuthGroup> authGroupList) {
		return authGroupList.stream().map(authGroup -> of(authGroup)).collect(toList());
	}
}
