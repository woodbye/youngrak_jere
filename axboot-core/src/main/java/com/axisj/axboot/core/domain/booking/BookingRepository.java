package com.axisj.axboot.core.domain.booking;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends JpaRepository<Booking, BookingId> {

	@org.springframework.cache.annotation.Cacheable
	Booking findOne(BookingId id);
	
	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	Page<Booking> findByBookDate(String bookDate, Pageable pageable) throws SQLException;
	
	Booking findFirstByBookDateAndRoomCdAndStatusOrderByBookSeqDesc(String bookDate, String roomCd, String status) throws SQLException;
	
	Booking findFirstByBookDateOrderByBookSeqDesc(String formatToDateString) throws SQLException;	

	Booking findFirstByBookDateAndRoomCdAndEdTimeGreaterThanEqualAndStatusOrderByEdTimeDesc(String bookDate, String roomCd, String stTime ,String status) throws SQLException;

	@Query(value=" SELECT A.* "
			+ "FROM BOOKING A "
			+ " INNER JOIN  (SELECT MAX(ED_TIME) AS ED_TIME, BOOK_DATE, BOOK_SEQ "
							+ "FROM BOOKING A "
							+ "WHERE A.BOOK_DATE = ?1 AND A.ED_TIME+?3 >= ?2 "
							+ "AND A.STATUS = 'R' GROUP BY BOOK_DATE, ROOM_CD) B "
			+ "ON A.BOOK_DATE = B.BOOK_DATE "
			+ "AND A.ED_TIME = B.ED_TIME GROUP BY ROOM_CD"
			, nativeQuery=true)
	List<Booking> selectRoomByLastEdTime(String bookDate,  String edTime, int cleanTime);
	
	Booking findFirstByBookDateAndContidAndEdTimeGreaterThanAndStatus(String bookDate, String conttid, String stTime, String status) throws SQLException;

	Page<Booking> findByBookDateAndEdTimeGreaterThanEqualAndStatus(String bookDate, String edTime, String string,Pageable pageable) throws SQLException;

	List<Booking> findByBookDateAndStatusAndStTimeGreaterThanEqual(String bookDate, String status,	String formatToDateString) throws SQLException;

	Booking findByBookDateAndBookSeq(String bookDate, Integer bookSeq) throws SQLException;
	
	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	@org.springframework.cache.annotation.Cacheable
	Booking findFirstByBookDateAndStatusAndEdTimeGreaterThanAndRoomCdOrderByEdTimeAsc(String bookDate, String string,	String edTime, String roomCd) throws SQLException;

	@Query(value="SELECT f_remain_time(?1) FROM DUAL" , nativeQuery=true) 
	String selectRemainTime(String edTime) throws SQLException;

	//@QueryHints(value=@QueryHint(name="org.hibernate.cacheable",value="true"))
	@org.springframework.cache.annotation.Cacheable
	@Query(value="SELECT *"
			+ " FROM BOOKING "
			+ " WHERE BOOK_DATE = ?1"
			+ " AND DATE_FORMAT(STR_TO_DATE(?2,'%Y%m%d%H%i'),'%H%i')"
			+ " BETWEEN  DATE_FORMAT(STR_TO_DATE(CONCAT(BOOK_DATE,ST_TIME),'%Y%m%d%H%i'),'%H%i')"
			+ " AND DATE_FORMAT(STR_TO_DATE(CONCAT(BOOK_DATE,F_ADD_TIME(ED_TIME,?3-1)),'%Y%m%d%H%i'),'%H%i')"
			+ " AND ROOM_CD = ?4 AND STATUS = 'R'" , nativeQuery=true) 
	Booking selectBooking(String bookDate, String stDate, Integer cleanTime, String roomCd) throws SQLException;

	
	

}
