package com.axisj.axboot.core.vto;

import static java.util.stream.Collectors.toList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.util.ObjectUtils;

import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.axisj.axboot.core.util.SpringUtils;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@ApiModel(value = "BookingVTO", description = "")
public class BookingVTO {

	@ApiModelProperty(value = "예약 일자")
	private String bookDate;
    
	@ApiModelProperty(value = "연번")
	private Integer bookSeq;
	
	@ApiModelProperty(value = "제례실 코드")
	private String roomCd;
	
	@ApiModelProperty(value = "제례실명")
	private String roomNm;
	
	@ApiModelProperty(value = "시작 시간")
	private String stTime;
	
	@ApiModelProperty(value = "연장 횟수")
	private Integer exCnt;
	
	@ApiModelProperty(value = "연장 시간")
	private Integer exMinute;
	
	@ApiModelProperty(value = "종료 시간")
	private String edTime;
	
	@ApiModelProperty(value = "사진 사용 여부")
	private String photoYn;
	
	@ApiModelProperty(value = "사진 ID")
	private String contid;

	@ApiModelProperty(value = "고인명")
	private String conttitle;
	
	@ApiModelProperty(value = "핸드폰 번호")
	private String phoneNo;
	
	@ApiModelProperty(value = "SMS 수신 여부")
	private String smsYn;
	
	@ApiModelProperty(value = "예약/취소 상태")
	private String status;
	
	@ApiModelProperty(value = "예약자")
	private String bookMan;
	
	@ApiModelProperty(value = "예약 비고")
	private String bookBigo;
	
	@ApiModelProperty(value = "취소자")
	private String cancelMan;
	
	@ApiModelProperty(value = "취소 비고")
	private String cancelBigo;
	
	@ApiModelProperty(value = "등록시간")
	private String regtime;
	
	@ApiModelProperty(value = "등록자")
	private String regid;
	
	@ApiModelProperty(value = "수정시간")
	private String udttime;
	
	@ApiModelProperty(value = "수정자")
	private String udtid;
	
	@ApiModelProperty(value = "룸리스트 대기시간")	
	private String getRemainTime;
	
	@ApiModelProperty(value = "예약 후 시작까지 대기시간")
	private String getRemainTime2;
	
	private MdcontentsVTO mdcontentsVTO;
	
	public String getRemainTime() throws ParseException{
		if(edTime == null){
			 return "";
		 }
		
		String strNow = DateUtils.formatToDateString("HHmm");		
		SimpleDateFormat fm = new SimpleDateFormat("HHmm");
		
		 long now = fm.parse(strNow).getTime();		 
		 long lastTime = fm.parse(edTime).getTime();
		 long diff = lastTime - now;
		 long diffTime = diff / 1000 / 60;
		 String result =diffTime+"";
         						
		
		return result;
	}
	//현재 시간보다 대기시간이 10분이상이면 문자발송여부 체크박스 띄우기 위해
	public String getRemainTime2() throws ParseException{
		if(stTime == null){
			 return "";
		 }
		
		String strNow = DateUtils.formatToDateString("HHmm");		
		SimpleDateFormat fm = new SimpleDateFormat("HHmm");
		
		 long now = fm.parse(strNow).getTime();		 
		 long lastTime = fm.parse(stTime).getTime();
		 long diff = lastTime - now;
		 if(diff < 0){
			 return "0";
		 }
		 long diffTime = diff / 1000 / 60;
		 String result =diffTime+"";
         						
		
		return result;
	}
	
	public String getTitle(){
		
		if(this.stTime == null || this.edTime == null){
			return "";
		}
		
		// 사용중/제례종료
		long currentTime = new Date().getTime();
		try {
			String yyyyMMdd = DateUtils.formatToDateString("yyyyMMdd");
			long stTime = DateUtils.parseDate("yyyyMMddHHmm", yyyyMMdd+this.stTime).getTime();
			long edTime = DateUtils.parseDate("yyyyMMddHHmm", yyyyMMdd+this.edTime).getTime();
			
			if(currentTime < stTime){
				return conttitle + "(" + this.stTime.charAt(2) + this.stTime.charAt(3) + ")";
			}else if(currentTime > stTime && currentTime < edTime){
				return "사용중";
			}else if(currentTime > edTime){
				return "제례종료";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	public static BookingVTO of(Booking booking) {
		if(!CommonUtils.canConvertMappingVTO(booking)){		
			return null;
		}
		
		BookingVTO bookingVTO = DozerBeanMapperUtils.map(booking, BookingVTO.class);
		
		if(CommonUtils.canConvertMappingVTO(booking.getRoom())){
			bookingVTO.setRoomNm(booking.getRoom().getRoomNm());
		}
		if(CommonUtils.canConvertMappingVTO(booking.getMdcontents())){
			bookingVTO.setMdcontentsVTO(MdcontentsVTO.of(booking.getMdcontents()));
		}
			
		return bookingVTO;
	}

	public static List<BookingVTO> of(List<Booking> bookingList) {
		return bookingList.stream().map(booking -> of(booking)).collect(toList());
	}

}
