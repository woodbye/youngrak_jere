package com.axisj.axboot.core.domain.mdcontents;

import java.text.ParseException;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.apache.ibatis.type.Alias;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.axisj.axboot.core.domain.BasicJpaModel;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectId;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Alias("mdcontents")
@Table(name="MDCONTENTS")
@ToString(includeFieldNames=true)
//@Cacheable
public class Mdcontents extends BasicJpaModel<String>   {
	
	@Id
	@Column(name="ContID", length=20)
	private String contId;
	
	@Column(name="ContType", length=20)
	private String contType;
	
	@Column(name="ContTitle", length=128)
	private String contTitle;
	
	@Column(name="ContFile", length=255)
	private String contFile;
	
	@Column(name="ContURL", length=255)
	private String contURL;
		
	@Column(name="ContDate", length=10)
	private String contDate;
	
	@Column(name="ContRegUser", length=64)
	private String contRegUser;
	
	@Column(name="ContRealFileName", length=255)
	private String contRealFileName;
	//@Column(name="ContOriFileName" length=100)
	//private String contOriFileName;
	
	/*@Override
	public MdcontentsId getId() {
		// TODO Auto-generated method stub
		return MdcontentsId.of(contId);
	}*/
	
	@PrePersist
	private void onPersist() throws ParseException {		
		//this.contRegUser = this.contRegUser = CommonUtils.getCurrentLoginUserCd();	
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return contId;
	}

}
