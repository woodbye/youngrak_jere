package com.axisj.axboot.core.domain.company;

import com.axisj.axboot.core.domain.BaseJpaModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "COMP_M")
public class Company extends BaseJpaModel<String> {

	private static final long serialVersionUID = -3999913853683458569L;

	@Id
	@Column(name = "COMP_CD", length = 10)
	private String compCd;

	@Column(name = "COMP_NM", length = 50)
	private String compNm;

	@Column(name = "COMP_CEO", length = 30)
	private String compCeo;

	@Column(name = "COMP_REGNO", length = 10)
	private String compRegno;

	@Column(name = "COMP_CONO", length = 20)
	private String compCono;

	@Column(name = "TEL_NO", length = 50)
	private String telNo;

	@Column(name = "ZIP_NO", length = 6)
	private String zipNo;

	@Column(name = "ADDR1", length = 100)
	private String addr1;

	@Column(name = "ADDR2", length = 100)
	private String addr2;

	@Column(name = "USE_YN", length = 1)
	private String useYn;

	@Override
	public String getId() {
		return compCd;
	}

	public static Company of(String name) {
		Company company = new Company();
		company.setCompCd("");
		company.setCompNm(name);
		return company;
	}
}
