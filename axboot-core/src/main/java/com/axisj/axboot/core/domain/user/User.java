package com.axisj.axboot.core.domain.user;

import com.axisj.axboot.core.code.Params;
import com.axisj.axboot.core.domain.BaseJpaModel;
import com.axisj.axboot.core.domain.user.auth.UserAuth;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "USER_M")
public class User extends BaseJpaModel<String> {

	@Id
	@Column(name = "USER_CD", length = 10)
	private String userCd;

	@Column(name = "USER_NM", length = 30)
	private String userNm;

	@Column(name = "USER_PS", length = 128)
	private String userPs;

	@Column(name = "COMP_CD", length = 10)
	private String compCd;

	@Column(name = "EMAIL", length = 50)
	private String email;

	@Column(name = "HP_NO", length = 15)
	private String hpNo;

	@Column(name = "USER_TYPE", length = 1)
	private String userType;

	@Column(name = "REMARK", length = 200)
	private String remark;

	@Column(name = "USE_YN", length = 1)
	private String useYn = Params.Y;

	@Column(name = "LAST_LOGIN_DATE")
	private Date lastLoginDate;

	@Column(name = "PASSWORD_UPDATE_DATE")
	private Date passwordUpdateDate = new Date();

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<UserAuth> userAuthList;

	@Override
	public String getId() {
		return userCd;
	}
}
