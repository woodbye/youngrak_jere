package com.axisj.axboot.core.domain.envset;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.axisj.axboot.core.domain.inspect.InspectId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;


@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName="of")
@ToString(includeFieldNames=true)
public class EnvsetId implements Serializable {

	@NonNull
	private String  workDate;
	@NonNull
	private int seqNo;
}
