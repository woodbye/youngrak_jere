package com.axisj.axboot.core.domain.user;

import com.axisj.axboot.core.code.Params;
import com.axisj.axboot.core.domain.BaseService;
import com.axisj.axboot.core.util.ArrayUtils;
import com.axisj.axboot.core.util.HashUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class UserService extends BaseService<User, String> {

	private UserRepository userRepository;

	@Inject
	public UserService(UserRepository userRepository) {
		super(userRepository);
		this.userRepository = userRepository;
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	public void updateUser(List<User> users) {
		if (!ArrayUtils.isEmpty(users)) {
			for (User user : users) {

				User originalUser = userRepository.findOne(user.getUserCd());

				if (originalUser != null) {
					if (!StringUtils.isEmpty(user.getUserPs())) {
						user.setPasswordUpdateDate(new Date());
						user.setUserPs(HashUtils.SHA512(user.getUserPs()));
					} else {
						user.setPasswordUpdateDate(originalUser.getPasswordUpdateDate());
						user.setUserPs(originalUser.getUserPs());
					}
				} else {
					user.setUserPs(HashUtils.SHA512(user.getUserPs()));
				}
				save(user);
			}
		}
	}

	public Page<User> findByCompCd(String compCd, PageRequest request) {
		return userRepository.findByCompCd(compCd, request);
	}

	public List<User> findByCompCd(String compCd) {
		return userRepository.findByCompCd(compCd);
	}

	public List<User> findByUserType(String userType) {
		return userRepository.findByUserType(userType);
	}

	public List<User> findByCompCdAndUserTypeAndUserCd(String compCd, String userType, String userCd) {
		List<Predicate> predicates = new ArrayList<>();

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();

		Root<User> user = criteriaQuery.from(User.class);

		if (!StringUtils.isEmpty(compCd)) {
			predicates.add(criteriaBuilder.equal(user.get(Params.COMP_CD), compCd));
		}

		if (!StringUtils.isEmpty(userType)) {
			predicates.add(criteriaBuilder.equal(user.get(Params.USER_TYPE), userType));
		}

		if (!StringUtils.isEmpty(userCd)) {
			predicates.add(criteriaBuilder.equal(user.get(Params.USER_CD), userCd));
		}

		criteriaQuery = criteriaQuery.select(user).where(predicates.toArray(new Predicate[]{})).orderBy(criteriaBuilder.asc(user.get(Params.COMP_CD)), criteriaBuilder.asc(user.get(Params.USER_CD)));

		return em.createQuery(criteriaQuery).getResultList();
	}

	public void updateMyInfo(User user) {
		AdminLoginUser adminLoginUser = null;

		User existUser = findOne(adminLoginUser.getUsername());

		existUser.setEmail(user.getEmail());

		if (!StringUtils.isEmpty(user.getUserPs())) {
			existUser.setUserPs(HashUtils.SHA512(user.getUserPs()));
			existUser.setPasswordUpdateDate(new Date());
		}

		existUser.setUserNm(user.getUserNm());
		existUser.setHpNo(user.getHpNo());

		save(existUser);
	}

	public void updateLastLoginDateTime(String userCd) {
		User user = findOne(userCd);
		user.setLastLoginDate(new Date());
		save(user);
	}
}
