AX-BOOT
=======
SpringBoot & AXISJ & AXU based Web Application Package.

```
axboot-admin : SpringBoot & AXU based Admin Application Package
```

```
axboot-api : SpringBoot & Swagger based RESTful API Application Package
```

```
axboot-batch : SpringBoot based Batch Application Package
```

```
axboot-core : AX-Boot Core Package (based SpringDataJPA(Hibernate 4.3.5), QueryDSL, Mybatis 3.2.8, HikariCP, Logback)
```


###Project Import [Intelli-J]
```
IntelliJ -> Import Project -> Maven
```

###Compile Settings
```
Open IntelliJ Preference
    - Version Control -> Git
    - Build, Execution, Deployment -> Compiler
        -> Check 'Make project automatically'
        -> Annotation Processors -> Check 'Enable Annotation processing' 
```

###Tomcat
```
Open Edit Configuration (located Toolbar)
    - Press [+]
    - Select Tomcat Server -> Local
    - Unchecking After launch
    - Setting Name & Port
    - Move Deployment tab -> Press [+]
        -> Artifact -> Select exploded war
```

### Environment
- Java 8
- Spring Boot 1.2.4
- Gradle 2.4
- Maven 3.3.3
- Java Server Page (JSP)

### Supported Database
- MySQL (MariaDB) [recommended]
- Oracle [test completed on Oracle11g]
- PostgreSQL [test completed on 9.3.x]
- MSSQL [test completed on SQL2005]

### TODO
- AX-API Integeration
- View Adapting (eg. Freemarker)