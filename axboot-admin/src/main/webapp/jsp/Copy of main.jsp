<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>
<ax:layout name="base.jsp">
    <ax:set name="title" value="메인페이지" />
    <ax:div name="css">
        <style type="text/css">

        </style>
    </ax:div>
	<ax:div name="header">
        <h1 id="cx-page-title" style="margin-left: 0px;"><i class="axi axi-dvr"></i>  대시보드</h1>
		<p class="desc">메인페이지</p>
	</ax:div>
	<ax:div name="contents">
		<ax:row>
			<ax:col size="12">

                <ax:custom customid="table">
                    <ax:custom customid="tr">
                        <ax:custom customid="td">

                            <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-notifications"></i> 공지사항</h2>
                                </div>
                                <div class="right">
                                    <!--
                                    <button type="button" class="AXButton Classic" id="notice-refresh" onclick="fnObj.grid.reload();"><i class="axi axi-ion-refresh"></i> 새로고침</button>
                                    <button type="button" class="AXButton Classic" id="notice-more" onclick="app.link_to('/jsp/system/system-operation-notice.jsp');">목록가기</button>
                                    -->
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                            <div class="ax-grid" id="grid-notice" style="height:230px;"></div>

                        </ax:custom>
                        <ax:custom customid="td" style="width:40%;">

                            <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-phone"></i> 서비스 담당자</h2>
                                </div>
                                <div class="right">
                                    <button type="button" class="AXButton" id=""><i class="axi axi-book3"></i>&nbsp;시스템 매뉴얼</button>
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                            <table cellpadding="0" cellspacing="0" class="AXGridTable">
                                <colgroup>
                                    <col width="60" />
                                    <col width="110" />
                                    <col />
                                </colgroup>
                                <thead>
                                    <tr align="center">
                                        <td>
                                            <div class="tdRel">
                                                구분
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tdRel">
                                                적용시간대
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tdRel">
                                                전화번호
                                            </div>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td align="center" rowspan="3">
                                            협력사
                                        </td>
                                        <td align="center">
                                            평일 영업시간<br/>
                                            (9시 ~ 18시)
                                        </td>
                                        <td align="center" rowspan="3">
                                            XXXX-XXXX
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            평일 영업시간 외
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            주말 / 공휴일
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" rowspan="3">
                                            본사
                                        </td>
                                        <td align="center">
                                            평일 영업시간<br/>
                                            (9시 ~ 18시)
                                        </td>
                                        <td align="center" rowspan="3">
                                            시스템 : OOO 대리 <br/>
                                            02-****-**** / 010-****-****
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            평일 영업시간 외
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            주말 / 공휴일
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </ax:custom>
                    </ax:custom>
                </ax:custom>

			</ax:col>
		</ax:row>
        <ax:row>
            <ax:col size="12" style="min-height:100px;">

                <ax:custom customid="table">
                    <ax:custom customid="tr">
                        <ax:custom customid="td">

                            <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-line-chart"></i> 주간 매출 추이</h2>
                                </div>
                                <div class="right">
                                    <span class="chart-legend" id="chart-001-legend"></span>
                                    <span class="chart-legend">(단위 : 천원)</span>
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                            <div style="padding-right:10px;">
                                <canvas id="chart-001" width="300" height="100">
                                    차트를 지원하지 않는 브라우저 입니다. IE9 이상의 브라우저를 사용해 주세요.
                                </canvas>
                            </div>

                        </ax:custom>
                        <ax:custom customid="td" style="width:40%;">

                            <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-pie-chart"></i> 주간 품목별매출 Top 5</h2>
                                </div>
                                <div class="right">
                                    <span class="chart-legend">(단위 : 천원)</span>
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                            <div style="padding-right:10px;">
                                <canvas id="chart-002" width="200" height="90">
                                    차트를 지원하지 않는 브라우저 입니다. IE9 이상의 브라우저를 사용해 주세요.
                                </canvas>
                            </div>

                            <div class="ax-button-group">
                                <div class="right">
                                    <span class="chart-legend" id="chart-002-legend"></span>
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                        </ax:custom>
                    </ax:custom>
                </ax:custom>

            </ax:col>
        </ax:row>
        <ax:row>
            <ax:col size="12">
                <div class="ax-button-group">
                    <div class="left">
                        <h2><i class="axi axi-list-alt"></i> 금일 매출속보</h2>
                    </div>
                    <div class="right">
                        <!--
                        <button type="button" class="AXButton" id="notice-refresh"><i class="axi axi-ion-refresh"></i> 새로고침</button>
                        <button type="button" class="AXButton" id="notice-more">목록가기</button>
                        -->
                    </div>
                    <div class="ax-clear"></div>
                </div>

                <div class="ax-grid" id="grid-sales-anal" style="height:400px;"></div>
            </ax:col>
        </ax:row>

	</ax:div>
	<ax:div name="scripts">
        <script>
        	// location.href = "${pageContext.request.contextPath}/jsp/resv/RESV1010.jsp"
            // 기타매출 컬럼 정보
            /*
            etcColumns =
            [
                {"key":"etc1Amt","label":"기타결제 A","width":"80", "align":"right"},
                {"key":"etc2Amt","label":"상품권","width":"80", "align":"right"},
                {"key":"etc3Amt","label":"기타결제 3","width":"80", "align":"right"}
            ];
            */
        </script>
        <script type="text/javascript" src="<c:url value='/static/js/view/main.js' />"></script>
	</ax:div>
</ax:layout>
