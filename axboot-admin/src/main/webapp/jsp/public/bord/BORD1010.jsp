<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>현황판</title>
<style type="text/css">
@font-face {
	font-family: '034';
	src: url('${pageContext.request.contextPath}/font/034.woff')
}

@font-face {
	font-family: 'Hanna';
	font-style: normal;
	font-weight: 400;
	src: url(${pageContext.request.contextPath}/static/font/BM-HANNA.eot);
	src:
		url(${pageContext.request.contextPath}/static/font/BM-HANNA.eot?#iefix)
		format('embedded-opentype'),
		url(${pageContext.request.contextPath}/static/font/BM-HANNA.woff2)
		format('woff2'),
		url(${pageContext.request.contextPath}/static/font/BM-HANNA.woff)
		format('woff'),
		url(${pageContext.request.contextPath}/static/font/BM-HANNA.ttf)
		format('truetype');
}

html, body{
	width: 100%;
	height: 100%;
	min-height: 100%;
	padding: 0px;
	border: 0px;
	margin: 0px;
	overflow: hidden;
    cursor: none;
}

body, label, th, td, input {
	font-family: 'Hanna';
	font-size: 40px;
}

table {
	table-layout: fixed;
	white-space: nowrap;
}

td, th {
	white-space: nowrap;
	overflow: visible;
	text-overflow: ellipsis;
	padding: 0px;
}

label {
	display: inline-block;
}
</style>
    <script type="text/javascript" src="<c:url value='/static/plugins/jquery/jquery.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/axisj/dist/AXJ.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/chartjs/Chart.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/ax5-polyfill.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/modal.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/app.js' />"></script>
<script type="text/javascript">

$(document).ready(function(){
	fnObj.getSetting();
});



var fnObj = {
		// 개방여부
		openYN : null,
		// 환경설정 값
		envSet : null,
		// 현재시간
		now : null,
		// 예약 가능 시간
		fastestTime : null,
		// 화면에 출력할 정보 모음
		table : {
			// 헤더 colspan 값
			colspan : null,
			// 금일 제례 정보
			bookingInfo : null,
			// 제례실
			rooms : null
		},
		// 30초 한번씩 현황판 페이지 출력 내용 및 환경설정 값 요청하는 함수
		getSetting : function(){
			$.ajax({
				type: "GET",
				dataType : 'json',
			    url: "/api/v1/public/BORD1010/selectSetting",
		        success:function(res){
		        	
		        	fnObj.openYN =res.mapResponse.map.openYN;    
		        	fnObj.envSet =res.mapResponse.map.envset;    
		        	fnObj.now =res.mapResponse.map.now;
		        	fnObj.fastestTime =res.mapResponse.map.fastestTime;
		        	
		        	fnObj.table.colspan =res.mapResponse.map.colspan;
		        	fnObj.table.rooms =res.mapResponse.map.rooms;
		        	fnObj.table.bookingInfo =res.mapResponse.map.bookingInfo;
		        	
	        		if($("#pageId").val() == "BORD1010"){
			        	if(fnObj.openYN == 'N'){
			        		fnObj.goUrl("/jsp/public/romr/notOpen.jsp",function(res){
					        	$('body').html(res);
					        });
		        		}else{
		        			fnObj.outputInfo();
		        		}
	        		}
	        		else if($("#pageId").val() == "notOpen"){
	        			if(fnObj.openYN == 'Y'){
	        				window.location.reload();
		        		}
	        		}
		        	
		        },
				complete : function (){				
					setTimeout(fnObj.getSetting,30000);
				}
		    });
		},
		// 페이지 전환
		goUrl : function(url, callback){
			$.ajax({
				type: "GET",
				dataType : 'text',
			    url: url,
		        success:callback
		    });
		},
		// 정보 출력 담당
		outputInfo : function(){
			
			$("#lb-currentTime").html(fnObj.now);
			$("#lb-fastestTime").html(fnObj.fastestTime);
			$("#thead-listHeader").html(fnObj.headerBuild());
			$("#tbody-listBody").html(fnObj.bodyBuild());
			fnObj.relativeFontSize(document.body);
		},
		// 출력 헤더 html 만들기
		headerBuild : function(){
			var headerHtml =
					'<tr style="height:0px;">'
		            + '<td width="284" height="0"></td><td width="174"></td><td width="174"></td><td width="177"></td><td width="176"></td>'
		            + '<td width="173"></td><td width="176"></td><td width="176"></td><td width="173"></td><td width="177"></td></tr>'
					+ '<tr><td width="280" height="109">&nbsp;</td>';
			var colspan=0;
			for(var key in fnObj.table.bookingInfo){
				if(colspan >= 9){
					break;
				}
				var colnum = (fnObj.table.colspan[key] == 0 ? 1: fnObj.table.colspan[key]);
				headerHtml += '<td align="center" style="background:url(/images/kiosk/kiosk_08_06.gif) 100% 100%; border-right:1px solid black;" colspan="' 
									+ colnum + '">' + key + '시</td>';
				colspan+=+colnum;
			}
			headerHtml += '</tr>';
			
			return headerHtml;
		},
		// 출력 바디 html 만들기
		bodyBuild : function(){
			var bodyHtml = '';
			for(var i=0; i<10; i++){
				var roomNm = ''
				var roomCd = '';
				if(fnObj.table.rooms[i]){
					roomNm = fnObj.table.rooms[i].roomNm;
					roomCd = fnObj.table.rooms[i].roomCd;
				}
				if(roomCd == ''){
					bodyHtml+='<tr><td align="center" height="62"><label size="0.6">' + roomNm + '</label></td></tr>';
				}else{
					bodyHtml+='<tr><td align="center" height="62"><label size="0.6">' + roomNm + '</label></td>';
					for(var key in fnObj.table.bookingInfo){
						var limit = fnObj.table.bookingInfo[key][roomCd].length == 0 ? 1 : fnObj.table.bookingInfo[key][roomCd].length;
						for(var j=0; j<limit; j++){
							var info = fnObj.table.bookingInfo[key][roomCd][j];
							if(info){
								bodyHtml += '<td align="center"><label size="0.6">'+info.title+'</label></td>';							
							}else{
								bodyHtml += '<td align="center"></td>';
							}
						}
					}
		            bodyHtml += '</tr>';
				}
				
			}
			return bodyHtml;
		},
		// 초기 폰트 사이즈 설정
		setFontSize : function(){

			var fontheight = document.body.clientHeight*0.15/2;

			var labels = document.getElementsByTagName('label');
			
			for(var i=0; i<labels.length; i++){
	 			if(labels[i].getAttribute('size')){
	 				labels[i].style.fontSize = (fontheight*labels[i].getAttribute('size'))+'px';
	 			}else{
	 				labels[i].style.fontSize = fontheight+'px';
	 			}
			}
		},
		// 출력값을 제대로 보여주기 위한 상대적 폰트 사이즈로 변경
		relativeFontSize : function(el){
			fnObj.setFontSize();
			do {
				var canChangeFontSize = false;
				var labels = el.querySelectorAll('label');
				for(var i=0; i<labels.length; i++){
					var parNodeWidth = labels[i].parentNode.clientWidth;
					var childNodeWidth = labels[i].clientWidth;
					if(parNodeWidth <= childNodeWidth){
						labels[i].style.fontSize = (+labels[i].style.fontSize.replace('px', '') - 1) + 'px';
						canChangeFontSize = true;
					}
				}
			}while(canChangeFontSize);
		}
	
}


</script>


</head>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<!-- ImageReady Slices (kiosk_08.PSD) -->
<div align="center">
  <TABLE WIDTH=1920 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	  <TR>
		  <TD height="130" background="/images/kiosk/kiosk_08_01.gif"></TD>
	  </TR>
	  <TR>
		  <TD height="64" background="/images/kiosk/kiosk_08_02.gif"><table width="1920" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="267" height="64">&nbsp;</td>
              <td width="258" align="center"><lebel id="lb-currentTime" style="font-size: 45px;"></lebel></td>
              <td width="382">&nbsp;</td>
              <td width="259" align="center"><lebel id="lb-fastestTime" style="font-size: 45px;"></lebel></td>
              <td width="225">&nbsp;</td>
              <td width="473" align="center"><lebel style="font-size: 45px;">고인명(시작시간)</lebel></td>
              <td width="56">&nbsp;</td>
            </tr>
          </table></TD>
	  </TR>
	  <TR>
		  <TD height="34" background="/images/kiosk/kiosk_08_03.gif"></TD>
	  </TR>
	  <TR>
		  <TD height="733" background="/images/kiosk/kiosk_08_04.gif"><table style="margin-top: -8px" width="1856" height="730" border="0" align="center" cellpadding="0" cellspacing="0" >
		  <thead id="thead-listHeader"">
            </thead>
            <tbody id="tbody-listBody">
            </tbody>
          </table>
          </TD>
	  </TR>
	  <TR>
		  <TD height="119" background="/images/kiosk/kiosk_08_05.gif"></TD>
	  </TR>
  </TABLE>
  <!-- End ImageReady Slices -->
  <input type="hidden" id="pageId" value="BORD1010">
</div>
</BODY>
</HTML>