<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
    <script type="text/javascript" src="<c:url value='/static/plugins/jquery/jquery.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/axisj/dist/AXJ.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/chartjs/Chart.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/ax5-polyfill.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/modal.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/app.js' />"></script>

<style type="text/css">
input[type="checkbox"] {
			display: inline-block;
			width: 19px;
			height: 19px;
			vertical-align: middle;
			background: url(${pageContext.request.contextPath}/images/checkbox-unchecked.png) no-repeat 0 0;
			background-size: 30px 30px;
			-webkit-appearance: none;
			border-radius: 0;
			border: 0;
			
			
		}
		
		input[type="checkbox"]:checked {
			display: inline-block;
			width: 19px;
			height: 19px;
			vertical-align: middle;
			background: url(${pageContext.request.contextPath}/images/checkbox-checked.png) no-repeat 0 0;
			background-size: 30px 30px;
			-webkit-appearance: none;
			border-radius: 0;
			border: 0;
			
		}
/* 마스크 뛰우기 */
 #mask {  
   position:absolute;  
   z-index:9000;  
   background-color:#000;  
   display:none;  
   left:0;
   top:0;
 } 
 .phoneChk {width: 50px; height: 30px; padding-left: 10px; padding-top: 5px;}
/*
 팝업으로 뜨는 윈도우 css  
*/ 
 .window{
   display: none;
   position:absolute;  
   left:60%;
   top:200px;
   margin-left: -700px;
   width:1000px;
   height:576px;
   background-color:#FFF;
   z-index:10000;   
 }
 @font-face {
	font-family: 'Noto Sans Korean';
	src: url('${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.woff')
}



@font-face {
    font-family: 'Noto Sans Korean';
    font-style: normal;
    font-weight: 200;
    src: 
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.eot),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.eot?#iefix) format('embedded-opentype'),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.woff) format('woff'),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.otf) format('opentype');
}

html, body{
	width: 100%;
	height: 100%;
	min-height: 100%;
	padding: 0px;
	border: 0px;
	margin: 0px;
	overflow: hidden;
     cursor: none;  
}
input {border :none; text-align: center;}

body, label, th, td, input {

	font-family: 'Noto Sans Korean';
	font-size: 40px;
	overflow:  hidden;

}

table {
	table-layout: fixed;
	white-space: nowrap;
}

td, th {
	white-space: nowrap;
	overflow: visible;
	text-overflow: ellipsis;
	padding: 0px;
}

label {
	display: inline-block;
}

img {
	border: none;
	padding: 0;
	margin: 0;
}
	
#time1 {font-size:20px; padding-top:25px}	
#time2 {font-size:20px; padding-bottom:15px}	
#time3 {font-size:35px; padding-left:25px; padding-top: 10px;}	

</style>
<script type="text/javascript">

$(document).ready(function(){

	
	setInterval(countTime, 1000);
	fnObj.getSetting();
	
	//fnObj.getSetting();
	 $('body').on("selectstart", function(event){ return false; });
	 $('body').on("dragstart", function(event){ return false; });
	
	//$("#wrap").css("height",$(window).height());
});
var fnObj = {
		envSet : null,			
		openYN	: "Y",
		time : null,
		//version : 0,	
		roomCd : "${roomCd}",
		inpectKiosk : null,
		deadInfo : function(){
			
		
			this.contTitle = "";
			this.contId = "";		
			this.bookSeq = "";
			this.bookDate = "";
		},
	
		getSetting : function(){
			var _this = this;		
			
			$.ajax({
				type: "GET",
				dataType : 'json',
			    url: "/api/v1/public/ROMR1010/selectSetting",
		        success:function(res){
		        	if(res.error){
		        		setTimeout(refreshThisPage, 5000);
		        	}else{
		        		fnObj.envSet =res.map.envset;    
			        	fnObj.time = res.map.time.split(","); 
			        	fnObj.openYN = res.map.openYN;
			        	fnObj.inpectKiosk = res.map.inpectKiosk;
			        	//if(fnObj.version != 0 && fnObj.version < res.map.version ){
			        	//  location.reload();
			        	//}		        	
			        	//fnObj.version = res.map.version;			        	
			        	var pageId = $('#pageId').val();
						eval(pageId+'()');
		        	}
		        },
				complete : function (){				
					//setTimeout(fnObj.getSetting,5000);					
				}
		        /* ,error:function(request,status,error){
		        	setTimeout(refreshThisPage, 5000);
		        	
		        	
		        	// alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }  */
		    }).always(function() {
		    	setTimeout(fnObj.getSetting, 5000);
		    });
		},
		// 초기 폰트 사이즈 설정
		setFontSize : function(){

			var fontheight = document.body.clientHeight*0.15/2;

			var labels = document.getElementsByTagName('label');
			
			for(var i=0; i<labels.length; i++){
	 			if(labels[i].getAttribute('size')){
	 				labels[i].style.fontSize = (fontheight*labels[i].getAttribute('size'))+'px';
	 			}else{
	 				labels[i].style.fontSize = fontheight+'px';
	 			}
			}
		},
		// 출력값을 제대로 보여주기 위한 상대적 폰트 사이즈로 변경
		relativeFontSize : function(el){
			fnObj.setFontSize();
			do {
				var canChangeFontSize = false;
				var labels = el.querySelectorAll('label');
				for(var i=0; i<labels.length; i++){
					var parNodeWidth = labels[i].parentNode.clientWidth;
					var childNodeWidth = labels[i].clientWidth;
					if(parNodeWidth <= childNodeWidth){
						labels[i].style.fontSize = (+labels[i].style.fontSize.replace('px', '') - 1) + 'px';
						canChangeFontSize = true;
					}
				}
			}while(canChangeFontSize);
		}
	
}



showPopup = function() {
	$("#popLayer").show();
	$("#popLayer").center();
}


var DeadInfo = function(){
	
	this.roomCd = "";		
	this.beforeHTML = [];
	this.contTitle = "";
	this.contId = "";
	this.status = "";	
	this.page = {
			currentPage: 1,
            pageSize: 0,
            totalElements: 0,
            totalPages: 0
        };
	this.booking = null;
}

var deadObj = new DeadInfo();

var time = 0;

var aud = new Audio();

function countTime(){
	time++;
	//console.log(time);
}

function countZero(){
	time = 0;
}

function timeOut(){
	
	if(time>60 ){
		Broadcast("ROMR11999");
		if(fnObj.openYN == "N"){
			goUrl('${pageContext.request.contextPath}/jsp/public/romr/notOpen.jsp');	
		}else{
			refreshThisPage();
		}
		countZero();
	
		deadObj = new DeadInfo();
		fnObj.deadInfo.contTitle= "";
		fnObj.deadInfo.contId= "";
		fnObj.deadInfo.bookSeq = "";
		fnObj.deadInfo.bookDate = "";
	}

}

function timeZone(){
	$("#time1").text(fnObj.time[0]);
	//$("#time1").css({"font-size":"20px","padding-top":"25px"});
	$("#time2").text(fnObj.time[1]);
//	$("#time2").css({"font-size":"20px","padding-bottom":"15px"});
	$("#time3").text(fnObj.time[2]);
	//$("#time3").css({"font-size":"35px","padding-left":"25px","padding-top":"10px"});
}


function goUrl(url,prams,p_callback){	
	
	$.ajax({
        url:url,
        data : prams,
        async : false,
        success : function (res){
        	deadObj.beforeHTML.push(document.querySelector('#wrap').innerHTML);
    		var div = document.createElement('div');
    		div.innerHTML = res;
    		
    		document.querySelector('#wrap').innerHTML= div.getElementsByTagName('div')[0].innerHTML;  
    		fnObj.relativeFontSize(document.querySelector('#wrap'));
    		timeZone();
    		
    		if(p_callback){
    			p_callback(res);
    		}
    		 $('body').on("selectstart", function(event){ return false; });
		     $('body').on("dragstart", function(event){ return false; });
        }
    });
	
}

function beforeSearch(){
	clearTimeout(fn_timeout);
	$(aud).unbind('ended');
	aud.pause();
	
	
	if($("#pageId").val()== "ROMR1030"){
		Broadcast("ROMR21010");
	}
	if($("#pageId").val()== "ROMR1040"){
		Broadcast("ROMR13010");
	}
	if($("#pageId").val()== "ROMR1050"){
		Broadcast("ROMR14010");
		roomList;
		cancelBooking();
		setTimeout(roomList, 5000);
	}
	
	document.querySelector('#wrap').innerHTML = deadObj.beforeHTML.pop();
	 $('body').on("selectstart", function(event){ return false; });
     $('body').on("dragstart", function(event){ return false; });
}


function startResv(){
	goUrl('/jsp/public/romr/ROMR1020.jsp',"", Broadcast("ROMR12010"));	 
}


var Broadcast = function (msgId,szText){
	
	if(fnObj.envSet.ttsYn == 'Y'){
		var url = "/api/v1/public/tts?msgId="+msgId;
		aud.pause();	
		if(szText != undefined){		
			url = "/api/v1/public/tts?msgId="+msgId+"&szText="+szText||"";
		}
					
		aud.src = url;
		 
		
		
		if (aud.paused) {
			//aud.play();			      
	    }
	    else {
	    	aud.pause();	      
	    }
		return true;
	}
	
}


function refreshThisPage(){
	 $('body').on("selectstart", function(event){ return false; });
     $('body').on("dragstart", function(event){ return false; });	
	aud.pause();
	$(aud).unbind('ended');
	deadObj = new DeadInfo();
	fnObj.deadInfo.contTitle= "";
	fnObj.deadInfo.contId= "";
	location.reload();		

	
}

function reStart(){
	
	cancelBooking();
	refreshThisPage();	

	
}


function defaultChk(){
	
	if(fnObj.envSet != null){
		
		if(fnObj.openYN == "N"){
			goUrl('${pageContext.request.contextPath}/jsp/public/romr/notOpen.jsp');		
			return false;
		}else{
			$(fnObj.inpectKiosk).each(function(i){
				var dd = this;
				if(this.roomCd == fnObj.roomCd ){
					goUrl('${pageContext.request.contextPath}/jsp/public/romr/inspectKiosk.jsp');		
					return false;
				}
			});
		}
	
		
		
		
		
	}	
	return true;
}



function selectDeadList(pageNo){
	
	if($("#deadName").val() != null){
		
		fnObj.deadInfo.contTitle = $("#deadName").val();
	}
	
	if(fnObj.deadInfo.contTitle.length < 2){		
		Broadcast("ROMR12020");
		return;
	}
	if(fnObj.deadInfo.contTitle == '삼우제'){
		wipae();
		return;
	}
	var pattern = /([^가-힣\x20])/i; 	
	
	if (pattern.test(fnObj.deadInfo.contTitle)) { 
		Broadcast("ROMR12030");
		return;
	} 
	
	keyPadDel('2');
	deadList(pageNo);
	
	
}


function deadList (pageNo){
		
	var pageNum = pageNo || 1;		
	$.ajax({
		type: "GET",
        url: "/api/v1/public/ROMR1030/selectDeadList",
        dataType : 'json',
        data: "pageNumber=" + (pageNum-1)+ "&pageSize=7&deadName=" + fnObj.deadInfo.contTitle,
		success : function(res){				
			deadObj.page = res.page;						
			deadObj.page.currentPage = pageNum;
			
			if(res.list.length == 0 || res.list == null){
				aud.pause();
				Broadcast("ROMR12040", fnObj.deadInfo.contTitle);
				return;
			}else if(res.list.length > 0){
				goUrl("/jsp/public/romr/ROMR1030.jsp",'');
				aud.pause();
				Broadcast("ROMR13010");
				var table = $("#tb_deadList");
				$("#tb_deadList tr:not(:first-child)").remove();
				$("#tb_page tr").remove();
				var tag = "";
				var items = res.list;
				
				

				
				for(i=0; i<items.length; i++){
					var conttitle = items[i].contFile.substr(0,items[i].contFile.indexOf("_"));
					if(conttitle.length == 0){
						conttitle = items[i].contFile.substr(0,items[i].contFile.indexOf(".jpg"));
					}
					tag+= "<tr hieght='84' style=\"text-align: center;\" onclick=\"selectedElement(this); setInfo(\'"+items[i].contTitle+"\',\'"+items[i].contId+"\')\";>";
					tag+= "<td width='327'>"+items[i].contTitle+"</td>";
					tag+= "<td width='392'>"+items[i].contId+"</td>";
					tag+= "<td width='400'>"+conttitle+"</td>";
					tag+= "<td width='390'>"+items[i].contDate+"</td>";
					tag+= "<td width='338'>"+items[i].contRegUser+"</td>";
					tag+= "</tr>";        		
					
					table.append(tag);
					
					
					tag = "";
				}
				for(i=0; i<7-items.length; i++){
					tag ="<tr hieght='84' style='text-align: center;'><td width='327'>&nbsp;</td><td width='392'>&nbsp;</td><td width='400'>&nbsp;&nbsp;</td><td width='390'>&nbsp;</td><td width='338'>&nbsp;</td></tr>";
					table.append(tag);
					tag = "";
				}
				
				if(items.lnength == 1){
					$("#tb_deadList tr").eq(1).click();
				}
				

				deadObj.contTitle ="";
				deadObj.contId ="";
				
				var totalPage = deadObj.page.totalPages;
				totalPage = totalPage == 0 ? 1 : totalPage;
				var tag ="<tr align='center' hieght='84'><td style='text-align:center;'>";        	
				if(pageNum-1 > 0){
					tag += "<a href='javascript:deadList("+(pageNum-1)+");'><img src='${pageContext.request.contextPath}/images/kiosk/back_01.png' style='vertical-align: middle;'/></a>";
					
				}
				if(pageNum-1 <= 0){
			 		tag += "<a href='#'><img src='${pageContext.request.contextPath}/images/kiosk/back_01.png' style='vertical-align: middle;'/></a>";
				}        	
				 
				 tag+=  "<span style='vertical-align: middle; line-height:84px;'>" +pageNum+ " / " +totalPage+ "</span>";
				
				 if(pageNum+1 > totalPage){
			   		 tag+= "<a href='#'><img src='${pageContext.request.contextPath}/images/kiosk/front_01.png' style='vertical-align: middle;'/></a>";
			   	 }
				 if(pageNum+1 <= totalPage){
					 
					 tag+= "<a href='javascript:deadList("+(pageNum+1)+");'><img src='${pageContext.request.contextPath}/images/kiosk/front_01.png' style='vertical-align: middle;'/></a>";
				 }
				
				 tag+="</td></tr>";
				$("#tb_page").append(tag); 
				
				if(res.list.length == 1){
					$("#tb_deadList tr").eq(1).click();
				}
			}
        }
    });
}

function wipae(){
	
	if($("#deadName").val() != null){
		
		deadObj.contTitle = $("#deadName").val();
	}
	
	if(deadObj.contTitle.length < 2){		
		Broadcast("ROMR12020");
		return;
	}
	
	deadObj.photoYn = 'N';
	selectDeadInfo();
	
}

function setInfo(name,id){
	deadObj.contTitle = name;
	deadObj.contId = id;
}

function setCancelInfo(bookDate,bookSeq,roomNm,deadName,bookTime){
	fnObj.deadInfo.bookDate = bookDate;
	fnObj.deadInfo.bookSeq = bookSeq;
	
	$("#pop_roomNm").val(roomNm);
	$("#pop_roomNm").css("width","598px");
	$("#pop_deadName").val(deadName);
	$("#pop_deadName").css("width","598px");
	$("#pop_bookTime").val(bookTime);	
	$("#pop_bookTime").css("width","598px");
}


function selectedElement(el){
	var tags = el.parentNode.getElementsByTagName(el.tagName);

	for(var i=0; i<tags.length; i++){
		tags[i].style.background = '';
		el.setAttribute('selected', '');
	}
	//el.style.background = '#eeeeee';
	el.style.background = '#8CBDED';
	el.setAttribute('selected', 'selected');
}

function selectBookingChk(){
	
	if(deadObj.contId == ""){	
		Broadcast("ROMR13030");
		return;
	}	
	
	$.ajax({
		type: "GET",
		dataType : 'json',
		sync : true,
	    url: "/api/v1/public/ROMR1030/selectBookingByContid?contid="+deadObj.contId,
        success: function(res){
        	
        	if(res==true){        		
        		Broadcast("ROMR13020");
           		return;
        	}else{
        		selectDeadInfo();
        	}         	
        }
		
    });
}

function selectDeadInfo(){	
	deadObj.contTitle = deadObj.contTitle;
	keyPadDel('2');
	goUrl("/jsp/public/romr/ROMR1040.jsp","",roomList);
	
	Broadcast("ROMR14010");
}




var fn_timeout = null;
var roomList = function(){
	
	
	$.ajax({
		type: "GET",
        url: "/api/v1/public/ROMR1040/selectRoomList",
        dataType : 'json',    
        sync : true,
		success : function(res){		
			
        	var inspectRoom = res.map.inspectRoom;   
        	var booking = res.map.booking;   
        	
        	$("#tb_room td[id]").each(function(i){		
        		$(this).css('background-image', "url('${pageContext.request.contextPath}/images/kiosk/kiosk_04_05.gif')");	
        		$(this).attr("status","D");       
        		$(this).removeClass("status");
        		$(this).html("");
        	});	
        	
        	$(booking).each(function(i){		
        		$("#tb_room td[id='"+this.roomCd+"']").css({'background-image':"url('${pageContext.request.contextPath}/images/kiosk/kiosk_04_06.gif')", "text-align":"center"});		
        		var remainTime =  Number(this.remainTime);        		
        		if(remainTime == 0){
        			remainTime = 1;
        		}
        		$("#tb_room td[id='"+this.roomCd+"']").html(remainTime);
        		$("#tb_room td[id='"+this.roomCd+"']").attr("status","R");        		
        	});	
        	
        	$(inspectRoom).each(function(i){				
        		$("#tb_room td[id='"+this.roomCd+"']").css('background-image', "url('${pageContext.request.contextPath}/images/kiosk/kiosk_04_07.gif')");		
        		$("#tb_room td[id='"+this.roomCd+"']").attr("status","I");        		
        	});
		},complete : function(){
			fn_timeout = setTimeout(roomList,5000);
		}
	});	
	
}

function selectRoom(id){
	
	
	 if($("#"+id).attr("status") == "I"){
		 return;
	 }
	
	 deadObj.roomCd = id;	 
	 insertBooking();
		
}
function insertBooking(){
	 var booking = {		
				 roomCd : deadObj.roomCd		
				,conttitle : deadObj.contTitle
				,contid : deadObj.contId
				,status : "R"
				,bookMan : "C"	
				,photoYn : "Y"
				,smsYN : "N"
			}
			
	$.ajax({
	type: "POST",
     url: "/api/v1/public/ROMR1040/saveBooking",
     dataType : 'json',      
     data: booking,
     sync : true,
		success : function(res){
			if(res.error){
				
			}else{
				clearTimeout(fn_timeout);
				deadObj.booking = res;
				deadObj.roomCd = null;
				goUrl("/jsp/public/romr/ROMR1050.jsp",null,getBookingInfo(res));
			}
			
		}
	});
}

function getBookingInfo(booking){
	
	
	
	 $.ajax({
		type: "GET",
        url: "/api/v1/public/ROMR1050/selectBookingInfo",
        dataType : 'json',      
        data: booking,	
        sync : true,
        success : function(res){
        	Broadcast("ROMR15010");
        	var stTime = res.stTime.substr(0,2) + ":" + res.stTime.substr(2,2);
        	var edTime = res.edTime.substr(0,2) + ":" + res.edTime.substr(2,2);
      
        	$("#roomNm").val(res.roomNm.replace("0",""));
        	$("#bookingTime").val(stTime + " ~ " + edTime);
        	if(res.roomCd == "101" || res.roomCd == "102" || res.roomCd == "103" || res.roomCd == "104" || res.roomCd == "125" || res.roomCd == "124"
        			|| res.roomCd == "123" || res.roomCd == "122" || res.roomCd == "122" || res.roomCd == "121" || res.roomCd == "120" || res.roomCd == "119"){
        		//$("#"+res.roomCd).css({'background-image':"url('${pageContext.request.contextPath}/images/kiosk/kiosk_03_08_1.png')", "background-repeat": "no-repeat"});	
        		$("#"+res.roomCd).html("<img src='${pageContext.request.contextPath}/images/kiosk/kiosk_03_08_1.png' height='90'>")
        	}else{
        		$("#"+res.roomCd).html("<img src='${pageContext.request.contextPath}/images/kiosk/kiosk_03_08.PNG' height='90'>")
        		//$("#"+res.roomCd).css({'background-image':"url('${pageContext.request.contextPath}/images/kiosk/kiosk_03_08.png')", "background-repeat": "no-repeat"});	        	
        	}
        	
       	}
 	 });
}

function cancelBooking(){
	var booking = {
		bookDate :deadObj.booking.bookDate
		,bookSeq: deadObj.booking.bookSeq
		,cancelMan : "C"
		,status : "C"		
	}
	
	 $.ajax({
			type: "POST",
	        url: "/api/v1/public/ROMR1050/cancelBooking",
	        dataType : 'json',      
	        data: booking,	
	        sync : true,
	        success : function(res){
	        	return true;
	        }
	 });
}

function completeBooking(){
	
	 
	 var booking = {
				bookDate :deadObj.booking.bookDate
				,bookSeq: deadObj.booking.bookSeq
				//,phoneNo : $("#phoneNo").val()
				,smsYn : "N"
				
	}
	 
	 aud.pause();
	 $.ajax({
			type: "POST",
	        url: "/api/v1/public/ROMR1050/completeBooking",
	        dataType : 'json',      
	        data: booking,	
	        sync : true,
	        success : function(res){
	        	if(!res.error){
		        	var msg = Number(res.stTime.substring(0,2)) + "시, " +  Number(res.stTime.substring(2,4)) + "분, " + res.roomNm.replace("0","")+",";
		        	
// 		        	if(Broadcast("ROMR15050",msg)){
// 		        		$(aud).bind('ended', function(){			 			    
// 				  			   refreshThisPage();
// 				  		});
// 		        	}else{
		        		  refreshThisPage();
// 		        	}
		        	 
	        	}
	        }
	 }).always(function(){keyPadDel('2');}) 
	
	
}

function startCancelResv(){
	goUrl("${pageContext.request.contextPath}/jsp/public/romr/ROMR1060.jsp",null,bookingList());
	Broadcast("ROMR21010");
}

function bookingList(pageNo){
	
	var pageNum = pageNo || 1;		
	$.ajax({
		type: "GET",
        url: "/api/v1/public/ROMR1060/selectBookinglList",
        dataType : 'json',     
        sync : true,
        data: "pageNumber=" + (pageNum-1)+ "&pageSize=7",
		success : function(res){				
			deadObj.page = res.page;						
			deadObj.page.currentPage = pageNum;
			var dd = deadObj;  
        	var table = $("#tb_bookingList");
        	$("#tb_bookingList tr:not(:first-child)").remove();
        	$("#tb_page tr").remove();
        	var tag = "";
        	var items= res.list;
        	
        	for(i=0; i<items.length; i++){
        		var conttitle = "";
        		var contDate = "";
        		var contRegUser = "";
        		var stTime = items[i].stTime.substr(0,2) + ":" + items[i].stTime.substr(2,2);
            	var edTime = items[i].edTime.substr(0,2) + ":" + items[i].edTime.substr(2,2);
        		if(items[i].mdcontentsVTO != null){
        			conttitle = items[i].mdcontentsVTO.contFile.substr(0,items[i].mdcontentsVTO.contFile.indexOf("_"));
        			
        			if(conttitle.length == 0){
            			conttitle = items[i].mdcontentsVTO.contFile.substr(0,items[i].mdcontentsVTO.contFile.indexOf(".jpg"));
            		}
        			
        			contDate  = items[i].mdcontentsVTO.contDate;
        			contRegUser = items[i].mdcontentsVTO.contRegUser;
        		}
        		
        		
        		
        		tag+= "<tr style=\"text-align: center;\" onclick=\"selectedElement(this); setCancelInfo(\'"+items[i].bookDate+"\',\'"+items[i].bookSeq+"\',\'"+items[i].roomNm.replace("0","")+"\',\'"+items[i].conttitle+"\',\'"+stTime+" ~ " +edTime+"\')\";>";
        		tag+= "<td width='332'>"+items[i].conttitle||"&nbsp;</td>";
        		tag+= "<td width='392'>"+contRegUser||"&nbsp;</td>";
        		tag+= "<td width='399'>"+items[i].roomNm||"&nbsp;</td>";
        		tag+= "<td width='733' style='text-align:center;'>"+stTime+" ~ " +edTime+"</td>";
        	
        		tag+= "</tr>";        		
        		
        		table.append(tag);
        		tag = "";
        	}
        	for(i=0; i<7-items.length; i++){
        		tag ="<tr style='text-align: center;'><td width='332'>&nbsp;</td><td width='392'>&nbsp;</td><td width='399'></td><td width='391'></td><td width='342'></td></tr>";
        		table.append(tag);
        		tag = "";
        	}
        
        	deadObj.contTitle ="";
        	deadObj.contId ="";
        	
        	var totalPage = deadObj.page.totalPages;        	
        	totalPage = totalPage == 0 ? 1 : totalPage;
        	
        	var tag ="<tr><td style='text-align:center;'>";        	
        	if(pageNum-1 > 0){
        		tag += "<a href='javascript:bookingList("+(pageNum-1)+");'><img src='${pageContext.request.contextPath}/images/kiosk/back_01.png' style='vertical-align: middle;'></a>";        		
        	}
        	if(pageNum-1 <= 0){
           		tag += "<a href='#'><img src='${pageContext.request.contextPath}/images/kiosk/back_01.png' style='vertical-align: middle;'></a>";        		
           	 }        	
        	 
         	 tag+=  "<span style='vertical-align: middle; line-height:84px;'>"+ pageNum+ "/ " +totalPage+"</span>";
         	         	
        	 if(pageNum+1 > totalPage){
          		 tag+= "<a href='#'> <img src='${pageContext.request.contextPath}/images/kiosk/front_01.png' style='vertical-align: middle;'></a>";
        	 }
        	 if(pageNum+1 <= totalPage){
        		 
        		 tag+= "<a href='javascript:bookingList("+(pageNum+1)+");'> <img src='${pageContext.request.contextPath}/images/kiosk/front_01.png' style='vertical-align: middle;'></a>";
        	 }
        	
        	 tag+="</td></tr>";
        	$("#tb_page").append(tag);        	
        	
        }
    });
}
function cancelResv(){	
	var flag = false;
	$("#tb_bookingList tr").each(function(){
		 
		  if($(this).attr("selected")){
			aud.pause();
			open();
			flag = true;
			Broadcast("ROMR21040");
			return;
		  }
	 });
	if(flag == false){
		  Broadcast("ROMR21030");
		  return;
	}
		
}


function cancelBooking2(){
	var booking = {
			bookDate : fnObj.deadInfo.bookDate
			,bookSeq : fnObj.deadInfo.bookSeq
			,cancelMan : "C"
			,status : "C"		
		
	}
	$.ajax({
		type: "POST",
        url: "/api/v1/public/ROMR1060/cancelBooking2",
        dataType : 'json',      
        data: booking,	
        sync : true,
        success : function (res){        	
        		Broadcast("ROMR21050", res.apiResponse.message);
        		close();
	  			refreshThisPage();
        	
        		$(aud).bind('ended', function(){
        		   close();
  	  			   refreshThisPage();
  	  			});
        		
        }
	});
}


function wrapWindowByMask(){
    //화면의 높이와 너비를 구한다.
    var maskHeight = $(document).height();  
    var maskWidth = $(window).width();  
        
    //마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
    $('#mask').css({'width':maskWidth,'height':maskHeight});  
    //애니메이션 효과 - 일단 0초동안 까맣게 됐다가 60% 불투명도로 간다.
    $('#mask').fadeIn(0);      
    $('#mask').fadeTo("slow",0.6);    
    //윈도우 같은 거 띄운다.
    $('.window').show();
}

//닫기 버튼을 눌렀을 때
function close(){
	  aud.pause();
	  $('#mask, .window').hide();  
	  $("#phoneNo").val("");
	  $("#etext").val("");
}  
function open(){
	 wrapWindowByMask();
}
    
        
    



function ROMR1010(){	
	timeZone();
	deadObj.beforeHTML=[];	
	
	fnObj.deadInfo.bookDate = "";
	fnObj.deadInfo.bookSeq = "";
	if(!defaultChk()){
		return;
	}
	deadObj = new DeadInfo();	
	 
}

function ROMR1020(){	
	if(!defaultChk()){
		return;
	}
	timeZone();
	
	
	//deadObj = new DeadInfo();
	//fnObj.deadInfo.contTitle= "";
	//fnObj.deadInfo.contId= "";
	 timeOut();
}

function ROMR1030(){	
	if(!defaultChk()){
		return;
	}
	timeZone();
	timeOut();
	
}

function ROMR1040(){
	
	if(!defaultChk()){
		return;
	}	 	
	timeZone();	
	timeOut();	 
}

function ROMR1050(){
	
	if(!defaultChk()){
		return;
	}	 
	timeZone();
	timeOut();	 
}

function ROMR1060(){
	
	if(!defaultChk()){
		return;
	}	 
	timeZone();
	timeOut();	 
}


function notOpen(){
	timeZone();
	if(fnObj.envSet != null){
		
		if(fnObj.openYN == "Y"){
			refreshThisPage();
			return;
		}
	
	}
	
}

function inspectKiosk(){
	var flag = false;
	if(!defaultChk()){
		return;
	}	
	$(fnObj.inpectKiosk).each(function(i){
		
		if(this.roomCd == fnObj.roomCd ){
			flag = true;
		}
	});
	if(flag == false){
		goUrl('${pageContext.request.contextPath}/api/v1/public/ROMR1010/'+fnObj.roomCd);	
	}
	
}

function keyPadChange(type){
	
	if(type == 'eng'){
		$("#hanKeypad").show();
		$("#engKeypad").hide();
	}else{
		$("#hanKeypad").hide();
		$("#engKeypad").show();
	}		
}

function keyPad(chk){	
	
	$("#etext").val($("#etext").val()+chk);
	translate();
	
	 // document.form1.etext.value =document.form1.etext.value+chk;
}

function keyPadDel(chk){
  if(chk=='1'){
	  $("#etext").val($("#etext").val().substring(0,$("#etext").val().length-1));
	  translate();
  //document.form1.etext.value =document.form1.etext.value.substring(0,document.form1.etext.value.length-1);
  }
  if(chk=='2'){
	  $("#etext").val("");
	  $("#deadName").val("");
  //document.form1.etext.value="";
  }
}

k1 = "ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ";
k2 = "ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ";
k3 = " ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ";

n1 = ' ';
n2 = ' ';
n3 = ' ';

function onkey(c, jamo)
{
    switch(jamo)
    {
        // 자음
        case 1:
            // 만약에 이전에 모음이 있었으면
            if(n2!=' ') 
            {        
                // 만약에 이전에 자음도 있었으면
                if(n3!=' ') 
                {
                    if(n3=='ㄱ' && c=='ㅅ')
                    {
                        n3='ㄳ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄴ' && c=='ㅎ')
                    {
                        n3='ㄶ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㄱ') 
                    {
                        n3='ㄺ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅁ') 
                    {
                        n3='ㄻ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅂ') 
                    {
                        n3='ㄼ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅅ') 
                    {
                        n3='ㄽ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅌ') 
                    {
                        n3='ㄾ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅍ') 
                    {
                        n3='ㄿ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅎ') 
                    {
                        n3='ㅀ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㅂ' && c=='ㅅ')
                    {
                        n3='ㅄ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else 
                    {
                        n1 = c;
                        n2 = ' ';
                        n3 = ' ';
                        last = res;
                        res += n1;
                    }
                }
                // 이전에 모음만 있고 자음은 없었으면 아마 받침이 될 것임
                else
                {
                    // 이것들은 받침이 될 수 없음 
                    if(c=='ㄸ'||c=='ㅃ'||c=='ㅉ')
                    {
                        // 무조건 새 글자
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        n1 = c;
                        n2 = ' ';
                        n3 = ' ';
                        last = res;
                        res += n1;
                    }
                    // 무조건 받침으로 들어감
                    else 
                    {
                        n3=c;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                }
            }
            // 이전에 모음이 없었던 경우이므로 무조건 출력함
            else
            {        
                n1=c;
                n2=' ';
                n3=' ';
                last = res;
                res += c;
            }
            break;
        // 모음
        case 2:
            // 만약에 이전에 모음이 있었으면
            if(n2!=' ') 
            {
                // 만약에 이전에 받침도 있었으면
                if(n3!=' ')
                {
                    if(n3=='ㄳ') 
                    {
                        n3='ㄱ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅅ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res += String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄵ') 
                    {
                        n3='ㄴ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅈ';
                        n2 = c;
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄶ') 
                    {
                        n3='ㄴ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅎ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄺ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㄱ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄻ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅁ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄼ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅂ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄽ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅅ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄾ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅌ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄿ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅍ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㅀ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅎ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㅄ') 
                    {
                        n3='ㅂ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅅ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    // 이전에 받침을 꾸어옴
                    else 
                    {
                        temp = n3;
                        n3 = ' ';
                        // 새 글자
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        n1 = temp;
                        n2 = c;
                        last = res;
                        res += String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                }
                else 
                {
                    if(n2=='ㅗ')
                    {
                        if(c=='ㅏ') n2='ㅘ';
                        if(c=='ㅐ') n2='ㅙ';
                        if(c=='ㅣ') n2='ㅚ';
                    }
                    if(n2=='ㅜ')
                    {
                        if(c=='ㅓ') n2='ㅝ';
                        if(c=='ㅔ') n2='ㅞ';
                        if(c=='ㅣ') n2='ㅟ';
                    }
                    if(n2=='ㅡ')
                    {
                        if(c=='ㅣ') n2='ㅢ';
                    }
                    res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                }
            }
            else 
            {
                // 만약에 이전에 자음이 있었으면 
                if(n1!=' ')
                {
                    n2=c;
                    res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                }
                else
                {
                    n1 = ' ';
                    n2 = ' ';
                    n3 = ' ';
                    last = res;
                    res += c;
                }
            }
            break;
        // 그 이외
        case 0:
            if(n1!=' '&&n2==' '&&n3==' ') 
            {
                last = res;
                res += n1;
            }
            else if(n1==' '&&n2!=' '&&n3==' ') 
            {
                last = res;
                res += n2;
            }
            n1 = ' ';
            n2 = ' ';
            n3 = ' ';
            last = res;
            res += c;
            break;
    }
}

function translate()
{
    res = "";
    last = "";
    n1 = ' ';
    n2 = ' ';
    n3 = ' ';
    eng = false;
    
    for(n=0; n<document.getElementById("etext").value.length; n++)
    {
        jamo = 0;
        c = document.getElementById("etext").value.charAt(n);
        if(c=='"\"') 
        {
            eng = !eng;
            continue;
        }
        if(!eng)
        {
            switch(c)
            {
                // 키보드 제일 윗줄
                case 'ㅂ': c='ㅂ'; jamo=1; break;
                case 'ㅃ': c='ㅃ'; jamo=1; break;
                case 'ㅈ': c='ㅈ'; jamo=1; break;
                case 'ㅉ': c='ㅉ'; jamo=1; break;
                case 'ㄷ': c='ㄷ'; jamo=1; break;
                case 'ㄸ': c='ㄸ'; jamo=1; break;
                case 'ㄱ': c='ㄱ'; jamo=1; break;
                case 'ㄲ': c='ㄲ'; jamo=1; break;
                case 'ㅅ': c='ㅅ'; jamo=1; break;
                case 'ㅆ': c='ㅆ'; jamo=1; break;
                case 'ㅛ': c='ㅛ'; jamo=2; break;
                case 'ㅕ': c='ㅕ'; jamo=2; break;
                case 'ㅑ': c='ㅑ'; jamo=2; break;
                case 'ㅐ': c='ㅐ'; jamo=2; break;
                case 'ㅒ': c='ㅒ'; jamo=2; break;
                case 'ㅔ': c='ㅔ'; jamo=2; break;
                case 'ㅖ': c='ㅖ'; jamo=2; break;
            
                // 키보드 두번째 줄 
                case 'ㅁ': c='ㅁ'; jamo=1; break;
                case 'ㄴ': c='ㄴ'; jamo=1; break;
                case 'ㅇ': c='ㅇ'; jamo=1; break;
                case 'ㄹ': c='ㄹ'; jamo=1; break;
                case 'ㅎ': c='ㅎ'; jamo=1; break;
                case 'ㅗ': c='ㅗ'; jamo=2; break;
                case 'ㅓ': c='ㅓ'; jamo=2; break;
                case 'ㅏ': c='ㅏ'; jamo=2; break;
                case 'ㅣ': c='ㅣ'; jamo=2; break;
            
                // 키보드 제일 밑 줄
                case 'ㅋ': c='ㅋ'; jamo=1; break;
                case 'ㅌ': c='ㅌ'; jamo=1; break;
                case 'ㅊ': c='ㅊ'; jamo=1; break;
                case 'ㅍ': c='ㅍ'; jamo=1; break;
                case 'ㅠ': c='ㅠ'; jamo=2; break;
                case 'ㅜ': c='ㅜ'; jamo=2; break;
                case 'ㅡ': c='ㅡ'; jamo=2; break;
            
            }
        }
        
       onkey(c,jamo);
    }
    
    if($("#pageId").val() == "ROMR1020" ){
    	document.form1.deadName.value = res;
    }else if($("#pageId").val() != "ROMR1020" ){ 
    	
    	$("#phoneNo").val(res);
    }
    
   
  
}



</script>


</head>
<BODY  onclick="countZero()" BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<!-- ImageReady Slices (kiosk_01.PSD) -->
<div id="wrap" align="center" >
  <div id="mask"></div> 
  <TABLE WIDTH=1920 BORDER=0 CELLPADDING=0 CELLSPACING=0 height="1080">
	  <TR>
		  <TD height="103" background="${pageContext.request.contextPath}/images/kiosk/kiosk_01_01.gif"  align="right">
		  	 <div id="timeZone" style="height:103px; width:300px; margin-right:70px;">
		  		<table height="100">
		  			<tr>
		  				<td id="time1"></td>
		  				<td rowspan="2" id="time3" width="100"></td>
		  			</tr>
		  			<tr>
		  				<td id="time2" width="100"></td>
		  			</tr>
		  		</table>
		  	</div> 
		  </TD>
	  </TR>
	  <TR>
		  <TD height="868" background="${pageContext.request.contextPath}/images/kiosk/kiosk_01_02.gif"></TD>
	  </TR>
	  <TR>
		  <TD WIDTH=1920  background="${pageContext.request.contextPath}/images/kiosk/kiosk_01_04.gif">
			  <div align="center" >
			  	<img src="${pageContext.request.contextPath}/images/kiosk/kiosk_01_05.gif" width="453" height="109" border="0" onclick="startResv();">
			  	<img src="${pageContext.request.contextPath}/images/kiosk/kiosk_01_06.gif" width="453" height="109" border="0" onclick="startCancelResv();">
			  </div>
		  </TD>
	  </TR>
  </TABLE>
 
   <input type="hidden" id="pageId" value="ROMR1010">
</div>
</BODY>
</html>