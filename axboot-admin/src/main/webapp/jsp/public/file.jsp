<%@page import="java.io.File"%>
<%@page import="com.axisj.axboot.core.db.dbcp.ManagedBasicDataSource"%>
<%@page import="org.springframework.web.servlet.FrameworkServlet"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR" %>
<%@ page import="
    javax.naming.*,
    javax.sql.*,
    org.apache.tomcat.*"
%>
<%@ page import = "java.sql.*" %>                    <!-- JSP에서 JDBC의 객체를 사용하기 위해 java.sql 패키지를 import 한다 -->

<html>
<head></head>
<body>
<table width="550" border="1">
<%

Connection conn = null;                                        // null로 초기화 한다.
PreparedStatement pstmt = null;

try{
String url = "jdbc:mysql://localhost:3306/axboot_youngrak_jere";        // 사용하려는 데이터베이스명을 포함한 URL 기술
String id = "root";                                                    // 사용자 계정
String pw = "1111";                                                // 사용자 계정의 패스워드

Class.forName("com.mysql.jdbc.Driver");                       // 데이터베이스와 연동하기 위해 DriverManager에 등록한다.
conn=DriverManager.getConnection(url,id,pw);              // DriverManager 객체로부터 Connection 객체를 얻어온다.
Statement stmt = conn.createStatement();
String sql = "select * from mdcontents";                        // sql 쿼리
ResultSet rs = stmt.executeQuery(sql);
                                       // 쿼리를 실행하고 결과를 ResultSet 객체에 담는다.

while(rs.next()){                                                        // 결과를 한 행씩 돌아가면서 가져온다.


	String ContID = rs.getString("ContID");
	String ContTitle = rs.getString("ContTitle");
	String ContFile = rs.getString("ContFile");
	String ContRealFileName = rs.getString("ContRealFileName");
	boolean flag = false;
	//C:\\apache-tomcat-8.0.30\\webapps\\photo\\
	File file = new File("C://apache-tomcat-8.0.30/webapps/photo/"+ContRealFileName);
	//file.
	if(file.isFile()){
		flag = true;
	}
	
	if(flag == false){
		%>
		<tr>
	<td width="100"><%=ContID%></td>
	<td width="100"><%=ContTitle%></td>
	<td width="100"><%=ContFile%></td>
	<td width="100"><%=ContRealFileName%></td>
	<td width="100"><%=flag%></td>
	</tr>
		<%
	}
	%>

	
	<%
}
                                       stmt.close();
                                       
}catch(Exception e){                                                    // 예외가 발생하면 예외 상황을 처리한다.
e.printStackTrace();
out.println("member 테이블 호출에 실패했습니다.");
}finally{      
	// 쿼리가 성공 또는 실패에 상관없이 사용한 자원을 해제 한다.  (순서중요)
if(conn != null) try{conn.close();}catch(SQLException sqle){}   // Connection 해제
}
%>
</table>
</body>
</html>

