<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
    <script type="text/javascript" src="<c:url value='/static/plugins/jquery/jquery.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/axisj/dist/AXJ.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/chartjs/Chart.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/ax5-polyfill.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/modal.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/app.js' />"></script>
   <!--  <script src="http://code.jquery.com/jquery-latest.js"></script>
 -->
<style type="text/css">
html, body{
	width: 100%;
	height: 100%;
	min-height: 100%;
	padding: 0px;
	border: 0px;
	margin: 0px;
	overflow: hidden;
   /*  cursor: none;  */
}
.img {
	height:741px;
	max-width:1850px;
}
.time { border: 0px;
		text-align: center;
		background-color: transparent;
		width: 80%;
		color: white;

  }

#time1 {font-size:20px; padding-top:25px}
#time2 {font-size:20px; padding-bottom:15px}
#time3 {font-size:35px; padding-left:25px; padding-top: 10px;}

 @font-face {
	font-family: 'Noto Sans Korean';
	src: url('${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.woff')
}



@font-face {
    font-family: 'Noto Sans Korean';
    font-style: normal;
    font-weight: 200;
    src:
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.eot),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.eot?#iefix) format('embedded-opentype'),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.woff) format('woff'),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.otf) format('opentype');
}




input {border :none; text-align: center;}

body, label, th, td, input {
	font-family: 'Noto Sans Korean';
	font-size: 50px;

}


.center { text-align:center; width:80px; height:300px; position: absolute; top:440px; left:925px;  }
.left {  width:80px;height:300px;   position: absolute; top:440px; left:820px;  }
.right {  width:80px;height:300px;  position: absolute; top:440px; left:1030px;  }

</style>
<script type="text/javascript">

$(document).ready(function(){

	fnObj.getSetting();
	setInterval(fnObj.refreshThisPage2,60000)
	 $('body').on("selectstart", function(event){ return false; });
     $('body').on("dragstart", function(event){ return false; });

});

var fnObj = {
		roomCd : "${roomCd}"
		,envset : null	// 운영환경 정보
		,booking : null	// 예약정보
		,inspectRoom : null // 점검제례실 정보
		,aud : new Audio()
		,BookingYN : "N" // Y: 예약중
		,openYN : "N" 	// Y: 개방 N :미개방
		,remainTime : "" // 대기시간
		,beforeHTML : [] // 이전화면
		,time : null // 현재시간
		,nextBookingYN : "N"  // Y: 대기자있음
		,getSetting : function(){
			$.ajax({
				type: "GET",
				dataType : 'json',
			    url: "/api/v1/public/ROMD1010/selectSetting",
			    data : "roomCd="+fnObj.roomCd,
		        success:function(res){

		        	if(res.error){
		        		setTimeout(fnObj.refreshThisPage, 5000);
		        	}else{
			        	fnObj.envset =res.map.envset;
			        	fnObj.booking =res.map.booking;
			        	fnObj.inspectRoom =res.map.inspectRoom;
			        	fnObj.remainTime = res.map.remainTime;
			        	fnObj.BookingYN = res.map.BookingYN;
			        	fnObj.openYN = res.map.openYN;
			        	fnObj.nextBookingYN = res.map.nextBookingYN;
			        	fnObj.time = res.map.time.split(",");

			        	var pageId = $('#pageId').val();
						eval(pageId+'()');
		        	}
		        },complete : function (){

		        },error:function(request,status,error){
		        	//setTimeout(fnObj.refreshThisPage, 5000);
		        	// alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }
		    }).always(function() {
		    	setTimeout(fnObj.getSetting, 5000);
		    });
		},refreshThisPage : function(){ // 화면리로드

			fnObj.aud.pause();
			$(fnObj.aud).unbind('ended');
			location.reload();
			 $('body').on("selectstart", function(event){ return false; });
		     $('body').on("dragstart", function(event){ return false; });
		},
		refreshThisPage2 : function(){ // 화면리로드
			if($('#pageId').val() == "ROMD1020"){
				return;
			}
			fnObj.aud.pause();
			$(fnObj.aud).unbind('ended');
			location.reload();
			 $('body').on("selectstart", function(event){ return false; });
		     $('body').on("dragstart", function(event){ return false; });
		}
		,start : function(){//제례시작
			$("#remainTime").val(fnObj.remainTime); //남은시간 세팅
			//위패인지 체크
			if(fnObj.booking.contid == "" || fnObj.booking.contid == null || fnObj.photoYn =='N' || typeof fnObj.photoYn == 'undifined'){
				return fnObj.wipae();
			}
			//사진
			var contUrl = "";
			contUrl = fnObj.booking.mdcontentsVTO.contURL;
			if($("#contFile").attr("src") != contUrl){
				$("#contFile").attr("src",contUrl);
				$(".left").hide();
				$(".right").hide();
				$(".center").hide();
				$("#background").css("background","url('${pageContext.request.contextPath}/images/kiosk/kiosk_07_02_2.gif')");
			}
			//fnObj.nextBookingYN = 'Y';

		},wipae : function (){
			//1인일 경우
			if(fnObj.booking.conttitle.indexOf(",") == -1){
				$("#background").css("background","url('${pageContext.request.contextPath}/images/kiosk/kiosk_07_02.gif') no-repeat");
				$(".center").text(fnObj.booking.conttitle);
				$(".left").hide();
				$(".right").hide();

			}
			//2인일 경우
			else{
				var conttitle = fnObj.booking.conttitle.split(",");
				$("#background").css("background","url('${pageContext.request.contextPath}/images/kiosk/kiosk_07_02_1.gif') no-repeat");
				$(".center").hide();
				$(".left").show().text(conttitle[0]);
				$(".right").show().text(conttitle[1]);
			}

		},end : function(){ // 제례종료

			var booking = {
					bookDate:fnObj.booking.bookDate
					,bookSeq:fnObj.booking.bookSeq
			};

			$.ajax({
				type: "POST",
				dataType : 'json',
			    url: "/api/v1/public/ROMD1010/updateEnd",
			    data : booking,
			    sync : true,
		        success:function(res){

		        	if(res.error){
		        		setTimeout(fnObj.refreshThisPage, 5000);
		        	}else{
		        		fnObj.cleanTime();
		        		fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romd/ROMD1030.jsp');
			        	fnObj.Broadcast("ROMD12010");
		        	}

		        }
		    });

		},cleanTime : function (){ // 정리시간이후 예약대기화면으로 이동
   			setTimeout(fnObj.stop, Number(fnObj.envset.cleanTime)*60*1000); //분을 초로 변환 (1/1000)
		}
		// 시간 연장가능여부 체크
		,extChk : function(){
			//예약확인
			if(fnObj.booking == null){
				return false;
			}
			// 남은시간
			if(fnObj.remainTime > 5 ){
				return false;
			}
			// 연장횟수
			if(typeof fnObj.booking.exCnt == 'undifined' || fnObj.booking.exCnt >= fnObj.envset.extCnt ){
				return false;
			}
			// 다음예약자 있나
			if(fnObj.nextBookingYN == 'Y' ){
				return false;
			}
			return true;
		}
		,extTime : function (res){ // 제례시간연장

			var booking = {
					bookDate:fnObj.booking.bookDate
					,bookSeq:fnObj.booking.bookSeq
				};
			 $.ajax({
				type: "POST",
				dataType : 'json',
			    url: "/api/v1/public/ROMD1010/updateExtTime",
			    data : booking,
			    sync : true,
		        success:function(res){
		        	if(res.error){
		        		setTimeout(fnObj.refreshThisPage, 5000);
		        	}else{
		        		fnObj.Broadcast("ROMD11010", fnObj.envset.extTime);
						$("#extBtn").hide();
		        	}
		        }
		    });
		}
		,stop: function (){		//첫화면으로 이동
			fnObj.nextBookingYN = "N";
			location.href = '${pageContext.request.contextPath}/api/v1/public/ROMD1010/'+fnObj.roomCd;
			//fnObj.goUrl('${pageContext.request.contextPath}/api/v1/public/ROMD1010/'+fnObj.roomCd);

		}
		// 개방여부 , 점검중 체크
		,defaultChk : function(){

			if(fnObj.envset != null){
				if(fnObj.openYN == "N" ){
					fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romr/notOpen.jsp');

					return false;
				}
				if(fnObj.inspectRoom != null ){
					fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romd/inspect.jsp');
					return false;
				}
			}
			return true;
		}
		,goUrl : function (url,p_callback){	 //화면변경

			$.ajax({
		        url:url,
		        sync : false,
		        success : function (res){
		        	var div = document.createElement('div');
		    		div.innerHTML = res;

		    		document.querySelector('#wrap').innerHTML= div.getElementsByTagName('div')[0].innerHTML;
		    		fnObj.timeZone();
		    		if(p_callback){
		    			p_callback();
		    		}
		    		 $('body').on("selectstart", function(event){ return false; });
				     $('body').on("dragstart", function(event){ return false; });
		        }
		    });

		}
		,Broadcast : function (msgId,szText){ //TTS

			if(fnObj.envset.ttsYn == 'Y'){
				var url = "/api/v1/public/tts?msgId="+msgId;
				fnObj.aud.pause();
				if(szText != undefined){
					url = "/api/v1/public/tts?msgId="+msgId+"&szText="+szText||"";
				}
				fnObj.aud.src = url;
				if (fnObj.aud.paused) {
					fnObj.aud.play();
			    }
			    else {
			    	fnObj.aud.pause();
			    }
			}
		}
		,timeZone : function(){  //화면상단 시간
			$("#time1").text(fnObj.time[0]);
			$("#time2").text(fnObj.time[1]);
			$("#time3").text(fnObj.time[2]);
		}



}

function ROMD1010(){ //예약대기 화면
	fnObj.timeZone();

	if(!fnObj.defaultChk()){
		return;
	}
	if(fnObj.BookingYN == "Y"){
		fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romd/ROMD1020.jsp',fnObj.start);
	}
}

function ROMD1020(){ //제례중 화면
	fnObj.timeZone();


	$("#extBtn").hide(); //연장버튼 숨김
	$("#remainTime").val(fnObj.remainTime); //남은시간 세팅

	if(fnObj.extChk() == true){ //연장여부 체크함수
		$("#extBtn").show();
	}

	if(fnObj.remainTime <= 0 ){ // 남은시간 0이하면 제례종료
		fnObj.cleanTime();
		fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romd/ROMD1030.jsp');
		fnObj.Broadcast("ROMD12010");
    	return;
	}

}

function ROMD1030(){	//제례종료 화면

	fnObj.timeZone();

	if(!fnObj.defaultChk()){
		return;
	}

	if(fnObj.remainTime > 0 ){
		fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romd/ROMD1010.jsp');
		return;
	}
}

function notOpen(){ //폐장 화면
	fnObj.timeZone();
	if(fnObj.BookingYN == "Y"){
		fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romd/ROMD1020.jsp',fnObj.start);
		return;
	}

	if(fnObj.openYN == 'Y'){
		fnObj.goUrl('${pageContext.request.contextPath}/api/v1/public/ROMD1010/'+fnObj.roomCd);
		return;
	}

}

function insepct(){ //점검중 화면
	fnObj.timeZone();
	if(fnObj.BookingYN == "Y"){
		fnObj.goUrl('${pageContext.request.contextPath}/jsp/public/romd/ROMD1020.jsp',fnObj.start);
		return;
	}
	if(fnObj.inspectRoom == null ){
		fnObj.goUrl('${pageContext.request.contextPath}/api/v1/public/ROMD1010/'+fnObj.roomCd);
		return;
	}
}

</script>

</head>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<div align="center" id="wrap">
  <TABLE WIDTH=1920 BORDER=0 CELLPADDING=0 CELLSPACING=0 height="1080">
	  <TR>
		  <TD height="103" background="${pageContext.request.contextPath}/images/kiosk/kiosk_01_01.gif"  align="right">
		  	 <div id="timeZone" style="height:103px; width:300px; margin-right:70px;">
		  		<table height="100">
		  			<tr>
		  				<td id="time1"></td>
		  				<td rowspan="2" id="time3" width="100"></td>
		  			</tr>
		  			<tr>
		  				<td id="time2" width="100"></td>
		  			</tr>
		  		</table>
		  	</div>
		  </TD>
	  </TR>
	  <TR>
		  <TD height="868" background="${pageContext.request.contextPath}/images/kiosk/kiosk_10_03.gif"></TD>
	  </TR>
	  <TR>
		  <TD WIDTH=1920  background="${pageContext.request.contextPath}/images/kiosk/kiosk_01_04.gif"></TD>
	  </TR>
  </TABLE>

     <input type="hidden" id="pageId" value="ROMD1010">
</div>
</BODY>
</HTML>