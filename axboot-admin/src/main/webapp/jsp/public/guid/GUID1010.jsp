<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
<style type="text/css">
/* 마스크 뛰우기 */
 #mask {  
   position:absolute;  
   z-index:9000;  
   background-color:#000;  
   display:none;  
   left:0;
   top:0;
 } 
 
/*
 팝업으로 뜨는 윈도우 css  
*/ 
 .window{
   display: none;
   position:absolute;  
   left:60%;
   top:50px;
   margin-left: -500px;
   width:720px;
   height:930px;
   background-color:#FFF;
   z-index:10000;   
 }

 @font-face {
	font-family: 'Noto Sans Korean';
	src: url('${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.woff')
}



@font-face {
    font-family: 'Noto Sans Korean';
    font-style: normal;
    font-weight: 200;
    src: 
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.eot),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.eot?#iefix) format('embedded-opentype'),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.woff) format('woff'),
    url(${pageContext.request.contextPath}/static/font/NotoSansKR-Black-Hestia.otf) format('opentype');
}

html, body{
	width: 100%;
	height: 100%;
	min-height: 100%;
	padding: 0px;
	border: 0px;
	margin: 0px;
	overflow: hidden;
   /*  cursor: none; */
}
input {border :none; text-align: center;}

body, label, th, td, input {
	font-family: 'Noto Sans Korean';
	font-size: 40px;
	overflow:  hidden;
}

table {
	table-layout: fixed;
	white-space: nowrap;
}

td, th {
	white-space: nowrap;
	overflow: visible;
	text-overflow: ellipsis;
	padding: 0px;
}

label {
	display: inline-block;
}

img {
	border: none;
	padding: 0;
	margin: 0;
}
#time1 {font-size:20px; padding-top:25px}	
#time2 {font-size:20px; padding-bottom:15px}	
#time3 {font-size:35px; padding-left:25px; padding-top: 10px;}	
</style>

    <script type="text/javascript" src="<c:url value='/static/plugins/jquery/jquery.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/axisj/dist/AXJ.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/plugins/chartjs/Chart.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/ax5-polyfill.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/modal.js' />"></script>
    <script type="text/javascript" src="<c:url value='/static/js/common/app.js' />"></script>
<!--     <script src="http://code.jquery.com/jquery-latest.js"></script> -->
<script type="text/javascript">

$(document).ready(function(){

	
	setTimeout(countTime, 1000);	
	fnObj.getSetting();
	 $('body').on("selectstart", function(event){ return false; });
	 $('body').on("dragstart", function(event){ return false; });
});

var fnObj = {
		envSet : null,			
		now : "",			
		deadName : "",
		time : null,
		roomCd : "${roomCd}",
		inpectKiosk : null,
		getSetting : function(){
			var _this = this;		
			
			$.ajax({
				type: "GET",
				dataType : 'json',
				url: "/api/v1/public/GUID1010/selectSetting",
		        success:function(res){
		        	fnObj.envSet =res.map.envset;    
		        	fnObj.time = res.map.time.split(","); 
		        	fnObj.openYN = res.map.openYN;
		        	fnObj.inpectKiosk = res.map.inpectKiosk;
		        	var pageId = $('#pageId').val();
		        	
					eval(pageId+'()');
		        },
		        complete : function (){				
					setTimeout(fnObj.getSetting,5000);
					
				}
		    });
		}
	
}



var exec = null;
function print(){
	if(exec==null){
		 exec = require('child_process').exec;
	}


	$("#deadName").val(deadObj.deadName);
	$("#loc").val(deadObj.loc);
	$("#kindName").val(deadObj.kindName);
	
	var html = $("#idPrint2").html();
	
	
	
    exec('printhtml.exe html="'+html+'" header= footer=', function(err, data) {  
        console.log(data.toString());                       
    }); 
}


var DeadInfo = function(){
	
	this.beforeHTML = [];
	this.kind = "";
	this.kindName = "";
	this.loc = "";
	this.deadName = "";
	this.page = {
			currentPage: 1,
            pageSize: 0,
            totalElements: 0,
            totalPages: 0
        };	
}

var deadObj = new DeadInfo();


var time = 0;

var aud = new Audio();

function countTime(){
	time++;
}

function countZero(){
	time = 0;
}

function timeOut(){
	
	if(time>60 ){
		Broadcast("ROMR11999");
		refreshThisPage();		
		countZero();	
	}

}
function timeZone(){
	$("#time1").text(fnObj.time[0]);
	$("#time1").css({"font-size":"20px","padding-top":"25px"});
	$("#time2").text(fnObj.time[1]);
	$("#time2").css({"font-size":"20px","padding-bottom":"15px"});
	$("#time3").text(fnObj.time[2]);
	$("#time3").css({"font-size":"35px","padding-left":"25px","padding-top":"10px"});
}


var Broadcast = function (msgId,szText){
	
	if(fnObj.envSet.ttsYn == 'Y'){
		var url = "/api/v1/public/tts?msgId="+msgId;
		aud.pause();	
		if(szText != undefined){		
			url = "/api/v1/public/tts?msgId="+msgId+"&szText="+szText||"";
		}
					
		aud.src = url;
		 
		
		
		if (aud.paused) {
			aud.play();			      
	    }
	    else {
	    	aud.pause();	      
	    }
		
	}
	
}


function refreshThisPage(){
	
	aud.pause();
	deadObj = new DeadInfo();
	goUrl(window.location.href);		
	
}

function goUrl(url,prams,p_callback){	
	
	$.ajax({
        url:url,
        data : prams,
        sync : false,
        success : function (res){
        	deadObj.beforeHTML.push(document.querySelector('#wrap').innerHTML);
    		var div = document.createElement('div');
    		div.innerHTML = res;
    	
    		document.querySelector('#wrap').innerHTML= div.getElementsByTagName('div')[0].innerHTML;  
    		timeZone();
    		
        },complete : function (){
        	
        	if(p_callback){
    			p_callback(data);
    		}
        	
        }
        
    });
	
}

function beforeSearch(){
	
	document.querySelector('#wrap').innerHTML = deadObj.beforeHTML.pop();
	
}




function startSch(kind){
	deadObj.kind =kind;
	goUrl("/jsp/public/guid/GUID1020.jsp",null,Broadcast("ROMR12010"))
	
	
	 
}

function selectDeadList(pageNo){
		
	if($("#deadName").val() != null){
		
		fnObj.deadName = $("#deadName").val();
	}
	if(fnObj.deadName.length < 2){		
		Broadcast("ROMR12020");
		return;
	}
	var pattern = /([^가-힣\x20])/i; 	
	
	if (pattern.test(fnObj.deadName)) { 
		Broadcast("ROMR12030");
		return;
	} 
	
	keyPadDel('2');
	deadList(pageNo);
	
}


function deadList(pageNo){
	deadObj.deadName = "";
	var pageNum = pageNo || 1;		
	$.ajax({
		type: "GET",	
        url: "/OPER1010/selectDeadSchList",
        dataType : 'json',
        data: "pageNumber=" + (pageNum-1)+ "&pageSize=7&deadName=" + fnObj.deadName+"&kind="+deadObj.kind,
		success : function(res){				
			deadObj.page = res.page;						
			deadObj.page.currentPage = pageNum;
			
			if(res.page.totalElements == 0){
				
				Broadcast("ROMR12040",fnObj.deadName);			
				return;
			}else if(res.page.totalElements > 0){
				goUrl("/jsp/public/guid/GUID1030.jsp",'');	
	        	var table = $("#tb_deadList");
	        	$("#tb_deadList tr:not(:first-child)").remove();
	        	$("#tb_page tr").remove();
	        	var tag = "";
	        	var items = res.list;
	        	
	        	for(i=0; i<items.length; i++){
	        	
	        		
	        		tag+= "<tr hieght='84' style=\"text-align: center;\" onclick=\"selectedElement(this); setInfo(\'"+items[i].deadName+"\',\'"+items[i].loc+"\',\'"+items[i].kindName+"\')\";>";
	        		tag+= "<td width='254'>"+items[i].deadName||+"&nbsp;"+"</td>";
	        		tag+= "<td width='126'>"+items[i].deadSex||+"&nbsp;"+"</td>";
	        		tag+= "<td width='267'>"+items[i].applName||+"&nbsp;"+"</td>";
	        		tag+= "<td width='302'>"+items[i].deadDate||+"&nbsp;"+"</td>";
	        		tag+= "<td width='305'>"+items[i].tombDate||+"&nbsp;"+"</td>";
	        		tag+= "<td width='206'>"+items[i].kindName||+"&nbsp;"+"</td>";
	        		tag+= "<td width='396'>"+items[i].loc||+"&nbsp;"+"</td>";
	        		tag+= "</tr>";        	
	        		
	        		table.append(tag);
	        		tag = "";
	        	}
	        	for(i=0; i<7-items.length; i++){
	        		tag ="<tr hieght='84' style='text-align: center;'><td width='254'>&nbsp;</td><td width='126'>&nbsp;</td><td width='267'></td><td width='302'></td><td width='305'></td><td width='206'></td><td width='396'></td></tr>";
	        		table.append(tag);
	        		tag = "";
	        	}
	                	
	        	
	        
	        	var totalPage = deadObj.page.totalPages;
	        	totalPage = totalPage == 0 ? 1 : totalPage;
	        	
	        	var tag ="<tr align=center><td style='text-align:center;'>";        	
	        	if(pageNum-1 > 0){
	        		tag += "<a href='javascript:deadList("+(pageNum-1)+");'><img src='${pageContext.request.contextPath}/images/kiosk/back_01.png' style='vertical-align: middle;'></a>";
	        		
	        	}
	        	if(pageNum-1 <= 0){
	           		tag += "<a href='#'><img src='${pageContext.request.contextPath}/images/kiosk/back_01.png' style='vertical-align: middle;'></a>";
	           	 }        	
	        	 
	          	 tag+=  "<span style='vertical-align: middle; line-height:84px;'>" +pageNum+ " / " +totalPage+ "</span>";
	          	         	
	        	 if(pageNum+1 > totalPage){
	           		 tag+= "<a href='#'> <img src='${pageContext.request.contextPath}/images/kiosk/front_01.png' style='vertical-align: middle;'></a>";
	           	 }
	        	 if(pageNum+1 <= totalPage){
	        		 
	        		 tag+= "<a href='javascript:deadList("+(pageNum+1)+");'> <img src='${pageContext.request.contextPath}/images/kiosk/front_01.png' style='vertical-align: middle;'></a>";
	        	 }
	        	
	        	 tag+="</td></tr>";
	        	$("#tb_page").append(tag);        	
			}
        }
    });
}


function setInfo(deadName,loc,kindName){
	
	deadObj.deadName = deadName;
	deadObj.loc = loc;
	deadObj.kindName = kindName;
}



function selectedElement(el){
	var tags = el.parentNode.getElementsByTagName(el.tagName);
	
	for(var i=0; i<tags.length; i++){
		tags[i].style.background = '';
		el.setAttribute('selected', '');
	}
	//el.style.background = '#eeeeee';
	el.style.background = '#8CBDED';
	el.setAttribute('selected', 'selected');
}

function wrapWindowByMask(){
    //화면의 높이와 너비를 구한다.
    var maskHeight = $(document).height();  
    var maskWidth = $(window).width();  
        
    //마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
    $('#mask').css({'width':maskWidth,'height':maskHeight});  
    //애니메이션 효과 - 일단 0초동안 까맣게 됐다가 60% 불투명도로 간다.
    $('#mask').fadeIn(0);      
    $('#mask').fadeTo("slow",0.6);    
    //윈도우 같은 거 띄운다.
    $('.window').show();
}

//닫기 버튼을 눌렀을 때
function close(){
	  $('#mask, .window').hide();  
	  $("#phoneNo").val("");
	  $("#etext").val("");
}  
function open(){
	 wrapWindowByMask();
}
    

// function print(){
// 	 var dd = deadObj.deadName;
// 	 /* $("#tb_deadList tr").each(function(){
		 
// 		  if($(this).attr("selected")){
// 				open();
// 		  }else{
// 			  Broadcast("GUID13020");
// 		  }
// 	 }); */
	
	
	
// 	 if(deadObj.deadName == ""){		
// 		Broadcast("GUID13020");
// 		return;
// 	}
// 	 open();
	
// }


var initBody;
/* 
function beforePrint()
{ 
	initBody = document.body.innerHTML; 
	document.body.innerHTML = idPrint2.innerHTML;
} 

function afterPrint()
{ 
	document.body.innerHTML = initBody;
} 



function printArea()
{
	window.onbeforeprint = beforePrint;
	window.onafterprint = afterPrint;
	window.print();
} 

function PrintPage(flag)
{
	if(deadObj.kindName == ""){
		//alert("제례대상 고인을 선택한 후 다음단계 버튼을눌러 주세요");
		Broadcast("GUID12010");
		return;
	}	
	
	
	if (flag == 'yes')
	{
			
			
		
			window.onbeforeprint = beforePrint; 
			window.onafterprint = afterPrint; 			

			window.focus();	//포커스를 이 프레임으로

			IEPageSetupX.Orientation = 1;//인쇄 방향 설정 - 가로0, 세로1
			IEPageSetupX.header='';//머리글
			IEPageSetupX.footer='';//바닥글
			IEPageSetupX.leftMargin=3;//왼쪽여백
			IEPageSetupX.rightMargin=3;//오른쪽여백
			IEPageSetupX.topMargin=5;//위쪽여백
			IEPageSetupX.bottomMargin=5;//아래쪽여백

            IEPageSetupX.Printer = IEPageSetupX.GetDefaultPrinter();
		
			IEPageSetupX.PrintBackground = true;
			IEPageSetupX.Print();//인쇄(인쇄 대화상자 표시)
			
			
			
			
		
}}
 */

function saveSms(){
	 var regExp = /^01([0|1|6|7|8|9]?)?([0-9]{3,4})?([0-9]{4})$/;
	 
	var phoneNo = $("#phoneNo").val();
		if(phoneNo.length == 0){
			Broadcast("ROMR21020");
			return;
	}
	if(!regExp.test( $("#phoneNo").val() ) ) {
		  Broadcast("ROMR15040");
		  //alert("잘못된 휴대폰 번호입니다.");
		     return;
	}	 
			
	 
	 
	 $.ajax({
			type: "GET",	
	        url: "/api/vi/public/ROMR1060/saveSms",
	        dataType : 'json',
	        data: deadObj,
			success : function(res){				
			
			}
	 });
 }
 
 function defaultChk(){
		
		if(fnObj.envSet != null){
			
			if(fnObj.openYN == "N"){
				goUrl('${pageContext.request.contextPath}/jsp/public/romr/notOpen.jsp');		
				return false;
			}else{
				$(fnObj.inpectKiosk).each(function(i){
					
					if(this.roomCd == fnObj.roomCd ){
						goUrl('${pageContext.request.contextPath}/jsp/public/romd/insepct.jsp');		
						return false;
					}
				});
			}
		}	
		return true;
	}

function GUID1010(){	
	timeZone();
	deadObj.beforeHTML=[];
	if(!defaultChk()){
		return;
	}
	deadObj = new DeadInfo();
	
}

function GUID1020(){	
	if(!defaultChk()){
		return;
	}
	timeZone();
	timeOut();
	 
}

function GUID1030(){	
	if(!defaultChk()){
		return;
	}
	timeZone();
	timeOut();
}

function keyPadChange(type){
	
	if(type == 'eng'){
		$("#hanKeypad").show();
		$("#engKeypad").hide();
	}else{
		$("#hanKeypad").hide();
		$("#engKeypad").show();
	}		
}

function keyPad(chk){	
	
	$("#etext").val($("#etext").val()+chk);
	translate();
	
	 // document.form1.etext.value =document.form1.etext.value+chk;
}

function keyPadDel(chk){
  if(chk=='1'){
	  $("#etext").val($("#etext").val().substring(0,$("#etext").val().length-1));
	  translate();
  //document.form1.etext.value =document.form1.etext.value.substring(0,document.form1.etext.value.length-1);
  }
  if(chk=='2'){
	  $("#etext").val("");
	  $("#deadName").val("");
  //document.form1.etext.value="";
  }
}

k1 = "ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ";
k2 = "ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ";
k3 = " ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ";

n1 = ' ';
n2 = ' ';
n3 = ' ';

function onkey(c, jamo)
{
    switch(jamo)
    {
        // 자음
        case 1:
            // 만약에 이전에 모음이 있었으면
            if(n2!=' ') 
            {        
                // 만약에 이전에 자음도 있었으면
                if(n3!=' ') 
                {
                    if(n3=='ㄱ' && c=='ㅅ')
                    {
                        n3='ㄳ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄴ' && c=='ㅎ')
                    {
                        n3='ㄶ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㄱ') 
                    {
                        n3='ㄺ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅁ') 
                    {
                        n3='ㄻ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅂ') 
                    {
                        n3='ㄼ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅅ') 
                    {
                        n3='ㄽ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅌ') 
                    {
                        n3='ㄾ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅍ') 
                    {
                        n3='ㄿ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄹ' && c=='ㅎ') 
                    {
                        n3='ㅀ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㅂ' && c=='ㅅ')
                    {
                        n3='ㅄ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else 
                    {
                        n1 = c;
                        n2 = ' ';
                        n3 = ' ';
                        last = res;
                        res += n1;
                    }
                }
                // 이전에 모음만 있고 자음은 없었으면 아마 받침이 될 것임
                else
                {
                    // 이것들은 받침이 될 수 없음 
                    if(c=='ㄸ'||c=='ㅃ'||c=='ㅉ')
                    {
                        // 무조건 새 글자
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        n1 = c;
                        n2 = ' ';
                        n3 = ' ';
                        last = res;
                        res += n1;
                    }
                    // 무조건 받침으로 들어감
                    else 
                    {
                        n3=c;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                }
            }
            // 이전에 모음이 없었던 경우이므로 무조건 출력함
            else
            {        
                n1=c;
                n2=' ';
                n3=' ';
                last = res;
                res += c;
            }
            break;
        // 모음
        case 2:
            // 만약에 이전에 모음이 있었으면
            if(n2!=' ') 
            {
                // 만약에 이전에 받침도 있었으면
                if(n3!=' ')
                {
                    if(n3=='ㄳ') 
                    {
                        n3='ㄱ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅅ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res += String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄵ') 
                    {
                        n3='ㄴ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅈ';
                        n2 = c;
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄶ') 
                    {
                        n3='ㄴ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅎ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄺ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㄱ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄻ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅁ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄼ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅂ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄽ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅅ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄾ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅌ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㄿ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅍ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㅀ') 
                    {
                        n3='ㄹ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅎ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    else if(n3=='ㅄ') 
                    {
                        n3='ㅂ';
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        // 새 글자
                        n1 = 'ㅅ';
                        n2 = c;
                        n3 = ' ';
                        last = res;
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                    // 이전에 받침을 꾸어옴
                    else 
                    {
                        temp = n3;
                        n3 = ' ';
                        // 새 글자
                        res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                        n1 = temp;
                        n2 = c;
                        last = res;
                        res += String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                    }
                }
                else 
                {
                    if(n2=='ㅗ')
                    {
                        if(c=='ㅏ') n2='ㅘ';
                        if(c=='ㅐ') n2='ㅙ';
                        if(c=='ㅣ') n2='ㅚ';
                    }
                    if(n2=='ㅜ')
                    {
                        if(c=='ㅓ') n2='ㅝ';
                        if(c=='ㅔ') n2='ㅞ';
                        if(c=='ㅣ') n2='ㅟ';
                    }
                    if(n2=='ㅡ')
                    {
                        if(c=='ㅣ') n2='ㅢ';
                    }
                    res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                }
            }
            else 
            {
                // 만약에 이전에 자음이 있었으면 
                if(n1!=' ')
                {
                    n2=c;
                    res = last + String.fromCharCode(0xAC00 + k1.indexOf(n1)*21*28 + k2.indexOf(n2)*28 + k3.indexOf(n3));
                }
                else
                {
                    n1 = ' ';
                    n2 = ' ';
                    n3 = ' ';
                    last = res;
                    res += c;
                }
            }
            break;
        // 그 이외
        case 0:
            if(n1!=' '&&n2==' '&&n3==' ') 
            {
                last = res;
                res += n1;
            }
            else if(n1==' '&&n2!=' '&&n3==' ') 
            {
                last = res;
                res += n2;
            }
            n1 = ' ';
            n2 = ' ';
            n3 = ' ';
            last = res;
            res += c;
            break;
    }
}

function translate()
{
    res = "";
    last = "";
    n1 = ' ';
    n2 = ' ';
    n3 = ' ';
    eng = false;
    
    for(n=0; n<document.getElementById("etext").value.length; n++)
    {
        jamo = 0;
        c = document.getElementById("etext").value.charAt(n);
        if(c=='"\"') 
        {
            eng = !eng;
            continue;
        }
        if(!eng)
        {
            switch(c)
            {
                // 키보드 제일 윗줄
                case 'ㅂ': c='ㅂ'; jamo=1; break;
                case 'ㅃ': c='ㅃ'; jamo=1; break;
                case 'ㅈ': c='ㅈ'; jamo=1; break;
                case 'ㅉ': c='ㅉ'; jamo=1; break;
                case 'ㄷ': c='ㄷ'; jamo=1; break;
                case 'ㄸ': c='ㄸ'; jamo=1; break;
                case 'ㄱ': c='ㄱ'; jamo=1; break;
                case 'ㄲ': c='ㄲ'; jamo=1; break;
                case 'ㅅ': c='ㅅ'; jamo=1; break;
                case 'ㅆ': c='ㅆ'; jamo=1; break;
                case 'ㅛ': c='ㅛ'; jamo=2; break;
                case 'ㅕ': c='ㅕ'; jamo=2; break;
                case 'ㅑ': c='ㅑ'; jamo=2; break;
                case 'ㅐ': c='ㅐ'; jamo=2; break;
                case 'ㅒ': c='ㅒ'; jamo=2; break;
                case 'ㅔ': c='ㅔ'; jamo=2; break;
                case 'ㅖ': c='ㅖ'; jamo=2; break;
            
                // 키보드 두번째 줄 
                case 'ㅁ': c='ㅁ'; jamo=1; break;
                case 'ㄴ': c='ㄴ'; jamo=1; break;
                case 'ㅇ': c='ㅇ'; jamo=1; break;
                case 'ㄹ': c='ㄹ'; jamo=1; break;
                case 'ㅎ': c='ㅎ'; jamo=1; break;
                case 'ㅗ': c='ㅗ'; jamo=2; break;
                case 'ㅓ': c='ㅓ'; jamo=2; break;
                case 'ㅏ': c='ㅏ'; jamo=2; break;
                case 'ㅣ': c='ㅣ'; jamo=2; break;
            
                // 키보드 제일 밑 줄
                case 'ㅋ': c='ㅋ'; jamo=1; break;
                case 'ㅌ': c='ㅌ'; jamo=1; break;
                case 'ㅊ': c='ㅊ'; jamo=1; break;
                case 'ㅍ': c='ㅍ'; jamo=1; break;
                case 'ㅠ': c='ㅠ'; jamo=2; break;
                case 'ㅜ': c='ㅜ'; jamo=2; break;
                case 'ㅡ': c='ㅡ'; jamo=2; break;
            
            }
        }
        
       onkey(c,jamo);
    }
    
   
    document.form1.deadName.value = res;
   
    
   
  
}

</script>
</HEAD>
<BODY onclick="countZero()" BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<div id="wrap" align="center" >
  <div id="mask"></div> 
  <TABLE WIDTH=1920 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	  <TR>
		  <TD height="103" background="${pageContext.request.contextPath}/images/search/search_01_01.gif" align="right">
		  <div id="timeZone" style="height:103px; width:300px; margin-right:70px;">
		  		<table height="100">
		  			<tr>
		  				<td id="time1"></td>
		  				<td rowspan="2" id="time3" width="100"></td>
		  			</tr>
		  			<tr>
		  				<td id="time2" width="100"></td>
		  			</tr>
		  		</table>
		  	</div> 
		  </TD>
	  </TR>
	  <TR>
		  <TD height="868" background="${pageContext.request.contextPath}/images/search/search_01_02.gif"></TD>
	  </TR>
	  <TR>
		  <TD height="109" background="${pageContext.request.contextPath}/images/search/search_01_03.gif">
			  <div align="center">
			  <a href="javascript:startSch('02');"><img src="${pageContext.request.contextPath}/images/search/search_01_04.gif" width="233" height="110" border="0"></a>
<%-- 			  <a href="javascript:startSch('03');"><img src="${pageContext.request.contextPath}/images/search/search_01_05.gif" width="233" height="110" border="0"></a> --%>
<%-- 			  <a href="javascript:startSch('05');"><img src="${pageContext.request.contextPath}/images/search/search_01_06.gif" width="233" height="110" border="0"></a> --%>
<%-- 			  <a href="javascript:startSch('04');"><img src="${pageContext.request.contextPath}/images/search/search_01_07.gif" width="233" height="110" border="0"></a> --%>
<%-- 			  <a href="javascript:startSch('06');"><img src="${pageContext.request.contextPath}/images/search/search_01_08.gif" width="233" height="110" border="0"></a> --%>
			  <a href="javascript:startSch('ALL');"><img src="${pageContext.request.contextPath}/images/search/search_01_09.gif" width="233" height="110" border="0"></a>
			  </div>
		  </TD>
	  </TR>
  </TABLE>
  <input type="hidden" id="pageId" value="GUID1010">
</div>
</BODY>
</HTML>
