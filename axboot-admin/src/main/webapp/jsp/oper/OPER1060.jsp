<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j"%>

<ax:layout name="base.jsp">
	<ax:set name="title" value="${PAGE_NAME}" />
	<ax:set name="page_desc" value="${PAGE_REMARK}" />

	<ax:div name="contents">
		<ax:row>
			<ax:col size="12">
				<ax:custom customid="page-button" pageId="${PAGE_ID}"
					searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}"
					excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}"
					function2Auth="${FUNCTION_2_AUTH}"
					function3Auth="${FUNCTION_3_AUTH}"
					function4Auth="${FUNCTION_4_AUTH}"
					function5Auth="${FUNCTION_5_AUTH}">
				</ax:custom>


				<div class="ax-button-group">
					<div class="left">
						<h2>
							<i class="axi axi-list-alt"></i> 모니터링 장비 목록
						</h2>
					</div>
					<div class="right">
						
						<input type="button" value="전체시작" class="AXButton" id="on"	onclick="fnObj.monitorOn();" /> 
						<input type="button" value="전체종료" class="AXButton" id="off" onclick="fnObj.monitorOff();" />
					</div>
					<div class="ax-clear">

						<!--  	<img alt="" src="/OPER1060/monitering?ip=192.168.1.38" id="img">  -->
					</div>
					
				</div>
				
				<div class="ax-clear">

						<!--  	<img alt="" src="/OPER1060/monitering?ip=192.168.1.38" id="img">  -->
					</div>
			
				
						<div class="left" id="contents">
				<!-- 	<img alt="" src="/OPER1060/monitering?ip=192.168.1.38"   class="thumb"> -->
						</div>
					
					
						
							
						
				
				

			</ax:col>
		</ax:row>

	</ax:div>
	<ax:div name="styles">
		<style type="text/css">
			
			.thumb {
		        width: 200px;
		        height: 200px;
		        
		      }
		</style>
	</ax:div>
	<ax:div name="scripts">	   
		<script type="text/javascript">
		axdom(document.body).ready(function() {
			
			
		});
	
		
         var fnObj = {
       		
             pageStart: function(){
            
                
                 this.bindEvent();
                 // 페이지 로딩 후 바로 검색 처리하기 (option)
                 fnObj.setPage();
                 
             },
             bindEvent: function(){
                 var _this = this;
                 $("#ax-page-btn-search").bind("click", function(){
                     _this.setPage();
                 });
                 $("#on").bind("click", function(){
                     _this.monitorOn();
                 });
                 $("#off").bind("click", function(){
                     _this.monitorOff();
                 });
             	
             },
            
             setPage: function(pageNo, searchParams){
                 var _target = this.target;
                 this.pageNo = pageNo;

                  app.ajax({
                      type: "GET",
                      url: "/OPER1060/selectMachineList", 
                      data : (searchParams)
                  }, function(res){
                      if(res.error){
                         alert(res.error.message);
                      }
                      else
                      {
                    	  $("#contents").empty();
                          var gridData = {
                              list: res.list,
                              
                          };          
                          var src = gridData.list[1].ipaddress;
                          var html = "";
                          $(gridData.list).each(function(i){
                    
                        	html += "<a href=javascript:fnObj.open('"+this.ipaddress+"','"+this.machineNm+"')>"
                          	html += "<img src='/OPER1060/monitering?ip="+this.ipaddress+"&width=150&height=150' width='150' height='150' class='thumb' alt='"+this.machineNm+"' />";
                          //	html += "<img src='/OPER1060/monitering?ip="+this.ipaddress+"'  onError='this.src=\"\/static/plugins/fancyBox/demo/1_b.jpg\"\'  width='150' height='150' class='thumb' />";
                          	html += "</a>"
                      
                          }) 	
                          $("#contents").append(html);
                        
                          //$("#contents").append("<img src='/OPER1060/monitering?ip="+src+"' class='thumb' open('"++"')>"); 
                          //$(".thumb").css({"width":200,"height":200});
                        
                      }
                  });
                  
             },
             open: function(ip,name){
            	
        		
                 app.modal.open({
                     url:"OPER1061.jsp",
                     pars:"ip="+ip+"&name="+name, // callBack 말고
                     //width:500, // 모달창의 너비 - 필수값이 아닙니다. 없으면 900
                     //top:100 // 모달창의 top 포지션 - 필수값이 아닙니다. 없으면 axdom(window).scrollTop() + 30
                 })
             },
             monitorOn : function (){
            	 app.ajax({
                     type: "GET",
                     url: "/OPER1060/monitorOn"                   
                 }, function(res){
                     if(res.error){
                        alert(res.error.message);
                     }
                     else
                     {
                    	 fnObj.setPage();
                     }
                 });
             },
             monitorOn : function (){
            	 app.ajax({
                     type: "GET",
                     url: "/OPER1060/monitorOff"                   
                 }, function(res){
                     if(res.error){
                        alert(res.error.message);
                     }
                     else
                     {
                    	 fnObj.setPage();
                     }
                 });
             }
             
         };
		</script>
	</ax:div>

</ax:layout>