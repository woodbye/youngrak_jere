<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j"%>

<ax:layout name="base.jsp">
	<ax:set name="title" value="${PAGE_NAME}" />
	<ax:set name="page_desc" value="${PAGE_REMARK}" />

	<ax:div name="contents">
		<ax:row>
			<ax:col size="12">
				<ax:custom customid="page-button" pageId="${PAGE_ID}"
					searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}"
					excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}"
					function2Auth="${FUNCTION_2_AUTH}"
					function3Auth="${FUNCTION_3_AUTH}"
					function4Auth="${FUNCTION_4_AUTH}"
					function5Auth="${FUNCTION_5_AUTH}">
				</ax:custom>


				<div class="ax-button-group">
					<div class="left">
						<h2>
							<i class="axi axi-list-alt"></i> 모니터링 장비 목록
						</h2>
					</div>
					<div class="right">
						<input type="button" value="화면보기" class="AXButton"	onclick="fnObj.grid.getScreen();" /> <input type="button" value="시작" class="AXButton"	onclick="fnObj.grid.getExcel('json');" /> 
						<input type="button" value="종료" class="AXButton" onclick="fnObj.grid.getExcel('json');" />
					</div>
					<div class="ax-clear">
							<a class="fancybox-media" href="/OPER1060/monitering?ip=192.168.1.38">
						  	<img alt="" src="/OPER1060/monitering?ip=192.168.1.38"  width="150" height="150">
						  	</a>
						  	
						  	
					</div>
				</div>
				 
					<!--  <p><img alt="" src="/OPER1060/monitering?ip=192.168.1.31" onError='this.src="/static/plugins/fancyBox/demo/1_b.jpg"' /></p> -->
					<p id="page-img-box" ></p>
				
				 <p>
					<a class="fancybox-buttons" data-fancybox-group="button" href="/static/plugins/fancyBox/demo/1_b.jpg"><img src="/static/plugins/fancyBox/demo/1_s.jpg" alt="" /></a>

					<a class="fancybox-buttons" data-fancybox-group="button" href="/static/plugins/fancyBox/demo/2_b.jpg"><img src="/static/plugins/fancyBox/demo/2_s.jpg" alt="" /></a>
			
					<a class="fancybox-buttons" data-fancybox-group="button" href="/static/plugins/fancyBox/demo/3_b.jpg"><img src="/static/plugins/fancyBox/demo/3_s.jpg" alt="" /></a>
			
					<a class="fancybox-buttons" data-fancybox-group="button" href="/static/plugins/fancyBox/demo/4_b.jpg"><img src="/static/plugins/fancyBox/demo/4_s.jpg" alt="" /></a>
					<a class="fancybox-buttons" data-fancybox-group="button" href="/static/plugins/fancyBox/demo/4_b.jpg"><img src="/static/plugins/fancyBox/demo/4_s.jpg" alt="" /></a>
					<a class="fancybox-buttons" data-fancybox-group="button" href="/static/plugins/fancyBox/demo/4_b.jpg"><img src="/static/plugins/fancyBox/demo/4_s.jpg" alt="" /></a>
					<a class="fancybox-buttons" data-fancybox-group="button" href="/static/plugins/fancyBox/demo/4_b.jpg"><img src="/static/plugins/fancyBox/demo/4_s.jpg" alt="" /></a>
						
				</p>
				<h3>Thumbnail helper</h3>
				<p>
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="/static/plugins/fancyBox/demo/2_b.jpg"><img src="/static/plugins/fancyBox/demo/2_s.jpg" alt="" /></a>
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="/static/plugins/fancyBox/demo/2_b.jpg"><img src="/static/plugins/fancyBox/demo/2_s.jpg" alt="" /></a>
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="/static/plugins/fancyBox/demo/2_b.jpg"><img src="/static/plugins/fancyBox/demo/2_s.jpg" alt="" /></a>
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="/static/plugins/fancyBox/demo/2_b.jpg"><img src="/static/plugins/fancyBox/demo/2_s.jpg" alt="" /></a>
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="/static/plugins/fancyBox/demo/2_b.jpg"><img src="/static/plugins/fancyBox/demo/2_s.jpg" alt="" /></a>
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="/static/plugins/fancyBox/demo/2_b.jpg"><img src="/static/plugins/fancyBox/demo/2_s.jpg" alt="" /></a>
			
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="/static/plugins/fancyBox/demo/1_b.jpg"><img src="/static/plugins/fancyBox/demo/1_s.jpg" alt="" /></a>
				</p>
				
			</ax:col>
		</ax:row>

	</ax:div>

	<ax:div name="styles">	
		<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

		body {
			max-width: 700px;
			margin: 0 auto;
		}
		</style>
	</ax:div>
	
	<ax:div name="scripts">		
		<!-- Add mousewheel plugin (this is optional) -->
		<script type="text/javascript" src="/static/plugins/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<!-- Add fancyBox main JS and CSS files -->
		<script type="text/javascript" src="/static/plugins/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="/static/plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<!-- Add Button helper (this is optional) -->
		<link rel="stylesheet" type="text/css" href="/static/plugins/fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="/static/plugins/fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		
		<link rel="stylesheet" type="text/css" href="/static/plugins/fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="/static/plugins/fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		
		<script type="text/javascript">
			
		$(document).ready(function() {
		
			
			app.ajax({
                type: "GET",
                url: "/SYST1020/selectMachineList",            
            }, function(res){
                if(res.error){
                   alert(res.error.message);
                }
                else
                {
               	
                    var gridData = {
                        list: res.list,
                        
                    };
                    var src = [];
                    var html = "";
                    $(gridData.list).each(function(i){
                    	
                    	html += "<a class='fancybox-thumbs' data-fancybox-group='thumb' >";
                    //	html += "<a class='fancybox-thumbs' data-fancybox-group='thumb' >";
                    	html += "<img src='/OPER1060/monitering?ip="+this.ipaddress+"'   width='150' height='150'/>";                    	
                    	html += "</a>";
                 		src = "/OPER1060/monitering?ip="+this.ipaddress;
                    	
                    })
                   
                    $("#page-img-box").append(html);
                    $('.fancybox').fancybox();
                }
            });
			
			$('.fancybox').fancybox();
			$('.fancybox-thumb').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',
				closeBtn  : true,
				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},
				afterLoad : function() {
				
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				},
				afterShow: function() {
			        
			         var _this = this;
			    }
			});
			
			$('.fancybox-thumb').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,
				closeBtn  : true,
				
				helpers : {
					title : {
						type : 'inside',
						position: 'top'
					},
					
					thumbs : {
						width  : 150,
						height : 150
					}
				},
				beforeLoad : function() {                    
			         //  this.width = parseInt(this.href.match(/width=[0-9]+/i)[0].replace('width=',''));  
			         //   this.height = parseInt(this.href.match(/height=[0-9]+/i)[0].replace('height=',''));
			    },
				afterLoad : function() {
				/* 	var _this = this;
					this.height = 1080;
					var src = $('.fancybox-thumbs img').eq(_this.index).attr("src");
					var dd = $('.fancybox-thumbs').eq(_this.index).attr("href",src);
					var cc = $('.fancybox-thumbs').eq(_this.index).attr("href"); */
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});
			
			
			$(".fancybox")
		    .attr('rel', 'gallery')
		    .fancybox({
		    	
		        beforeShow: function () {
		            /* Disable right click */
		            $.fancybox.wrap.bind("contextmenu", function (e) {
		                    return false; 
		            });
		        }
		    });
			
			
			$('.fancybox-media').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {}
				}
			});
			
			/* $('.fancybox-buttons').fancybox({
				fitToView	: false,
				width		: '40%',
				height		: '40%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {
						
					},
					thumbs : {
			            width: 50,
			            height: 50
			        }
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
				,beforeShow: function () {
					if (this.title) {
		                // New line
		                this.title += '<br />';		                
		                // Add tweet button
		                this.title += '<a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="' + this.href + '"><input type="button" value="시작" class="AXButton" />  </a> ';
		                
		                      }		       
		        },
		        afterShow: function() {
		        
		           twttr.widgets.load();
		        }
		       
			}); */

		});
		</script>
	</ax:div>

</ax:layout>