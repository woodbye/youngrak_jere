<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>
<%
request.setAttribute("ip", request.getParameter("ip"));
request.setAttribute("name", request.getParameter("name"));

%>
<ax:layout name="modal.jsp">
	<ax:set name="title" value="${name} 모니터" />
	<ax:div name="contents">
		<%--  <img alt="" src="${pageContext.request.contextPath}/images/kiosk/kiosk_11_03.gif" id="img"> --%>
		 <img alt="" src="" id="img"  width="1280" height="720"  onload="setTimeout(fnObj.onload, 1000);">
		<!--  <img alt="" src="" id="img" onError='this.src="/static/plugins/fancyBox/demo/1_b.jpg"' width="100%" height="100%"> -->
		 <div class="ax-button-group">
	       
	          <div class="center" align="center">
	              <button type="button" class="AXButton" id="btn-start"><i class="axi axi-plus-circle"></i> 시작</button>
	              <button type="button" class="AXButton" id="btn-close"><i class="axi axi-minus-circle"></i> 종료</button>
	              <div class="loading" style="color:#ffffff;"></div>
	          </div>
	          <div class="ax-clear"></div>
    	  </div>
    </ax:div>
	
  
    	<ax:div name="scripts">
		<script type="text/javascript">
	  
		    var pageID = "OPER1061";
		    var ip = "${ip}";
		    var myProgress = new AXProgress();	
		    var loading_mask = new AXMask(); // 새로운 인스턴스 생성
			loading_mask.setConfig(); // 초기화
			loading_mask.setContent({ // 마스크 컨텐츠 정의
				width:200, height:200,
				html: '<div class="loading" style="color:#ffffff;">' +
				'<i class="axi axi-spinner axi-spin" style="font-size:100px;"></i>' +
				'<div>Loading..</div>' +
				'</div>'
			});	
		    
		    var fnObj = {
		        pageStart: function(){
		        
		          
		        	$("#img").attr("src","/OPER1060/monitering?ip="+ip+"&width=300&height=300&"+ new Date().getTime());	
		          	this.bindEvent();
		          //	this.onError();
		          
		          
		        },
		        onload : function (){
		        	$("#img").attr("src","/OPER1060/monitering?ip="+ip+"&width=300&height=300&"+ new Date().getTime());	
		        }
		        ,bindEvent: function(){
                	
                    var _this = this;                   
                    $("#btn-start").bind("click", function(){
                       _this.processCommand("startProcess",ip);
                       //_this.processCommand("restart",ip);
                    });   
                    $("#btn-close").bind("click", function(){
                    	_this.processCommand("destroyProcess",ip);
                     });
                    
                    
                    $("#img").bind("load", function(){
                    	loading_mask.close();
                    });
                    
                },
                
		        onError : function(){
		        	
		        	//<![CDATA[
					$("#img").each(function() {
					 var instance = $(this);
					 var img = new Image();
					 $(img).error(function() {
					  instance.attr("src", "/static/plugins/fancyBox/demo/1_b.jpg");
					 }).attr("src", instance.attr("src"));
					});
					//]]>
		        },
		        
		        close: function(){
		           
		                window.close();
		           
		        },   
	            processCommand : function(cmd,ip){
	            	 app.ajax({
	                     type: "GET",
	                     url: "/OPER1060/processCommand", 
	                     data : "command="+cmd+"&ip="+ip,
	                    
	                 }, function(res){
	                     if(res.error){
	                        alert(res.error.message);
	                     }
	                     else
	                     {
	                    	 
	                    	 //$("#img").attr("src","/OPER1060/monitering?ip="+ip+"&width=300&height=300");
	                    	
	                     	//$("#img").removeAttr("src");
	                     	/*  if(cmd == 'restart' || cmd == 'startProcess'){
	                     		 setTimeout(function(){
	                     			$("#img").attr("src","/OPER1060/monitering?ip="+ip+"&width=300&height=300");
	                     		 },7000)
	                     	 }else{
	                     		$("#img").attr("src","/OPER1060/monitering?ip="+ip+"&width=300&height=300");
	                     	 } */
	                    	 
	                    	 
	                    	
	                     }
	                 });
			     },
			   
		        
		    };
		    axdom(document.body).ready(function() {
				fnObj.pageStart();
			});
			axdom(window).resize(fnObj.pageResize);
		</script>
	</ax:div>
</ax:layout>


	

