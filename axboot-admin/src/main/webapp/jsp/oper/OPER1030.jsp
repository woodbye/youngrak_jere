<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>

<ax:layout name="base.jsp">
    <ax:set name="title" value="${PAGE_NAME}" />
    <ax:set name="page_desc" value="${PAGE_REMARK}" />
    <ax:div name="contents">
        <ax:row>
            <ax:col size="12">
                <ax:custom customid="page-button" pageId="${PAGE_ID}" searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}" excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}" function2Auth="${FUNCTION_2_AUTH}" function3Auth="${FUNCTION_3_AUTH}" function4Auth="${FUNCTION_4_AUTH}" function5Auth="${FUNCTION_5_AUTH}"></ax:custom>
               
                <ax:custom customid="table">
                    <ax:custom customid="tr">
                        <ax:custom customid="td">                         
                             <div class="ax-search" id="page-search-box"></div>
								<div class="ax-button-group">
				                   	 <div class="left">
				                		     	<h2><i class="axi axi-list-alt"></i> 운영환경 목록</h2>
				              			 </div>
				                 	 <div class="right">				                      
				                 	   <button type="button" class="AXButton" id="ax-form-btn-new"><i class="axi axi-plus-circle"></i> 신규</button>
				                       <button type="button" class="AXButton" id="ax-grid-btn-del"><i class="axi axi-minus-circle"></i> 삭제</button>
				               		 </div>
				                	 <div class="ax-clear"></div>
				               	 </div>
                            <%-- %%%%%%%%%% 그리드 (업체정보) %%%%%%%%%% --%>
                            <div class="ax-grid" id="page-grid-box" style="min-height: 300px; max-height: 400px;"></div>
                        </ax:custom>
                    </ax:custom>
                    <ax:custom customid="tr">
                        <ax:custom customid="td">
                        	 <%-- %%%%%%%%%% 신규 버튼 (업체등록) %%%%%%%%%% --%>
                            <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-table"></i> 정보등록</h2>
                                </div>
                                <div class="right">
                                 <!--      <button type="button" class="AXButton" id="ax-form-btn-copy"><i class="axi axi-plus-circle"></i> 복제</button> -->
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                            <%-- %%%%%%%%%% 폼 (info) %%%%%%%%%% --%>
                            <ax:form id="form-info" name="form-info" method="post">
                                <ax:fields>
                                    <ax:field label="작업일자">
                                        <input type="text" id="info-workDate" name="workDate" title="작업일자" maxlength="8" class="AXInput W100 av-required" value=""/>
                                    </ax:field>
                                    <ax:field label="연번">
                                        <input type="text" id="info-seqNo" name="seqNo" title="연번" maxlength="8" class="AXInput W100" value=""  readonly="readonly" placeholder="시스템에서 자동생성"/>
                                    </ax:field>
                                    <ax:field label="적용일시">
                                        <input type="text" id="info-stDate" name="stDate" title="적용일시" maxlength="16" min="16" class="AXInput W150 av-required" placeholder="0000-00-00 00:00" />
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                 	<ax:field label="개장시간">
                                        <input type="text" id="info-openTime" name="openTime" title="개장시간" maxlength="5"  class="AXInput W150 av-required" value="" placeholder="00:00"/>
                                    </ax:field>
                                     <ax:field label="패장시간">
                                        <input type="text" id="info-closeTime" name="closeTime" title=패장시간 maxlength="5" class="AXInput W150 av-required" value="" placeholder="00:00"/>
                                    </ax:field>
                                     <ax:field label="제례시간">
                                        <input type="text" id="info-runingTime" name="runingTime" title="제례시간" maxlength="2" class="AXInput W150 av-required" value=""  placeholder="00"/>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields >
                                    <ax:field label="연장시간">
                                        <input type="text" id="info-extTime" name="extTime" title="연장시간" maxlength="2" class="AXInput W150 av-required" value="" placeholder="예) 00" />
                                    </ax:field>
                                    <ax:field label="연장횟수">
                                        <input type="text" id="info-extCnt" name="extCnt" title="연장횟수" maxlength="1" class="AXInput W100 av-required" value="" />
                                    </ax:field>
                                     <ax:field label="정리시간">
                                        <input type="text" id="info-cleanTime" name="cleanTime" title="정리시간" maxlength="2" class="AXInput W100 av-required" value=""  placeholder="00"/>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
			                        <ax:field label="TTS 사용여부" >
			                           <select class="AXSelect" id="info-ttsYn" name="ttsYn">
			                                <option value="Y" >Y</option>
			                                <option value="N" >N</option>
			                           </select>
			                        </ax:field>
			                        <ax:field label="MMS 사용여부" >
			                            <select class="AXSelect" id="info-mmsYn" name="mmsYn">
			                                <option value="Y" >Y</option>
			                                <option value="N" >N</option>
			                           </select>
			                        </ax:field>
			                    </ax:fields>                              
                                <ax:fields>
                                    <ax:field label="비고">
                                        <input type="text" id="info-remark" name="remark" title="비고" maxlength="200" class="AXInput W400"  value="" />
                                    </ax:field>
                                </ax:fields>
                            </ax:form>
                        </ax:custom>
                    </ax:custom>
                </ax:custom>

            </ax:col>
        </ax:row>
    </ax:div>
    <ax:div name="scripts">
        <script type="text/javascript">
            var resize_elements = [
                {id:"page-grid-box", adjust:-97}
            ];
            
            function strReplace(str){
              	 regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%\\\\(\'\"]/gi
                   //	return  _this.target.stdate.replace(regExp, "").replace(/\s/g,''); 
              	  var val = "";
              	  if(str != "" && str != null && typeof str !== "undefined" ){
              		
              	   val =str.replace(regExp, "").replace(/\s/gi,''); 
              	  }
               
                  return  val;
            }
            
            var fnObj = {
                CODES: {
                    "etc3": [
                        {CD:'1', NM:"코드"},
                        {CD:'2', NM:"CODE"},
                        {CD:'4', NM:"VA"}
                    ],
                    "_etc3": {"1":"코드", "2":"CODE", "4":"VA"}
                },
                pageStart: function(){
                    this.search.bind();
                    this.grid.bind();
                    this.form.bind();
                    this.bindEvent();
                    // 페이지 로딩 후 바로 검색 처리하기 (option)
                    this.search.submit();
                },
                bindEvent: function(){
                    var _this = this;
                    $("#ax-page-btn-search").bind("click", function(){
                        _this.search.submit();
                    });
                    $("#ax-page-btn-save").bind("click", function(){
                        setTimeout(function() {
                            _this.save();
                        }, 500);
                    });
                    $("#ax-grid-btn-del").bind("click", function(){
                        _this.grid.del();
                    });
                   
                    
                    $("#ax-page-btn-excel").bind("click", function(){
                        app.modal.excel({
                            pars:"target=${className}"
                        });
                    });
                    
                    $("#ax-form-btn-copy").bind("click", function(){
                    	 _this.grid.add();
                    });
                    
                             	
                   
                  
                    
                },
                save: function(){
                    var validateResult = fnObj.form.validate_target.validate();
                    if (!validateResult) {
                        var msg = fnObj.form.validate_target.getErrorMessage();
                        axf.alert(msg);
                        fnObj.form.validate_target.getErrorElement().focus();
                        return false;
                    }

                    var info = fnObj.form.getJSON();
                    info.workDate =  strReplace(info.workDate);
                    info.stDate =  strReplace(info.stDate);
                    info.openTime =  strReplace(info.openTime);
                    info.closeTime =  strReplace(info.closeTime);
                    
                    //info.ttsYn = $("input:checkbox[id='info-ttsYn']").is(":checked") == true ? "Y" :"N"; 
                    //info.mmsYn = $("input:checkbox[id='info-mmsYn']").is(":checked") == true ? "Y" :"N";                    
                   
               
                    app.ajax({
                        type: "PUT",
                        url: "/OPER1030/saveEnvset",
                        data: Object.toJSON([info])
                    },
                    function(res){
                        if(res.error){
                            console.log(res.error.message);
                            alert(res.error.message);
                        }
                        else
                        {
                            toast.push("저장되었습니다.");
                            fnObj.search.submit();
                            fnObj.form.clear();
                        }
                    });
                },
                search: {
                    target: new AXSearch(),
                    bind: function(){
                        var _this = this;
                        this.target.setConfig({
                            targetID:"page-search-box",
                            theme : "AXSearch",
                            /*
                             mediaQuery: {
                             mx:{min:"N", max:767}, dx:{min:767}
                             },
                             */
                            onsubmit: function(){
                                // 버튼이 선언되지 않았거나 submit 개체가 있는 경우 발동 합니다.
                                fnObj.search.submit();
                            },
                            rows:[
                                {display:true, addClass:"", style:"", list:[
                                    {label:"검색", labelWidth:"", type:"inputText", width:"100", key:"start", addClass:"", valueBoxStyle:"",  value : new Date().add(-1, 'y').print('yyyy-mm-dd'),
                                    	AXBind:{
            								type:"date", config:{
            									align:"right", valign:"top", defaultDate:(new Date()).print(), 
            									onChange:function(){
            										//toast.push(Object.toJSON(this));
            									}
            								}
            							},
                                    },
                                    {label:"~", labelWidth:"0", type:"inputText", width:"100", key:"end", addClass:"", valueBoxStyle:"", value:new Date().print(), 
                                    	AXBind:{
            								type:"date", config:{
            									align:"right", valign:"top", defaultDate:(new Date()).print(), 
            									onChange:function(){
            										//toast.push(Object.toJSON(this));
            									}
            								}
            							},
                                    }
                                ]}
                            ]
                        });
                    },
                    submit: function(){
                        var pars = this.target.getParam();
                        fnObj.grid.setPage(fnObj.grid.pageNo, pars);
                    }
                },

                grid: {
                    pageNo: 1,
                    target: new AXGrid(),
                    bind: function(){
                        var target = this.target, _this = this;
                        target.setConfig({
                            targetID : "page-grid-box",
                            theme : "AXGrid",
                            colHeadAlign:"center",
                            /*
                             mediaQuery: {
                             mx:{min:"N", max:767}, dx:{min:767}
                             },
                             */
                            colGroup : [
                                {key:"index", label:"선택", width:"35", align:"center", formatter:"checkbox"},
                                {key:"workDate", label:"작업일자", width:"100", align:"center"
                                 	,formatter : function (){                                	
                            		return this.item.workDate.date().print("yyyy-mm-dd");
                            		//return (val).print("yyyy-mm-dd hh:mi");
                            		} 
                           		},
                                {key:"seqNo", label:"연번", width:"50" , align:"center"},
                                {key:"stDate", label:"적용시작일", width:"150" , align:"center"
                                 	,formatter : function (){
                                	
                                		return this.item.stDate.date().print("yyyy-mm-dd hh:mi");
                                		//return (val).print("yyyy-mm-dd hh:mi");
                                	}   
                                },
                                {key:"openTime", label:"개방시간", width:"150" , align:"center"
                                	 ,formatter : function (){
                                		//return this.item.openTime.date().print("hh:mi");         
                                		return  this.item.openTime.substr(0,2)+":"+this.item.openTime.substr(2,4);
                                	}   
                                },
                                {key:"closeTime", label:"폐장시간", width:"100" , align:"center" 
                                	 ,formatter : function (){
                                		//return this.item.closeTime.date().print("hh:mi");
                                		return  this.item.closeTime.substr(0,2)+":"+this.item.closeTime.substr(2,4);
                                	}                             
                                },
                                {key:"runingTime", label:"제례시간(분)", width:"100", align:"center"},
                                {key:"extTime", label:"연장시간(분)", width:"100", align:"center"},
                                {key:"extCnt", label:"연장횟수", width:"100", align:"center"},
                                {key:"cleanTime", label:"정리시간(분)", width:"100", align:"center"},
                                {key:"ttsYn", label:"방송여부", width:"100", align:"center"},
                                {key:"mmsYn", label:"문자발송여부", width:"100", align:"center"},
                                {key:"remark", label:"비고", width:"250", align:"center"},
                              /*   {key:"etc3", label:"ㅇㅇ", width:"150", align:"center",
                                    formatter: function(val){
                                        return fnObj.CODES._etc3[ ( (Object.isObject(this.value)) ? this.value.NM : this.value ) ];
                                    }
                                } */
                            ],
                            body : {
                                onclick: function(){
                                    fnObj.form.setJSON(this.item);
                                    
                                 
                                }
                            },
                            page: {
                                display: true,
                                paging: true,
                                onchange: function(pageNo){
                                    _this.setPage(pageNo);
                                }
                            }
                        });
                    },
                    add:function(){
                    	 var _target = this.target;
                    	var items = _target.getSelectedItem(); 
                    	items = [].concat(items);
                        
                    	
                        this.target.appendList(items,0);
                        this.target.setFocus(this.target.list.length-1);
                    },
                    del:function(){
                        var _target = this.target,
                                nextFn = function() {
                                    _target.removeListIndex(checkedList);
                                    toast.push("삭제 되었습니다.");
                                };

                        var checkedList = _target.getCheckedListWithIndex(0);// colSeq
                        if(checkedList.length == 0){
                            alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                            return;
                        }
                        var dto_list = [];
                        $.each(checkedList, function(){
                            if(this.item._CUD != "C"){
                            
                                dto_list.push({"workDate":strReplace(this.item.workDate),"seqNo":this.item.seqNo}); // ajax delete 요청 목록 수집
                             
                            }
                        });

                        if(dto_list.length == 0) {
                            nextFn(); // 스크립트로 목록 제거
                        }
                        else {
                            app.ajax({
                                        type: "DELETE",
                                        url: "/OPER1030/deleteEnvList",
                                        data: Object.toJSON(dto_list)
                                    },
                                    function(res) {
                                        if (res.error) {
                                            alert(res.error.message);
                                        } else {
                                            nextFn(); // 스크립트로 목록 제거
                                        }
                                    });
                        }
                    },
                    setPage: function(pageNo, searchParams){
                        var _target = this.target;
                        this.pageNo = pageNo;

                        app.ajax({
                            type: "GET",
                            url: "/OPER1030/selectEnvList",
                            data: "dummy="+ axf.timekey() +"&pageNumber=" + (pageNo-1) + "&pageSize=50&" + (strReplace(searchParams)||strReplace(fnObj.search.target.getParam()))
                        }, function(res){
                            if(res.error){
                                alert(res.error.message);
                            }
                            else
                            {
                                var gridData = {
                                    list: res.list,
                                    page:{
                                        pageNo: res.page.currentPage.number()+1,
                                        pageSize: res.page.pageSize,
                                        pageCount: res.page.totalPages,
                                        listCount: res.page.totalElements
                                    }
                                };
                                _target.setData(gridData);
                            }
                        });
                    }
                },

                /*******************************************************
                 * 상세 폼
                 */
                form: {
                	
                    target: $('#form-info'),
                    validate_target: new AXValidator(),
                    bind: function() {
                        var _this = this;
                        var pDate = new Date();
                        this.validate_target.setConfig({
                            targetFormName : "form-info"
                        });
						
                        $('#info-workDate').bindDate().val( (new Date()).print() );
                        $('#info-seqNo').attr("readonly", "readonly");
                        /* $('#info-etc3').bindSelect({
                            reserveKeys: {
                                optionValue: "CD", optionText: "NM"
                            },
                            options: fnObj.CODES.etc3
                        }); */
                     // $("#info-stDate").val( (new Date()).print("yyyy-mm-dd hh:mi") );
                   
                        $("#info-stDate").bindPattern({pattern: "datetime"});
                        $("#info-openTime").bindPattern({pattern: "time"});
                        $("#info-closeTime").bindPattern({pattern: "time"});
                      
                        // form clear 처리 시
                        $('#ax-form-btn-new').click(function() {
                        	
                            fnObj.form.clear()
                        });
                    },
                    setJSON: function(item) {
                        var _this = this;

                        // 수정시 입력 방지 처리 필드 처리
                        $('#info-workDate').attr("readonly", "readonly");
						
                      
                       
                    
                      
                        var info = $.extend({}, item);
                        app.form.fillForm(_this.target, info, 'info-');
                      
                        // 추가적인 값 수정이 필요한 경우 처리
                        // $('#info-useYn').bindSelectSetValue( info.useYn );
                    },
                    getJSON: function() {
                        return app.form.serializeObjectWithIds(this.target, 'info-');
                    },
                    clear: function() {
                        app.form.clearForm(this.target);
                        $('#info-key').removeAttr("readonly");
                        $('#info-key').removeAttr("checked");
                        
                    }
                } // form
            };
        </script>
    </ax:div>
</ax:layout>