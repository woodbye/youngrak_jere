<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>

<ax:layout name="base.jsp">
    <ax:set name="title" value="${PAGE_NAME}" />
    <ax:set name="page_desc" value="${PAGE_REMARK}" />

    <ax:div name="contents">    	
        <ax:row>
            <ax:col size="12">
                <ax:custom customid="page-button" pageId="${PAGE_ID}" searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}" excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}" function2Auth="${FUNCTION_2_AUTH}" function3Auth="${FUNCTION_3_AUTH}" function4Auth="${FUNCTION_4_AUTH}" function5Auth="${FUNCTION_5_AUTH}"></ax:custom>
			      
				<div class="ax-search" id="page-search-box"></div>
				
				 <div class="ax-button-group">
                     <div class="left">
                         <h2><i class="axi axi-list-alt"></i> 점검 제례실 목록</h2>
                     </div>
                    <div class="right">
                        <button type="button" class="AXButton" id="ax-grid-btn-add"><i class="axi axi-plus-circle"></i> 추가</button>
                        <button type="button" class="AXButton" id="ax-grid-btn-del"><i class="axi axi-minus-circle"></i> 삭제</button>
                    </div>
                     <div class="ax-clear"></div>
                 </div>
                 
                 <div class="ax-grid" id="page-grid-box"  style="min-height: 300px; max-height: 300px;"></div>	
	         
                <ax:custom customid="table">
                    <ax:custom customid="tr">
                        <ax:custom customid="td">
                            <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-list-alt"></i> 제례실</h2>
                                </div>
                                <div class="right"></div>
                                <div class="ax-clear"></div>
                            </div>

                            <div class="ax-grid" id="page-grid2-box"  style="min-height: 300px; max-height: 300px;"></div>

                        </ax:custom>
                        <ax:custom customid="td" style="width:30px;text-align:center;vertical-align:middle;">
                            <div>
                                <div class="center"><button type="button" class="AXButton" id="move_room" ><i class="axi axi-chevron-left2"></i> </button></div>
                                <div class="H10"></div>
                                <div class="center"><button type="button" class="AXButton" id="move_inpectRoom" ><i class="axi axi-chevron-right2"></i> </button></div>
                            </div>
                        </ax:custom>
                        <ax:custom customid="td">
                           <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-list-alt"></i>점검 제례실</h2>
                                </div>
                                <div class="right"></div>
                                <div class="ax-clear"></div>
                            </div>
                            <div class="ax-grid" id="page-grid3-box"  style="min-height: 300px; max-height: 300px;"></div>
                            
                        </ax:custom>
                    </ax:custom>
                </ax:custom>

            </ax:col>
        </ax:row>
    </ax:div>
    <ax:div name="scripts">
        <script type="text/javascript">
            var resize_elements = [
				{id:"page-grid-box", adjust:-97},
				{id:"page-grid2-box", adjust:-97},
				{id:"page-grid3-box", adjust:-97}
				
              
            ];
            
            function strReplace(str){
           	 regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi
                //	return  _this.target.stdate.replace(regExp, "").replace(/\s/g,''); 
           	  var val = "";
           	  if(str != "" && str != null && typeof str !== "undefined" ){
           		
           	   val =str.replace(regExp, "").replace(/\s/gi,''); 
           	  }
            
               return  val;
            }
            
        	
            var fnObj = {
          		CODES: {
                      "useYn": [
                          {CD:'Y', NM:"Y"},
                          {CD:'N', NM:"N"}                        
                      ],
                      "_useYn": {"Y":"Y", "N":"N"}
                  },
                pageStart: function(){
                	this.search.bind();
                	this.grid.bind();
                    this.grid2.bind();
                    this.grid3.bind();                    
                    this.bindEvent();
                    this.search.submit();
                  
                },
                bindEvent: function(){
                    var _this = this;  
                    $("#ax-page-btn-search").bind("click", function(){                        
                        _this.search.submit();                      
                    });
                    $("#ax-page-btn-save").bind("click", function(){
                        setTimeout(function() {
                            _this.grid.save();
                        }, 500);
                    });                    

                    $("#ax-grid-btn-del").bind("click", function(){
                        _this.grid.del();
                    });

                    $("#ax-grid-btn-add").bind("click", function(){
                        _this.grid.add();
                    });
                    $("#move_inpectRoom").bind("click", function(){                    	
                        _this.grid2.move_inspectRoom();
                    });
                    
                    $("#move_room").bind("click", function(){
                        _this.grid3.move_room();
                    });
                 
                
                    
                },
                
                search: {
                    target: new AXSearch(),
                    get: function(){ return this.target },
                    bind: function(){
                    	var _this = this;
                    	
                        this.target.setConfig({
                            targetID:"page-search-box",
                            theme : "AXSearch",
                           
                            onsubmit: function(){
                                // 버튼이 선언되지 않았거나 submit 개체가 있는 경우 발동 합니다.
                                fnObj.search.submit();
                            },
                            rows:[
                                {display:true, addClass:"", style:"", list:[
                                    {label:"검색", labelWidth:"", type:"inputText", width:"100", key:"strDate", addClass:"", valueBoxStyle:"",  value : new Date().add(-1, 'y').print('yyyy-mm-dd'),
                                    	AXBind:{
            								type:"date", config:{
            									align:"right", valign:"top", defaultDate:(new Date()).print(), 
            									onChange:function(){
            										//toast.push(Object.toJSON(this));
            									}
            								}
            							},
                                    },
                                    {label:"~", labelWidth:"0", type:"inputText", width:"100", key:"endDate", addClass:"", valueBoxStyle:"", value:new Date().print(), 
                                    	AXBind:{
            								type:"date", config:{
            									align:"right", valign:"top", defaultDate:(new Date()).print(), 
            									onChange:function(){
            										//toast.push(Object.toJSON(this));
            									}
            								}
            							},
                                    }
                                ]}
                            ]
                        });
                      
                    },
               
                    /* submit: function(){
                        var pars = this.target.getParam();
                        //toast.push("콘솔창에 파라미터 정보를 출력하였습니다.");
                        //trace(pars);

                        fnObj.grid2.target.setList({
                            ajaxUrl:"<c:url value='/list.json' />", ajaxPars:pars, onLoad:function(){
                                //trace(this);
                            }
                        });
                    } */
                     submit: function(){
                    	
                        var pars = this.target.getParam();                        
                        fnObj.grid.setPage(fnObj.grid.pageNo, pars);                       
                     	//trace(pars);
                    } 
                },
                grid: {
                    pageNo: 0,
                    target: new AXGrid(),
                    get: function(){ return this.target },
                    bind: function(){
                        var _this = this;
                        _this.target.setConfig({
                            targetID : "page-grid-box",
                            theme : "AXGrid",
                            colHeadAlign:"center",
                            height:300,
                          	
                            /*
                            mediaQuery: {
                                mx:{min:0, max:767}, dx:{min:767}
                            },
                            */
                            colGroup : [
                                {key:"index", label:"선택", width:"35", align:"center", formatter:"checkbox"},
                                {key:"workDate", label:"작업일자", width:"150", align:"center",pattern: "date(-)", 
                                	editor:{
                                        type: "calendar",
                                        maxLength: 8,
                                        disabled: function(){
                                            return this.item._CUD != "C";
                                        }
                                		,formatter : function (){
                                			//return new Date().add(-1, 'y').print('yyyy-mm-dd');
                                		}
                                    }
                                	,config: {
                                       
                                        //pattern: "date(-)"
                                    }
                                	/* , formatter : function(){
                                    	//alert(this.item.workDate);
                                    	//return this.item.workDate.substr(0,4)+"-"+this.item.workDate.substr(4,2)+"-"+this.item.workDate.substr(6,2)
                                    } */
                                },
                                {key:"seqNo", label:"연번", width:"140"},
                                {key:"stdate", label:"적용시작일", width:"150", 
                                	editor:{
                                        type: "text",
                                        maxLength: 16
                                        /* formatter : function (){
                                        	return strReplace(this.item.stdate);
                                        }, */
                                       
                                        
                                    }
                                	
                                },
                              /*   {key:"eddate", label:"작성일", width:"130", align:"center", AXBind :"", formatter:function(){
                                    return '<input type="text" name="" id="AXdate_'+this.index+'" class="AXInput W100 AXdate" value="'+ this.value +'" />';
                                }}, */
                               {key:"eddate", label:"적용종료일", width:"150", 
                                	editor:{
                                        type: "text",
                                        maxLength: 16,
                                        minLength: 12
                                    }
                                	                         		
                            	}, 
                                {key:"remark", label:"점검사유", width:"400" ,editor:{type:"text"}}
                            ],
                            body: {
                            	onclick: function(){   
                            		
                                    fnObj.grid2.setParent(this.item);                                    
                                    fnObj.grid3.setParent(this.item);                                    
                                }
                            },
                            page: {
                                display: true,
                                paging: true,
                                onchange: function(pageNo){
                                    _this.setPage(pageNo);
                                }
                            },                           
                            onsubmit: function(){
                                //점검대상 리스트 조회 후 실 
                                fnObj.search.submit();
                            }
                        });
                      
                    },
                    add:function(){
                        this.target.pushList({});
                        this.target.setFocus(this.target.list.length-1);
                    },
                    setPage: function(pageNo, searchParams){                    	
                        var _target = this.target;
                        this.pageNo = pageNo;		
                        
                        
                         app.ajax({
                             type: "GET",
                             url: "/OPER1040/selectInspectList", 
                             data : searchParams.replace(/-/gi, "")
                         }, function(res){
                             if(res.error){
                                alert(res.error.message);
                             }
                             else
                             {
                                 var gridData = {
                                     list: res.list,
                                     page:{
                                         pageNo: res.page.currentPage.number()+1,
                                         pageSize: res.page.pageSize,
                                         pageCount: res.page.totalPages,
                                         listCount: res.page.totalElements
                                     }
                                 };
                                
                                 var list = gridData.list;
                                 $.each(list, function(){
                                	 //alert(this.workDate +"/"+ this.workDate.substr(4,2));
                                	 //this.workDate = new Date(this.workDate.substr(0,4),this.workDate.substr(4,2)-1, this.workDate.substr(6,2)).print("yyyy-mm-dd");
                                	 //this.eddate = new Date(this.eddate.substr(0,4),this.eddate.substr(4,2)-1, this.eddate.substr(6,2), this.eddate.substr(8,2), this.eddate.substr(10,2)).print("yyyymmddhhmi");
                                	 //this.stdate = new Date(this.stdate.substr(0,4),this.stdate.substr(4,2)-1, this.stdate.substr(6,2), this.stdate.substr(8,2), this.stdate.substr(10,2)).print("yyyymmddhhmi");
                                	 
                                	  //this.workDate = new Date(this.workDate.substr(0,4),this.workDate.substr(4,2), this.workDate.substr(6,2)).print("yyyy-mm-dd");
                                	 //this.eddate = new Date(this.eddate.substr(0,4),this.eddate.substr(4,2), this.eddate.substr(6,2), this.eddate.substr(8,2), this.eddate.substr(10,2)).date().print("yyyy-mm-dd hh:mi");
                                	 //this.stdate = new Date(this.stdate.substr(0,4),this.stdate.substr(4,2), this.stdate.substr(6,2), this.stdate.substr(8,2), this.stdate.substr(10,2)).date().print("yyyy-mm-dd hh:mi");                               	 
                                 });
                                 _target.setData(gridData);
                                
                             }
                         });
                    },
                    save: function(){
                        var items = fnObj.grid.target.list;
                        
                        if (items.length == 0) {
                            alert("저장할 내용이 없습니다.");
                            return;
                        }
                        
                        
                        
                        var dto_list = [];
                        $.each(items, function(){
                            if(this._CUD){
                            	
                            	var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi
                                	if(typeof this.workDate !== "undefined" && this.workDate != ""  ) {                            		
                                		
                                		this.workDate = strReplace(this.workDate);
                                		this.stdate = strReplace(this.stdate);
                                		this.eddate = strReplace(this.eddate);
                                		//this.workDate = this.workDate.replace(regExp, "").replace(/\s/g,''); 
                                    	//this.stdate = this.stdate.replace(regExp, "").replace(/\s/g,''); 
                                    	//this.eddate = this.eddate.replace(regExp, "").replace(/\s/g,''); 
                                    	 dto_list.push(this); // ajax put 요청 목록 수집
                                     
                                   }
                            	  
                            }                           
                        });

                         app.ajax({
                             type: "PUT",
                             url: "/OPER1040/saveInspectList",
                             data: Object.toJSON(dto_list)
                         },
                         function(res){
                             if(res.error){
                                 console.log(res.error.message);
                                 alert(res.error.message);
                             }
                             else
                             {                               
                                 fnObj.search.submit();                                 
                                 fnObj.grid3.save_inspectRoom();                                 
                             }
                         });
                         
                         
                    },
                    del:function(){
                        var _target = this.target,
                            nextFn = function() {
                                _target.removeListIndex(checkedList);
                                toast.push("삭제 되었습니다.");
                            };

                        var checkedList = _target.getCheckedListWithIndex(0);// colSeq
                        if(checkedList.length == 0){
                            alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                            return;
                        }
                        var dto_list = [];
                        $.each(checkedList, function(){
                            if(this.item._CUD != "C"){
	                                dto_list.push(
	                                			{"workDate":this.item.workDate.replace(/-/gi, "")
	                                				,"seqNo":this.item.seqNo  }
	                                ); // ajax delete 요청 목록 수집
                                }
                              
                        });

                        if(dto_list.length == 0) {
                            nextFn(); // 스크립트로 목록 제거
                        }
                        else {
                            app.ajax({
                                type: "DELETE",
                                url: "/OPER1040/saveInspectList",
                                data: Object.toJSON(dto_list)
                            },
                            function(res) {
                                if (res.error) {
                                    alert(res.error.message);
                                } else {
                                    nextFn(); // 스크립트로 목록 제거
                                }
                            });
                        }
                    },                 
                },
                grid2: {
                	parent: {},
                    pageNo: 0,
                    target: new AXGrid(),                    
                    bind: function(){
                    	var target = this.target, _this = this;
                        _this.target.setConfig({
                            targetID : "page-grid2-box",
                            theme : "AXGrid",
                            colHeadAlign:"center",
                            /*
                            mediaQuery: {
                                mx:{min:0, max:767}, dx:{min:767}
                            },
                            
                            */
                            colGroup : [
                                {key:"roomCd", label:"코드", width:"*", align:"center"},
                                {key:"roomNm", label:"제례관", width:"*"},
                                {key:"areaCd", label:"제례실명", width:"*"}
                            ],
                            body : {
                                onclick: function(){
                                    //toast.push(Object.toJSON({index:this.index, item:this.item}));
                                    //fnObj.form.edit(this.item);
                                }
                            },
                            page:{
                                display:true,
                                paging:false,
                                onchange: function(pageNo){
                                    _this.setPage(pageNo-1);
                                }
                            }
                            
                        });
                       
                    },
                    setParent: function(item){
                    	
                        if(typeof item.workDate === "undefined" || item.workDate == "" || typeof item.seqNo === "undefined" || item.seqNo == "") {
                            this.clear();
                        }else{
                            this.parent = item;
                            this.setPage( 1, item );
                        } 
                    },
                    clear: function(){
                        this.parent = {};
                        this.target.setList([]);
                    },                    
                    setPage: function(pageNo, searchParams) {                    	
                        var _this = this;
                        _this.pageNo = pageNo;
                        var Params = "workDate="+searchParams.workDate.replace(/-/gi, "")+"&seqNo="+searchParams.seqNo;
                        app.ajax({
                            type: "GET",
                            url: "/OPER1040/selectInspectNotRoomList",
                            data : Params
                            //data: searchParams
                        }, function(res){
                        	                        	
                            if(res.error){
                                alert(res.error.message);
                            }
                            else
                            {	
                            	var gridData = {
                                        list: res.list,
                                        page:{
                                            pageNo: res.page.currentPage.number()+1,
                                            pageSize: res.page.pageSize,
                                            pageCount: res.page.totalPages,
                                            listCount: res.page.totalElements
                                   }
                                };   
                            	
                                _this.target.setData(gridData); 
                            }
                        });
                    },
                    move_inspectRoom: function(){   
                    	
                        var items = this.target.getSelectedItem(); 
                        var target = fnObj.grid3.target;
                        var list = [].concat(items);
                        var collect = [];
                        var hasItem = false;        
                        
                        if(items.error) return false;
                        
                        for(var i= 0;i<list.length;i++){
                        	
                       	 hasItem = false;
                       	 for(var j= 0;j<target.list.length;j++) {
                                if(target.list[j].roomCd == list[i].item.roomCd){
                                    hasItem = true;
                                    break;
                                }
                            }
                       	 if(!hasItem){
                       		list[i].item.useYn ="Y";
                                collect.push(list[i].item);
                              
                            }
                        }
                        if(collect.length > 0) target.pushList(collect,0);
                      
                        var removeList = [];   
                        $.each(collect, function(){                        	
                           removeList.push({roomCd:this.roomCd});
                        });
                        target.setFocus(0);
                        this.del(removeList);                        
                        
                    },
                    del: function(removeList){     
                    	var items = this.target.getSelectedItem();
                    	this.target.removeList(removeList);
                        this.target.setFocus(items.index-1);
                    },                    
                },
                grid3: {
                	parent: {},
                    pageNo: 0,
                    target: new AXGrid(),                    
                    bind: function(){
                    	var target = this.target, _this = this;
                        _this.target.setConfig({
                            targetID : "page-grid3-box",
                            theme : "AXGrid",
                            colHeadAlign:"center",
                            /*
                            mediaQuery: {
                                mx:{min:0, max:767}, dx:{min:767}
                            },
                            
                            */
                            colGroup : [
                                {key:"roomCd", label:"코드", width:"*", align:"center"},
                                {key:"roomNm", label:"제례관", width:"*", align:"center" },
                                {key:"areaCd", label:"제례실명", width:"*", align:"center"},                              
                                {key:"useYn", label:"사용여부", width:"*", align:"center",
                                    formatter: function(val){
                                        return fnObj.CODES._useYn[ ( (Object.isObject(this.value)) ? this.value.NM : this.value ) ];
                                        // return (Object.isObject(this.value)) ? this.value.NM : this.value;
                                        // ajax 로 값을 받으면 문자열, inline-editor에서 값을 받으면 {CD:'', NM:''} 이 되므로 예외 처리 되게 합니다.
                                        // ajax 에서도 Object로 값을 전달 해도 좋습니다.
                                    },
                                    editor: {
                                        type: "select",
                                        optionValue: "CD",
                                        optionText: "NM",
                                        options: fnObj.CODES.useYn,
                                        beforeUpdate: function(val){ // 수정이 되기전 value를 처리 할 수 있음.
                                            // console.log(val);
                                            return val.CD; // return 이 반드시 있어야 함.
                                        },
                                        afterUpdate: function(val){ // 수정이 처리된 후
                                            // 수정이 된 후 액션.
                                            // console.log(this);
                                        },
                                        updateWith: ["_CUD"]
                                    }
                                }
                            ],
                            body : {
                                onclick: function(){
                                    //toast.push(Object.toJSON({index:this.index, item:this.item}));
                                    //fnObj.form.edit(this.item);
                                }
                            },
                            page:{
                                display:true,
                                paging:false,
                                onchange: function(pageNo){
                                    _this.setPage(pageNo-1);
                                }
                            }
                            
                        });
                       
                    },
                    setParent: function(item){
                    	
                        if(typeof item.workDate === "undefined" || item.workDate == "" || typeof item.seqNo === "undefined" || item.seqNo == "") {
                            this.clear();
                        }else{
                            this.parent = item;
                            this.setPage( 1, item );
                        } 
                    },
                    clear: function(){
                        this.parent = {};
                        this.target.setList([]);
                    },                    
                    setPage: function(pageNo, searchParams) {                    	
                        var _this = this;
                        _this.pageNo = pageNo;
                        var Params = "workDate="+searchParams.workDate.replace(/-/gi, "")+"&seqNo="+searchParams.seqNo;
                        app.ajax({
                            type: "GET",
                            url: "/OPER1040/selectInspectRoomList",
                            data : Params
                            //data: searchParams
                        }, function(res){
                        	                        	
                            if(res.error){
                                alert(res.error.message);
                            }
                            else
                            {	
                            	var gridData = {
                                        list: res.list,
                                        page:{
                                            pageNo: res.page.currentPage.number()+1,
                                            pageSize: res.page.pageSize,
                                            pageCount: res.page.totalPages,
                                            listCount: res.page.totalElements
                                   }
                                };   
                            	
                                _this.target.setData(gridData); 
                            }
                        });
                    },
 					move_room: function(){   
                    	
                        var items = this.target.getSelectedItem();   
                        if(typeof items.item == 'undefined'){
                        	return;
                        }
                        var target = fnObj.grid2.target;
                        var list = [].concat(items);
                        var collect = [];
                        var hasItem = false;        
                        
                        if(items.error) return false;
                        var dd = list.length;
                        
                        for(var i= 0;i<list.length;i++){
                                                	
                       	 hasItem = false;
                       	 for(var j= 0;j<target.list.length;j++) {
                                if(target.list[j].roomCd == list[i].item.roomCd){
                                	
                                    hasItem = true;
                                    break;
                                }
                            }
                       	 if(!hasItem){
                            
                                collect.push(list[i].item);
                            }
                        }
                        if(collect.length > 0) target.pushList(collect,0);
                      
                        var removeList = [];   
                        $.each(collect, function(){                        	
                           removeList.push({roomCd:this.roomCd});
                        });
                        target.setFocus(0);                       
                        this.del(removeList);     
                       
                    },
                    del: function(removeList){                    	
                    	 var items = this.target.getSelectedItem();                    	 
                          fnObj.grid3.target.removeList(removeList);
                          this.target.setFocus(items.index-1);
                    },
                    save_inspectRoom : function(){
                        var inspect = (fnObj.grid.target.getSelectedItem());    
                        var inspectRoom = fnObj.grid3.target.list;  
                        var dto_list = [];
                        var dto_sub_list = [];
                    
                       
                        if(typeof inspect == null || typeof inspect == "undefined"){
                        	return;                        
                        }
                       var cc = inspectRoom.length; 	
                        if(inspectRoom.length == 0){
                        	var item = {
                    				"workDate": inspect.item.workDate.replace(/-/gi, "")
                    				,"seqNo":inspect.item.seqNo
                    				,room: {"roomCd":null}
                    				,"roomCd":null
                    				                       				
                    				
                    		}
                    		dto_list.push(item);
                        }else{
                            $.each(inspectRoom, function (i, d){
                        		var item = {
                        				"workDate": inspect.item.workDate.replace(/-/gi, "")
                        				,"seqNo":inspect.item.seqNo
                        				,room: {"roomCd":d.roomCd}
                        				,"roomCd":d.roomCd
                        				,"useYn":d.useYN||d.useYn                        				
                        				
                        		}
                        		dto_list.push(item);
                        	});
                        }
                    
                      
                        
                         app.ajax({
                             type: "PUT",
                             url: "/OPER1040/saveInspectRoomList",
                             data: Object.toJSON(dto_list)
                         },
                         function(res){
                             if(res.error){
                                 console.log(res.error.message);
                                 alert(res.error.message);
                             }
                             else
                             {
                                toast.push("저장되었습니다.");
                                 fnObj.search.submit();
                              
                             }
                         });
                    }         
                }
            };
        </script>
    </ax:div>
</ax:layout>