<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>

<ax:layout name="base.jsp">
    <ax:set name="title" value="${PAGE_NAME}" />
    <ax:set name="page_desc" value="${PAGE_REMARK}" />
    <ax:div name="contents">
        <ax:row>
            <ax:col size="12">
                <ax:custom customid="page-button" pageId="${PAGE_ID}" searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}" excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}" function2Auth="${FUNCTION_2_AUTH}" function3Auth="${FUNCTION_3_AUTH}" function4Auth="${FUNCTION_4_AUTH}" function5Auth="${FUNCTION_5_AUTH}"></ax:custom>
               
                <ax:custom customid="table">
                    <ax:custom customid="tr">
                        <ax:custom customid="td">                         
                             <div class="ax-search" id="page-search-box"></div>
								<div class="ax-button-group">
				                   	 <div class="left">
				                		     	<h2><i class="axi axi-list-alt"></i> 고인 목록</h2>
				              		 </div>
				              		 <div class="right">				                      
				                 	   <button type="button" class="AXButton" id="ax-form-btn-new"><i class="axi axi-plus-circle"></i> 신규</button>
				                       <button type="button" class="AXButton" id="ax-grid-btn-del"><i class="axi axi-minus-circle"></i> 삭제</button>
				               		 </div>
				                	 <div class="ax-clear"></div>
				               	 </div>
                            <%-- %%%%%%%%%% 그리드 (업체정보) %%%%%%%%%% --%>
                            <div class="ax-grid" id="page-grid-box" style="min-height: 300px; max-height: 350px;" ></div>                            
                            
                        </ax:custom>                        
                    </ax:custom>                         
                </ax:custom>
            </ax:col>
        </ax:row>
        <ax:row>
            <ax:col size="12" wrap="true">
         		<ax:form id="form-info" name="form-info" method="post" enctype="multipart/form-data" >
					<table class="AXFormTable">
						<colgroup>
                                  <col width="100" />
                                  <col width="300" />     
                                  <col width="450"/>
                          	   </colgroup>
                          	   <tbody>
							<tr>
								<th>고인명</th>
								<td> 
									<input type="text" id="info-contTitle" name="contTitle" title="고인명" maxlength="128" class="AXInput W150 av-required" value=""/>
								</td>
								
								<td rowspan="4">
									<img id="img-preview" src="" width="400" height="250"/>
								</td>										
								<!-- <td rowspan="4"><div id="uploadQueueBox"  class="AXUpload5QueueBox"></div></td>		 -->								
							</tr>	
							<tr>
								<th>안치번호</th>
								<td>
									 <input type="text" id="info-contId" name="contId" title="등록번호" maxlength="20" class="AXInput W150 av-required" value=""/>
								</td>
							</tr>						
							<tr>
								<th >신청자</th>										
								<td >
									 <input type="text" id="info-contRegUser" name="contRegUser" title="신청자" maxlength="64" class="AXInput W150 av-required" value=""/>
								</td>																	
							</tr>							
							<tr>
								<th >파일명</th>										
								<td>
									 <input type="text" id="info-contFile" name="contFile" title="파일명" maxlength="255" class="AXInput W300 av-required" value=""/>
								</td>																	
							</tr>
							<tr>								
								<th>등록일자</th>
								<td >
									 <input type="text" id="info-contDate" name="contDate" title="등록일자" maxlength="10" class="AXInput W150 av-required" value=""/>
									 <input type="hidden" id="info-contType" name="contType"  maxlength="20" class="AXInput W150" value="영정사진"  />
									 <input type="hidden" id="info-contRealFileName" name="contRealFileName"  maxlength="255" class="AXInput W150"  />
									 <input type="hidden" id="info-contURL" name="contURL"  maxlength="255" class="AXInput W150"  />
								</td>
								<td align="center" colspan="1">
										
											<input id="info-file" type="file" name="contMultipartFile" class="AXButton Blue">
																				
								</td>								
							</tr>
						
						</tbody>
					</table>
				</ax:form>
            </ax:col>
        </ax:row>
    </ax:div>
    <ax:div name="scripts">
        <script type="text/javascript">
            var resize_elements = [
                {id:"page-grid-box", adjust:-97}
            ];
            
            function strReplace(str){
              	 regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%\\\\(\'\"]/gi
                   //	return  _this.target.stdate.replace(regExp, "").replace(/\s/g,''); 
              	  var val = "";
              	  if(str != "" && str != null && typeof str !== "undefined" ){
              		
              	   val =str.replace(regExp, "").replace(/\s/gi,''); 
              	  }
               
                  return  val;
            }
           
           
		
            
            
            var fnObj = {
                
                pageStart: function(){
                    this.search.bind();
                    this.grid.bind();                  
                    this.bindEvent();
                    this.form.bind();
                    // 페이지 로딩 후 바로 검색 처리하기 (option)
                    //this.search.submit();
                  
                    
                    $("input[name='keyword']").keydown(function (key) {
                        if (key.keyCode == 13) {                         
                            fnObj.search.submit();
                            }
                    });
                },
                bindEvent: function(){
                    var _this = this;
         		   
                    $("#ax-page-btn-search").bind("click", function(){
                       _this.search.submit();
                    });   
                    $("#ax-page-btn-save").bind("click", function(){
                        setTimeout(function() {
                            _this.save();
                        }, 500);
                    });
                	 // form clear 처리 시
                    $('#ax-form-btn-new').click(function() {
                    	
                        fnObj.form.clear();
                    });
                    $("#ax-grid-btn-del").bind("click", function(){
                        _this.grid.del();
                    });
                    
                },
                save: function(){
                    var validateResult = fnObj.form.validate_target.validate();
                    if (!validateResult) {
                        var msg = fnObj.form.validate_target.getErrorMessage();
                        axf.alert(msg);
                        fnObj.form.validate_target.getErrorElement().focus();
                        return false;
                    }
					var contURL = $("#info-contURL").val();
					
                    
                    if(contURL == "" && typeof $("#info-file")[0].files[0] == "undefined"){
						alert("선택된 파일이 없습니다.");
						return false;
					}
                    
                    var info = fnObj.form.getJSON();
                    
                   // info.workDate =  strReplace(info.workDate);
                    //info.stDate =  strReplace(info.stDate);
                    //info.openTime =  strReplace(info.openTime);
                    //info.closeTime =  strReplace(info.closeTime);
                    
                    //info.ttsYn = $("input:checkbox[id='info-ttsYn']").is(":checked") == true ? "Y" :"N"; 
                    //info.mmsYn = $("input:checkbox[id='info-mmsYn']").is(":checked") == true ? "Y" :"N";                    
                   
                    var formData = new FormData();
                    for(var key in info){
                    	formData.append(key, info[key]);
                    }
                    
                    
                    
                    formData.append("file", $("#info-file")[0].files[0]);
               
                    $.ajax({
                        url: '/OPER1020/saveMdcontent',
                        method:"POST",
                        data: formData,
                        // THIS MUST BE DONE FOR FILE UPLOADING
                        contentType: false,
                        processData: false,
                        // ... Other options like success and etc
                        success : function(){
                        	toast.push("저장되었습니다.");
                        	var parms = "key=ContTitle&keyword="+info.contTitle;
                        	fnObj.grid.setPage(fnObj.grid.pageNo, parms);
                            fnObj.form.clear();
                        }
                    });
                    
                    /* app.ajax({
                        type: "PUT",
                        url: "/OPER1020/saveMdcontent",
                        data: Object.toJSON([info])
                    },
                    function(res){
                        if(res.error){
                            console.log(res.error.message);
                            alert(res.error.message);
                        }
                        else
                        {
                        	//var _info = info;
                        	var parms = "key=ContTitle&keyword="+info.contTitle;
                            toast.push("저장되었습니다.");
                            fnObj.grid.setPage(fnObj.grid.pageNo, parms);
                            fnObj.form.clear();
                        }
                    }); */
                },
                search: {
    	            target: new AXSearch(),    	            
                    bind: function(){
                        var _this = this;
                        this.target.setConfig({
                            targetID:"page-search-box",
                            theme : "AXSearch",
                            /*
                            mediaQuery: {
                                mx:{min:"N", max:767}, dx:{min:767}
                            },
                            */
                            onsubmit: function(){
    	                        // 버튼이 선언되지 않았거나 submit 개체가 있는 경우 발동 합니다.
    	                    },
    	                    rows:[
                                {display:true, addClass:"", style:"", list:[
                                    {label:"검색조건", labelWidth:"100", type:"selectBox", width:"", key:"key", addClass:"key", valueBoxStyle:"", value:"",
                                        options:[{optionValue:"ContTitle", optionText:"고인명"}
                                            , {optionValue:"ContRegUser", optionText:"신청자"}
                                            ],
                                            
                                        AXBind:{
                                            type:"select",                                            
                                            setValue:"ContTitle",
                                            config:{
                                                onChange:function(){
                                                    //toast.push(Object.toJSON(this));

                                                }                                    			
                                            }
                                        }
                                    },
                                    
                                    {label:"", labelWidth:"", type:"inputText", width:"150", key:"keyword",  addClass:"keyword", valueBoxStyle:"", value:"" ,                                  	  
                                        onChange: function(changedValue){
                                      	  
                                            //아래 2개의 값을 사용 하실 수 있습니다.
                                            //toast.push(Object.toJSON(this));
                                            //dialog.push(changedValue);
                                        }                                        
                                    }                                
                                ]}
    	                    ]
    	                });
                       
                    },
    	            submit: function(){    	            	
    	            	
    	                var pars = this.target.getParam();   
    	                //toast.push("콘솔창에 파라미터 정보를 출력하였습니다.");
                        fnObj.grid.setPage(fnObj.grid.pageNo, pars);
                      
    	            }
    	        },

                grid: {
                    pageNo: 1,
                    target: new AXGrid(),
                    bind: function(){
                        var target = this.target, _this = this;
                        target.setConfig({
                            targetID : "page-grid-box",
                            theme : "AXGrid",
                            colHeadAlign:"center",
                            /*
                             mediaQuery: {
                             mx:{min:"N", max:767}, dx:{min:767}
                             },
                             */
                            colGroup : [
                                {key:"index", label:"선택", width:"35", align:"center", formatter:"checkbox"},
                                {key:"contTitle", label:"고인명", width:"100", align:"center"},
                                {key:"contId", label:"안치번호", width:"100", align:"center"},                              
                                {key:"contRegUser", label:"신청자", width:"100", align:"center"},
                                {key:"contURL", label:"파일위치", width:"300", align:"center"},
                                {key:"contFile", label:"파일명", width:"250", align:"center"},                             
                                {key:"contDate", label:"등록일자", width:"100", align:"center"}
                              /*   {key:"etc3", label:"ㅇㅇ", width:"150", align:"center",
                                    formatter: function(val){
                                        return fnObj.CODES._etc3[ ( (Object.isObject(this.value)) ? this.value.NM : this.value ) ];
                                    }
                                } */
                            ],
                            body : {
                                onclick: function(){
                                    fnObj.form.setJSON(this.item);
                                }
                            },
                            page: {
                                display: true,
                                paging: true,
                                onchange: function(pageNo){
                                    _this.setPage(pageNo);
                                }
                            }
                        });
                    },                    
                    setPage: function(pageNo, searchParams){
                        var _target = this.target;
                        this.pageNo = pageNo;

                        app.ajax({
                            type: "GET",
                            url: "/OPER1020/selectMdcontentsList",
                          //  data: "dummy="+ axf.timekey() +"&pageNumber=" + (pageNo-1) + "&pageSize=50&" + (searchParams||"")
                        	data: "pageNumber=" + (pageNo-1) + "&pageSize=50&" + (searchParams||fnObj.search.target.getParam())
                            
                        }, function(res){
                            if(res.error){
                                alert(res.error.message);
                            }
                            else
                            {
                                var gridData = {
                                    list: res.list,
                                    page:{
                                        pageNo: res.page.currentPage.number()+1,
                                        pageSize: res.page.pageSize,
                                        pageCount: res.page.totalPages,
                                        listCount: res.page.totalElements
                                    }
                                };
                                _target.setData(gridData);
                            }
                        });
                    },
                    del:function(){
                    	
                        var _target = this.target,
                                nextFn = function() {
                                    _target.removeListIndex(checkedList);
                                    toast.push("삭제 되었습니다.");
                                };

                        var checkedList = _target.getCheckedListWithIndex(0);// colSeq
                        if(checkedList.length == 0){
                            alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                            return;
                        }
                        var dto_list = [];
                        $.each(checkedList, function(){
                            if(this.item._CUD != "C"){
                            
                                dto_list.push("contId="+ this.item.contId); // ajax delete 요청 목록 수집
                            }
                        });

                        if(dto_list.length == 0) {
                            nextFn(); // 스크립트로 목록 제거
                        }
                        else {
                            app.ajax({
                                        type: "DELETE",
                                        url: "/OPER1020/deleteMdcontentList?"+dto_list.join("&"),
                                       // data: Object.toJSON(dto_list)
                                    },
                                    function(res) {
                                        if (res.error) {
                                            alert(res.error.message);
                                        } else {
                                            nextFn(); // 스크립트로 목록 제거
                                            fnObj.form.clear();
//                                         	fnObj.upload.target.deleteSelect("all");
                                            
                                        }
                                    });
                        }
                    }
                },
    	        /*******************************************************
                 * 상세 폼
                 */
                form: {
                	
                    target: $('#form-info'),
                    validate_target: new AXValidator(),
                    bind: function() {
                        var _this = this;
                        var pDate = new Date();
                        this.validate_target.setConfig({
                            targetFormName : "form-info"
                        });
                        $('#info-contDate').bindDate().val( (new Date()).print() );
                        
                        //$("#info-contDate").bindPattern({pattern: "date"});
                        
                        $("#info-file").change(function(){
                        	fnObj.readImg('info-file', 'img-preview');
                        });
                      
                        // form clear 처리 시
                        $('#ax-form-btn-new').click(function() {
                        	
                            fnObj.form.clear()
                        });
                    },
                    setJSON: function(item) {
                        var _this = this;

                        // 수정시 입력 방지 처리 필드 처리
                       $('#info-contId').attr("readonly", "readonly");
                       $('#info-conFile').attr("readonly", "readonly");
                       
                       $('#img-preview').attr("src", item.contURL);
						
                      
                       /* var myFileList = [];
                       var fileItem = {
                    	   id		   : item.contId,
                           name        : item.contFile, //{string} - setConfig.fileKeys 에서 정의한 json key
                         
                           saveName    : "", //{string} - setConfig.fileKeys 에서 정의한 json key
                           fileSize    : 0, //{string} - setConfig.fileKeys 에서 정의한 json key
                           uploadedPath: item.contURL, //{string} - setConfig.fileKeys 에서 정의한 json key
                           thumbPath    : "" //{string} - setConfig.fileKeys 에서 정의한 json key
                       };
                       myFileList.push(fileItem); */
                      
                        var info = $.extend({}, item);
                        app.form.fillForm(_this.target, info, 'info-');
//                         fnObj.upload.target.setUploadedList(myFileList);
                        // $("#uploadQueueBox").append()
                    	//$(".AXUploadItem").css({"width":"250","height":"250"});
                    	//$(".AXUploadIcon").css({"background" :"url('"+item.contUrl+"')"});
   						
   					
                        // 추가적인 값 수정이 필요한 경우 처리
                        // $('#info-useYn').bindSelectSetValue( info.useYn );
                    },
                    getJSON: function() {
                        return app.form.serializeObjectWithIds(this.target, 'info-');
                    },
                    clear: function() {
                        app.form.clearForm(this.target);
                        $('#info-contId').removeAttr("readonly");
                        $('#info-conFile').removeAttr("readonly");
                        $("#info-contURL").val("");
   						$("#info-contRealFileName").val("");
   						$("#img-preview").attr("src", "");
   						$("#info-file").val("");
                    }
                } // form    

				, readImg : function(inputId, outputId) {

					var file = document
							.getElementById(inputId).files[0];

					var reader = new FileReader();

					reader.readAsDataURL(file);

					reader.onload = function() {
						var output = document
								.getElementById(outputId);
						output.src = reader.result;
					}

					reader.onerror = function(e) {
						alert("읽기 오류:"
								+ e.target.error.code);
						return;
					}
				}

			};
		</script>
    </ax:div>
</ax:layout>