<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>

<ax:layout name="base.jsp">
    <ax:set name="title" value="${PAGE_NAME}" />
    <ax:set name="page_desc" value="${PAGE_REMARK}" />
    <ax:div name="contents">
        <ax:row>
            <ax:col size="12">
                <ax:custom customid="page-button" pageId="${PAGE_ID}" searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}" excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}" function2Auth="${FUNCTION_2_AUTH}" function3Auth="${FUNCTION_3_AUTH}" function4Auth="${FUNCTION_4_AUTH}" function5Auth="${FUNCTION_5_AUTH}"></ax:custom>
                <div class="ax-search" id="page-search-box"></div>

                <ax:custom customid="table">
                    <ax:custom customid="tr">
                        <ax:custom customid="td">
                            <div class="ax-button-group">
				                   	 <div class="left">
				                		     	<h2><i class="axi axi-list-alt"></i> 알림문구 목록</h2>
				              			 </div>
				                 	 <div class="right">				                      
				                 	   <button type="button" class="AXButton" id="ax-form-btn-new"><i class="axi axi-plus-circle"></i> 신규</button>
				                       <button type="button" class="AXButton" id="ax-grid-btn-del"><i class="axi axi-minus-circle"></i> 삭제</button>
				               		 </div>
				                	 <div class="ax-clear"></div>
				               	 </div>
                            <div class="ax-grid" id="page-grid-box" style="min-height: 300px; max-height: 300px;"></div>
                        </ax:custom>                        
                    </ax:custom>
                    <ax:custom customid="tr">                  
                    	<ax:custom customid="td">
                            <%-- %%%%%%%%%% 신규 버튼 (업체등록) %%%%%%%%%% --%>
                            <div class="ax-button-group">
                                <div class="left">
                                    <h2><i class="axi axi-table"></i> 정보등록</h2>
                                </div>
                                <div class="right">
                                   <!--  <button type="button" class="AXButton" id="ax-form-btn-new"><i class="axi axi-plus-circle"></i> 신규</button> -->
                                
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                            <%-- %%%%%%%%%% 폼 (info) %%%%%%%%%% --%>                           
                            <ax:form id="form-info" name="form-info" method="get">                            
                                <ax:fields>
                                    <ax:field label="ID">
                                        <input type="text" id="info-msgId" name="msgId" title="ID" maxlength="10" class="AXInput W100 av-required" value=""/>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="용도" >
                                        <input type="text" id="info-msgUse" name="msgUse" title="용도" maxlength="200" class="AXInput W150" value="" style="min-width:600px; max-width:1000px; "/>
                                    </ax:field>
                                </ax:fields>                               
                                <ax:fields>
                                    <ax:field label="내용">
                                        <input type="text" id="info-msgContent" name="msgContent" title="내용" maxlength="200" class="AXInput W150" value="" style="min-width:600px; max-width:1000px;" />
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="선택타입"  width="150px">
                                        <select id="info-msgType" name="msgType" title="선택타입" class="AXSelect W100"  ></select>                                        
                                    </ax:field>
                                </ax:fields>   
                                 <ax:fields>
                                 	 <ax:field label="방송간격" width="150px">
                                        <input type="text" id="info-msgTerm" name="msgTerm" title="방송간격" maxlength="11" class="AXInput W100" value="" />
                                  	 </ax:field> 
                                 </ax:fields>  
                                 <ax:fields>
                                     <ax:field label="반복횟수" width="150px">
                                        <input type="text" id="info-repeatCnt" name="repeatCnt" title="반복횟수" maxlength="10" class="AXInput W100 " value=""  />
                                    </ax:field>   
                                 </ax:fields>
                                 <ax:fields>
                                     <ax:field label="복귀시간" width="150px">
                                        <input type="text" id="info-closeTime" name="closeTime" title="복귀시간" maxlength="10" class="AXInput W100" value="" />
                                    </ax:field> 
                                 </ax:fields>  
                                  <ax:fields>
                                    <ax:field label="비고">
                                        <input type="text" id="info-remark" name="remark" title="비고" maxlength="200" class="AXInput W400" value="" style="min-width:600px;" />
                                    </ax:field>
                                </ax:fields>                         
                            </ax:form>                         
                        </ax:custom>                            
                    </ax:custom>
                    
                </ax:custom>

            </ax:col>
        </ax:row>
    </ax:div>
    <ax:div name="scripts">
        <script type="text/javascript">
            var resize_elements = [
                {id:"page-grid-box", adjust:-97}
            ];
            var fnObj = {
            	CODES: {
                    "msgType": [
                        {CD:'D', NM:"Display"},
                        {CD:'T', NM:"TTS"},
                        {CD:'B', NM:"둘다"}
                    ],
                    "_msgType": {"D":"Display", "T":"TTS", "B":"둘다"}
                },
                pageStart: function(){
                    this.search.bind();
                    this.grid.bind();
                    this.form.bind();
                    this.bindEvent();
                    // 페이지 로딩 후 바로 검색 처리하기 (option)
                    this.search.submit();
                },
                bindEvent: function(){
                    var _this = this;
                    $("#ax-page-btn-search").bind("click", function(){
                        _this.search.submit();
                    });
                    $("#ax-page-btn-save").bind("click", function(){
                        setTimeout(function() {
                            _this.save();
                        }, 500);
                    });
                    $("#ax-page-btn-excel").bind("click", function(){
                        app.modal.excel({
                            pars:"target=${className}"
                        });
                    });            
                    $("#ax-grid-btn-del").bind("click", function(){
                        fnObj.grid.del();
                    });
                },
                save: function(){
                    var validateResult = fnObj.form.validate_target.validate();
                    if (!validateResult) {
                        var msg = fnObj.form.validate_target.getErrorMessage();
                        axf.alert(msg);
                        fnObj.form.validate_target.getErrorElement().focus();
                        return false;
                    }

                    var info = fnObj.form.getJSON();
                    app.ajax({
                        type: "PUT",
                        url: "/OPER1050/saveMsgUse",
                        data: Object.toJSON([info])
                    },
                    function(res){
                        if(res.error){
                            console.log(res.error.message);
                            alert(res.error.message);
                        }
                        else
                        {
                            toast.push("저장되었습니다.");
                            fnObj.search.submit();
                            fnObj.form.clear();
                        }
                    });
                },
                search: {
                    target: new AXSearch(),
                    bind: function(){
                        var _this = this;
                        this.target.setConfig({
                            targetID:"page-search-box",
                            theme : "AXSearch",
                            /*
                             mediaQuery: {
                             mx:{min:"N", max:767}, dx:{min:767}
                             },
                             */
                            onsubmit: function(){
                                // 버튼이 선언되지 않았거나 submit 개체가 있는 경우 발동 합니다.
                                fnObj.search.submit();
                            },
                            rows:[
                                {display:true, addClass:"", style:"", list:[
                                    {label:"용도", labelWidth:"", type:"inputText", width:"150", key:"msgUse", addClass:"", valueBoxStyle:"", value:"",
                                        onChange: function(changedValue){
                                            //아래 2개의 값을 사용 하실 수 있습니다.
                                            //toast.push(Object.toJSON(this));
                                            //dialog.push(changedValue);
                                        }
                                    }
                                ]}
                            ]
                        });
                    },
                    submit: function(){
                        var pars = this.target.getParam();
                        fnObj.grid.setPage(fnObj.grid.pageNo, pars);
                    }
                },

                grid: {
                    pageNo: 1,
                    target: new AXGrid(),
                    bind: function(){
                        var target = this.target, _this = this;
                        target.setConfig({
                            targetID : "page-grid-box",
                            theme : "AXGrid",
                            colHeadAlign:"center",
                            sort        : true, //정렬을 원하지 않을 경우 (tip
                            colHeadTool : true, // column tool use
                           // fitToWidth  : true, // 너비에 자동 맞춤
                            /*
                             mediaQuery: {
                             mx:{min:"N", max:767}, dx:{min:767}
                             },
                             */
                            colGroup : [
                                {key:"index", label:"선택", width:"35", align:"center", formatter:"checkbox"},
                                {key:"msgId", label:"ID", width:"100", align:"left"},
                                {key:"msgUse", label:"용도", width:"200"},
                                {key:"msgContent", label:"내용", width:"400"},
                                {key:"msgType", label:"방송타입", width:"150" ,align:"center",
                                    formatter: function(val){
                                        return fnObj.CODES._msgType[ ( (Object.isObject(this.value)) ? this.value.NM : this.value ) ];
                                    }
                                },
                                {key:"msgTerm", label:"방송간격", width:"100", align:"center"},
                                {key:"repeatCnt", label:"방송횟수", width:"100", align:"center"},
                                {key:"closeTime", label:"복귀시간", width:"100", align:"center"},
                                {key:"remark", label:"비고", width:"350", align:"left"},
                              /*   {key:"btns",  label:"삭제", width:"100", align:"center", formatter: function(){
                                    return '<button type="button" name="delete" onclick="fnObj.grid.deleteItem(' + this.index + ');"><i class="axi axi-trash-o"></i></button>';
                                }}, */
                            ],
                            body : {
                                onclick: function(){
                                    fnObj.form.setJSON(this.item);
                                }
                            },
                            page: {
                                display: true,
                                paging: true,
                                onchange: function(pageNo){
                                    _this.setPage(pageNo);
                                }
                            }
                        });
                    },
                    
                    /* deleteItem: function(index) {                   	
                    	
                        $.Event(event).stopPropagation(); // 버튼클릭 이벤트가 row click 이벤트를 발생시키지 않도록 합니다.
                        var item = fnObj.grid.target.list[index];       
                       
                        app.ajax({
                                    type: "DELETE",
                                    url: "/OPER1050/saveMsgUse",
                                    data : item.msgId
                                },
                                function(res) {
                                    if (res.error) {
                                        alert(res.error.message);
                                    }else{
                                    	fnObj.search.submit();
                                    }
                                });
                       
                        
                    }, */
                    add:function(){
                        this.target.pushList({});
                        this.target.setFocus(this.target.list.length-1);
                    },
                    del:function(){
                        var _target = this.target,
                                nextFn = function() {
                                    _target.removeListIndex(checkedList);
                                    toast.push("삭제 되었습니다.");
                                };

                        var checkedList = _target.getCheckedListWithIndex(0);// colSeq
                        if(checkedList.length == 0){
                            alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                            return;
                        }
                        var dto_list = [];
                        $.each(checkedList, function(){
                            if(this.item._CUD != "C"){
                            
                                dto_list.push({"msgId":this.item.msgId}); // ajax delete 요청 목록 수집
                             
                            }
                        });

                        if(dto_list.length == 0) {
                            nextFn(); // 스크립트로 목록 제거
                        }
                        else {
                            app.ajax({
		                            	type: "DELETE",
		                                url: "/OPER1050/saveMsgUse",		                                
		                                data: Object.toJSON(dto_list)
                                    },
                                    function(res) {
                                        if (res.error) {
                                            alert(res.error.message);
                                        } else {
                                            nextFn(); // 스크립트로 목록 제거
                                        }
                                    });
                        }
                    },
                    setPage: function(pageNo, searchParams){
                        var _target = this.target;
                        this.pageNo = pageNo;

                        app.ajax({
                            type: "GET",
                            url: "/OPER1050/selectMsgUseList",
                            data: (searchParams||fnObj.search.target.getParam())
                        }, function(res){
                            if(res.error){
                                alert(res.error.message);
                            }
                            else
                            {
                                var gridData = {
                                    list: res.list,
                                    page:{
                                        pageNo: res.page.currentPage.number()+1,
                                        pageSize: res.page.pageSize,
                                        pageCount: res.page.totalPages,
                                        listCount: res.page.totalElements
                                    }
                                };
                                _target.setData(gridData);
                            }
                        });
                    }
                },

                /*******************************************************
                 * 상세 폼
                 */
                form: {
                    target: $('#form-info'),
                    validate_target: new AXValidator(),
                    bind: function() {
                        var _this = this;

                        this.validate_target.setConfig({
                            targetFormName : "form-info"
                        });

                        //$('#info-etc2').bindDate().val( (new Date()).print() );

                        $('#info-msgType').bindSelect({
                            reserveKeys: {
                                optionValue: "CD", optionText: "NM"
                            },
                            options: fnObj.CODES.msgType
                        });

                        // form clear 처리 시
                        $('#ax-form-btn-new').click(function() {
                            fnObj.form.clear();                          
                        });
                        
                     
                    },
                    setJSON: function(item) {
                        var _this = this;

                        // 수정시 입력 방지 처리 필드 처리
                        $('#info-msgId').attr("readonly", "readonly");

                        var info = $.extend({}, item);
                        app.form.fillForm(_this.target, info, 'info-');
                        // 추가적인 값 수정이 필요한 경우 처리
                        // $('#info-useYn').bindSelectSetValue( info.useYn );
                    },
                    getJSON: function() {
                        return app.form.serializeObjectWithIds(this.target, 'info-');
                    },
                    clear: function() {
                        app.form.clearForm(this.target);
                        $('#info-msgId').removeAttr("readonly");
                    }
                } // form
            };
        </script>
    </ax:div>
</ax:layout>