<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>

<ax:layout name="base.jsp">
    <ax:set name="title" value="${PAGE_NAME}" />
    <ax:set name="page_desc" value="${PAGE_REMARK}" />
    
    <ax:div name="styles">
        <style type="text/css">
         .findImg{
            float:left;border:1px solid #666;border-radius:8px;box-shadow:0px 0px 3px #888;margin:5px;height:200px;
        }
        </style>
    </ax:div>
    
    <ax:div name="scripts">
    
        <script type="text/javascript">
        var pageID = "AXSlideViewer";

        var myViewer = new AXSlideViewer();
        myViewer.setConfig({
            id:"myViewer01",
            fitToHeight: false
        });

        /*
         // 이미지 list 의 키 네임을 변경해야 한다면. setConfig 에
         myViewer.setConfig({
         id:"myViewer01",
         fitToHeight: false // true 로 하면 이미지가 꽉찬 형태로 표시 됩니다.
         reserveKeys = {
         title: "title", // 값을 변경 하세요.
         description: "description",
         url: "url"
         }
         });

         //이미지 뷰어를 오픈하려면 아래의 메소드를 이용 하세요.
         // list 는 이미지 배열 전체.
         // selectedIndex 는 오픈되자 마자 봐야할 이미지 순번
         myViewer.open({id:"myViewer01", list:slides, selectedIndex:itemIndex});
         */

        var slides = [
            {title:"젓병뚜겅 먹어버릴 테다.", description:"아빠와 즐거운 한때.", url:"/OPER1060/monitering?ip=192.168.1.38", thumb:"source/thumb/01.png"},
            {title:"젓병뚜겅 먹어버릴 테다.", description:"아빠와 즐거운 한때.", url:"/OPER1060/monitering?ip=192.168.1.38", thumb:"source/thumb/01.png"},
            {title:"젓병뚜겅 먹어버릴 테다.", description:"아빠와 즐거운 한때.", url:"/OPER1060/monitering?ip=192.168.1.38", thumb:"source/thumb/01.png"},
            {title:"젓병뚜겅 먹어버릴 테다.", description:"아빠와 즐거운 한때.", url:"/OPER1060/monitering?ip=192.168.1.38", thumb:"source/thumb/01.png"},
            {title:"젓병뚜겅 먹어버릴 테다.", description:"아빠와 즐거운 한때.", url:"/OPER1060/monitering?ip=192.168.1.38", thumb:"source/thumb/01.png"},
            {title:"젓병뚜겅 먹어버릴 테다.", description:"아빠와 즐거운 한때.", url:"/OPER1060/monitering?ip=192.168.1.38", thumb:"source/thumb/01.png"}           
         
        ];


        var fnObj = {
            pageStart: function(){
                fnObj.slidePreview();
                //prettyPrint();
            },
            slidePreview: function(){
                var po = [];

                for (var item, itemIndex = 0; (itemIndex < slides.length && (item = slides[itemIndex])); itemIndex++) {
                    po.push( '<img src="'+item.thumb+'" id="item_AX_'+ itemIndex +'" ');
                    po.push( 'style="float:left;border:1px solid #666;border-radius:8px;box-shadow:0px 0px 3px #888;margin:5px;height:100px;">' );
                }

                $("#sliderIcons").append(po.join(''));
                $("#sliderIcons").find("img").bind("click", function(event){
                    var itemIndex = event.target.id.split(/_AX_/gi).last();
                    fnObj.sliderView(itemIndex);
                });
            },
            sliderView: function(itemIndex){
                myViewer.open({id:"myViewer01", list:slides, selectedIndex:itemIndex});
            }, machineList : function (){
            	app.ajax({
                    type: "GET",
                    url: "/SYST1020/selectMachineList", 
                    data : (searchParams)
                }, function(res){
                    if(res.error){
                       alert(res.error.message);
                    }
                    else
                    {
                   	                       
                    }
                });
            }
            
        };
        $(document.body).ready(fnObj.pageStart.delay(0.1));
        </script>
    </ax:div>
    <ax:div name="header">
        <h1>Hello AXU4j</h1><p class="desc">AXU4J를 소개합니다.</p>
    </ax:div>
    <ax:div name="contents">
        <ax:row>
            <ax:col size="12">
                <ax:custom customid="page-button" pageId="${PAGE_ID}" searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}" excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}" function2Auth="${FUNCTION_2_AUTH}" function3Auth="${FUNCTION_3_AUTH}" function4Auth="${FUNCTION_4_AUTH}" function5Auth="${FUNCTION_5_AUTH}"></ax:custom>

              
                <div class="ax-button-group">
                    <div class="left">
                        <h2><i class="axi axi-list-alt"></i> 모니터링 장비 목록</h2>
                    </div>
                    <div class="right">
                          <input type="button" value="화면보기" class="AXButton" onclick="fnObj.grid.getScreen();" />
                  		  <input type="button" value="시작" class="AXButton" onclick="fnObj.grid.getExcel('json');" />
                  		  <input type="button" value="종료" class="AXButton" onclick="fnObj.grid.getExcel('json');" />      
                    </div>
                    <div class="ax-clear">
                    	  <img alt="" src="" id="img"> 
                   <!--  	<img alt="" src="/OPER1060/monitering?ip=192.168.1.38" id="img">  -->
                    </div>
                </div>
    <!--           	<div class="AXHspace20 clear"></div>

            <div id="sliderIcons00" class="sliderIcons">
                <img src="/OPER1060/monitering?ip=192.168.1.38" class="findImg" id="0item_AX_0" title="장서우" longdesc="wkdtjdn장서우 장서우" />
               
            </div>

            <div class="AXHspace20 clear"></div>

            <div id="sliderIcons01" class="sliderIcons">
                <img src="/OPER1060/monitering?ip=192.168.1.38" class="findImg" id="1item_AX_0" />
               
            </div>
            <div class="AXHspace20 clear"></div> -->
              	
		

            <div class="AXHspace20 clear"></div>
            <div id="sliderIcons"></div>
            <div class="AXHspace20 clear"></div>

   


            </ax:col>
        </ax:row>
        
    </ax:div>  
   
</ax:layout>