<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ax" uri="http://axisj.com/axu4j" %>

<ax:layout name="base.jsp">
    <ax:set name="title" value="${PAGE_NAME}" />
    <ax:set name="page_desc" value="${PAGE_REMARK}" />
    <ax:div name="contents">
        <ax:row>
            <ax:col size="12">
                <ax:custom customid="page-button" pageId="${PAGE_ID}" searchAuth="${SEARCH_AUTH}" saveAuth="${SAVE_AUTH}" excelAuth="${EXCEL_AUTH}" function1Auth="${FUNCTION_1_AUTH}" function2Auth="${FUNCTION_2_AUTH}" function3Auth="${FUNCTION_3_AUTH}" function4Auth="${FUNCTION_4_AUTH}" function5Auth="${FUNCTION_5_AUTH}"></ax:custom>
                <div class="ax-search" id="page-search-box"></div>

                <ax:custom customid="table">
                    <ax:custom customid="tr">
                        <ax:custom customid="td">
                            <h2><i class="axi axi-list-alt"></i> 예약 등록</h2>
                            <div class="ax-grid" id="page-grid-box" style="height: 400px; min-height: 300px;" ></div>
                        </ax:custom>
					</ax:custom>
                    <ax:custom customid="tr">
                        <ax:custom customid="td">
                            <%-- %%%%%%%%%% 신규 버튼 (업체등록) %%%%%%%%%% --%>
                            <div class="ax-button-group">
<!--                                 <div class="left"> -->
<!--                                     <h2><i class="axi axi-table"></i> 정보등록</h2> -->
<!--                                 </div> -->
                                <div class="right">
                                    <button type="button" class="AXButton" id="ax-form-btn-new"><i class="axi axi-plus-circle"></i> 신규</button>
                                </div>
                                <div class="ax-clear"></div>
                            </div>

                            <%-- %%%%%%%%%% 폼 (info) %%%%%%%%%% --%>
                            <ax:form id="form-info" name="form-info" method="get">
                                <ax:fields>
                                    <ax:field label="예약일자">
                                        <input type="text" id="info-bookDate" name="bookDate" title="예약일자" maxlength="8" class="AXInput W100 av-required" value="" readonly="readonly" placeholder="예약일자"/>
                                    </ax:field>
                                    <ax:field label="일련번호">
                                        <input type="text" id="info-bookSeq" name="bookSeq" title="일련번호" maxlength="3" class="AXInput W50" value="" readonly="readonly" placeholder="일련번호"/>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="고인 ID">
                                        <input type="text" id="info-contid" name="contid" title="고인 ID" maxlength="20" class="AXInput W150" value="" readonly="readonly"  placeholder="고인 ID"/>
                                        <button type="button" class="AXButton" id="info-contid-btn-search"><i class="axi axi-search"></i> 조회</button>
                                    </ax:field>
                                    <ax:field label="고인명">
                                        <input type="text" id="info-conttitle" name="conttitle" title="고인명" maxlength="20" class="AXInput W150 av-required" value="" placeholder="고인명"/>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="제례실">
                                        <select id="info-roomCd" name="roomCd" title="제례실" class="AXSelect"></select>
                                    </ax:field>
                                    <ax:field label="신청자전화">
                                        <input type="tel" id="info-phoneNo" name="phoneNo" title="신청자전화" maxlength="15" class="AXInput W150" value="" placeholder="신청자 전화"/>
                                    </ax:field>
                                    <ax:field label="사진사용">
                                    	<select id="info-photoYn" name="photoYn" title="사진사용" class="AXSelect">
                                    		<option value="Y" selected="selected">Y</option>
                                    		<option value="N">N</option>
                                    	</select>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="사용시작">
                                        <input type="text" id="info-stTime" name="stTime" title="사용시작" maxlength="6" class="AXInput W100" value="" readonly="readonly" placeholder="사용시작시간"/>
                                    </ax:field>
                                    <ax:field label="연장횟수">
                                        <input type="text" id="info-exCnt" name="exCnt" title="연장횟수" maxlength="1" class="AXInput W100" value="" readonly="readonly" placeholder="연장횟수"/>
                                    </ax:field>
                                    <ax:field label="연장시간">
                                        <input type="text" id="info-exMinute" name="exMinute" title="연장시간" maxlength="2" class="AXInput W100" value="" readonly="readonly" placeholder="연장시간"/>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="사용종료">
                                        <input type="text" id="info-edTime" name="edTime" title="사용종료" maxlength="6" class="AXInput W100" value="" readonly="readonly" placeholder="종료 시간"/>
                                    </ax:field>
                                    <ax:field label="진행상태">
                                    	<select id="info-status" name="status" title="진행상태" class="AXSelect">
                                    		<option value="R" selected="selected">예약</option>
                                    		<option value="C">취소</option>
                                    	</select>
                                    </ax:field>
                                    <ax:field label="SMS 신청">
                                    	<select id="info-smsYn" name="smsYn" title="SMS 신청" class="AXSelect">
                                    		<option value="Y" selected="selected">Y</option>
                                    		<option value="N">N</option>
                                    	</select>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="예약자">
                                    	<select id="info-bookMan" name="bookMan" title="예약자" class="AXSelect"></select>
                                    </ax:field>
                                    <ax:field label="예약사유">
                                        <input type="text" id="info-bookBigo" name="bookBigo" title="예약사유" maxlength="100" class="AXInput" style="width:100%" value=""  placeholder="예약사유"/>
                                    </ax:field>
                                </ax:fields>
                                <ax:fields>
                                    <ax:field label="취소자">
                                    	<select id="info-cancelMan" name="cancelMan" title="취소자" class="AXSelect"></select>
                                    </ax:field>
                                    <ax:field label="취소사유">
                                        <input type="text" id="info-cancelBigo" name="cancelBigo" title="취소자" maxlength="100" class="AXInput" style="width:100%" value=""  placeholder="취소사유"/>
                                    </ax:field>
                                </ax:fields>
                            </ax:form>

                        </ax:custom>
                    </ax:custom>
                </ax:custom>

            </ax:col>
        </ax:row>
    </ax:div>
    <ax:div name="scripts">
        <script type="text/javascript">
            var resize_elements = [
                {id:"page-grid-box", adjust:-400}
            ];
            var fnObj = {
                pageStart: function(){
                    this.search.bind();
                    this.grid.bind();
                    this.form.bind();
                    this.bindEvent();
                    // 페이지 로딩 후 바로 검색 처리하기 (option)
                    this.search.submit();
                },
                bindEvent: function(){
                    var _this = this;
                    $("#ax-page-btn-search").bind("click", function(){
                        _this.search.submit();
                    });
                    $("#ax-page-btn-save").bind("click", function(){
                        setTimeout(function() {
                            _this.save();
                        }, 500);
                    });
                    
                    $("#info-contid-btn-search").bind("click", function(){
	                    app.modal.open({
	                        url:"RESV1011.jsp",
	                        pars:"callBack=fnObj.setContid" //+ this.index, // callBack 말고
	                        //width:500, // 모달창의 너비 - 필수값이 아닙니다. 없으면 900
	                        //top:100 // 모달창의 top 포지션 - 필수값이 아닙니다. 없으면 axdom(window).scrollTop() + 30
	                    })
                    });
                    
//                     $("input[type='checkbox']").bindChecked();
                },
                setContid: function(contid, conttitle){
                	$("#info-contid").val(contid);
                	$("#info-conttitle").val(conttitle);
                },
                save: function(){
                    var validateResult = fnObj.form.validate_target.validate();
                    if (!validateResult) {
                        var msg = fnObj.form.validate_target.getErrorMessage();
                        axf.alert(msg);
                        fnObj.form.validate_target.getErrorElement().focus();
                        return false;
                    }

                    var info = fnObj.form.getJSON();
                    info.stTime = info.stTime.replace(":","");
                    info.edTime = info.edTime.replace(":","");
              		var cc = info;
                    app.ajax({
                        type: "PUT",
                        url: "/RESV1010/saveBooking",
                        data: Object.toJSON(info)
                    },
                    function(res){
                        if(res.error){
                            console.log(res.error.message);
                            alert(res.error.message);
                        }
                        else
                        {
                            toast.push("저장되었습니다.");
                            fnObj.search.submit();
                            fnObj.form.clear();
                        }
                    });
                },
                search: {
                    target: new AXSearch(),
                    bind: function(){
                        var _this = this;
                        this.target.setConfig({
                            targetID:"page-search-box",
                            theme : "AXSearch",
                            /*
                             mediaQuery: {
                             mx:{min:"N", max:767}, dx:{min:767}
                             },
                             */
                            onsubmit: function(){
                                // 버튼이 선언되지 않았거나 submit 개체가 있는 경우 발동 합니다.
                                fnObj.search.submit();
                            },
                            rows:[
                                {display:true, addClass:"", style:"", list:[
                                    {label:"예약일자", labelWidth:"", type:"inputText", width:"150", key:"bookDate", addClass:"", valueBoxStyle:"", value:(new Date).print(),
                                    	AXBind       : {
                            				type: "date"
                            			},
                                        onChange: function(changedValue){
                                            //아래 2개의 값을 사용 하실 수 있습니다.
                                            //toast.push(Object.toJSON(this));
                                            //dialog.push(changedValue);
                                        }
                                    }
                                ]}
                            ]
                        });
                    },
                    submit: function(){
                        var pars = this.target.getParam();
                        fnObj.grid.setPage(fnObj.grid.pageNo, pars);
                    }
                },

                grid: {
                    pageNo: 1,
                    target: new AXGrid(),
                    bind: function(){
                        var target = this.target, _this = this;
                        target.setConfig({
                            targetID : "page-grid-box",
                            theme : "AXGrid",
                            colHeadAlign:"center",
                            /*
                             mediaQuery: {
                             mx:{min:"N", max:767}, dx:{min:767}
                             },
                             */
                            colGroup : [
                                //{key:"index", label:"선택", width:"35", align:"center", formatter:"checkbox"},
                                {key:"bookSeq", label:"연번", width:"100", align:"center"},
                                {key:"stTime", label:"시작시간", width:"150", align:"center", formatter : function (){
	                            		return  this.item.stTime.substr(0,2)+":"+this.item.stTime.substr(2,4);
                            		}
                                },
                                {key:"edTime", label:"종료시간", width:"150", align:"center", formatter : function (){
                            			return  this.item.edTime.substr(0,2)+":"+this.item.edTime.substr(2,4);
                        			}
                            	},
                                {key:"roomNm", label:"제례실", width:"150", align:"center"},
                                {key:"conttitle", label:"고인명", width:"150", align:"center"},
                                {key:"exCnt", label:"연장횟수", width:"150", align:"center"},
                                {key:"status", label:"진행상태", width:"150", align:"center",
                                	formatter: function(val){
                                        return this.value == "R" ? "예약" : "취소";
                                    }
                               	},
                                {key:"phoneNo", label:"신청자전화", width:"150", align:"center"},
                                {key:"smsYn", label:"SMS", width:"150", align:"center"}
                            ],
                            body : {
                                onclick: function(){
                                    fnObj.form.setJSON(this.item);
//                                     toast.push(Object.toJSON(this.item));
                                }
                            },
                            page: {
                                display: true,
                                paging: true,
                                onchange: function(pageNo){
                                    _this.setPage(pageNo);
                                }
                            }
                        });
                    },
                    add:function(){
                        this.target.pushList({});
                        this.target.setFocus(this.target.list.length-1);
                    },
                    del:function(){
                        var _target = this.target,
                                nextFn = function() {
                                    _target.removeListIndex(checkedList);
                                    toast.push("삭제 되었습니다.");
                                };

                        var checkedList = _target.getCheckedListWithIndex(0);// colSeq
                        if(checkedList.length == 0){
                            alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                            return;
                        }
                        var dto_list = [];
                        $.each(checkedList, function(){
                            if(this.item._CUD != "C"){
                                dto_list.push("key=" + this.item.key); // ajax delete 요청 목록 수집
                            }
                        });

                        if(dto_list.length == 0) {
                            nextFn(); // 스크립트로 목록 제거
                        }
                        else {
                            app.ajax({
                                        type: "DELETE",
                                        url: "/api/v1/samples/parent",
                                        data: dto_list.join("&")
                                    },
                                    function(res) {
                                        if (res.error) {
                                            alert(res.error.message);
                                        } else {
                                            nextFn(); // 스크립트로 목록 제거
                                        }
                                    });
                        }
                    },
                    setPage: function(pageNo, bookDate){
                        var _target = this.target;
                        this.pageNo = pageNo;

                        app.ajax({
                            type: "GET",
                            url: "/RESV1010/selectBookList",
//                             url: "/api/v1/samples/parent",
                            data: "dummy="+ axf.timekey() +"&pageNumber=" + (pageNo-1) + "&pageSize=50&" + (bookDate||fnObj.search.target.getParam())
                        }, function(res){
                            if(res.error){
                                alert(res.error.message);
                            }
                            else
                            {
                                var gridData = {
                                    list: res.list,
                                    page:{
                                        pageNo: res.page.currentPage.number()+1,
                                        pageSize: res.page.pageSize,
                                        pageCount: res.page.totalPages,
                                        listCount: res.page.totalElements
                                    }
                                };
                                _target.setData(gridData);
                            }
                        });
                    }
                },

                /*******************************************************
                 * 상세 폼
                 */
                form: {
                    target: $('#form-info'),
                    validate_target: new AXValidator(),
                    bind: function() {
                        var _this = this;

                        this.validate_target.setConfig({
                            targetFormName : "form-info"
                        });
                        $('#info-bookDate').bindDate();
                        $('#info-roomCd').bindSelect({
                            reserveKeys: {
                            	options: "list",
                                optionValue: "roomCd", optionText: "roomNm"
                            },
//                             options: fnObj.CODES.etc3
                            direction: "bottom", // selector 박스가 열리는 방향
                			method: "GET", // 정의하지 않으면 "POST"
                			ajaxUrl: "/RESV1010/selectRoomList",
                			ajaxPars: "",
                			onchange: function(){
                				console.log(this);
                			}
                        });
                        $('#info-bookMan').bindSelect({
                        	options:[
                     				{optionValue:"C", optionText:"고객"},
                     				{optionValue:"M", optionText:"관리자"}
                     			],
                   			setValue: "M"
                        });
                        $('#info-cancelMan').bindSelect({
                        	options:[
                     				{optionValue:"C", optionText:"고객"},
                     				{optionValue:"M", optionText:"관리자"}
                     			],
                   			setValue: "M"
                        });
                        
                        $('#info-smsYn').bindSelect();
                        $('#info-photoYn').bindSelect();
                        $('#info-status').bindSelect();
                        // form clear 처리 시
                        $('#ax-form-btn-new').click(function() {
                            fnObj.form.clear()
                        });
                        
                        $("#info-stTime").bindPattern({pattern: "time"});
                        $("#info-edTime").bindPattern({pattern: "time"});
                    },
                    setJSON: function(item) {
                        var _this = this;

                        // 수정시 입력 방지 처리 필드 처리
                        $('#info-bookMan').attr("readonly", "readonly");
                        $('#info-cancelMan').attr("readonly", "readonly");
                       // _this.itme
                        var info = $.extend({}, item);
                        app.form.fillForm(_this.target, info, 'info-');
                        // 추가적인 값 수정이 필요한 경우 처리
                        // $('#info-useYn').bindSelectSetValue( info.useYn );
                    },
                    getJSON: function() {
                        return app.form.serializeObjectWithIds(this.target, 'info-');
                    },
                    clear: function() {
                        app.form.clearForm(this.target);
                        axdom('#info-bookMan').bindSelectSetValue("M");
                        axdom('#info-cancelMan').bindSelectSetValue("M");
                        axdom('#info-status').bindSelectSetValue("R");
                       
                        $('#info-bookDate').bindDate().val( (new Date()).print() );
                    }
                } // form
            };
        </script>
    </ax:div>
</ax:layout>