package com.axisj.axboot.admin.controllers.oper;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.ws.soap.AddressingFeature.Responses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.asp.AspMaster;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectRepository;
import com.axisj.axboot.core.domain.inspect.InspectService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomRepository;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.msgset.MsgSet;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.domain.program.menu.testMapper;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.util.ListUtils;
import com.axisj.axboot.core.vto.InspectRoomVTO;
import com.axisj.axboot.core.vto.InspectVTO;
import com.axisj.axboot.core.vto.MsgSetVTO;
import com.axisj.axboot.core.vto.RoomVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
public class OPER1040Controller extends BaseController{
	
	@Inject
	InspectService inspectService;	
	
	@Inject
	InspectroomService inspectroomService;
	
	@Inject
	private BaseConverter baseConverter;
	
	@ApiOperation(value = "점검 일정 목록", notes = "점검 일정 목록을 보여준다.")
	@RequestMapping(value="/OPER1040/selectInspectList", produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse selectInspectList(String strDate, String endDate) throws Exception{
		
		 List<Inspect> inspectList =  inspectService.selectInspectList(strDate, endDate);
		 return CommonListResponseParams.ListResponse.of(InspectVTO.of(inspectList));
		
	}
	
	@ApiOperation(value = "점검 제례실 일정을 등록/수정", notes = "점검 제례실을 등록/수정 한다.")
	@RequestMapping(value="/OPER1040/saveInspectList",method = RequestMethod.PUT , produces = APPLICATION_JSON)	
	public ApiResponse saveInspectList(@RequestBody List<Inspect> inspects) throws Exception{		
		
		inspectService.saveInspectList(inspects);
		return ok();
	}
	
	@ApiOperation(value = "점검 일정을 삭제", notes = "점검 일정을 삭제 한다.")
	@RequestMapping(value="/OPER1040/saveInspectList",method = RequestMethod.DELETE , produces = APPLICATION_JSON)	
	public ApiResponse deleteInspectList(@RequestBody List<InspectVTO> inspects) throws Exception{	
		
		List<Inspect> list = baseConverter.convert(inspects, Inspect.class);
		inspectService.deleteInspectList(list);					
		return ok();
	}	
	
	@ApiOperation(value = "점검대상인 제례실 목록", notes = "점검대상 제례실 목록을 보여준다.")
	@RequestMapping(value="/OPER1040/selectInspectRoomList", produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse selectInspectRoomList(String workDate, int seqNo) throws Exception{
		
		 List<Inspectroom> inspectrooms =  inspectroomService.selectInspectRoomList(workDate,seqNo);		 
		 return CommonListResponseParams.ListResponse.of(InspectRoomVTO.of(inspectrooms));
		
	}
	@ApiOperation(value = "점검대상이 아닌 제례실 목록", notes = "점검대상이아닌 제례실 목록을 보여준다.")
	@RequestMapping(value="/OPER1040/selectInspectNotRoomList", produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse selectInspectNotRoomList(String workDate, int seqNo) throws Exception{
		
		List<Room> rooms =  inspectroomService.selectInspectNotRoomList(workDate,seqNo);		 
		 return CommonListResponseParams.ListResponse.of(RoomVTO.of(rooms));
		
	}
	
	
	
	@ApiOperation(value = "점검제례실 등록/수정", notes = "점검 제례실을 등록/수정 한다.")
	@RequestMapping(value="/OPER1040/saveInspectRoomList",method = RequestMethod.PUT , produces = APPLICATION_JSON)	
	public ApiResponse saveInspectRoomList(@RequestBody List<InspectRoomVTO> inspectRooms) throws Exception{		
		List<Inspectroom> list = baseConverter.convert(inspectRooms, Inspectroom.class);
		inspectroomService.saveInspectRoomList(list);
		return ok();
	}
	

}
