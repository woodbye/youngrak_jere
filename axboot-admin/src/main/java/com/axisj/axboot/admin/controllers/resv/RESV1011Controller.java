package com.axisj.axboot.admin.controllers.resv;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomService;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.util.DozerBeanMapperUtils;
import com.axisj.axboot.core.vto.BookingVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.axisj.axboot.core.vto.RoomVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
public class RESV1011Controller extends BaseController{
	
	@Inject
	MdcontentsService mdcontentsService;
	
	@ApiOperation(value = "고인검색", notes = "관리자 예약 페이지에서 고인 검색")
	@RequestMapping(value="/RESV1011/selectMdcontentsList", method=RequestMethod.GET, produces = APPLICATION_JSON)
	public PageableResponseParams.PageResponse selectRoomList(
			@RequestParam(value="contTitle", required=false, defaultValue="") String contTitle
			, Pageable pageable
			) throws Exception{
		
		Page<Mdcontents> pages = mdcontentsService.findByFirstnameContaining(contTitle, pageable);
		return PageableResponseParams.PageResponse.of(MdcontentsVTO.of(pages.getContent()), PageableTO.of(pages));
	}
	
//	/**
//	 * 
//	 * 메소드 명칭 : saveBookList
//	 * 메소드 설명 : 예약 변경사항 저장		  
//	 * ----------------------------------------
//	 * 이력사항 2014. 10. 17. 이승호 최초작성
//	 *
//	 * @param eDto
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value="/RESV1010/saveBookList", method={RequestMethod.POST, RequestMethod.PUT}, produces = APPLICATION_JSON)
//	public ApiResponse saveBookList(@RequestBody List<BookingVTO> bookingVTOList) throws Exception{
//		bookingService.saveBookList(bookingVTOList);
//		return ok();
//	}
	
	
	
	
//	/**
//	 * 
//	 * 메소드 명칭 : selectMate
//	 * 메소드 설명 : 배우자 명  
//	 * ----------------------------------------
//	 * 이력사항 2014. 11. 18. 이승호 최초작성
//	 *
//	 * @param params
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping("/RESV1010/selectMate.ex")
//	public ModelAndView selectMate(@RequestParam Map<String,String> params) throws Exception{
//		Map<String, Object> data = resv1010Service.selectMate(params);
//		return makeModelForExria(data);
//	}
//	
//	/**
//	 * 
//	 * 메소드 명칭 : saveBookList
//	 * 메소드 설명 : 예약 변경사항 저장		  
//	 * ----------------------------------------
//	 * 이력사항 2014. 10. 17. 이승호 최초작성
//	 *
//	 * @param eDto
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping("/RESV1010/saveBookList.ex")
//	public ModelAndView saveBookList(ExriaUDDTO eDto, @RequestParam("BOOK_DATE") String bookDate) throws Exception{
//		Map<String, Object> data = resv1010Service.saveBookList(eDto.getRequestData(), bookDate);
//		return makeModelForExria(data);
//	}
//	
//	/**
//	 * 
//	 * 메소드 명칭 : exportExcelBookList
//	 * 메소드 설명 : 예약 목록 엑셀		  
//	 * ----------------------------------------
//	 * 이력사항 2014. 11. 4. 이승호 최초작성
//	 *
//	 * @param bookDate
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping("/RESV1010/exportExcelBookList.ex")
//	public ModelAndView exportExcelBookList(@RequestParam("BOOK_DATE") String bookDate) throws Exception{
//		
//		Map<String, Object> data = resv1010Service.exportExcelBookList(bookDate);
//		
//		return makeModelForExria(data);
//	}
	

}
