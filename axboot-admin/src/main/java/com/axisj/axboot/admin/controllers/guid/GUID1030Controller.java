package com.axisj.axboot.admin.controllers.guid;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.deadsch.DeadSchService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.sample.parent.ParentSample;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.vto.DeadSchVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.ParentSampleVTO;

@Controller
public class GUID1030Controller extends BaseController {

	@Inject
	private BaseConverter baseConverter;
	
	
	//private DeadSchService deadSchService;
	
	@RequestMapping(value="/GUID1030/savsSms" ,method = {RequestMethod.PUT,RequestMethod.POST} ,produces = APPLICATION_JSON)
	public ApiResponse saveSms(DeadSch deadSch) throws Exception{
			
		return ok();
		 
	}
}
