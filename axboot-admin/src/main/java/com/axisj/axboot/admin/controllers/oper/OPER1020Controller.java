package com.axisj.axboot.admin.controllers.oper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.CommonListResponseParams.ListResponse;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.deadsch.DeadSchService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.domain.sample.parent.ParentSample;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.vto.DeadSchVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.InspectVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.axisj.axboot.core.vto.ParentSampleVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
public class OPER1020Controller extends BaseController {

	@Inject
	private BaseConverter baseConverter;
	
	@Inject
	private MdcontentsService mdcontentsService;
	
	@ApiOperation(value = "제례실 고인정보", notes = "제례실 고인정보 조회")
	@RequestMapping(value="/OPER1020/selectMdcontentsList" ,method = RequestMethod.GET ,produces = APPLICATION_JSON)
	public PageableResponseParams.PageResponse selectMdcontentsList(Pageable pageable,@RequestParam(defaultValue="") String key, String keyword) throws Exception{
			
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("key", key);
		params.put("keyword", keyword);
		params.put("pageable", pageable);
		
		Page<Mdcontents> pages = mdcontentsService.selectMdcontentsList(params);		
		return PageableResponseParams.PageResponse.of(MdcontentsVTO.of(pages.getContent()), PageableTO.of(pages));
		
		 
	}

	@ApiOperation(value = "고인정보", notes = "고인정보, 사진 저장")
	@RequestMapping(value="/OPER1020/saveMdcontent",method = {RequestMethod.POST, RequestMethod.PUT} , produces = APPLICATION_JSON)	
	public ApiResponse saveMdcontent(
				MultipartHttpServletRequest request
			) throws Exception{
		
		
		MdcontentsVTO mdcontentsVTO = new MdcontentsVTO();
		
		MultipartFile file = request.getFile("file");
		mdcontentsVTO.setContMultipartFile(file);
		mdcontentsVTO.setContDate(request.getParameter("contDate"));
		mdcontentsVTO.setContTitle(request.getParameter("contTitle"));
		mdcontentsVTO.setContId(request.getParameter("contId"));
		mdcontentsVTO.setContRegUser(request.getParameter("contRegUser"));
		mdcontentsVTO.setContFile(request.getParameter("contFile"));
		
		List<MdcontentsVTO> mdcontentsVTOs = new ArrayList<>();
		
		mdcontentsVTOs.add(mdcontentsVTO);
		
		mdcontentsService.saveMdcontent(mdcontentsVTOs);
		return ok();
	}
	
	@ApiOperation(value = "고인 삭제", notes = "고인을 삭제 한다.")
	@RequestMapping(value="/OPER1020/deleteMdcontentList",method = RequestMethod.DELETE , produces = APPLICATION_JSON)	
	public ApiResponse deleteMdcontentList(@RequestParam(value = "contId", required = true) List<String> contId) throws Exception{	
	
		mdcontentsService.deleteMdcontentList(contId);					
		return ok();
	}	
}
