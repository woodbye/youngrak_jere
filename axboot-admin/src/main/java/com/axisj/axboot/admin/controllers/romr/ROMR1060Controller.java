package com.axisj.axboot.admin.controllers.romr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.json.simple.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.code.ApiStatus;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingId;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomService;
import com.axisj.axboot.core.domain.user.auth.group.AuthGroup;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.SpringUtils;
import com.axisj.axboot.core.vto.BookingVTO;
import com.axisj.axboot.core.vto.DeadSchVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.InspectRoomVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/public")
public class ROMR1060Controller extends BaseController{

	@Inject
	BookingService bookingService;
	
	@Inject
	RoomService roomService;
	
	@Inject
	private BaseConverter baseConverter;
	
	
	@ApiOperation(value = "예약리스트", notes = "키오스크 취소화면의 현재예약리스트")
	@RequestMapping(value="/ROMR1060/selectBookinglList" , produces=APPLICATION_JSON)
	public PageableResponseParams.PageResponse selectBookinglList(Pageable pageable) throws Exception{
		
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");				
		Page<Booking> pages = bookingService.selectAllBookingList(stDate,pageable);
		return PageableResponseParams.PageResponse.of(BookingVTO.of(pages.getContent()), PageableTO.of(pages));		
	}
		
	@ApiOperation(value = "예약취소 ", notes = " 예약 취소")
	@RequestMapping(value="/ROMR1060/cancelBooking2" , method=RequestMethod.POST,  produces=APPLICATION_JSON)
	public ApiResponse cancelBooking(BookingVTO bookingVTO) throws Exception{
		
		Booking booking = baseConverter.convert(bookingVTO,Booking.class);	
		Booking result = bookingService.findOne(booking.getId());
		
		//TTS 메세지
		String msg = result.getStTime().substring(0,2) + "시, " +  result.getStTime().substring(2,4) + "분, " + result.getRoom().getRoomNm().replace("0", "")+",";
		result.setStatus(booking.getStatus());			
		result.setCancelMan(booking.getCancelMan());
		bookingService.save(result);
		
		return ApiResponse.of(ApiStatus.SUCCESS, msg);
	}

}
