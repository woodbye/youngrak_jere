package com.axisj.axboot.admin.security;

import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.code.ApiStatus;
import com.axisj.axboot.core.domain.user.AdminLoginUser;
import com.axisj.axboot.core.domain.user.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminLoginFilter extends AbstractAuthenticationProcessingFilter {

	private final AdminTokenAuthenticationService tokenAuthenticationService;
	private final AdminUserDetailsService userDetailsService;
	private final UserService userService;

	public AdminLoginFilter(String urlMapping, AdminTokenAuthenticationService tokenAuthenticationService, AdminUserDetailsService userDetailsService, AuthenticationManager authManager, UserService userService) {
		super(new AntPathRequestMatcher(urlMapping));

		this.userDetailsService = userDetailsService;
		this.tokenAuthenticationService = tokenAuthenticationService;
		this.userService = userService;
		this.setAuthenticationFailureHandler(new LoginFailureHandler());

		setAuthenticationManager(authManager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
		final AdminLoginUser user = new ObjectMapper().readValue(request.getInputStream(), AdminLoginUser.class);
		final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
		return getAuthenticationManager().authenticate(loginToken);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
		final AdminLoginUser authenticatedUser = userDetailsService.loadUserByUsername(authentication.getName());
		final AdminUserAuthentication userAuthentication = new AdminUserAuthentication(authenticatedUser);

		tokenAuthenticationService.addAuthentication(response, userAuthentication);

		SecurityContextHolder.getContext().setAuthentication(userAuthentication);

		userService.updateLastLoginDateTime(authenticatedUser.getUsername());
	}

	class LoginFailureHandler implements AuthenticationFailureHandler {
		@Override
		public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
			String message = exception.getMessage();

			if (exception instanceof BadCredentialsException) {
				message = "패스워드를 확인하세요";
			}

			ApiResponse apiResponse = ApiResponse.error(ApiStatus.SYSTEM_ERROR, message);
			AdminAuthenticationEntryPoint.jsonExceptionResponse(request, response, apiResponse);
		}
	}
}
