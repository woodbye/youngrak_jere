package com.axisj.axboot.admin.controllers.bord;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomService;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.ListUtils;
import com.axisj.axboot.core.vto.BookingVTO;
import com.axisj.axboot.core.vto.RoomVTO;
import com.wordnik.swagger.annotations.Api;

@Controller
@Api(value = "BORD1010", description = "BORD1010 API")
@SessionAttributes("roomPage")
@RequestMapping("/api/v1/public")
public class BORD1010Controller extends BaseController {
	
    private static final Logger log = LoggerFactory.getLogger(BORD1010Controller.class);
	
	private final static int LIMIT_CHASU = 9;
	
	@Autowired
	EnvsetService envsetService;
	
	@Autowired
	BookingService bookingService;
	
	@Autowired
	RoomService roomService;
	
	@ModelAttribute("roomPage")
	public Pageable setRoomPage(Pageable roomPage){
		if(roomPage == null || roomPage.getPageSize() != 10){
			return new PageRequest(0, 10);
		}
		return roomPage;
	}
	
	public boolean canAddBooking(TreeMap<Long,Long> colspan){
		long totalColCount = 0;
		for(Entry<Long,Long> entry : colspan.entrySet()){
			totalColCount+=entry.getValue();
		}
		return totalColCount<LIMIT_CHASU;
	}
	
	/**
	 * 
	 * 메소드명칭 : selectSetting
	 * 메소드설명 :	현황판 정보 	
	 * ----------------------------------------
	 * 이력사항 2016. 1. 29. 이승호 최초작성
	 *
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value="/BORD1010/selectSetting", method=RequestMethod.GET,  produces = APPLICATION_JSON)
	public CommonListResponseParams.MapResponse selectSetting(Model model, @ModelAttribute("roomPage") Pageable roomPage) throws Exception{
		
		// 금일
		String yyyyMMdd = DateUtils.formatToDateString("yyyyMMdd");
		
		// 금일 환경설정
		Envset envset = envsetService.selectEnvSet(yyyyMMdd);

		// 조건 시간
		Date conditionTime = new Date();
		
		// 현재 시간
		Date now = new Date();

		// 시간들을 long값으로 환산
		Long openTime = DateUtils.parseDate("yyyyMMddHHmm", yyyyMMdd+envset.getOpenTime()).getTime();
		Long closeTime = DateUtils.parseDate("yyyyMMddHHmm", yyyyMMdd+envset.getCloseTime()).getTime();
		
		// 개방 여부
		String openYN = openTime < conditionTime.getTime() && closeTime > conditionTime.getTime() ? "Y" : "N";
		
		conditionTime.setHours(conditionTime.getHours()-1);
		conditionTime.setMinutes(0);
		
		// 금일 예약건
		List<Booking> books = bookingService.findAfterBookings(yyyyMMdd, "R");
		List<BookingVTO> bookVTOs = BookingVTO.of(books);
		
		// 가장 빠른 예약 가능 시간
		String fastestTime = bookingService.getFastestTime(books, envset.getCleanTime());
		
		// 금일 시간 범위(1시간전부터)
		List<Long> timeRange = null;
		Long lastHours = Long.parseLong(envset.getCloseTime().substring(0, 2))+1L;
		if(conditionTime.getHours() <= lastHours){
			timeRange = ListUtils.range(
						conditionTime.getHours()
						, lastHours
						, 1L);
		}else{
			timeRange = new ArrayList<>();
		}
		
		// 제례실 정보(최대 10개)
		Page<Room> roomPages = roomService.findAll(roomPage);
		List<RoomVTO> rooms = RoomVTO.of(roomPages.getContent());
		
		if(roomPages.hasNext()){
			model.addAttribute("roomPage", roomPage.next());
		}else{
			model.addAttribute("roomPage", roomPage.first());
		}
		
		// 화면에 뿌려줄 데이터 구조 형성
		// 시간, 정보
		TreeMap<Long, TreeMap<String, List<BookingVTO>>> bookingInfo = new TreeMap<>();
		TreeMap<Long, Long> colspan = new TreeMap<>();
		
		for(Long time : timeRange){
			
			TreeMap<String, List<BookingVTO>> book = new TreeMap<>();
			for(RoomVTO roomVTO : rooms){
				book.put(roomVTO.getRoomCd(),  new ArrayList<BookingVTO>());
			}
			for(BookingVTO booking : bookVTOs){
				if(time == Long.parseLong(booking.getStTime().substring(0, 2))){
					if(book.get(booking.getRoomCd())==null){
						continue;
					}
					if(!canAddBooking(colspan)){
						break;
					}
					book.get(booking.getRoomCd()).add(booking);
//					log.debug("booking.getRoomCd() : " + booking.getRoomCd());
				}
			}
			
			for(Entry<String, List<BookingVTO>> entry : book.entrySet()){
				long size = entry.getValue().size();
				Long col = colspan.get(time);
				if(col == null || col < size){
					colspan.put(time, size);
				}
			}
			
			bookingInfo.put(time,book);
//			log.debug("time : " + time);
			
		};
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("envset", envset);
		map.put("openYN", openYN);
		map.put("now", DateUtils.formatToDateString("HH:mm", now));
		map.put("colspan", colspan);
		map.put("bookingInfo", bookingInfo);
		map.put("rooms", rooms);
		map.put("fastestTime", fastestTime);
		
		return CommonListResponseParams.MapResponse.of(map);
	}
	
}
