package com.axisj.axboot.admin.controllers;

import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.asp.AspMaster;
import com.axisj.axboot.core.domain.asp.AspMasterService;
import com.axisj.axboot.core.vto.AspMasterVTO;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.util.List;

@Controller
@RequestMapping(value = "/api/v1/asp/masters")
@Api(value = "ASP Master", description = "ASP Master")
public class AspMasterController extends BaseController {

	@Inject
	private AspMasterService aspMasterService;

	@Inject
	private BaseConverter baseConverter;

	@ApiOperation(value = "ASP Master", notes = "ASP Master 정보")
	@RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse list() {
		List<AspMaster> aspMasterList = aspMasterService.findAll();
		return CommonListResponseParams.ListResponse.of(AspMasterVTO.of(aspMasterList));
	}

	@ApiOperation(value = "권한그룹 등록/수정", notes = "권한그룹을 등록/수정 한다.")
	@RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT}, produces = APPLICATION_JSON)
	public ApiResponse save(@RequestBody List<AspMasterVTO> request) {
		List<AspMaster> aspMasterList = baseConverter.convert(request, AspMaster.class);
		aspMasterService.save(aspMasterList);
		return ok();
	}
}
