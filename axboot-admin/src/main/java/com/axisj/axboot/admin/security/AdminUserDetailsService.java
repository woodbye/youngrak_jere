package com.axisj.axboot.admin.security;

import com.axisj.axboot.core.code.Params;
import com.axisj.axboot.core.domain.company.Company;
import com.axisj.axboot.core.domain.company.CompanyService;
import com.axisj.axboot.core.domain.program.menu.MenuService;
import com.axisj.axboot.core.domain.program.menu.UserMenuInfo;
import com.axisj.axboot.core.domain.user.AdminLoginUser;
import com.axisj.axboot.core.domain.user.User;
import com.axisj.axboot.core.domain.user.UserService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class AdminUserDetailsService implements UserDetailsService {

	@Inject
	private UserService userService;

	@Inject
	private MenuService menuService;

	@Inject
	private CompanyService companyService;

	@Override
	public final AdminLoginUser loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.findOne(username);

		if (user == null) {
			throw new UsernameNotFoundException("사용자 정보가 정확하지 않습니다.");
		}

		if (user.getUseYn().equals(Params.N)) {
			throw new UsernameNotFoundException("미사용 처리된 사용자 입니다.");
		}

		AdminLoginUser loginUser = new AdminLoginUser();
		loginUser.setUsername(user.getUserCd());
		loginUser.setUserNm(user.getUserNm());
		loginUser.setPassword(user.getUserPs());
		loginUser.setUserType(user.getUserType());

		if (user.getCompCd() != null) {
			Company company = companyService.findOne(user.getCompCd());

			if (company != null) {
				loginUser.setCompanyCode(company.getCompCd());
				loginUser.setCompanyName(company.getCompNm());
			}
		}

		UserMenuInfo userMenuInfo = menuService.getUserMenuInfo(loginUser.getUsername());

		loginUser.setMenuJsonHash(userMenuInfo.getMainMenuJsonHash());

		return loginUser;
	}
}
