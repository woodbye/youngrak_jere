package com.axisj.axboot.admin.controllers.romr;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.vto.DeadSchVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/public")
public class ROMR1020Controller extends BaseController{
	
/*	@ApiOperation(value = "고인 목록 검색", notes = "고인 목록 검색")
	@RequestMapping(value="/ROMR1020" , method= RequestMethod.GET)
	public String maing() throws Exception{
		
		return "/jsp/romr/ROMR1020";
	}*/
}
