package com.axisj.axboot.admin.controllers.com;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.domain.machine.Machine;
import com.axisj.axboot.core.domain.machine.MachineService;
import com.axisj.axboot.core.util.SpringUtils;
import com.axisj.axboot.core.vto.MachineVTO;
import com.wordnik.swagger.annotations.Api;

@Controller
@Api(value = "SYST1020", description = "SYST1020 API")
public class SYST1020Controller extends BaseController {
	@Inject
	private MachineService machineService;
	
	/**
	 * 
	 * 메소드명칭 : selectMachineList
	 * 메소드설명 :	설치장비 목록 출력		 	
	 * ----------------------------------------
	 * 이력사항 2016. 1. 5. 김동호 최초작성
	 *
	 * @param machineName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/SYST1020/selectMachineList" ,  produces = APPLICATION_JSON)	
	public CommonListResponseParams.ListResponse selectMachineList(@RequestParam(required = false, defaultValue = "") String machineName) throws Exception{
				
		List<Machine> machines = machineService.selectMachineList(machineName);
		return CommonListResponseParams.ListResponse.of(MachineVTO.of(machines));
			
		
	}		
	/**
	 * 
	 * 메소드명칭 : saveMachineList
	 * 메소드설명 :	설치장비 등록,수정	
	 * ----------------------------------------
	 * 이력사항 2016. 1. 5. 김동호 최초작성
	 *
	 * @param machines 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/SYST1020/saveMachineList",method = RequestMethod.PUT , produces = APPLICATION_JSON)	
	public CommonListResponseParams.ListResponse saveMachineList(@RequestBody List<Machine> machines) throws Exception{		
		
		machineService.saveMachineList(machines);
		return selectMachineList("");
	}
	/**
	 * 
	 * 메소드명칭 : delMachineList
	 * 메소드설명 :	설치장비 삭제	
	 * ----------------------------------------
	 * 이력사항 2016. 1. 5. 김동호 최초작성
	 *
	 * @param machineCd
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/SYST1020/saveMachineList",method = RequestMethod.DELETE , produces = APPLICATION_JSON)	
	public CommonListResponseParams.ListResponse delMachineList(@RequestParam(value="machineCd") List<String> machineCd) throws Exception{		
		
		machineService.deleteRoom(machineCd);
		return selectMachineList("");
	}
	
	/**
	 * 
	 * 메소드명칭 : goPageOfMachine
	 * 메소드설명 :	설치 장비의 기본 페이지로 이동(이동 조건 IP)
	 * ----------------------------------------
	 * 이력사항 2016. 1. 22. 이승호 최초작성
	 *
	 * @return
	 * @throws Exception
	 */
	/*@RequestMapping(value="/SYST1020/goPageOfMachine",method = RequestMethod.GET)	
	public String goPageOfMachine(HttpServletRequest request) throws Exception{		
		
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		String url = machineService.findUrlByIpaddress(ip);
		
		return redirect(url);
	}*/
	
	@RequestMapping(value="/SYST1020/goPageOfMachine",method = RequestMethod.GET)	
	public String goPageOfMachine() throws Exception{		
		
		String url = machineService.findUrlByIpaddress(SpringUtils.getClientIp());
		
		return redirect(url);
	}
	
}
