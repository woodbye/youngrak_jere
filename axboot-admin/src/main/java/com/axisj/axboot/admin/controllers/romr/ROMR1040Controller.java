package com.axisj.axboot.admin.controllers.romr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.domain.user.auth.group.AuthGroup;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.vto.BookingVTO;
import com.axisj.axboot.core.vto.DeadSchVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.InspectRoomVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/public")
public class ROMR1040Controller extends BaseController{

	@Inject
	MdcontentsService mdcontentsService;
	
	@Inject
	BookingService bookingService;
	
	@Inject
	InspectService inspectService;
	
	@Inject
	InspectroomService inspectroomService;
	
	@Inject
	private BaseConverter baseConverter;
	



	
	@ApiOperation(value = "제례실 예약", notes = "선택한 제례실 예약처리")
	@RequestMapping(value="/ROMR1040/saveBooking", method=RequestMethod.POST, produces = APPLICATION_JSON)
	public Object saveBooking(BookingVTO bookingVTO) throws Exception{
		
		String bookDate = DateUtils.formatToDateString("yyyyMMdd");		
		bookingVTO.setBookDate(bookDate);		
		Booking booking = baseConverter.convert(bookingVTO, Booking.class);		
		Booking result = bookingService.romrSaveBooking(booking);		
		
		return BookingVTO.of(result);
	}
	
	
	@ApiOperation(value = "키오스크 제례실 현황조회", notes = "현재 예약제례실, 점검제례실 조회")
	@RequestMapping(value="/ROMR1040/selectRoomList" ,produces = APPLICATION_JSON)
	public CommonListResponseParams.MapResponse selectEnv() throws Exception{
		
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");			
		Map<String, Object> map= new HashMap<String, Object>();
		
		Inspect inspect = inspectService.selectInspect(stDate);
		List<Booking> bookingList = bookingService.selectBookingList(stDate);		
		List<Inspectroom> inspectroomList = inspectroomService.selectInspectRoomList(inspect.getWorkDate(), inspect.getSeqNo());
		
		map.put("booking", BookingVTO.of(bookingList));
		map.put("inspectRoom", InspectRoomVTO.of(inspectroomList));	
		
		
		return CommonListResponseParams.MapResponse.of(map);
	}

	
}
