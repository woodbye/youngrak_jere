package com.axisj.axboot.admin.controllers.com;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.domain.machine.Machine;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomRepository;
import com.axisj.axboot.core.domain.room.RoomService;
import com.axisj.axboot.core.vto.RoomVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
public class SYST1030Controller extends BaseController {

	@Inject
	private RoomService roomService;
	@ApiOperation(value = "제례실 목록", notes = "제례실 목록을 보여준다.")
	@RequestMapping(value="/SYST1030/selectRoomList", produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse selectRoomList(@RequestParam(required = false, defaultValue = "") String roomNm) throws Exception{
		
		List<Room> rooms = roomService.selectRoomList(roomNm);		
		return CommonListResponseParams.ListResponse.of(RoomVTO.of(rooms));
	}
	
	@ApiOperation(value = "제례실 등록/수정", notes = "제레실코드를 등록/수정 한다.")
	@RequestMapping(value="/SYST1030/saveRoomList",method = RequestMethod.PUT , produces = APPLICATION_JSON)	
	public CommonListResponseParams.ListResponse saveRoomList(@RequestBody List<Room> rooms) throws Exception{		
		
		roomService.saveRoomList(rooms);
		return selectRoomList("");
	}
	@ApiOperation(value = "제례실 삭제", notes = "제례실코드를 삭제 한다.")
	@RequestMapping(value="/SYST1030/saveRoomList",method = RequestMethod.DELETE , produces = APPLICATION_JSON)	
	public CommonListResponseParams.ListResponse delMachineList(@RequestParam(value="roomCd") List<String> roomCd) throws Exception{		
		
		roomService.deleteRoom(roomCd);
		return selectRoomList("");
	}
}
