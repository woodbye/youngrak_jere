package com.axisj.axboot.admin.controllers.oper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.deadsch.DeadSchService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.sample.parent.ParentSample;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.vto.DeadSchVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.ParentSampleVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
public class OPER1010Controller extends BaseController {

	@Inject
	private BaseConverter baseConverter;
	
	@Inject
	private DeadSchService deadSchService;
	
	@ApiOperation(value = "장사시스템 고인검색", notes = "장사시스템 고인검색")	
	@RequestMapping(value="/OPER1010/selectDeadSchList" ,method = RequestMethod.GET ,produces = APPLICATION_JSON)
	public PageableResponseParams.PageResponse selectDeadSchList(Pageable pageable,@RequestParam(defaultValue="ALL") String kind, String deadName) throws Exception{
			
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("kind", kind);
		params.put("deadName", deadName);
		params.put("pageable", pageable);
		
		Page<DeadSch> pages = deadSchService.selectDeadSchList(params);		
		return PageableResponseParams.PageResponse.of(DeadSchVTO.of(pages.getContent()), PageableTO.of(pages));
		
		 
	}
}
