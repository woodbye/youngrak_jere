package com.axisj.axboot.admin.controllers.oper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * 
 * 업무분류   : 운영관리
 * 기능분류   : 운영환경 등록
 * 프로그램명 : OPER1030Controller
 * 설      명 : 운영환경 등록
 * ------------------------------------------
 *
 * 이력사항 2014. 10. 23. 이승호 최초작성 <BR/>
 */
@Controller
public class OPER1030Controller extends BaseController{

	@Inject
	private EnvsetService envsetService;
	
	@Inject
	private BaseConverter baseConverter;
	/**
	 * 
	 * 메소드 명칭 : selectEnvList
	 * 메소드 설명 : 운영환경 목록		  
	 * ----------------------------------------
	 * 이력사항 2014. 10. 24. 이승호 최초작성
	 *
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/OPER1030/selectEnvList" ,produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse selectEnvList(String start, String end) throws Exception{
		List<Envset> envsets = envsetService.findByWorkDateBetween(start, end);
		return CommonListResponseParams.ListResponse.of(EnvsetVTO.of(envsets));
	}
	
	/**
	 * 
	 * 메소드 명칭 : exportExcel
	 * 메소드 설명 : 엑셀 출력
	 * ----------------------------------------
	 * 이력사항 2014. 11. 18. 이승호 최초작성
	 *
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/OPER1030/exportExcel")
	public void exportExcel(String start, String end, HttpServletResponse response) throws Exception{
		envsetService.exportExcel(start, end, response);
	}
	
	/**
	 * 
	 * 메소드 명칭 : saveEnvList
	 * 메소드 설명 : 운영환경 저장		  
	 * ----------------------------------------
	 * 이력사항 2014. 10. 25. 이승호 최초작성
	 *
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/OPER1030/saveEnvset",method = RequestMethod.PUT , produces = APPLICATION_JSON)	
	public  ApiResponse saveEnvset(@RequestBody List<EnvsetVTO> envsets) throws Exception{
		
		List<Envset> list = baseConverter.convert(envsets, Envset.class);
		envsetService.saveEnvset(list);
		return ok();
		
		/*if(envsetService.saveEnvList(list)){
			return this.selectEnvList(start, end);
		}else{
			return null;
		}*/
		
	}
	
	@RequestMapping(value="/OPER1030/deleteEnvList",method = RequestMethod.DELETE , produces = APPLICATION_JSON)	
	public  ApiResponse deleteEnvList(@RequestBody List<EnvsetVTO> envsets, String start, String end) throws Exception{
		
		List<Envset> list = baseConverter.convert(envsets, Envset.class);
		envsetService.deleteEnvList(list);
		return ok();		
		
	}
	
}
