package com.axisj.axboot.admin.controllers.romd;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.domain.version.ProgramVersionRepository;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.util.SpringUtils;
import com.axisj.axboot.core.vto.BookingVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.InspectRoomVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.ApiOperation;

import antlr.Utils;

@Controller
@RequestMapping(value = "/api/v1/public")
public class ROMD1010Controller extends BaseController{
	private static final Logger logger = LoggerFactory.getLogger(BaseController.class);
	
	
	@Inject
	EnvsetService envsetService;	// 운영정보

	@Inject
	MdcontentsService mdcontentsService; //사진등록 고인
	
	@Inject
	BookingService bookingService; // 예약
	
	@Inject
	InspectService inspectService; //점검일정 
	
	@Inject
	InspectroomService inspectroomService; // 점검제례실
		
	@Inject
	private BaseConverter baseConverter; 
	@Inject
	ProgramVersionRepository programVersionRepository;
	
	@ApiOperation(value = "제례시작 화면", notes = "제례시작 화면")
	@RequestMapping(value="/ROMD1010/{roomCd}" , method= RequestMethod.GET )
	public String main(@PathVariable (value="roomCd") String roomCd ,Model model) throws Exception{
		
		model.addAttribute(roomCd);
		return "/jsp/public/romd/ROMD1010";
	}
	
	@Transactional
	@ApiOperation(value = "제례실 세팅 정보", notes = "제례실 기본세팅")
	@RequestMapping(value="/ROMD1010/selectSetting" ,produces = APPLICATION_JSON)
	public CommonListResponseParams.MapResponse selectSetting(String roomCd) throws Exception{
	
		
		Map<String, Object> map= new HashMap<String, Object>();		
		Inspectroom inspectroom = null;
		Booking booking = null;
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");	
		String BookingYN = "N"; // 제례진행여부 Y : 제례진행 , N : 대기	
		String nextBookingYN = "N"; // 대기자여부 Y : 대기중 , N : 없음	
		Long remainTime = 0L; // 남은시간
				
		//현재 운영환경 조회
		Envset envset = envsetService.selectEnvSet(stDate);			
		
		Long now = DateUtils.parseDate("yyyyMMddHHmm",stDate).getTime();
		Long openTime = DateUtils.parseDate("yyyyMMddHHmm", stDate.substring(0,8)+envset.getOpenTime()).getTime(); //오픈시간
		Long closeTime = DateUtils.parseDate("yyyyMMddHHmm", stDate.substring(0,8)+envset.getCloseTime()).getTime(); //폐장시간		
		String openYN = now > openTime && now < closeTime ? "Y" : "N" ;// 개방여부 Y: 개방, N: 폐장
		
		
		// 폐장시간이 아닐경우 
		if(now < closeTime){
						
			Inspect inspect = inspectService.selectInspect(stDate); //점검일정
			booking = bookingService.selectBooking(envset.getCleanTime(), roomCd);	// 예약정보			
			inspectroom = inspectroomService.selectInspectRoom(inspect.getWorkDate(), inspect.getSeqNo(), roomCd); // 점검제례실
			// NULL 체크
			if(CommonUtils.canConvertMappingVTO(booking)){											
				
				Long edTime = DateUtils.parseDate("yyyyMMddHHmm",booking.getBookDate()+booking.getEdTime()).getTime();				
				remainTime = (edTime-now)/(60*1000); //남은시간(분) 변환
				if(CommonUtils.canConvertMappingVTO(bookingService.nextBooking(booking))){
					nextBookingYN = "Y";
				}
				BookingYN = "Y"; 
			}
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("M월 dd일, E요일 a, HH:mm");		
		map.put("time", sdf.format(new Date()));
		map.put("inspectRoom", InspectRoomVTO.of(inspectroom));	
		map.put("envset", EnvsetVTO.of(envset));
		map.put("booking", BookingVTO.of(booking));		
		map.put("remainTime", remainTime);
		map.put("openYN", openYN);
		map.put("BookingYN", BookingYN);					
		map.put("nextBookingYN", nextBookingYN);		
		
		return CommonListResponseParams.MapResponse.of(map);
	}
	
	@ApiOperation(value = "제례시간 연장", notes = "제례시간 연장")
	@RequestMapping(value="/ROMD1010/updateExtTime" , method= RequestMethod.POST,produces = APPLICATION_JSON )
	public ApiResponse updateExtTime(BookingVTO bookingVTO) throws Exception{
		Booking booking = baseConverter.convert(bookingVTO, Booking.class);
		bookingService.updateExtTime(booking);		
		return ok();
	}
	
	@ApiOperation(value = "예약자 확인", notes = "현재 제례실에 다음 예약자가 있나 확인")
	@RequestMapping(value="/ROMD1010/nextBooking" , method= RequestMethod.GET,produces = APPLICATION_JSON )
	public Object nextBooking(BookingVTO bookingVTO) throws Exception{
		Booking booking = baseConverter.convert(bookingVTO, Booking.class);
		booking = bookingService.findOne(booking.getId());
		return bookingService.nextBooking(booking);				
		
	}
	
	@ApiOperation(value = "제례종료", notes = "제례종료")
	@RequestMapping(value="/ROMD1010/updateEnd" , method= RequestMethod.POST,produces = APPLICATION_JSON )
	public ApiResponse updateEnd(BookingVTO bookingVTO) throws Exception{
		Booking booking = baseConverter.convert(bookingVTO, Booking.class);
		bookingService.updateEnd(booking);		
		return ok();
	}
}
