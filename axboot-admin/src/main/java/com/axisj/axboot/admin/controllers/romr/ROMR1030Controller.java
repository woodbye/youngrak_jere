package com.axisj.axboot.admin.controllers.romr;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/public")
public class ROMR1030Controller extends BaseController{

	@Inject
	MdcontentsService mdcontentsService;
	
	@Inject
	BookingService bookingService;
	
/*	@ApiOperation(value = "고인 목록 화면", notes = "고인목록 화면")
	@RequestMapping(value="/ROMR1030" , method= RequestMethod.GET )
	public String maing() throws Exception{
		
		return "/jsp/romr/ROMR1030";
	}
	*/
	@ApiOperation(value = "키오스크 고인 검색", notes = "키오스크에서 고인명으로 고인검색")
	@RequestMapping(value="/ROMR1030/selectDeadList" , method= RequestMethod.GET , produces=APPLICATION_JSON)
	public PageableResponseParams.PageResponse selectDeadList(Pageable pageable, String deadName) throws Exception{
		
		Map<String, Object> params = new HashMap<String, Object>();
	
		params.put("pageable", pageable);
		params.put("key", "contTitle");
		params.put("keyword", deadName);
		
		Page<Mdcontents> pages = mdcontentsService.selectMdcontentsList(params);	
		return PageableResponseParams.PageResponse.of(MdcontentsVTO.of(pages.getContent()), PageableTO.of(pages));
		
	}
	
	@ApiOperation(value = "예약 확인", notes = "현재 제례 예약중인지 확인")
	@ResponseBody
	@RequestMapping(value="/ROMR1030/selectBookingByContid" , method= RequestMethod.GET)
	public boolean selectBookingByContTitle(String contid) throws Exception{
		
		String bookDate = DateUtils.formatToDateString("yyyyMMdd");		
		String stTime = DateUtils.formatToDateString("HHmm");		
		Booking booking = bookingService.selectBookingByContTitle(bookDate,stTime,contid);		
		
		
		return booking == null ? false : true;
		
	}
	
	
	
	
}
