package com.axisj.axboot.admin.controllers.romr;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.domain.version.ProgramVersionRepository;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.vto.BookingVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.InspectRoomVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/public")
public class ROMR1010Controller extends BaseController{

	@Inject
	EnvsetService envsetService;

	//@Inject
	//ProgramVersionRepository programVersionRepository;
	
	@Inject
	InspectService inspectService;
	
	@Inject 
	InspectroomService inspectroomService;
	
	@ApiOperation(value = "키오스크 화면", notes = "키오스크 화면")
	@RequestMapping(value="/ROMR1010/{roomCd}" , method= RequestMethod.GET )
	public String main(@PathVariable (value="roomCd") String roomCd ,Model model) throws Exception{
		
		model.addAttribute(roomCd);
		return "/jsp/public/romr/ROMR1010";
	}
	
	@Transactional
	@ApiOperation(value = "운영환경 정보", notes = "운영환경 정보를 조회")
	@RequestMapping(value="/ROMR1010/selectSetting" ,produces = APPLICATION_JSON)
	public CommonListResponseParams.MapResponse selectEnv() throws Exception{
		//Double version = programVersionRepository.findAll().get(0).getVersion();
		String stDate = DateUtils.formatToDateString("yyyyMMddHHmm");			
		Map<String, Object> map= new HashMap<String, Object>();
		
		Envset envset = envsetService.selectEnvSet(stDate);	//운영환경 			
		Inspect inspect = inspectService.selectInspect(stDate);	 // 점검일정
		List<Inspectroom> inspectroomList = inspectroomService.selectInspectRoomList(inspect.getWorkDate(), inspect.getSeqNo()); //점검제례실
				
		Date conditionTime = new Date();  //비교할 시간	
		// 시간들을 long값으로 환산
		Long openTime = DateUtils.parseDate("yyyyMMddHHmm", stDate.substring(0,8)+envset.getOpenTime()).getTime();
		Long closeTime = DateUtils.parseDate("yyyyMMddHHmm", stDate.substring(0,8)+envset.getCloseTime()).getTime();
		// 개방 여부
		String openYN = openTime < conditionTime.getTime() && closeTime > conditionTime.getTime() ? "Y" : "N";  // 개방여부 Y: 개방, N: 폐장
						
		map.put("envset", EnvsetVTO.of(envset));
		map.put("openYN", openYN);
		SimpleDateFormat sdf = new SimpleDateFormat("M월 dd일, E요일 a, HH:mm");		
		map.put("time", sdf.format(new Date()));
		//map.put("version", version);
		map.put("inpectKiosk", inspectroomList);
				
		return CommonListResponseParams.MapResponse.of(map);
	}


}
