package com.axisj.axboot.admin.controllers.romr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.json.simple.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.admin.parameter.PageableResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.booking.Booking;
import com.axisj.axboot.core.domain.booking.BookingId;
import com.axisj.axboot.core.domain.booking.BookingService;
import com.axisj.axboot.core.domain.deadsch.DeadSch;
import com.axisj.axboot.core.domain.envset.Envset;
import com.axisj.axboot.core.domain.envset.EnvsetService;
import com.axisj.axboot.core.domain.inspect.Inspect;
import com.axisj.axboot.core.domain.inspect.InspectService;
import com.axisj.axboot.core.domain.inspectroom.Inspectroom;
import com.axisj.axboot.core.domain.inspectroom.InspectroomService;
import com.axisj.axboot.core.domain.mdcontents.Mdcontents;
import com.axisj.axboot.core.domain.mdcontents.MdcontentsService;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.domain.room.RoomService;
import com.axisj.axboot.core.domain.user.auth.group.AuthGroup;
import com.axisj.axboot.core.dto.PageableTO;
import com.axisj.axboot.core.util.DateUtils;
import com.axisj.axboot.core.vto.BookingVTO;
import com.axisj.axboot.core.vto.DeadSchVTO;
import com.axisj.axboot.core.vto.EnvsetVTO;
import com.axisj.axboot.core.vto.InspectRoomVTO;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/public")
public class ROMR1050Controller extends BaseController{

	@Inject
	BookingService bookingService;
	
	@Inject
	RoomService roomService;
	
	@Inject
	private BaseConverter baseConverter;
	
	
	@ApiOperation(value = "예약정보 확인", notes = " 예약 확인")
	@RequestMapping(value="/ROMR1050/selectBookingInfo" , produces=APPLICATION_JSON)
	public Object selectBooking(BookingVTO bookingVTO) throws Exception{
		
		Booking booking = baseConverter.convert(bookingVTO,Booking.class);			
		return BookingVTO.of(bookingService.findOne(booking.getId()));
		
	}

	@ApiOperation(value = "예약취소 ", notes = "예약확인 페이지에서 뒤로가기 눌렀을떄 예약 취소")
	@RequestMapping(value="/ROMR1050/cancelBooking" , method=RequestMethod.POST,  produces=APPLICATION_JSON)
	public ApiResponse cancelBooking(BookingVTO bookingVTO) throws Exception{
		
		Booking booking = baseConverter.convert(bookingVTO,Booking.class);	
		booking = bookingService.findOne(booking.getId());
		booking.setCancelMan(bookingVTO.getCancelMan());
		booking.setStatus(bookingVTO.getStatus());			
		bookingService.save(booking);
		
		return ok();
		
	}

	@ApiOperation(value = "예약확정 ", notes = "예약완료")
	@RequestMapping(value="/ROMR1050/completeBooking" , method=RequestMethod.POST,  produces=APPLICATION_JSON)
	public BookingVTO completeBooking(BookingVTO bookingVTO) throws Exception{
		
		Booking booking = baseConverter.convert(bookingVTO,Booking.class);	
		booking = bookingService.findOne(booking.getId());				
		bookingService.save(booking);
		
		return BookingVTO.of(booking);
		
	}


	
}
