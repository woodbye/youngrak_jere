package com.axisj.axboot.admin.controllers.oper;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.Demon;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.domain.machine.Machine;
import com.axisj.axboot.core.domain.machine.MachineService;
import com.axisj.axboot.core.util.CommonUtils;
import com.axisj.axboot.core.vto.MachineVTO;
import com.wordnik.swagger.annotations.ApiOperation;

import net.bigers.machinecontrolle.Message;

@Controller
public class OPER1060Controller extends BaseController{
	
	//@Inject
	//private AccessLog accessLog;	
	
	@Inject
	private MachineService machineService;
	
	@Inject
	private Demon demon;
	
	@ApiOperation(value = "모니터링 장비 목록", notes = "모니터링 장비 목록을 보여준다.")
	@RequestMapping(value="/OPER1060/selectMachineList", produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse selectMachineList() throws Exception{
		
		
		List<Machine> machines = machineService.selectMachineList("");
		return CommonListResponseParams.ListResponse.of(MachineVTO.of(machines));
		
	}
	
	@ApiOperation(value = "화면캡쳐", notes = "요청 아이피의 캡쳐이미지를 가져온다.")
	@RequestMapping(value="/OPER1060/monitering")
	@ResponseBody
	public void screenShot(String ip, HttpServletResponse response, @RequestParam(defaultValue="0", required=false) int width, @RequestParam(defaultValue="0", required=false) int height) throws Exception{
		
		if(!CommonUtils.isIpaddress(ip)){
			return;
		}
		Long start = System.currentTimeMillis();
		Socket s = new Socket(ip, 9900);
		
		ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
		ObjectInputStream ois = new ObjectInputStream(s.getInputStream());

		oos.writeObject(Message.of("screenShot",width,height));
		Message m = (Message)ois.readObject();
		
		oos.close();
		ois.close();
		Long time = System.currentTimeMillis() - start;
		System.out.println("///////////////////////////////" +time);
		ImageIcon screenShot = m.getScreenShot();
		//int w = width == 0 ? screenShot.getIconWidth() : width;
		//int h = height == 0 ? screenShot.getIconHeight() : height;
		Long start1 = System.currentTimeMillis();
		BufferedImage bi = new BufferedImage(screenShot.getIconWidth(), screenShot.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		//bi.getGraphics().drawImage(screenShot.getImage().getScaledInstance(screenShot.getIconWidth(), screenShot.getIconHeight(), BufferedImage.SCALE_SMOOTH), 0, 0, null);
		bi.getGraphics().drawImage(screenShot.getImage(), 0, 0, null);
		
		bi.getGraphics().dispose();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		ImageIO.write( bi, "png", baos );
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		s.close();
		
		response.setContentType("image/png");
		response.setContentLength(imageInByte.length);
		response.getOutputStream().write(imageInByte);
		Long time1 = System.currentTimeMillis() - start1;
		System.out.println("///////////////////////////////" +time1);
		//String dd = Base64.getEncoder().encodeToString(imageInByte);
		//return "data: image/png; base64," + Base64.getEncoder().encodeToString(imageInByte);
	}
	
	@ApiOperation(value = "모니터링 장비 프로세스명렁어 ", notes = "모니터링 장비로 프로세스명령어 전송")
	@RequestMapping(value="/OPER1060/processCommand", produces = APPLICATION_JSON)
	public void processCommand(String command, String ip ) throws Exception{
				
		Socket s = new Socket(ip, 9900);
		String str = "";
		ObjectOutputStream oos = null;		
		DataInputStream inputStream = null;
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			oos.writeObject(Message.of(command));
			inputStream = new DataInputStream(s.getInputStream());
			str = inputStream.readUTF();
			if(str.equals("OK")){
				s.close();				
			};
			
		}catch(EOFException e){
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			oos.close();
			s.close();
		}
		
	}
	
	@ApiOperation(value = "브라우저 시작", notes = "nwjs 시작")
	@RequestMapping(value="/OPER1060/monitorOn", produces = APPLICATION_JSON)
	public void monitorOn() throws Exception{				
		demon.monitorOn();
	}
	
	@ApiOperation(value = "브라우저 종료", notes = "nwjs 종료")
	@RequestMapping(value="/OPER1060/monitorOff", produces = APPLICATION_JSON)
	public void monitorOff() throws Exception{
		demon.monitorOff();
	}
		
}


