package com.axisj.axboot.admin.controllers;

import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.converter.BaseConverter;
import com.axisj.axboot.core.domain.code.BasicCode;
import com.axisj.axboot.core.domain.code.BasicCodeService;
import com.axisj.axboot.core.vto.BasicCodeVTO;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import java.util.List;

@Controller
@RequestMapping(value = "/api/v1/basicCodes")
@Api(value = "BasicCode", description = "BasicCode API")
public class BasicCodeController extends BaseController {

    @Inject
    private BasicCodeService basicCodeService;

    @Inject
    private BaseConverter baseConverter;

    @ApiOperation(value = "기초코드 목록", notes = "등록된 기초코드 목록을 모두 보여준다.")
    @RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON)
    public CommonListResponseParams.ListResponse list(
            @ApiParam(value = "코드/코드명", required = false, defaultValue = "") @RequestParam(required = false, defaultValue = "") String searchParams) {
        List<BasicCode> basicCodes = basicCodeService.searchBasicCode(searchParams);
        return CommonListResponseParams.ListResponse.of(BasicCodeVTO.of(basicCodes));
    }

    @ApiOperation(value = "기초코드 그룹 목록", notes = "등록된 기초코드 중 해당 그룹 목록을 모두 보여준다.")
    @RequestMapping(value = "/group", method = RequestMethod.GET, produces = APPLICATION_JSON)
    public CommonListResponseParams.ListResponse groupList(
            @ApiParam(value = "기초코드 그룹", required = false, defaultValue = "0") @RequestParam(required = false, defaultValue = "0") String basicCd) {

       // List<BasicCode> basicCodes = basicCodeService.findByBasicCd(basicCd);
    	 List<BasicCode> basicCodes = basicCodeService.findByBasicCdAndUseYn(basicCd);    
        return CommonListResponseParams.ListResponse.of(BasicCodeVTO.of(basicCodes));
    }

    @ApiOperation(value = "기초코드 등록/수정", notes = "기초코드를 등록/수정 한다.")
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT}, produces = APPLICATION_JSON)
    public ApiResponse save(@RequestBody List<BasicCodeVTO> request) {
        List<BasicCode> basicCodes = baseConverter.convert(request, BasicCode.class);
        basicCodeService.save(basicCodes);
        return ok();
    }
}
