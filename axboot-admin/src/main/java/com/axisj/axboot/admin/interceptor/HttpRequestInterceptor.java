package com.axisj.axboot.admin.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.axisj.axboot.core.domain.log.AccessLogService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class HttpRequestInterceptor extends HandlerInterceptorAdapter {

	@Inject 
	private AccessLogService accessLogService;
	
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
      	//accessLogService.saveAccessLog(request);
        return true;
    }

    public boolean responseError(HttpServletResponse response, String errorMessage) throws Exception {
        return false;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }
}
