package com.axisj.axboot.admin.controllers.oper;

import java.util.List;

import javax.inject.Inject;
import javax.xml.ws.soap.AddressingFeature.Responses;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisj.axboot.admin.controllers.BaseController;
import com.axisj.axboot.admin.parameter.CommonListResponseParams;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.domain.msgset.MsgSet;
import com.axisj.axboot.core.domain.msgset.MsgSetService;
import com.axisj.axboot.core.domain.room.Room;
import com.axisj.axboot.core.vto.MsgSetVTO;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
public class OPER1050Controller extends BaseController{
	
	@Inject
	MsgSetService msgSetService;	
	
	@ApiOperation(value = "알림문구 목록", notes = "알림문구 목록을 보여준다.")
	@RequestMapping(value="/OPER1050/selectMsgUseList", produces = APPLICATION_JSON)
	public CommonListResponseParams.ListResponse selectMsgUseList(@RequestParam(required=false , defaultValue = "") String msgUse) throws Exception{
		
		 List<MsgSet> msgs =  msgSetService.selectMsgUseList(msgUse);
		 return CommonListResponseParams.ListResponse.of(MsgSetVTO.of(msgs));
		
	}
	
	@ApiOperation(value = "알림문구 등록/수정", notes = "알림문구를 등록/수정 한다.")
	@RequestMapping(value="/OPER1050/saveMsgUse",method = RequestMethod.PUT , produces = APPLICATION_JSON)	
	public ApiResponse saveMsgUseList(@RequestBody List<MsgSet> msgSets) throws Exception{		
		
		msgSetService.save(msgSets);
		return ok();
	}
	@ApiOperation(value = "알림문구 삭제", notes = "알림문구를 삭제 한다.")
	@RequestMapping(value="/OPER1050/saveMsgUse",method = RequestMethod.DELETE , produces = APPLICATION_JSON)	
	public ApiResponse delMsgUseList(@RequestBody List<MsgSet> msgSets) throws Exception{		
		
		
		msgSetService.deleteMsgset(msgSets);
		return ok();
	}

}
