package com.axisj.axboot.admin.controllers;

import java.io.File;

import javax.inject.Inject;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.axisj.axboot.core.FileService;
import com.axisj.axboot.core.api.response.ApiResponse;
import com.axisj.axboot.core.util.FileUtils;
import com.axisj.axboot.core.vto.MdcontentsVTO;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller

@Api(value = "File Upload", description = "파일 업로드")
public class FileUploadController extends BaseController{

	@Inject
	private FileService fileService;
	
	@ApiOperation(value = "File Upload", notes = "파일 업로드")
	@RequestMapping(value="/api/v1/fileUpload", method=RequestMethod.POST, produces = TEXT_PLAIN)
	@ResponseBody
	public String fileUpload(MultipartFile fileData){
		JSONObject jo = new JSONObject();
		jo.putAll(fileService.fileUpload(fileData));
		return jo.toJSONString();
	}
	
	@ApiOperation(value = "File delete", notes = "파일 삭제")
	@RequestMapping(value="/api/v1/fileDelete", method=RequestMethod.POST, produces = APPLICATION_JSON)
	@ResponseBody
	public ApiResponse fileDelete(String uploadedPath, String saveName) throws Exception{
		JSONObject jo = new JSONObject();
		
		//jo.putAll(fileService.fileUpload(fileData));
		FileUtils.fileDelete(uploadedPath+saveName);
		
		return ok();
	}
	
	
	@ApiOperation(value = "File delete", notes = "파일 삭제")
	@RequestMapping(value="/api/v1/fileListLoad",  produces = APPLICATION_JSON)
	@ResponseBody
	public ApiResponse fileDelete() throws Exception{
		JSONObject jo = new JSONObject();
		
		//jo.putAll(fileService.fileUpload(fileData));
		//FileUtils.fileDelete(uploadedPath+saveName);
		
		return ok();
	}
	
	
}
